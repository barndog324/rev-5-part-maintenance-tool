﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmAddPartRuleHead : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        private frmMain m_parent;
        
        public frmAddPartRuleHead(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }
               
        private void btnAddPRH_Cancel_Click(object sender, EventArgs e)
        {
            m_parent.mCategoryName = "NoUpdate";
            this.Close();
        }

        private void btnAddPRH_Add_Click(object sender, EventArgs e)
        {
            string pickListValStr = "No";
            string configPart = "No";
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int slashPos = modByStr.IndexOf("\\");

            if (validAddPartRuleHead() == true)
            {
                objPart.PartNum = cbAddPRH_PartNum.SelectedValue.ToString();
                objPart.PartCategory = cbAddPRH_PartCategory.Text;
                objPart.RelatedOperation = cbAddPRH_RelatedOp.Text;
                objPart.AssemblySeq = txtAddPRH_AsmSeq.Text;
                objPart.PartType = txtPartType.Text;
                objPart.StationLoc = cbStationLoc.Text;
                objPart.LinesideBin = txtLinesideBin.Text;

                if (chkBoxPicklist.Checked == true)
                {
                    pickListValStr = "Yes";
                }

                if (chkBoxConfigPart.Checked == true)
                {
                    configPart = "Yes";
                }

                objPart.PickList = pickListValStr;
                objPart.ConfigurablePart = configPart;

                if (slashPos > 0)
                {
                    objPart.UserName = modByStr.Substring((slashPos + 1), modByStr.Length - (slashPos + 1));
                }
                else
                {
                    objPart.UserName = "";
                }

                objPart.InsertPartRulesHead();
                m_parent.mPartNumber = objPart.PartNum;                              
                this.Close();
            }
        }

        private bool validAddPartRuleHead()
        {
            bool retVal = true;
            string errors = String.Empty;

            if (cbAddPRH_PartNum.SelectedIndex == 0)
            {              
                errors += "Error - No Part Number has been selected.\n";                
            }

            if (cbAddPRH_PartCategory.SelectedIndex == 0)
            {
                errors += "Error - No Part Category has been selected.\n";
            }

            //if (cbAddPRH_RelatedOp.SelectedIndex == 0)
            //{
            //    errors += "Error - No Related Operation has been selected.\n";
            //}

            if (txtAddPRH_AsmSeq.Text.Length == 0)
            {
                errors += "Error - AssemblySeq is a required integer field.\n";
            }
            else
            {
                try
                {
                    int asmSeqInt = Int32.Parse(txtAddPRH_AsmSeq.Text);
                }
                catch
                {
                    errors += "Error - Invalid data in the AssemblySeq field. AssemblySeq is an integer field.\n";
                }
            }

            objPart.PartNum = cbAddPRH_PartNum.SelectedValue.ToString().ToUpper();
            DataTable dtPart = objPart.GetPartRulesHead();
            if (dtPart.Rows.Count > 0)
            {
                errors += "Error - Duplicate Part Number exist.\n";
            }

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }
      
    }
}
