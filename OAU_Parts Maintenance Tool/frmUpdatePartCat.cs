﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdatePartCat : Form
    {
        private frmMain m_parent;
        PartCategoryBO objCat = new PartCategoryBO();                     
                     
        public frmUpdatePartCat(frmMain frmMn)
        {
            m_parent = frmMn;   
            InitializeComponent();
        }

        private void btnUpdPartCatCancel_Click(object sender, EventArgs e)
        {
            m_parent.mPullOrder = "NoUpdate";
            this.Close();
        }

        private void btnUpdPartCatSave_Click(object sender, EventArgs e)
        {
            if (validPartCategoryUpdates() == true)
            {
                objCat.ID = lbUpdPartCatID.Text;
                objCat.CategoryName = txtUpdPartCat_CategoryName.Text;
                objCat.CategoryDesc = txtUpdPartCat_CategoryDesc.Text;

                if (cbUpdPartCatCritComp.Checked == true)
                {
                    objCat.CriticalComp = "True";
                }
                else
                {
                    objCat.CriticalComp = "False";
                }

                objCat.PullOrder = txtUpdPartCatPullOrder.Text;             
                objCat.ModBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                objCat.DateMod = DateTime.Now.ToString("g");  // 2/27/2009 12:12 PM format     

                m_parent.mCategoryName = objCat.CategoryName;
                m_parent.mCategoryDesc = objCat.CategoryDesc;
                m_parent.mPullOrder = objCat.PullOrder;              
                m_parent.mModBy = objCat.ModBy;
                m_parent.mLastModDate = objCat.DateMod;

                if (this.btnUpdPartCatSave.Text == "Update")
                {
                    objCat.UpdatePartCategoryHead();
                }
                else if (this.btnUpdPartCatSave.Text == "Add")
                {
                    objCat.InsertPartCategoryHead();
                }
                else if (this.btnUpdPartCatSave.Text == "Delete")
                {
                    DialogResult result1 = MessageBox.Show("Are you sure you want to Delete Category Name: " + objCat.CategoryName + "? Press Yes to delete; No to cancel!",
                                                      "Deleting a R6_OA_PartCategoryHead Row",
                                                      MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes)
                    {
                        objCat.DeletePartCategoryHead();
                    }
                }
                this.Close();
            }
        }

        private bool validPartCategoryUpdates()
        {
            bool retVal = true;
                      
            int iPullOrder;
           
            string errors = String.Empty;

            if (txtUpdPartCat_CategoryName.Text.Length == 0)
            {
                errors += "Error - Category Name is required field.\n";
            }           

             if (txtUpdPartCat_CategoryDesc.Text.Length == 0)
            {
                errors += "Error - Category Desc is required field.\n";
            }           


            try
            {
                iPullOrder = Int32.Parse(txtUpdPartCatPullOrder.Text);

                if (iPullOrder < 0)
                {
                    errors += "Error - Invalid Pull Order value, field must be a positive integer value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Pull Order value, field must be a positive integer value or zero.\n";
            }                      

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }        
    }
}
