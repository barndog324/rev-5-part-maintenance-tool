﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdatePartRuleHead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPRH_RuleHeadID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.cbPRH_RelatedOp = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbPRH_PartCategory = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPRH_PartNum = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPRH_ModBy = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPRH_DateModified = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPRH_PartDesc = new System.Windows.Forms.TextBox();
            this.txtPRH_AsmSeq = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtPRH_PartType = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbStationLoc = new System.Windows.Forms.ComboBox();
            this.chkBoxConfigPart = new System.Windows.Forms.CheckBox();
            this.chkBoxPicklist = new System.Windows.Forms.CheckBox();
            this.txtLinesideBin = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtPRH_RuleHeadID
            // 
            this.txtPRH_RuleHeadID.Enabled = false;
            this.txtPRH_RuleHeadID.Location = new System.Drawing.Point(175, 26);
            this.txtPRH_RuleHeadID.Name = "txtPRH_RuleHeadID";
            this.txtPRH_RuleHeadID.Size = new System.Drawing.Size(100, 20);
            this.txtPRH_RuleHeadID.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(28, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(140, 20);
            this.label5.TabIndex = 97;
            this.label5.Text = "ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Green;
            this.btnSave.Location = new System.Drawing.Point(460, 224);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Update";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdCancel.ForeColor = System.Drawing.Color.Red;
            this.btnUpdCancel.Location = new System.Drawing.Point(460, 265);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCancel.TabIndex = 12;
            this.btnUpdCancel.Text = "Cancel";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // cbPRH_RelatedOp
            // 
            this.cbPRH_RelatedOp.FormattingEnabled = true;
            this.cbPRH_RelatedOp.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "35",
            "36",
            "40",
            "41",
            "45",
            "50",
            "51",
            "55",
            "60",
            "65",
            "70 ",
            "71",
            "72",
            "80",
            "90"});
            this.cbPRH_RelatedOp.Location = new System.Drawing.Point(175, 131);
            this.cbPRH_RelatedOp.Name = "cbPRH_RelatedOp";
            this.cbPRH_RelatedOp.Size = new System.Drawing.Size(103, 21);
            this.cbPRH_RelatedOp.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(28, 129);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 20);
            this.label8.TabIndex = 93;
            this.label8.Text = "Related Operation:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPRH_PartCategory
            // 
            this.cbPRH_PartCategory.FormattingEnabled = true;
            this.cbPRH_PartCategory.Location = new System.Drawing.Point(175, 104);
            this.cbPRH_PartCategory.Name = "cbPRH_PartCategory";
            this.cbPRH_PartCategory.Size = new System.Drawing.Size(243, 21);
            this.cbPRH_PartCategory.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(28, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 20);
            this.label4.TabIndex = 91;
            this.label4.Text = "Part Category:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_PartNum
            // 
            this.txtPRH_PartNum.Enabled = false;
            this.txtPRH_PartNum.Location = new System.Drawing.Point(175, 52);
            this.txtPRH_PartNum.Name = "txtPRH_PartNum";
            this.txtPRH_PartNum.Size = new System.Drawing.Size(170, 20);
            this.txtPRH_PartNum.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(28, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 20);
            this.label3.TabIndex = 89;
            this.label3.Text = "Part Number:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_ModBy
            // 
            this.txtPRH_ModBy.Enabled = false;
            this.txtPRH_ModBy.Location = new System.Drawing.Point(175, 290);
            this.txtPRH_ModBy.Name = "txtPRH_ModBy";
            this.txtPRH_ModBy.Size = new System.Drawing.Size(170, 20);
            this.txtPRH_ModBy.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(28, 288);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 20);
            this.label1.TabIndex = 99;
            this.label1.Text = "Modified By:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_DateModified
            // 
            this.txtPRH_DateModified.Enabled = false;
            this.txtPRH_DateModified.Location = new System.Drawing.Point(175, 316);
            this.txtPRH_DateModified.Name = "txtPRH_DateModified";
            this.txtPRH_DateModified.Size = new System.Drawing.Size(170, 20);
            this.txtPRH_DateModified.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(28, 314);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 20);
            this.label2.TabIndex = 101;
            this.label2.Text = "Date Modified:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(28, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 20);
            this.label6.TabIndex = 103;
            this.label6.Text = "Part Description:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_PartDesc
            // 
            this.txtPRH_PartDesc.Enabled = false;
            this.txtPRH_PartDesc.Location = new System.Drawing.Point(175, 78);
            this.txtPRH_PartDesc.Name = "txtPRH_PartDesc";
            this.txtPRH_PartDesc.Size = new System.Drawing.Size(243, 20);
            this.txtPRH_PartDesc.TabIndex = 3;
            // 
            // txtPRH_AsmSeq
            // 
            this.txtPRH_AsmSeq.Location = new System.Drawing.Point(175, 158);
            this.txtPRH_AsmSeq.Name = "txtPRH_AsmSeq";
            this.txtPRH_AsmSeq.Size = new System.Drawing.Size(103, 20);
            this.txtPRH_AsmSeq.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(28, 156);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 20);
            this.label7.TabIndex = 105;
            this.label7.Text = "Assembly Seq:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(460, 304);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 13;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(24, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(145, 20);
            this.label9.TabIndex = 111;
            this.label9.Text = "Station Location:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPRH_PartType
            // 
            this.txtPRH_PartType.Location = new System.Drawing.Point(175, 184);
            this.txtPRH_PartType.Name = "txtPRH_PartType";
            this.txtPRH_PartType.Size = new System.Drawing.Size(201, 20);
            this.txtPRH_PartType.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(22, 184);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(145, 20);
            this.label10.TabIndex = 108;
            this.label10.Text = "Part Type:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbStationLoc
            // 
            this.cbStationLoc.FormattingEnabled = true;
            this.cbStationLoc.Items.AddRange(new object[] {
            "",
            "ASY 1",
            "ASY 2",
            "ASY 3",
            "ASY 4",
            "ASY 5",
            "ASY 6",
            "ASY 7",
            "ASY 8",
            "ASY 9",
            "SA 1",
            "SA 2",
            "SA 3",
            "SA 4",
            "SA 5",
            "SA 6",
            "SA 7",
            "SA 8",
            "SA 9",
            "SA 10",
            "SA 11",
            "SA 12",
            "SA 13"});
            this.cbStationLoc.Location = new System.Drawing.Point(175, 210);
            this.cbStationLoc.Name = "cbStationLoc";
            this.cbStationLoc.Size = new System.Drawing.Size(121, 21);
            this.cbStationLoc.TabIndex = 8;
            // 
            // chkBoxConfigPart
            // 
            this.chkBoxConfigPart.AutoSize = true;
            this.chkBoxConfigPart.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxConfigPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxConfigPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxConfigPart.Location = new System.Drawing.Point(230, 263);
            this.chkBoxConfigPart.Name = "chkBoxConfigPart";
            this.chkBoxConfigPart.Size = new System.Drawing.Size(146, 19);
            this.chkBoxConfigPart.TabIndex = 122;
            this.chkBoxConfigPart.Text = "Configurable Part: ";
            this.chkBoxConfigPart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxConfigPart.UseVisualStyleBackColor = true;
            // 
            // chkBoxPicklist
            // 
            this.chkBoxPicklist.AutoSize = true;
            this.chkBoxPicklist.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxPicklist.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxPicklist.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxPicklist.Location = new System.Drawing.Point(115, 263);
            this.chkBoxPicklist.Name = "chkBoxPicklist";
            this.chkBoxPicklist.Size = new System.Drawing.Size(75, 17);
            this.chkBoxPicklist.TabIndex = 121;
            this.chkBoxPicklist.Text = "Picklist: ";
            this.chkBoxPicklist.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxPicklist.UseVisualStyleBackColor = true;
            // 
            // txtLinesideBin
            // 
            this.txtLinesideBin.Location = new System.Drawing.Point(175, 237);
            this.txtLinesideBin.Name = "txtLinesideBin";
            this.txtLinesideBin.Size = new System.Drawing.Size(80, 20);
            this.txtLinesideBin.TabIndex = 124;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(28, 235);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 20);
            this.label11.TabIndex = 123;
            this.label11.Text = "Lineside Bin:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmUpdatePartRuleHead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 353);
            this.Controls.Add(this.txtLinesideBin);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.chkBoxConfigPart);
            this.Controls.Add(this.chkBoxPicklist);
            this.Controls.Add(this.cbStationLoc);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtPRH_PartType);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.txtPRH_AsmSeq);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtPRH_PartDesc);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPRH_DateModified);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPRH_ModBy);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtPRH_RuleHeadID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdCancel);
            this.Controls.Add(this.cbPRH_RelatedOp);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbPRH_PartCategory);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtPRH_PartNum);
            this.Controls.Add(this.label3);
            this.Name = "frmUpdatePartRuleHead";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Part Rules Head";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtPRH_RuleHeadID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnUpdCancel;
        public System.Windows.Forms.ComboBox cbPRH_RelatedOp;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox cbPRH_PartCategory;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtPRH_PartNum;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtPRH_ModBy;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtPRH_DateModified;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtPRH_PartDesc;
        public System.Windows.Forms.TextBox txtPRH_AsmSeq;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtPRH_PartType;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox cbStationLoc;
        public System.Windows.Forms.CheckBox chkBoxConfigPart;
        public System.Windows.Forms.CheckBox chkBoxPicklist;
        public System.Windows.Forms.TextBox txtLinesideBin;
        private System.Windows.Forms.Label label11;
    }
}