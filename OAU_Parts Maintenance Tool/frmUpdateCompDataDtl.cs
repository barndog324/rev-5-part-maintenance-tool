﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateCompDataDtl : Form
    {
        CompressorBO objComp = new CompressorBO();        

        private frmMain m_parent;

        public frmUpdateCompDataDtl(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }

        private void btnCompDtlSave_Click(object sender, EventArgs e)
        {            
            int iVoltage = -1;

            if (validCompDtlUpdates() == true)
            {
                objComp.ID = lbCompDtlID.Text;
                objComp.RuleHeadID = txtCompDtlRuleHeadID.Text;
                objComp.Phase = txtCompDtlPhase.Text;
                objComp.RLA = txtCompDtlRLA.Text;
                objComp.LRA = txtCompDtlLRA.Text;
                objComp.ModBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                objComp.DateMod = DateTime.Now.ToString("g");  // 2/27/2009 12:12 PM format

                if (rbCompDtlVolt208.Checked == true)
                {
                    iVoltage = 208;
                }
                else if (rbCompDtlVolt460.Checked == true)
                {
                    iVoltage = 460;
                }
                else if (rbCompDtlVolt575.Checked == true)
                {
                    iVoltage = 575;
                }

                objComp.Voltage = iVoltage.ToString();

                if (this.btnCompDtlSave.Text == "Update")
                {
                    objComp.Update_ROA_CompressorDataDTL();
                    m_parent.mVoltage = objComp.Voltage;
                    m_parent.mPhase = objComp.Phase;
                    m_parent.mRLA = objComp.RLA;
                    m_parent.mLRA = objComp.LRA;
                    m_parent.mModBy = objComp.ModBy;
                    m_parent.mLastModDate = objComp.DateMod;
                    this.Close();

                }
                else if (this.btnCompDtlSave.Text == "Add")
                {
                    DataTable dt = objComp.GetROA_CompressorDataDtl(objComp.PartNum, iVoltage);
                    if (dt.Rows.Count > 0)
                    {
                        MessageBox.Show("ERROR - Compressor already exist in ROA_CompressorDataDtl table!");
                    }
                    else
                    {                       
                        objComp.InsertROA_CompressorDataDTL();
                        this.Close();
                    }
                }              
            }
        }       

        private void btnCompDtlCancel_Click(object sender, EventArgs e)
        {
            m_parent.mVoltage = "NoUpdate";
            this.Close();
        }

        private bool validCompDtlUpdates()
        {
            bool retVal = true;
            int iPhase;
            decimal dRLA;
            decimal dLRA;
            string errors = String.Empty;           

            try
            {
                iPhase = Int32.Parse(txtCompDtlPhase.Text);

                if (iPhase < 0)
                {
                    errors += "Error - Invalid Phase value, field must be a positive integer value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Phase value, field must be a positive integer value or zero.\n";
            }

            try
            {
                dRLA = decimal.Parse(txtCompDtlRLA.Text);

                if (dRLA < 0)
                {
                    errors += "Error - Invalid RLA value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid RLA value, field must be a positive decimal value or zero.\n";
            }

            try
            {
                dLRA = decimal.Parse(txtCompDtlLRA.Text);

                if (dLRA < 0)
                {
                    errors += "Error - Invalid LRA value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid LRA value, field must be a positive decimal value or zero.\n";
            }

            if (rbCompDtlVolt208.Checked == false && rbCompDtlVolt460.Checked == false && rbCompDtlVolt575.Checked == false)
            {
                errors += "Error - Voltage selection required!\n";
            }           

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }

        private void cbCompDtl_PartNum_SelectedIndexChanged(object sender, EventArgs e)
        {            
            if (cbCompDtl_PartNum.SelectedIndex != 0)
            {
                objComp.RuleHeadID = cbCompDtl_PartNum.SelectedValue.ToString();
                objComp.GetOAU_CompressorDetailData();
                txtCompDtlRuleHeadID.Text = objComp.RuleHeadID;
                txtCompDtlPartDesc.Text = objComp.PartDescription;
                txtCompDtlPartCategory.Text = objComp.PartCategory;
                if (objComp.PartDescription.Contains("208"))
                {
                    rbCompDtlVolt208.Checked = true;
                }
                else if (objComp.PartDescription.Contains("460"))
                {
                    rbCompDtlVolt460.Checked = true;
                }
                else if (objComp.PartDescription.Contains("575"))
                {
                    rbCompDtlVolt575.Checked = true;
                }
                txtCompDtlPhase.Focus();
            }
           
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {            

            DialogResult result1 = MessageBox.Show("Are you sure you want to do this CompressorDataDtl Row? Press Yes to delete & No to cancel.",
                                                       "Deleting a CompressorDataDtl Row",
                                                       MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                objComp.ID = lbCompDtlID.Text;
                objComp.DeleteROA_CompressorDataDTL();
                m_parent.mVoltage = "NoUpdate";
                this.Close();
            }
        }
       
    }
}
