﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateCompCharge
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbUpdCCD_UnitModel = new System.Windows.Forms.ComboBox();
            this.txtUpdCCD_Circuit1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUpdCCD_LastModDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUpdCCD_ModifedBy = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUpdCCD_Circuit2Reheat = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUpdCCD_Circuit2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gbUpdCCD_HeatType = new System.Windows.Forms.GroupBox();
            this.rbUpdCCD_Water = new System.Windows.Forms.RadioButton();
            this.rbUpdCCD_Normal = new System.Windows.Forms.RadioButton();
            this.rbUpdCCD_Air = new System.Windows.Forms.RadioButton();
            this.txtUpdCCD_Circuit1Reheat = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUpdCCD_ID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUpdCCD_Save = new System.Windows.Forms.Button();
            this.btnUpdCCD_Cancel = new System.Windows.Forms.Button();
            this.cbUpdCCD_CoolingCapacity = new System.Windows.Forms.ComboBox();
            this.gbUpdCCD_HeatType.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbUpdCCD_UnitModel
            // 
            this.cbUpdCCD_UnitModel.DropDownWidth = 60;
            this.cbUpdCCD_UnitModel.Enabled = false;
            this.cbUpdCCD_UnitModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_UnitModel.FormattingEnabled = true;
            this.cbUpdCCD_UnitModel.Items.AddRange(new object[] {
            "OA1",
            "OA2",
            "OA3",
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAL",
            "OAN"});
            this.cbUpdCCD_UnitModel.Location = new System.Drawing.Point(174, 52);
            this.cbUpdCCD_UnitModel.Name = "cbUpdCCD_UnitModel";
            this.cbUpdCCD_UnitModel.Size = new System.Drawing.Size(90, 23);
            this.cbUpdCCD_UnitModel.TabIndex = 1;
            // 
            // txtUpdCCD_Circuit1
            // 
            this.txtUpdCCD_Circuit1.Location = new System.Drawing.Point(174, 107);
            this.txtUpdCCD_Circuit1.Name = "txtUpdCCD_Circuit1";
            this.txtUpdCCD_Circuit1.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit1.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(12, 106);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 20);
            this.label10.TabIndex = 153;
            this.label10.Text = "Circuit1:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_LastModDate
            // 
            this.txtUpdCCD_LastModDate.Enabled = false;
            this.txtUpdCCD_LastModDate.Location = new System.Drawing.Point(174, 237);
            this.txtUpdCCD_LastModDate.Name = "txtUpdCCD_LastModDate";
            this.txtUpdCCD_LastModDate.Size = new System.Drawing.Size(147, 20);
            this.txtUpdCCD_LastModDate.TabIndex = 152;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(12, 237);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 20);
            this.label9.TabIndex = 151;
            this.label9.Text = "Last Modified:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_ModifedBy
            // 
            this.txtUpdCCD_ModifedBy.Enabled = false;
            this.txtUpdCCD_ModifedBy.Location = new System.Drawing.Point(174, 211);
            this.txtUpdCCD_ModifedBy.Name = "txtUpdCCD_ModifedBy";
            this.txtUpdCCD_ModifedBy.Size = new System.Drawing.Size(147, 20);
            this.txtUpdCCD_ModifedBy.TabIndex = 150;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(12, 211);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 20);
            this.label8.TabIndex = 149;
            this.label8.Text = "Modified By:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_Circuit2Reheat
            // 
            this.txtUpdCCD_Circuit2Reheat.Location = new System.Drawing.Point(174, 185);
            this.txtUpdCCD_Circuit2Reheat.Name = "txtUpdCCD_Circuit2Reheat";
            this.txtUpdCCD_Circuit2Reheat.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit2Reheat.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(12, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 20);
            this.label7.TabIndex = 147;
            this.label7.Text = "Circuit2 Reheat:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_Circuit2
            // 
            this.txtUpdCCD_Circuit2.Location = new System.Drawing.Point(174, 159);
            this.txtUpdCCD_Circuit2.Name = "txtUpdCCD_Circuit2";
            this.txtUpdCCD_Circuit2.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit2.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(12, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 20);
            this.label6.TabIndex = 145;
            this.label6.Text = "Circuit2:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbUpdCCD_HeatType
            // 
            this.gbUpdCCD_HeatType.Controls.Add(this.rbUpdCCD_Water);
            this.gbUpdCCD_HeatType.Controls.Add(this.rbUpdCCD_Normal);
            this.gbUpdCCD_HeatType.Controls.Add(this.rbUpdCCD_Air);
            this.gbUpdCCD_HeatType.Enabled = false;
            this.gbUpdCCD_HeatType.ForeColor = System.Drawing.Color.Red;
            this.gbUpdCCD_HeatType.Location = new System.Drawing.Point(399, 33);
            this.gbUpdCCD_HeatType.Name = "gbUpdCCD_HeatType";
            this.gbUpdCCD_HeatType.Size = new System.Drawing.Size(84, 94);
            this.gbUpdCCD_HeatType.TabIndex = 12;
            this.gbUpdCCD_HeatType.TabStop = false;
            this.gbUpdCCD_HeatType.Text = "Heat Type";
            // 
            // rbUpdCCD_Water
            // 
            this.rbUpdCCD_Water.AutoSize = true;
            this.rbUpdCCD_Water.Location = new System.Drawing.Point(16, 63);
            this.rbUpdCCD_Water.Name = "rbUpdCCD_Water";
            this.rbUpdCCD_Water.Size = new System.Drawing.Size(54, 17);
            this.rbUpdCCD_Water.TabIndex = 3;
            this.rbUpdCCD_Water.TabStop = true;
            this.rbUpdCCD_Water.Text = "Water";
            this.rbUpdCCD_Water.UseVisualStyleBackColor = true;
            // 
            // rbUpdCCD_Normal
            // 
            this.rbUpdCCD_Normal.AutoSize = true;
            this.rbUpdCCD_Normal.Location = new System.Drawing.Point(16, 41);
            this.rbUpdCCD_Normal.Name = "rbUpdCCD_Normal";
            this.rbUpdCCD_Normal.Size = new System.Drawing.Size(58, 17);
            this.rbUpdCCD_Normal.TabIndex = 2;
            this.rbUpdCCD_Normal.TabStop = true;
            this.rbUpdCCD_Normal.Text = "Normal";
            this.rbUpdCCD_Normal.UseVisualStyleBackColor = true;
            // 
            // rbUpdCCD_Air
            // 
            this.rbUpdCCD_Air.AutoSize = true;
            this.rbUpdCCD_Air.Location = new System.Drawing.Point(16, 20);
            this.rbUpdCCD_Air.Name = "rbUpdCCD_Air";
            this.rbUpdCCD_Air.Size = new System.Drawing.Size(37, 17);
            this.rbUpdCCD_Air.TabIndex = 1;
            this.rbUpdCCD_Air.TabStop = true;
            this.rbUpdCCD_Air.Text = "Air";
            this.rbUpdCCD_Air.UseVisualStyleBackColor = true;
            // 
            // txtUpdCCD_Circuit1Reheat
            // 
            this.txtUpdCCD_Circuit1Reheat.Location = new System.Drawing.Point(174, 133);
            this.txtUpdCCD_Circuit1Reheat.Name = "txtUpdCCD_Circuit1Reheat";
            this.txtUpdCCD_Circuit1Reheat.Size = new System.Drawing.Size(66, 20);
            this.txtUpdCCD_Circuit1Reheat.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(12, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 20);
            this.label4.TabIndex = 141;
            this.label4.Text = "Circuit1 Reheat:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 20);
            this.label2.TabIndex = 139;
            this.label2.Text = "Cooling Capacity:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(12, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 20);
            this.label1.TabIndex = 138;
            this.label1.Text = "Unit Model:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdCCD_ID
            // 
            this.txtUpdCCD_ID.Enabled = false;
            this.txtUpdCCD_ID.Location = new System.Drawing.Point(174, 28);
            this.txtUpdCCD_ID.Name = "txtUpdCCD_ID";
            this.txtUpdCCD_ID.Size = new System.Drawing.Size(59, 20);
            this.txtUpdCCD_ID.TabIndex = 137;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(12, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 20);
            this.label5.TabIndex = 136;
            this.label5.Text = "ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUpdCCD_Save
            // 
            this.btnUpdCCD_Save.Location = new System.Drawing.Point(408, 190);
            this.btnUpdCCD_Save.Name = "btnUpdCCD_Save";
            this.btnUpdCCD_Save.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCCD_Save.TabIndex = 10;
            this.btnUpdCCD_Save.Text = "Update";
            this.btnUpdCCD_Save.UseVisualStyleBackColor = true;
            this.btnUpdCCD_Save.Click += new System.EventHandler(this.btnCompDtlSave_Click);
            // 
            // btnUpdCCD_Cancel
            // 
            this.btnUpdCCD_Cancel.Location = new System.Drawing.Point(408, 227);
            this.btnUpdCCD_Cancel.Name = "btnUpdCCD_Cancel";
            this.btnUpdCCD_Cancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCCD_Cancel.TabIndex = 11;
            this.btnUpdCCD_Cancel.Text = "Cancel";
            this.btnUpdCCD_Cancel.UseVisualStyleBackColor = true;
            this.btnUpdCCD_Cancel.Click += new System.EventHandler(this.btnCompDtlCancel_Click);
            // 
            // cbUpdCCD_CoolingCapacity
            // 
            this.cbUpdCCD_CoolingCapacity.DropDownWidth = 60;
            this.cbUpdCCD_CoolingCapacity.Enabled = false;
            this.cbUpdCCD_CoolingCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdCCD_CoolingCapacity.FormattingEnabled = true;
            this.cbUpdCCD_CoolingCapacity.Items.AddRange(new object[] {
            "060",
            "072",
            "084",
            "096",
            "120",
            "144",
            "180",
            "210",
            "240",
            "264",
            "300",
            "360",
            "420",
            "480",
            "540",
            "600",
            "648"});
            this.cbUpdCCD_CoolingCapacity.Location = new System.Drawing.Point(174, 79);
            this.cbUpdCCD_CoolingCapacity.Name = "cbUpdCCD_CoolingCapacity";
            this.cbUpdCCD_CoolingCapacity.Size = new System.Drawing.Size(90, 23);
            this.cbUpdCCD_CoolingCapacity.TabIndex = 2;
            // 
            // frmUpdateCompCharge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 279);
            this.Controls.Add(this.cbUpdCCD_CoolingCapacity);
            this.Controls.Add(this.cbUpdCCD_UnitModel);
            this.Controls.Add(this.txtUpdCCD_Circuit1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtUpdCCD_LastModDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtUpdCCD_ModifedBy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtUpdCCD_Circuit2Reheat);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtUpdCCD_Circuit2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.gbUpdCCD_HeatType);
            this.Controls.Add(this.txtUpdCCD_Circuit1Reheat);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUpdCCD_ID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnUpdCCD_Save);
            this.Controls.Add(this.btnUpdCCD_Cancel);
            this.Name = "frmUpdateCompCharge";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Compressor Charge Data";
            this.gbUpdCCD_HeatType.ResumeLayout(false);
            this.gbUpdCCD_HeatType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox cbUpdCCD_UnitModel;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit1;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtUpdCCD_LastModDate;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtUpdCCD_ModifedBy;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit2Reheat;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit2;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.RadioButton rbUpdCCD_Water;
        public System.Windows.Forms.RadioButton rbUpdCCD_Normal;
        public System.Windows.Forms.RadioButton rbUpdCCD_Air;
        public System.Windows.Forms.TextBox txtUpdCCD_Circuit1Reheat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtUpdCCD_ID;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btnUpdCCD_Save;
        private System.Windows.Forms.Button btnUpdCCD_Cancel;
        public System.Windows.Forms.ComboBox cbUpdCCD_CoolingCapacity;
        public System.Windows.Forms.GroupBox gbUpdCCD_HeatType;
    }
}