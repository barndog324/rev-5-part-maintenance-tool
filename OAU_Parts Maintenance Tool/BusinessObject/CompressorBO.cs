﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    class CompressorBO : CompressorDTO
    {
        public DataTable GetROA_CompressorDataDtl(string partNum, int iVoltage)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetCompressorDataDtl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", partNum);
                        cmd.Parameters.AddWithValue("@Voltage", iVoltage);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);                              
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void GetOAU_CompressorDetailData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetCompressorDetailData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);

                                if (dt.Rows.Count > 0)
                                {
                                    DataRow dr = dt.Rows[0];
                                    RuleHeadID = dr["RuleheadID"].ToString();
                                    PartNum = dr["PartNum"].ToString();
                                    PartDescription = dr["PartDescription"].ToString();
                                    PartCategory = dr["PartCategory"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       

        public void Update_ROA_CompressorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R5_ROA_UpdateCompressorDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@Voltage", Decimal.Parse(Voltage));
                        command.Parameters.AddWithValue("@Phase", Decimal.Parse(Phase));
                        command.Parameters.AddWithValue("@RLA", Decimal.Parse(RLA));
                        command.Parameters.AddWithValue("@LRA", Decimal.Parse(LRA));
                        command.Parameters.AddWithValue("@ModBy", ModBy);                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertROA_CompressorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R5_ROA_InsertCompressorDataDtl", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@Voltage", Decimal.Parse(Voltage));
                        command.Parameters.AddWithValue("@Phase", Decimal.Parse(Phase));
                        command.Parameters.AddWithValue("@RLA", Decimal.Parse(RLA));
                        command.Parameters.AddWithValue("@LRA", Decimal.Parse(LRA));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteROA_CompressorDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R5_ROA_DeleteCompressorDataDtl", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetROA_CompressorChargeData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetROA_CompressorChargeData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UnitModel", UnitModel);
                        cmd.Parameters.AddWithValue("@CoolingCap", CoolingCap);
                        cmd.Parameters.AddWithValue("@HeatType", HeatType);   
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertROA_CompressorChargeDataHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.InsertROA_CompressorChargeDataHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UnitModel", UnitModel);
                        command.Parameters.AddWithValue("@CoolingCap", CoolingCap);
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@Circuit1", Decimal.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit1_Reheat", Decimal.Parse(Circuit1_Reheat));
                        command.Parameters.AddWithValue("@Circuit2", Decimal.Parse(Circuit2));
                        command.Parameters.AddWithValue("@Circuit2_Reheat", Decimal.Parse(Circuit2_Reheat));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateROA_CompressorChargeDataHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.UpdateROA_CompressorChargeDataHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@Circuit1", Decimal.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit1_Reheat", Decimal.Parse(Circuit1_Reheat));
                        command.Parameters.AddWithValue("@Circuit2", Decimal.Parse(Circuit2));
                        command.Parameters.AddWithValue("@Circuit2_Reheat", Decimal.Parse(Circuit2_Reheat));
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteROA_CompressorChargeDataHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.DeleteROA_CompressorChargeDataHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
