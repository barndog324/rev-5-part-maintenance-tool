﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    class PartsMainBO : PartsMainDTO
    {
        #region SQL Methods
        public DataTable GetOAU_PartsAndRulesList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartsAndRulesList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

       

        public DataTable GetOAU_PartRulesByRuleHeadID_AndRuleBatchID()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartRulesByRuleHeadID_AndRuleBatchID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetVMEASM_PartsWithRules()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetVMEASM_PartsWithRules", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


        public DataTable GetPartRulesByPartNumber()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartRulesDataByPartNum", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        //cmd.Parameters.AddWithValue("@UnitType", UnitType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartDetail()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartDetail", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPartCategoryList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartCategoryList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;                                     
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOAU_MainPartList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetMainPartList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Category", PartCategory);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOAU_PartRulesByRuleHeadId()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartRulesByRuleHeadID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadID", RuleHeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void  GetOAU_PartNumberInfo()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartNumberInfo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                                if (dt.Rows.Count > 0)
                                {
                                    DataRow dr = dt.Rows[0];
                                    RuleHeadID = dr["RuleHeadID"].ToString();
                                    RuleBatchID = dr["RuleBatchID"].ToString();
                                    PartDescription = dr["PartDescription"].ToString();
                                    RelatedOperation = dr["RelatedOperation"].ToString();
                                    PartCategory = dr["PartCategory"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return;
        }

        public DataTable GetOAU_PartNumbersByCategory(string categoryList)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetPartNumbersByCategory", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CategoryList", categoryList);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSubAsmPartNums()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_GetSubAsmPartNums", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetInnerAssemblyPartNums()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_GetInnerAssemblyPartNums", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AsmPartNumType", AssemblyPartType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetAssemblyPartNumDetail()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_VKG_GetAssemblyPartNumDetail", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", AssemblyPartNum);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetSubAssemblyParentData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_GetSubAsmParentData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        cmd.Parameters.AddWithValue("@RevType", RevType);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void InsertSubAsmParentData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_InsertSubAsmParentData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        cmd.Parameters.AddWithValue("@BOM_Level", BOM_Level);
                        cmd.Parameters.AddWithValue("@PrimaryAssemblyPartNum", PrimaryAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@InnerAssemblyPartNum", InnerAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@ImmediateParentPartNum", ImmediateParent);
                        cmd.Parameters.AddWithValue("@UnitType", "OA");
                        cmd.Parameters.AddWithValue("@RevType", RevType);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error inserting data into R6_OA_VKG_SubAsmParentData table - " + f);
            }
        }

        public void UpdateSubAsmParentData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_VKG_UpdateSubAsmParentData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        cmd.Parameters.AddWithValue("@BOM_Level", BOM_Level);
                        cmd.Parameters.AddWithValue("@PrimaryAssemblyPartNum", PrimaryAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@InnerAssemblyPartNum", InnerAssemblyPartNum);
                        cmd.Parameters.AddWithValue("@ImmediateParentPartNum", ImmediateParent);
                        cmd.Parameters.AddWithValue("@UnitType", "OA");
                        cmd.Parameters.AddWithValue("@RevType", RevType);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception f)
            {
                MessageBox.Show("Error updating data into R6_VKG_SubAssemblyParentData table - " + f);
            }
        }

        public void DeleteSubAsmParentData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_VKG_DeleteSubAsmParentData", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@BatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@UnitType", "OA");
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update_ROA_PartConditionsDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R5_ROA_UpdatePartConditionsDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@Digit", Int32.Parse(Digit));
                        command.Parameters.AddWithValue("@UnitType", UnitType);
                        command.Parameters.AddWithValue("@Value", Value);
                        command.Parameters.AddWithValue("@Qty", decimal.Parse(Qty));
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));
                        command.Parameters.AddWithValue("@RfgComp", Boolean.Parse(RfgComponent));
                        command.Parameters.AddWithValue("@Circuit1", Boolean.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit2", Boolean.Parse(Circuit2));
                        command.Parameters.AddWithValue("@CircuitCharge", decimal.Parse(CircuitCharge));
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@SubAssemblyPart", SubAssemblyPart);
                        command.Parameters.AddWithValue("@SubAsmMtlPart", SubAsmMtlPart);
                        command.Parameters.AddWithValue("@ImmediateParent", ImmediateParent);
                        command.Parameters.AddWithValue("@Comments", Comments);
                        command.Parameters.AddWithValue("@UserName", UserName);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Insert_ROA_PartConditionsDTL()
        {
            string ruleBatchIdOut = "0";

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R5_ROA_InsertPartConditionsDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@Digit", Int32.Parse(Digit));
                        command.Parameters.AddWithValue("@UnitType", UnitType);
                        command.Parameters.AddWithValue("@Value", Value);
                        command.Parameters.AddWithValue("@Qty", decimal.Parse(Qty));
                        command.Parameters.AddWithValue("@HeatType", HeatType);
                        command.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));
                        command.Parameters.AddWithValue("@RfgComp", Boolean.Parse(RfgComponent));
                        command.Parameters.AddWithValue("@Circuit1", Boolean.Parse(Circuit1));
                        command.Parameters.AddWithValue("@Circuit2", Boolean.Parse(Circuit2));
                        command.Parameters.AddWithValue("@CircuitCharge", decimal.Parse(CircuitCharge));
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@SubAssemblyPart", SubAssemblyPart);
                        command.Parameters.AddWithValue("@SubAsmMtlPart", SubAsmMtlPart);
                        command.Parameters.AddWithValue("@ImmediateParent", ImmediateParent);
                        command.Parameters.AddWithValue("@Comments", Comments);
                        command.Parameters.AddWithValue("@UserName", UserName);    
                        command.Parameters.Add("@RuleBatchIdOut", SqlDbType.BigInt);
                        command.Parameters["@RuleBatchIdOut"].Direction = ParameterDirection.Output;
                        connection.Open();
                        command.ExecuteNonQuery();
                        ruleBatchIdOut = command.Parameters["@RuleBatchIdOut"].Value.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ruleBatchIdOut;
        }

        public void Insert_PartMaster(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_InsertPartMaster", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PartMasterTbl", dt);
                            connection.Open();
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                    }

                }
                catch (Exception f)
                {
                    MessageBox.Show("Error inserting data into PartMaster table - " + f);
                }
            }
        }

        public void DeleteFromOAU_PartMaster()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R5_ROA_DeleteFromPartMaster", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));                        
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPartRulesHead()
        {            
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_InsertPartRulesHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);
                        cmd.Parameters.AddWithValue("@PartCategory", PartCategory);
                        cmd.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));                        
                        cmd.Parameters.AddWithValue("@AssemblySeq", Int32.Parse(AssemblySeq));
                        cmd.Parameters.AddWithValue("@LinesideBin", LinesideBin);
                        cmd.Parameters.AddWithValue("@PartType", PartType);
                        cmd.Parameters.AddWithValue("@StationLoc", StationLoc);
                        cmd.Parameters.AddWithValue("@PickList", PickList);
                        cmd.Parameters.AddWithValue("@ConfigPart", ConfigurablePart);
                        cmd.Parameters.AddWithValue("@ModBy", UserName);
                        
                        connection.Open();
                        cmd.ExecuteNonQuery();                                
                    }
                }

            }
                catch (Exception f)
                {
                    MessageBox.Show("Error inserting data into R6_OA_PartRulesHead table - " + f);
                }
            
        }

        public DataTable GetPartRulesHead()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartRulesHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PartNum", PartNum);    
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void DeletePartRulesHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeletePartRulesHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(RuleHeadID));                       
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePartRulesHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdatePartRulesHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@PartCategory", PartCategory);
                        command.Parameters.AddWithValue("@RelatedOp", Int32.Parse(RelatedOperation));                        
                        command.Parameters.AddWithValue("@AssemblySeq", Int32.Parse(AssemblySeq));
                        command.Parameters.AddWithValue("@PartType", PartType);
                        command.Parameters.AddWithValue("@StationLoc", StationLoc);
                        command.Parameters.AddWithValue("@LinesideBin", LinesideBin);
                        command.Parameters.AddWithValue("@PickList", PickList);
                        command.Parameters.AddWithValue("@ConfigPart", ConfigurablePart);
                        command.Parameters.AddWithValue("@ModBy", UserName);
                        command.Parameters.AddWithValue("@ID", Int32.Parse(RuleHeadID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteFromROA_PartConditionsDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R5_ROA_DeleteFromPartConditionsDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PartNum", PartNum);
                        command.Parameters.AddWithValue("@RuleBatchID", Int32.Parse(RuleBatchID));
                        command.Parameters.AddWithValue("@Digit", Int32.Parse(Digit));
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetOA_PartNumbersBasedOnClassID()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartNumbersBasedOnClassID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region Other Methods
        public int getDigitSelectedIndex(string digitStr, string heatTypeStr, ComboBox cbDigit)
        {
            int selIdx = 0;
            string digitHeatStr = "";

            digitHeatStr = digitStr + '/' + heatTypeStr;

            for (int x = 0; x < cbDigit.Items.Count; ++x)
            {
                string value = cbDigit.GetItemText(cbDigit.Items[x]);
                if (value.Substring(0, value.IndexOf(' ')) == digitHeatStr)
                {
                    selIdx = x;
                    break;
                }
            }

            return selIdx;
        }

        public void UpdatePartMaster(DataTable dt)
        {           
            int curRuleBatchID = Int32.Parse(RuleBatchID);
            int curRuleHeadID = Int32.Parse(RuleHeadID);
            int curRelatedOperation = 0;
            int newRowsNeededCount = 1;
            int numValues = 0;                    
            int batchRuleCount = 0;           
            int digitInt;                                            
            int valueLen;           
            int tonageNumValues = 0;
            int rdi = 0;
            int sri = 0;

            bool firstRow = true;
            string curPartNum = "";
            string curPartCategory = "";
            string modelNo = "";            
            string curPartType = "";
            string qtyStr = "";
            string valueStr = "";
            string tonageValueStr = "";
            string unitType = "";
            string digitVal = "";
            string tmp1 = "";
            string tmp2 = "";
            string tmp3 = "";
            string[,] ruleData = new string[10, 3];
            string[] tonageData = new string[10];
            string[,] singleValueRuleData = new string[10, 2];
            string createBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int slashPos = createBy.IndexOf("\\");

            createBy = createBy.Substring((slashPos + 1), createBy.Length - (slashPos + 1));

            decimal qtyDec = 0;

            DateTime curDate = DateTime.Now;

            DataTable dtNew = new DataTable();           
            dtNew.Columns.Add("CurrentRuleHeadID");
            dtNew.Columns.Add("CurrentRuleBatchID");
            dtNew.Columns.Add("PartNum");
            dtNew.Columns.Add("ModelNo");
            dtNew.Columns.Add("RelatedOpertion");
            dtNew.Columns.Add("PartCategory");
            dtNew.Columns.Add("PartType");
            dtNew.Columns.Add("Qty");
            dtNew.Columns.Add("CreateBy", typeof(string)).MaxLength = 50;
            dtNew.Columns.Add("CreateDate", typeof(DateTime));
            dtNew.Columns.Add("ModBy", typeof(string)).MaxLength = 50;
            dtNew.Columns.Add("ModDate", typeof(DateTime));       
           
            foreach (DataRow row in dt.Rows)
            {
                curPartNum = row["PartNum"].ToString();;
                curRelatedOperation = Int32.Parse(row["RelatedOperation"].ToString());
                curPartCategory = row["PartCategory"].ToString();               
                unitType = row["Unit"].ToString();
                digitVal = row["Digit"].ToString();
                digitInt = Int32.Parse(digitVal);
                valueStr = row["Value"].ToString();
                qtyStr = row["Qty"].ToString();

                if ((curPartNum == "INS2.00X2.10LBS-PIC") && (curRuleBatchID == 10))
                    curPartNum = "INS2.00X2.10LBS-PIC";

                if (qtyStr == "" || qtyStr == null)
                {
                    qtyDec = 0;
                }
                else
                {
                    qtyDec = decimal.Parse(qtyStr);
                }
               
                numValues = 0;                
              
                string[] values = valueStr.Split(',');
                numValues = values.Length;                
               
                if (curPartCategory == "Motor" || curPartCategory.Contains("Mtr"))
                {
                    if (digitInt == 13 || digitInt == 14) 
                    {
                        curPartType = "Condensor";
                    }
                    else if (digitInt == 17 || digitInt == 18)
                    {
                        curPartType = "Indoor FAN";
                    }
                    else if (digitInt == 29)
                    {
                        curPartType = "Powered Exhaust";
                    }
                    else if (digitInt == 32)
                    {
                        curPartType = "ERV";
                    }
                }
                else if (curPartCategory == "Compressor")
                {
                    curPartType = "Compressor";
                }
                else if (curPartCategory == "DigitalScroll")
                {
                    curPartType = "DigitalScroll";
                }
                else if (curPartCategory == "DigitalScrollTandem")
                {
                    curPartType = "DigitalScrollTandem";
                }
                else if (curPartCategory == "TandemCompAssembly")
                {
                    curPartType = "TandemCompAssembly";
                }     

                if (Int32.Parse(digitVal) == 567)
                {
                    tonageData = valueStr.Split(',');
                    tonageNumValues = tonageData.Length;
                }
                else if (numValues == 1)
                {
                    singleValueRuleData[sri, 0] = valueStr;
                    singleValueRuleData[sri++, 1] = digitVal;
                }
                else
                {
                    ruleData[rdi, 0] = valueStr;
                    ruleData[rdi, 1] = digitVal;
                    ruleData[rdi++, 2] = numValues.ToString();
                    ++batchRuleCount;
                }

                if (firstRow == true)
                {
                    newRowsNeededCount = numValues;
                    firstRow = false;
                }
                else
                {
                    newRowsNeededCount *= numValues;
                }
                          
            }

            for (int i = 0; i < rdi; i++)
            {
                for (int j = 0; j < rdi - 1; j++)
                {
                    if (Int32.Parse(ruleData[j, 2]) < Int32.Parse(ruleData[j + 1, 2]))
                    {
                        tmp1 = ruleData[j, 0];
                        tmp2 = ruleData[j, 1];
                        tmp3 = ruleData[j, 2];
                        ruleData[j, 0] = ruleData[j + 1, 0];
                        ruleData[j, 1] = ruleData[j + 1, 1];
                        ruleData[j, 2] = ruleData[j + 1, 2];
                        ruleData[j + 1, 0] = tmp1;
                        ruleData[j + 1, 1] = tmp2;
                        ruleData[j + 1, 2] = tmp3;
                    }
                }
            }

            int tonageIdx = 0;
            int tonageSwitchVal = 0;
            if (tonageNumValues > 1)
            {
                tonageSwitchVal = newRowsNeededCount / tonageNumValues;
                tonageValueStr = tonageData[tonageIdx++];
            }
            else
            {
                tonageSwitchVal = newRowsNeededCount;
                tonageIdx = 1;
                if (tonageNumValues == 1)
                {
                    tonageValueStr = tonageValueStr = tonageData[0];
                }
                else
                {
                    tonageValueStr = "***";
                }
            }

            List<string> modelNoLst = new List<string>();

            for (int x = 0; x < newRowsNeededCount; ++x)
            {
                if ((x / tonageIdx) == tonageSwitchVal)
                {
                    tonageValueStr = tonageData[tonageIdx++];
                }
                //modelNo = "*******************************************";
                modelNo = unitType + "*" + tonageValueStr + "************************************";
                modelNoLst.Add(modelNo);
            }

            for (int a = 0; a < sri; ++a)
            {
                valueStr = singleValueRuleData[a, 0];
                valueLen = valueStr.Length;                

                digitInt = getDigitValue(Int32.Parse(singleValueRuleData[a, 1].ToString()));

                for (int b = 0; b < newRowsNeededCount; ++b)
                {
                    modelNo = modelNoLst[b];
                    
                    if (digitInt == 40)
                    {
                        modelNo = modelNo.Remove((digitInt - 1), 1).Insert((digitInt - 1), "0");
                        valueLen = valueStr.Length;
                        modelNo = modelNo.Remove(digitInt, valueLen).Insert(digitInt, valueStr);
                    }
                    else
                    {
                        modelNo = modelNo.Remove((digitInt - 1), valueLen).Insert((digitInt - 1), valueStr);
                    }
                    
                    modelNoLst[b] = modelNo;
                }
            }

            string[] r1Vals = new string[25];
            string[] r2Vals = new string[25];
            string[] r3Vals = new string[25];
            string[] r4Vals = new string[25];
            string[] r5Vals = new string[25];

            int r1Digit = 0;
            int r2Digit = 0;
            int r3Digit = 0;
            int r4Digit = 0;
            int r5Digit = 0;
            int r1Len = 0;
            int r2Len = 0;
            int r3Len = 0;
            int r4Len = 0;
            int r5Len = 0;

            if (batchRuleCount > 0)
            {
                r1Vals = ruleData[0, 0].Split(',');
                r1Digit = getDigitValue(Int32.Parse(ruleData[0, 1].ToString()));
                r1Len = r1Vals[0].Length;
            }
            if (batchRuleCount > 1)
            {
                r2Vals = ruleData[1, 0].Split(',');
                r2Digit = getDigitValue(Int32.Parse(ruleData[1, 1].ToString()));
                r2Len = r2Vals[0].Length;
            }
            if (batchRuleCount > 2)
            {
                r3Vals = ruleData[2, 0].Split(',');
                r3Digit = getDigitValue(Int32.Parse(ruleData[2, 1].ToString()));
                r3Len = r3Vals[0].Length;
            }
            if (batchRuleCount > 3)
            {
                r4Vals = ruleData[3, 0].Split(',');
                r4Digit = getDigitValue(Int32.Parse(ruleData[3, 1].ToString()));
                r4Len = r4Vals[0].Length;
            }
            if (batchRuleCount > 4)
            {
                r5Vals = ruleData[4, 0].Split(',');
                r5Digit = getDigitValue(Int32.Parse(ruleData[4, 1].ToString()));
                r5Len = r5Vals[0].Length;
            }

            List<string> modelNoLst2 = new List<string>();

            int loopIdx = 0;
            if (tonageNumValues > 0)
            {
                loopIdx = tonageNumValues;
            }
            else
            {
                loopIdx = 1;
                tonageSwitchVal = 1;
            }

            string modelNo2 = "";
            if (batchRuleCount > 0)
            {
                int y = 0;
                for (int x = 0; x < loopIdx; ++x)
                {
                    y = 0;
                    while (y < tonageSwitchVal)
                    {
                        tonageIdx = (x * tonageSwitchVal) + y;
                        modelNo = modelNoLst[tonageIdx];
                        foreach (string r1 in r1Vals)
                        {
                            if (batchRuleCount > 1)
                            {
                                foreach (string r2 in r2Vals)
                                {
                                    if (batchRuleCount > 2)
                                    {
                                        foreach (string r3 in r3Vals)
                                        {
                                            if (batchRuleCount > 3)
                                            {
                                                foreach (string r4 in r4Vals)
                                                {
                                                    if (batchRuleCount > 4)
                                                    {
                                                        foreach (string r5 in r5Vals)
                                                        {
                                                            modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                            modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                            modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                            modelNo2 = modelNo2.Remove((r4Digit - 1), r4Len).Insert((r4Digit - 1), r4);
                                                            modelNo2 = modelNo2.Remove((r5Digit - 1), r5Len).Insert((r5Digit - 1), r5);
                                                            modelNoLst2.Add(modelNo2);
                                                            ++y;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                        modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                        modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                        modelNo2 = modelNo2.Remove((r4Digit - 1), r4Len).Insert((r4Digit - 1), r4);
                                                        modelNoLst2.Add(modelNo2);
                                                        ++y;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                                modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                                modelNo2 = modelNo2.Remove((r3Digit - 1), r3Len).Insert((r3Digit - 1), r3);
                                                modelNoLst2.Add(modelNo2);
                                                ++y;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                        modelNo2 = modelNo2.Remove((r2Digit - 1), r2Len).Insert((r2Digit - 1), r2);
                                        modelNoLst2.Add(modelNo2);
                                        ++y;
                                    }

                                }
                            }
                            else
                            {
                                modelNo2 = modelNo.Remove((r1Digit - 1), r1Len).Insert((r1Digit - 1), r1);
                                modelNoLst2.Add(modelNo2);
                                ++y;
                            }
                        }
                    }
                }
            }
            else
            {
                for (int c = 0; c < newRowsNeededCount; ++c)
                {
                    modelNoLst2.Add(modelNoLst[c]);
                }
            }

            for (int x = 0; x < newRowsNeededCount; ++x)
            {
                modelNo = modelNoLst2[x];
                if (firstRow == false)
                {
                    var dr = dtNew.NewRow();                  
                    dr["CurrentRuleHeadID"] = curRuleHeadID;
                    dr["CurrentRuleBatchID"] = curRuleBatchID;
                    dr["PartNum"] = curPartNum;
                    dr["ModelNo"] = modelNo;
                    dr["RelatedOpertion"] = curRelatedOperation;
                    dr["PartCategory"] = curPartCategory;
                    dr["PartType"] = curPartType;
                    dr["Qty"] = qtyDec;
                    dr["CreateBy"] = createBy;
                    dr["CreateDate"] = curDate;
                    dr["ModBy"] = createBy;
                    dr["ModDate"] = curDate;
                    dtNew.Rows.Add(dr);
                }
            }

            DeleteFromOAU_PartMaster();
            Insert_PartMaster(dtNew);
        }

        private int getDigitValue(int digitVal)
        {
            int retDigitVal = 0;

            if (digitVal == 567)
            {
                retDigitVal = 5;
            }
            else if (digitVal == 2526)
            {
                retDigitVal = 25;
            }
            else if (digitVal == 0)
            {
                retDigitVal = 40;
            }
            else
            {
                retDigitVal = digitVal;
            }

            return retDigitVal;
        }       

        #endregion
    }
}
