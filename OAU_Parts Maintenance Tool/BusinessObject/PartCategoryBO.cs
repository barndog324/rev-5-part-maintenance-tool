﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    class PartCategoryBO : PartCategoryDTO
    {
        public DataTable GetPartCategoryHead()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R6_OA_GetPartCategoryHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PullOrder", Int32.Parse(PullOrder));                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void UpdatePartCategoryHead()
        {
            bool bCritComp;

            if (CriticalComp == "True")
            {
                bCritComp = true;
            }
            else
            {
                bCritComp = false;
            }

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_UpdatePartCategoryHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@CategoryName", CategoryName);
                        command.Parameters.AddWithValue("@CategoryDesc", CategoryDesc);
                        command.Parameters.AddWithValue("@PullOrder", Int32.Parse(PullOrder));
                        command.Parameters.AddWithValue("@CritComp", bCritComp);         
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPartCategoryHead()
        {
            bool bCritComp;

            if (CriticalComp == "True")
            {
                bCritComp = true;
            }
            else
            {
                bCritComp = false;
            }

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_InsertPartCategoryHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;                        
                        command.Parameters.AddWithValue("@CategoryName", CategoryName);
                        command.Parameters.AddWithValue("@CategoryDesc", CategoryDesc);
                        command.Parameters.AddWithValue("@PullOrder", Int32.Parse(PullOrder));
                        command.Parameters.AddWithValue("@CritComp", bCritComp);  
                        command.Parameters.AddWithValue("@ModBy", ModBy);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeletePartCategoryHead()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.R6_OA_DeletePartCategoryHead", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
