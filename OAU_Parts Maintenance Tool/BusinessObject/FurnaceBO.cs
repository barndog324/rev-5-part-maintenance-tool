﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    class FurnaceBO : FurnaceDTO
    {
        public DataTable GetROA_FurnaceDataDTL()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.R5_ROA_GetFurnaceDataDTL", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));
                        //cmd.Parameters.AddWithValue("@Voltage", iVoltage);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public void Update_ROA_FurnaceDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.Update_ROA_FurnaceDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        command.Parameters.AddWithValue("@Amps", Amps);
                        command.Parameters.AddWithValue("@Volts", Decimal.Parse(Volts));
                        command.Parameters.AddWithValue("@BurnerRatio", BurnerRatio);
                        command.Parameters.AddWithValue("@Phase", Decimal.Parse(Phase));                       
                        command.Parameters.AddWithValue("@FuelType", FuelType);
                        command.Parameters.AddWithValue("@MBHkW", MBHkW);
                        command.Parameters.AddWithValue("@ModBy", Username);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertROA_FurnaceDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.InsertROA_FurnaceDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@RuleHeadID", Int32.Parse(RuleHeadID));                        
                        command.Parameters.AddWithValue("@Amps", Decimal.Parse(Amps));
                        command.Parameters.AddWithValue("@Volts", Decimal.Parse(Volts));
                        command.Parameters.AddWithValue("@BurnerRatio", BurnerRatio);
                        command.Parameters.AddWithValue("@Phase", Decimal.Parse(Phase));
                        command.Parameters.AddWithValue("@FuelType", FuelType);
                        command.Parameters.AddWithValue("@MBHkW", MBHkW);
                        command.Parameters.AddWithValue("@ModBy", Username);
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteROA_FurnaceDataDTL()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("KCC.DeleteROA_FurnaceDataDTL", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@ID", Int32.Parse(ID));
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
