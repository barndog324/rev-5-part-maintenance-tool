﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAU_Parts_Maintenance_Tool
{
    class ModelNumDescDTO
    {
        public string ID { get; set; }

        public string ComboBoxItemPos { get; set; }

        public string DigitNo { get; set; }

        public string DigitValue { get; set; }

        public string DigitDescription { get; set; }

        public string DigitRepresentation { get; set; }

        public string HeatType { get; set; }

        public string OAUTypeCode { get; set; }
        
    }
}
