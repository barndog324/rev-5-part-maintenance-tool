﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAU_Parts_Maintenance_Tool
{
    class CompressorDTO
    {
        public string Circuit1 { get; set; }

        public string Circuit1_Reheat { get; set; }

        public string Circuit2 { get; set; }

        public string Circuit2_Reheat { get; set; }

        public string CoolingCap { get; set; }

        public string DateMod { get; set; }

        public string HeatType { get; set; }

        public string ID { get; set; }

        public string LRA { get; set; }

        public string ModBy { get; set; }

        public string PartCategory { get; set; }        

        public string PartDescription { get; set; }

        public string PartNum { get; set; }

        public string Phase { get; set; }

        public string RLA { get; set; }

        public string RuleHeadID { get; set; }

        public string UnitModel { get; set; }

        public string Voltage { get; set; }                         
       
    }
}
