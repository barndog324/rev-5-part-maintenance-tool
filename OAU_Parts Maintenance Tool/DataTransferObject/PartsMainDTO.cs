﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAU_Parts_Maintenance_Tool
{
    class PartsMainDTO
    {
        public string AssemblyPartNum { get; set; }

        public string AssemblyPartType { get; set; }  

        public string AssemblySeq { get; set; }

        public string Circuit1 { get; set; }

        public string Circuit2 { get; set; }

        public string CircuitCharge { get; set; }

        public string Comments { get; set; }

        public string ConfigurablePart { get; set; }

        public string CriticalComp { get; set; }

        public string DateMod { get; set; }

        public string Digit { get; set; }

        public string HeatType { get; set; }

        public string ID { get; set; }

        public string ImmediateParent { get; set; } 

        public string InnerAssemblyPartNum { get; set; }

        public string LinesideBin { get; set; }

        public string PartCategory { get; set; }

        public string PartDescription { get; set; }

        public string PartNum { get; set; }

        public string PartType { get; set; }

        public string PickList { get; set; }

        public string PrimaryAssemblyPartNum { get; set; }

        public string Qty { get; set; }

        public string RelatedOperation { get; set; }

        public string RevType { get; set; }

        public string RfgComponent { get; set; }

        public string RuleBatchID { get; set; }

        public string RuleHeadID { get; set; }

        public string StationLoc { get; set; }

        public string UnitType { get; set; }

        public string UserName { get; set; }

        public string Value { get; set; }

        public int BOM_Level { get; set; }

        public bool SubAssemblyPart { get; set; }

        public bool SubAsmMtlPart { get; set; }

        public bool PreConfigAsm { get; set; }
        
    }
}
