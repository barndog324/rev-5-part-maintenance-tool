﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateModelNoDesc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtModelNoUpdDigitNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.txtModelNoUpdDigitVal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtModelNoUpdDigitDesc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtModelNoUpdDigitRep = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.gbModelNoHeatType = new System.Windows.Forms.GroupBox();
            this.rbModelNoHeatTypeHotWater = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeNA = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeDF = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeElec = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeIF = new System.Windows.Forms.RadioButton();
            this.lbModelNoID = new System.Windows.Forms.Label();
            this.gbUnitType = new System.Windows.Forms.GroupBox();
            this.rbOALBG = new System.Windows.Forms.RadioButton();
            this.rbOAU123DKN = new System.Windows.Forms.RadioButton();
            this.btnUpdDelete = new System.Windows.Forms.Button();
            this.gbModelNoHeatType.SuspendLayout();
            this.gbUnitType.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtModelNoUpdDigitNo
            // 
            this.txtModelNoUpdDigitNo.Enabled = false;
            this.txtModelNoUpdDigitNo.Location = new System.Drawing.Point(181, 14);
            this.txtModelNoUpdDigitNo.Name = "txtModelNoUpdDigitNo";
            this.txtModelNoUpdDigitNo.Size = new System.Drawing.Size(45, 20);
            this.txtModelNoUpdDigitNo.TabIndex = 95;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(19, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 20);
            this.label5.TabIndex = 94;
            this.label5.Text = "Digit No:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Blue;
            this.btnSave.Location = new System.Drawing.Point(85, 134);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 93;
            this.btnSave.Text = "Update";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdCancel.ForeColor = System.Drawing.Color.Red;
            this.btnUpdCancel.Location = new System.Drawing.Point(180, 134);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCancel.TabIndex = 92;
            this.btnUpdCancel.Text = "Cancel";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // txtModelNoUpdDigitVal
            // 
            this.txtModelNoUpdDigitVal.Location = new System.Drawing.Point(181, 44);
            this.txtModelNoUpdDigitVal.Name = "txtModelNoUpdDigitVal";
            this.txtModelNoUpdDigitVal.Size = new System.Drawing.Size(45, 20);
            this.txtModelNoUpdDigitVal.TabIndex = 97;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(19, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 20);
            this.label1.TabIndex = 96;
            this.label1.Text = "Digit Value:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNoUpdDigitDesc
            // 
            this.txtModelNoUpdDigitDesc.Location = new System.Drawing.Point(181, 74);
            this.txtModelNoUpdDigitDesc.Name = "txtModelNoUpdDigitDesc";
            this.txtModelNoUpdDigitDesc.Size = new System.Drawing.Size(169, 20);
            this.txtModelNoUpdDigitDesc.TabIndex = 99;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(19, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 20);
            this.label2.TabIndex = 98;
            this.label2.Text = "Digit Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtModelNoUpdDigitRep
            // 
            this.txtModelNoUpdDigitRep.Location = new System.Drawing.Point(181, 104);
            this.txtModelNoUpdDigitRep.Name = "txtModelNoUpdDigitRep";
            this.txtModelNoUpdDigitRep.Size = new System.Drawing.Size(169, 20);
            this.txtModelNoUpdDigitRep.TabIndex = 101;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(19, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 20);
            this.label3.TabIndex = 100;
            this.label3.Text = "Digit Representation:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbModelNoHeatType
            // 
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeHotWater);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeNA);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeDF);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeElec);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeIF);
            this.gbModelNoHeatType.ForeColor = System.Drawing.Color.Red;
            this.gbModelNoHeatType.Location = new System.Drawing.Point(367, 14);
            this.gbModelNoHeatType.Name = "gbModelNoHeatType";
            this.gbModelNoHeatType.Size = new System.Drawing.Size(201, 83);
            this.gbModelNoHeatType.TabIndex = 104;
            this.gbModelNoHeatType.TabStop = false;
            this.gbModelNoHeatType.Text = "Heat Type";
            // 
            // rbModelNoHeatTypeHotWater
            // 
            this.rbModelNoHeatTypeHotWater.AutoSize = true;
            this.rbModelNoHeatTypeHotWater.Location = new System.Drawing.Point(107, 37);
            this.rbModelNoHeatTypeHotWater.Name = "rbModelNoHeatTypeHotWater";
            this.rbModelNoHeatTypeHotWater.Size = new System.Drawing.Size(74, 17);
            this.rbModelNoHeatTypeHotWater.TabIndex = 10;
            this.rbModelNoHeatTypeHotWater.Text = "Hot Water";
            this.rbModelNoHeatTypeHotWater.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeNA
            // 
            this.rbModelNoHeatTypeNA.AutoSize = true;
            this.rbModelNoHeatTypeNA.Location = new System.Drawing.Point(7, 19);
            this.rbModelNoHeatTypeNA.Name = "rbModelNoHeatTypeNA";
            this.rbModelNoHeatTypeNA.Size = new System.Drawing.Size(40, 17);
            this.rbModelNoHeatTypeNA.TabIndex = 9;
            this.rbModelNoHeatTypeNA.Text = "NA";
            this.rbModelNoHeatTypeNA.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeDF
            // 
            this.rbModelNoHeatTypeDF.AutoSize = true;
            this.rbModelNoHeatTypeDF.Location = new System.Drawing.Point(107, 19);
            this.rbModelNoHeatTypeDF.Name = "rbModelNoHeatTypeDF";
            this.rbModelNoHeatTypeDF.Size = new System.Drawing.Size(79, 17);
            this.rbModelNoHeatTypeDF.TabIndex = 8;
            this.rbModelNoHeatTypeDF.Text = "Direct Fired";
            this.rbModelNoHeatTypeDF.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeElec
            // 
            this.rbModelNoHeatTypeElec.AutoSize = true;
            this.rbModelNoHeatTypeElec.Location = new System.Drawing.Point(7, 37);
            this.rbModelNoHeatTypeElec.Name = "rbModelNoHeatTypeElec";
            this.rbModelNoHeatTypeElec.Size = new System.Drawing.Size(60, 17);
            this.rbModelNoHeatTypeElec.TabIndex = 8;
            this.rbModelNoHeatTypeElec.Text = "Electric";
            this.rbModelNoHeatTypeElec.UseVisualStyleBackColor = true;
            // 
            // rbModelNoHeatTypeIF
            // 
            this.rbModelNoHeatTypeIF.AutoSize = true;
            this.rbModelNoHeatTypeIF.Location = new System.Drawing.Point(7, 55);
            this.rbModelNoHeatTypeIF.Name = "rbModelNoHeatTypeIF";
            this.rbModelNoHeatTypeIF.Size = new System.Drawing.Size(86, 17);
            this.rbModelNoHeatTypeIF.TabIndex = 8;
            this.rbModelNoHeatTypeIF.Text = "Indirect Fired";
            this.rbModelNoHeatTypeIF.UseVisualStyleBackColor = true;
            // 
            // lbModelNoID
            // 
            this.lbModelNoID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModelNoID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbModelNoID.Location = new System.Drawing.Point(465, 163);
            this.lbModelNoID.Name = "lbModelNoID";
            this.lbModelNoID.Size = new System.Drawing.Size(75, 20);
            this.lbModelNoID.TabIndex = 106;
            this.lbModelNoID.Text = "DigitNo:";
            this.lbModelNoID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbModelNoID.Visible = false;
            // 
            // gbUnitType
            // 
            this.gbUnitType.Controls.Add(this.rbOALBG);
            this.gbUnitType.Controls.Add(this.rbOAU123DKN);
            this.gbUnitType.ForeColor = System.Drawing.Color.Green;
            this.gbUnitType.Location = new System.Drawing.Point(367, 113);
            this.gbUnitType.Name = "gbUnitType";
            this.gbUnitType.Size = new System.Drawing.Size(201, 51);
            this.gbUnitType.TabIndex = 107;
            this.gbUnitType.TabStop = false;
            this.gbUnitType.Text = "Unit Type";
            // 
            // rbOALBG
            // 
            this.rbOALBG.AutoSize = true;
            this.rbOALBG.Location = new System.Drawing.Point(7, 19);
            this.rbOALBG.Name = "rbOALBG";
            this.rbOALBG.Size = new System.Drawing.Size(61, 17);
            this.rbOALBG.TabIndex = 9;
            this.rbOALBG.Text = "OALBG";
            this.rbOALBG.UseVisualStyleBackColor = true;
            // 
            // rbOAU123DKN
            // 
            this.rbOAU123DKN.AutoSize = true;
            this.rbOAU123DKN.Location = new System.Drawing.Point(101, 19);
            this.rbOAU123DKN.Name = "rbOAU123DKN";
            this.rbOAU123DKN.Size = new System.Drawing.Size(89, 17);
            this.rbOAU123DKN.TabIndex = 8;
            this.rbOAU123DKN.Text = "OAU123DKN";
            this.rbOAU123DKN.UseVisualStyleBackColor = true;
            // 
            // btnUpdDelete
            // 
            this.btnUpdDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdDelete.Location = new System.Drawing.Point(275, 134);
            this.btnUpdDelete.Name = "btnUpdDelete";
            this.btnUpdDelete.Size = new System.Drawing.Size(75, 30);
            this.btnUpdDelete.TabIndex = 108;
            this.btnUpdDelete.Text = "Delete";
            this.btnUpdDelete.UseVisualStyleBackColor = true;
            this.btnUpdDelete.Click += new System.EventHandler(this.btnUpdDelete_Click);
            // 
            // frmUpdateModelNoDesc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 182);
            this.Controls.Add(this.btnUpdDelete);
            this.Controls.Add(this.gbUnitType);
            this.Controls.Add(this.lbModelNoID);
            this.Controls.Add(this.gbModelNoHeatType);
            this.Controls.Add(this.txtModelNoUpdDigitRep);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtModelNoUpdDigitDesc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtModelNoUpdDigitVal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtModelNoUpdDigitNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdCancel);
            this.Name = "frmUpdateModelNoDesc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Model No Update";
            this.gbModelNoHeatType.ResumeLayout(false);
            this.gbModelNoHeatType.PerformLayout();
            this.gbUnitType.ResumeLayout(false);
            this.gbUnitType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtModelNoUpdDigitNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnUpdCancel;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitVal;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitDesc;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtModelNoUpdDigitRep;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbModelNoHeatType;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeNA;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeDF;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeElec;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeIF;
        public System.Windows.Forms.Label lbModelNoID;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.RadioButton rbModelNoHeatTypeHotWater;
        private System.Windows.Forms.GroupBox gbUnitType;
        public System.Windows.Forms.RadioButton rbOALBG;
        public System.Windows.Forms.RadioButton rbOAU123DKN;
        public System.Windows.Forms.Button btnUpdDelete;
    }
}