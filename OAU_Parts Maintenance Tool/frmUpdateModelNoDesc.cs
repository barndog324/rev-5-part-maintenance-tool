﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateModelNoDesc : Form
    {
        ModelNumDescBO objModel = new ModelNumDescBO();

        private frmMain m_parent;

        public frmUpdateModelNoDesc(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }

        private void btnUpdCancel_Click(object sender, EventArgs e)
        {
            m_parent.mHeatType = "NoUpdate";
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string errors = string.Empty;
           
            if (validModelNumUpdates() == true)
            {
                objModel.DigitNo = txtModelNoUpdDigitNo.Text;
                objModel.DigitValue = txtModelNoUpdDigitVal.Text;
                objModel.DigitDescription = txtModelNoUpdDigitDesc.Text;
                objModel.DigitRepresentation = txtModelNoUpdDigitRep.Text;                
                objModel.ID = lbModelNoID.Text;

                m_parent.mDigitNo = objModel.DigitNo;
                m_parent.mDigitVal = objModel.DigitValue;                
                m_parent.mDigitDesc = objModel.DigitDescription;
                m_parent.mDigitRep = objModel.DigitRepresentation;                               

                if (rbModelNoHeatTypeNA.Checked == true)
                {
                    objModel.HeatType = "NA";
                }
                else if (rbModelNoHeatTypeIF.Checked == true)
                {
                    objModel.HeatType = "IF";
                }
                else if (rbModelNoHeatTypeElec.Checked == true)
                {
                    objModel.HeatType = "ELEC";
                }
                else if (rbModelNoHeatTypeDF.Checked == true)
                {
                    objModel.HeatType = "DF";
                }
                else if (rbModelNoHeatTypeHotWater.Checked == true)
                {
                    objModel.HeatType = "HOTWATER";
                }          

                m_parent.mHeatType = objModel.HeatType;       
         
                if (rbOALBG.Checked == true)
                {
                    objModel.OAUTypeCode = "OALBG";
                }
                else
                {
                    objModel.OAUTypeCode = "OAU123DKN";
                }

                m_parent.mOAUTypeCode = objModel.OAUTypeCode;

                if (checkForDuplicateRow(btnSave.Text) == false)
                {
                    if (this.btnSave.Text == "Update")
                    {
                        objModel.UpdateModelNumDesc();

                    }
                    else if (this.btnSave.Text == "Add")
                    {
                        objModel.ID = "";
                        objModel.InsertModelNumDesc();
                    }

                    this.Close();
                }
                else
                {
                    MessageBox.Show("ERROR: Duplicate record exist in the database, please check your data and re-enter.");
                }
               
               

            }
        }

        private void btnUpdDelete_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("Are you sure you want to do this ModelNumDesc Row? Press Yes to delete!",
                                                       "Deleting a ModelNumDesc Row",
                                                       MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                objModel.ID = lbModelNoID.Text;
                objModel.DeleteModelNumDesc();
                m_parent.mHeatType = "NoUpdate";
                this.Close();
            }
        }       
        
        private bool validModelNumUpdates()
        {
            bool retVal = true;
            
            string errors = String.Empty;

            if (txtModelNoUpdDigitVal.Text.Length == 0)
            {
                errors += "Error - Digit Val is a required value.\n";
                retVal = false;
            }

            if (txtModelNoUpdDigitDesc.Text.Length == 0)
            {
                errors += "Error - Digit Description is a required value.\n";                
            }

            if (txtModelNoUpdDigitRep.Text.Length == 0)
            {
                errors += "Error - Digit Representation is a required value.\n";                
            }           

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }

        private bool checkForDuplicateRow(string updType)
        {
            bool dupFound = false;

            DataTable dt = objModel.GetOAU_ModelNoValues(Int32.Parse(objModel.DigitNo), objModel.HeatType, objModel.DigitValue, objModel.OAUTypeCode);

            if (updType == "Add")
            {
                if (dt.Rows.Count > 0)
                {
                    dupFound = true;
                }
            }
            else
            {
                if (dt.Rows.Count > 1)
                {
                    dupFound = true;
                }
            }

            return dupFound;
        }
       
    }
}
