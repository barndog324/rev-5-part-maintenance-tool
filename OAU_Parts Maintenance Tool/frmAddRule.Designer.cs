﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmAddRule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRuleID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.txtBatchID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbDigit = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudReqQty = new System.Windows.Forms.NumericUpDown();
            this.lbVal30 = new System.Windows.Forms.Label();
            this.lbVal29 = new System.Windows.Forms.Label();
            this.lbVal28 = new System.Windows.Forms.Label();
            this.lbVal27 = new System.Windows.Forms.Label();
            this.lbVal26 = new System.Windows.Forms.Label();
            this.lbVal25 = new System.Windows.Forms.Label();
            this.lbVal24 = new System.Windows.Forms.Label();
            this.lbVal23 = new System.Windows.Forms.Label();
            this.lbVal22 = new System.Windows.Forms.Label();
            this.lbVal21 = new System.Windows.Forms.Label();
            this.lbVal20 = new System.Windows.Forms.Label();
            this.lbVal19 = new System.Windows.Forms.Label();
            this.lbVal18 = new System.Windows.Forms.Label();
            this.lbVal17 = new System.Windows.Forms.Label();
            this.lbVal16 = new System.Windows.Forms.Label();
            this.lbVal15 = new System.Windows.Forms.Label();
            this.lbVal14 = new System.Windows.Forms.Label();
            this.lbVal13 = new System.Windows.Forms.Label();
            this.lbVal12 = new System.Windows.Forms.Label();
            this.lbVal11 = new System.Windows.Forms.Label();
            this.lbVal10 = new System.Windows.Forms.Label();
            this.lbVal9 = new System.Windows.Forms.Label();
            this.lbVal8 = new System.Windows.Forms.Label();
            this.lbVal7 = new System.Windows.Forms.Label();
            this.lbVal6 = new System.Windows.Forms.Label();
            this.lbVal5 = new System.Windows.Forms.Label();
            this.lbVal4 = new System.Windows.Forms.Label();
            this.lbVal3 = new System.Windows.Forms.Label();
            this.lbVal2 = new System.Windows.Forms.Label();
            this.lbVal1 = new System.Windows.Forms.Label();
            this.cbVal30 = new System.Windows.Forms.CheckBox();
            this.cbVal29 = new System.Windows.Forms.CheckBox();
            this.cbVal28 = new System.Windows.Forms.CheckBox();
            this.cbVal27 = new System.Windows.Forms.CheckBox();
            this.cbVal26 = new System.Windows.Forms.CheckBox();
            this.cbVal25 = new System.Windows.Forms.CheckBox();
            this.cbVal24 = new System.Windows.Forms.CheckBox();
            this.cbVal23 = new System.Windows.Forms.CheckBox();
            this.cbVal22 = new System.Windows.Forms.CheckBox();
            this.cbVal21 = new System.Windows.Forms.CheckBox();
            this.cbVal20 = new System.Windows.Forms.CheckBox();
            this.cbVal19 = new System.Windows.Forms.CheckBox();
            this.cbVal18 = new System.Windows.Forms.CheckBox();
            this.cbVal17 = new System.Windows.Forms.CheckBox();
            this.cbVal16 = new System.Windows.Forms.CheckBox();
            this.cbVal15 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.cbVal14 = new System.Windows.Forms.CheckBox();
            this.cbVal13 = new System.Windows.Forms.CheckBox();
            this.cbVal12 = new System.Windows.Forms.CheckBox();
            this.cbVal11 = new System.Windows.Forms.CheckBox();
            this.cbVal10 = new System.Windows.Forms.CheckBox();
            this.cbVal9 = new System.Windows.Forms.CheckBox();
            this.cbVal8 = new System.Windows.Forms.CheckBox();
            this.cbVal7 = new System.Windows.Forms.CheckBox();
            this.cbVal6 = new System.Windows.Forms.CheckBox();
            this.cbVal5 = new System.Windows.Forms.CheckBox();
            this.cbVal4 = new System.Windows.Forms.CheckBox();
            this.cbVal3 = new System.Windows.Forms.CheckBox();
            this.cbVal2 = new System.Windows.Forms.CheckBox();
            this.cbVal1 = new System.Windows.Forms.CheckBox();
            this.cbRelatedOp = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPartDesc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbUnitType = new System.Windows.Forms.ComboBox();
            this.cbPartCategory = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbPartNum = new System.Windows.Forms.ComboBox();
            this.gbHeatType = new System.Windows.Forms.GroupBox();
            this.rbDirectFired = new System.Windows.Forms.RadioButton();
            this.rbElectric = new System.Windows.Forms.RadioButton();
            this.rbIndirectFired = new System.Windows.Forms.RadioButton();
            this.gbUnitType = new System.Windows.Forms.GroupBox();
            this.rbOAU123DKN = new System.Windows.Forms.RadioButton();
            this.rbOALBG = new System.Windows.Forms.RadioButton();
            this.lbAddMsg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqQty)).BeginInit();
            this.gbHeatType.SuspendLayout();
            this.gbUnitType.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRuleID
            // 
            this.txtRuleID.Enabled = false;
            this.txtRuleID.Location = new System.Drawing.Point(142, 26);
            this.txtRuleID.Name = "txtRuleID";
            this.txtRuleID.Size = new System.Drawing.Size(100, 20);
            this.txtRuleID.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(29, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 169;
            this.label5.Text = "Rule ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(951, 30);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 30);
            this.btnSave.TabIndex = 40;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Location = new System.Drawing.Point(951, 71);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdCancel.TabIndex = 41;
            this.btnUpdCancel.Text = "Cancel";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // txtBatchID
            // 
            this.txtBatchID.Location = new System.Drawing.Point(387, 26);
            this.txtBatchID.Name = "txtBatchID";
            this.txtBatchID.Size = new System.Drawing.Size(100, 20);
            this.txtBatchID.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(274, 26);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 20);
            this.label10.TabIndex = 165;
            this.label10.Text = "Batch ID:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit
            // 
            this.cbDigit.FormattingEnabled = true;
            this.cbDigit.Items.AddRange(new object[] {
            "0",
            "3",
            "4",
            "567",
            "8",
            "9",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "20",
            "21",
            "22",
            "23",
            "24",
            "2526",
            "27",
            "28",
            "29",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39"});
            this.cbDigit.Location = new System.Drawing.Point(142, 105);
            this.cbDigit.Name = "cbDigit";
            this.cbDigit.Size = new System.Drawing.Size(103, 21);
            this.cbDigit.TabIndex = 9;
            this.cbDigit.SelectedIndexChanged += new System.EventHandler(this.cbDigit_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(426, 78);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 163;
            this.label9.Text = "Required Qty:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudReqQty
            // 
            this.nudReqQty.DecimalPlaces = 2;
            this.nudReqQty.Location = new System.Drawing.Point(532, 78);
            this.nudReqQty.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudReqQty.Name = "nudReqQty";
            this.nudReqQty.Size = new System.Drawing.Size(98, 20);
            this.nudReqQty.TabIndex = 7;
            this.nudReqQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbVal30
            // 
            this.lbVal30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal30.ForeColor = System.Drawing.Color.Black;
            this.lbVal30.Location = new System.Drawing.Point(1010, 175);
            this.lbVal30.Name = "lbVal30";
            this.lbVal30.Size = new System.Drawing.Size(16, 176);
            this.lbVal30.TabIndex = 161;
            this.lbVal30.Text = "Heat Type:";
            this.lbVal30.Visible = false;
            // 
            // lbVal29
            // 
            this.lbVal29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal29.ForeColor = System.Drawing.Color.Black;
            this.lbVal29.Location = new System.Drawing.Point(980, 175);
            this.lbVal29.Name = "lbVal29";
            this.lbVal29.Size = new System.Drawing.Size(16, 176);
            this.lbVal29.TabIndex = 160;
            this.lbVal29.Text = "Heat Type:";
            this.lbVal29.Visible = false;
            // 
            // lbVal28
            // 
            this.lbVal28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal28.ForeColor = System.Drawing.Color.Black;
            this.lbVal28.Location = new System.Drawing.Point(950, 175);
            this.lbVal28.Name = "lbVal28";
            this.lbVal28.Size = new System.Drawing.Size(16, 176);
            this.lbVal28.TabIndex = 159;
            this.lbVal28.Text = "Heat Type:";
            this.lbVal28.Visible = false;
            // 
            // lbVal27
            // 
            this.lbVal27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal27.ForeColor = System.Drawing.Color.Black;
            this.lbVal27.Location = new System.Drawing.Point(920, 175);
            this.lbVal27.Name = "lbVal27";
            this.lbVal27.Size = new System.Drawing.Size(16, 176);
            this.lbVal27.TabIndex = 158;
            this.lbVal27.Text = "Heat Type:";
            this.lbVal27.Visible = false;
            // 
            // lbVal26
            // 
            this.lbVal26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal26.ForeColor = System.Drawing.Color.Black;
            this.lbVal26.Location = new System.Drawing.Point(890, 175);
            this.lbVal26.Name = "lbVal26";
            this.lbVal26.Size = new System.Drawing.Size(16, 176);
            this.lbVal26.TabIndex = 157;
            this.lbVal26.Text = "Heat Type:";
            this.lbVal26.Visible = false;
            // 
            // lbVal25
            // 
            this.lbVal25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal25.ForeColor = System.Drawing.Color.Black;
            this.lbVal25.Location = new System.Drawing.Point(860, 175);
            this.lbVal25.Name = "lbVal25";
            this.lbVal25.Size = new System.Drawing.Size(16, 176);
            this.lbVal25.TabIndex = 156;
            this.lbVal25.Text = "Heat Type:";
            this.lbVal25.Visible = false;
            // 
            // lbVal24
            // 
            this.lbVal24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal24.ForeColor = System.Drawing.Color.Black;
            this.lbVal24.Location = new System.Drawing.Point(830, 175);
            this.lbVal24.Name = "lbVal24";
            this.lbVal24.Size = new System.Drawing.Size(16, 176);
            this.lbVal24.TabIndex = 155;
            this.lbVal24.Text = "Heat Type:";
            this.lbVal24.Visible = false;
            // 
            // lbVal23
            // 
            this.lbVal23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal23.ForeColor = System.Drawing.Color.Black;
            this.lbVal23.Location = new System.Drawing.Point(800, 175);
            this.lbVal23.Name = "lbVal23";
            this.lbVal23.Size = new System.Drawing.Size(16, 176);
            this.lbVal23.TabIndex = 154;
            this.lbVal23.Text = "Heat Type:";
            this.lbVal23.Visible = false;
            // 
            // lbVal22
            // 
            this.lbVal22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal22.ForeColor = System.Drawing.Color.Black;
            this.lbVal22.Location = new System.Drawing.Point(770, 175);
            this.lbVal22.Name = "lbVal22";
            this.lbVal22.Size = new System.Drawing.Size(16, 176);
            this.lbVal22.TabIndex = 153;
            this.lbVal22.Text = "Heat Type:";
            this.lbVal22.Visible = false;
            // 
            // lbVal21
            // 
            this.lbVal21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal21.ForeColor = System.Drawing.Color.Black;
            this.lbVal21.Location = new System.Drawing.Point(740, 175);
            this.lbVal21.Name = "lbVal21";
            this.lbVal21.Size = new System.Drawing.Size(16, 176);
            this.lbVal21.TabIndex = 152;
            this.lbVal21.Text = "Heat Type:";
            this.lbVal21.Visible = false;
            // 
            // lbVal20
            // 
            this.lbVal20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal20.ForeColor = System.Drawing.Color.Black;
            this.lbVal20.Location = new System.Drawing.Point(710, 175);
            this.lbVal20.Name = "lbVal20";
            this.lbVal20.Size = new System.Drawing.Size(16, 176);
            this.lbVal20.TabIndex = 151;
            this.lbVal20.Text = "Heat Type:";
            this.lbVal20.Visible = false;
            // 
            // lbVal19
            // 
            this.lbVal19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal19.ForeColor = System.Drawing.Color.Black;
            this.lbVal19.Location = new System.Drawing.Point(680, 175);
            this.lbVal19.Name = "lbVal19";
            this.lbVal19.Size = new System.Drawing.Size(16, 176);
            this.lbVal19.TabIndex = 150;
            this.lbVal19.Text = "Heat Type:";
            this.lbVal19.Visible = false;
            // 
            // lbVal18
            // 
            this.lbVal18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal18.ForeColor = System.Drawing.Color.Black;
            this.lbVal18.Location = new System.Drawing.Point(650, 175);
            this.lbVal18.Name = "lbVal18";
            this.lbVal18.Size = new System.Drawing.Size(16, 176);
            this.lbVal18.TabIndex = 149;
            this.lbVal18.Text = "Heat Type:";
            this.lbVal18.Visible = false;
            // 
            // lbVal17
            // 
            this.lbVal17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal17.ForeColor = System.Drawing.Color.Black;
            this.lbVal17.Location = new System.Drawing.Point(620, 175);
            this.lbVal17.Name = "lbVal17";
            this.lbVal17.Size = new System.Drawing.Size(16, 176);
            this.lbVal17.TabIndex = 148;
            this.lbVal17.Text = "Heat Type:";
            this.lbVal17.Visible = false;
            // 
            // lbVal16
            // 
            this.lbVal16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal16.ForeColor = System.Drawing.Color.Black;
            this.lbVal16.Location = new System.Drawing.Point(590, 175);
            this.lbVal16.Name = "lbVal16";
            this.lbVal16.Size = new System.Drawing.Size(16, 176);
            this.lbVal16.TabIndex = 147;
            this.lbVal16.Text = "Heat Type:";
            this.lbVal16.Visible = false;
            // 
            // lbVal15
            // 
            this.lbVal15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal15.ForeColor = System.Drawing.Color.Black;
            this.lbVal15.Location = new System.Drawing.Point(560, 175);
            this.lbVal15.Name = "lbVal15";
            this.lbVal15.Size = new System.Drawing.Size(16, 176);
            this.lbVal15.TabIndex = 146;
            this.lbVal15.Text = "Heat Type:";
            this.lbVal15.Visible = false;
            // 
            // lbVal14
            // 
            this.lbVal14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal14.ForeColor = System.Drawing.Color.Black;
            this.lbVal14.Location = new System.Drawing.Point(530, 175);
            this.lbVal14.Name = "lbVal14";
            this.lbVal14.Size = new System.Drawing.Size(16, 176);
            this.lbVal14.TabIndex = 145;
            this.lbVal14.Text = "Heat Type:";
            this.lbVal14.Visible = false;
            // 
            // lbVal13
            // 
            this.lbVal13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal13.ForeColor = System.Drawing.Color.Black;
            this.lbVal13.Location = new System.Drawing.Point(500, 175);
            this.lbVal13.Name = "lbVal13";
            this.lbVal13.Size = new System.Drawing.Size(16, 176);
            this.lbVal13.TabIndex = 144;
            this.lbVal13.Text = "Heat Type:";
            this.lbVal13.Visible = false;
            // 
            // lbVal12
            // 
            this.lbVal12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal12.ForeColor = System.Drawing.Color.Black;
            this.lbVal12.Location = new System.Drawing.Point(470, 175);
            this.lbVal12.Name = "lbVal12";
            this.lbVal12.Size = new System.Drawing.Size(16, 176);
            this.lbVal12.TabIndex = 143;
            this.lbVal12.Text = "Heat Type:";
            this.lbVal12.Visible = false;
            // 
            // lbVal11
            // 
            this.lbVal11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal11.ForeColor = System.Drawing.Color.Black;
            this.lbVal11.Location = new System.Drawing.Point(440, 175);
            this.lbVal11.Name = "lbVal11";
            this.lbVal11.Size = new System.Drawing.Size(16, 176);
            this.lbVal11.TabIndex = 142;
            this.lbVal11.Text = "Heat Type:";
            this.lbVal11.Visible = false;
            // 
            // lbVal10
            // 
            this.lbVal10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal10.ForeColor = System.Drawing.Color.Black;
            this.lbVal10.Location = new System.Drawing.Point(410, 175);
            this.lbVal10.Name = "lbVal10";
            this.lbVal10.Size = new System.Drawing.Size(16, 176);
            this.lbVal10.TabIndex = 141;
            this.lbVal10.Text = "Heat Type:";
            this.lbVal10.Visible = false;
            // 
            // lbVal9
            // 
            this.lbVal9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal9.ForeColor = System.Drawing.Color.Black;
            this.lbVal9.Location = new System.Drawing.Point(380, 175);
            this.lbVal9.Name = "lbVal9";
            this.lbVal9.Size = new System.Drawing.Size(16, 176);
            this.lbVal9.TabIndex = 140;
            this.lbVal9.Text = "Heat Type:";
            this.lbVal9.Visible = false;
            // 
            // lbVal8
            // 
            this.lbVal8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal8.ForeColor = System.Drawing.Color.Black;
            this.lbVal8.Location = new System.Drawing.Point(350, 175);
            this.lbVal8.Name = "lbVal8";
            this.lbVal8.Size = new System.Drawing.Size(16, 176);
            this.lbVal8.TabIndex = 139;
            this.lbVal8.Text = "Heat Type:";
            this.lbVal8.Visible = false;
            // 
            // lbVal7
            // 
            this.lbVal7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal7.ForeColor = System.Drawing.Color.Black;
            this.lbVal7.Location = new System.Drawing.Point(320, 175);
            this.lbVal7.Name = "lbVal7";
            this.lbVal7.Size = new System.Drawing.Size(16, 176);
            this.lbVal7.TabIndex = 138;
            this.lbVal7.Text = "Heat Type:";
            this.lbVal7.Visible = false;
            // 
            // lbVal6
            // 
            this.lbVal6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal6.ForeColor = System.Drawing.Color.Black;
            this.lbVal6.Location = new System.Drawing.Point(290, 175);
            this.lbVal6.Name = "lbVal6";
            this.lbVal6.Size = new System.Drawing.Size(16, 176);
            this.lbVal6.TabIndex = 137;
            this.lbVal6.Text = "Heat Type:";
            this.lbVal6.Visible = false;
            // 
            // lbVal5
            // 
            this.lbVal5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal5.ForeColor = System.Drawing.Color.Black;
            this.lbVal5.Location = new System.Drawing.Point(260, 175);
            this.lbVal5.Name = "lbVal5";
            this.lbVal5.Size = new System.Drawing.Size(16, 176);
            this.lbVal5.TabIndex = 136;
            this.lbVal5.Text = "Heat Type:";
            this.lbVal5.Visible = false;
            // 
            // lbVal4
            // 
            this.lbVal4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal4.ForeColor = System.Drawing.Color.Black;
            this.lbVal4.Location = new System.Drawing.Point(230, 175);
            this.lbVal4.Name = "lbVal4";
            this.lbVal4.Size = new System.Drawing.Size(16, 176);
            this.lbVal4.TabIndex = 135;
            this.lbVal4.Text = "Heat Type:";
            this.lbVal4.Visible = false;
            // 
            // lbVal3
            // 
            this.lbVal3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal3.ForeColor = System.Drawing.Color.Black;
            this.lbVal3.Location = new System.Drawing.Point(200, 175);
            this.lbVal3.Name = "lbVal3";
            this.lbVal3.Size = new System.Drawing.Size(16, 176);
            this.lbVal3.TabIndex = 134;
            this.lbVal3.Text = "Heat Type:";
            this.lbVal3.Visible = false;
            // 
            // lbVal2
            // 
            this.lbVal2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal2.ForeColor = System.Drawing.Color.Black;
            this.lbVal2.Location = new System.Drawing.Point(170, 175);
            this.lbVal2.Name = "lbVal2";
            this.lbVal2.Size = new System.Drawing.Size(16, 176);
            this.lbVal2.TabIndex = 133;
            this.lbVal2.Text = "Heat Type:";
            this.lbVal2.Visible = false;
            // 
            // lbVal1
            // 
            this.lbVal1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal1.ForeColor = System.Drawing.Color.Black;
            this.lbVal1.Location = new System.Drawing.Point(140, 175);
            this.lbVal1.Name = "lbVal1";
            this.lbVal1.Size = new System.Drawing.Size(16, 176);
            this.lbVal1.TabIndex = 132;
            this.lbVal1.Tag = "1";
            this.lbVal1.Text = "Heat Type:";
            this.lbVal1.Visible = false;
            // 
            // cbVal30
            // 
            this.cbVal30.AutoSize = true;
            this.cbVal30.Location = new System.Drawing.Point(1012, 158);
            this.cbVal30.Name = "cbVal30";
            this.cbVal30.Size = new System.Drawing.Size(15, 14);
            this.cbVal30.TabIndex = 39;
            this.cbVal30.UseVisualStyleBackColor = true;
            this.cbVal30.Visible = false;
            // 
            // cbVal29
            // 
            this.cbVal29.AutoSize = true;
            this.cbVal29.Location = new System.Drawing.Point(982, 158);
            this.cbVal29.Name = "cbVal29";
            this.cbVal29.Size = new System.Drawing.Size(15, 14);
            this.cbVal29.TabIndex = 38;
            this.cbVal29.UseVisualStyleBackColor = true;
            this.cbVal29.Visible = false;
            // 
            // cbVal28
            // 
            this.cbVal28.AutoSize = true;
            this.cbVal28.Location = new System.Drawing.Point(952, 158);
            this.cbVal28.Name = "cbVal28";
            this.cbVal28.Size = new System.Drawing.Size(15, 14);
            this.cbVal28.TabIndex = 37;
            this.cbVal28.UseVisualStyleBackColor = true;
            this.cbVal28.Visible = false;
            // 
            // cbVal27
            // 
            this.cbVal27.AutoSize = true;
            this.cbVal27.Location = new System.Drawing.Point(922, 158);
            this.cbVal27.Name = "cbVal27";
            this.cbVal27.Size = new System.Drawing.Size(15, 14);
            this.cbVal27.TabIndex = 36;
            this.cbVal27.UseVisualStyleBackColor = true;
            this.cbVal27.Visible = false;
            // 
            // cbVal26
            // 
            this.cbVal26.AutoSize = true;
            this.cbVal26.Location = new System.Drawing.Point(892, 158);
            this.cbVal26.Name = "cbVal26";
            this.cbVal26.Size = new System.Drawing.Size(15, 14);
            this.cbVal26.TabIndex = 35;
            this.cbVal26.UseVisualStyleBackColor = true;
            this.cbVal26.Visible = false;
            // 
            // cbVal25
            // 
            this.cbVal25.AutoSize = true;
            this.cbVal25.Location = new System.Drawing.Point(862, 158);
            this.cbVal25.Name = "cbVal25";
            this.cbVal25.Size = new System.Drawing.Size(15, 14);
            this.cbVal25.TabIndex = 34;
            this.cbVal25.UseVisualStyleBackColor = true;
            this.cbVal25.Visible = false;
            // 
            // cbVal24
            // 
            this.cbVal24.AutoSize = true;
            this.cbVal24.Location = new System.Drawing.Point(832, 158);
            this.cbVal24.Name = "cbVal24";
            this.cbVal24.Size = new System.Drawing.Size(15, 14);
            this.cbVal24.TabIndex = 33;
            this.cbVal24.UseVisualStyleBackColor = true;
            this.cbVal24.Visible = false;
            // 
            // cbVal23
            // 
            this.cbVal23.AutoSize = true;
            this.cbVal23.Location = new System.Drawing.Point(802, 158);
            this.cbVal23.Name = "cbVal23";
            this.cbVal23.Size = new System.Drawing.Size(15, 14);
            this.cbVal23.TabIndex = 32;
            this.cbVal23.UseVisualStyleBackColor = true;
            this.cbVal23.Visible = false;
            // 
            // cbVal22
            // 
            this.cbVal22.AutoSize = true;
            this.cbVal22.Location = new System.Drawing.Point(772, 158);
            this.cbVal22.Name = "cbVal22";
            this.cbVal22.Size = new System.Drawing.Size(15, 14);
            this.cbVal22.TabIndex = 31;
            this.cbVal22.UseVisualStyleBackColor = true;
            this.cbVal22.Visible = false;
            // 
            // cbVal21
            // 
            this.cbVal21.AutoSize = true;
            this.cbVal21.Location = new System.Drawing.Point(742, 158);
            this.cbVal21.Name = "cbVal21";
            this.cbVal21.Size = new System.Drawing.Size(15, 14);
            this.cbVal21.TabIndex = 30;
            this.cbVal21.UseVisualStyleBackColor = true;
            this.cbVal21.Visible = false;
            // 
            // cbVal20
            // 
            this.cbVal20.AutoSize = true;
            this.cbVal20.Location = new System.Drawing.Point(712, 158);
            this.cbVal20.Name = "cbVal20";
            this.cbVal20.Size = new System.Drawing.Size(15, 14);
            this.cbVal20.TabIndex = 29;
            this.cbVal20.UseVisualStyleBackColor = true;
            this.cbVal20.Visible = false;
            // 
            // cbVal19
            // 
            this.cbVal19.AutoSize = true;
            this.cbVal19.Location = new System.Drawing.Point(682, 158);
            this.cbVal19.Name = "cbVal19";
            this.cbVal19.Size = new System.Drawing.Size(15, 14);
            this.cbVal19.TabIndex = 28;
            this.cbVal19.UseVisualStyleBackColor = true;
            this.cbVal19.Visible = false;
            // 
            // cbVal18
            // 
            this.cbVal18.AutoSize = true;
            this.cbVal18.Location = new System.Drawing.Point(652, 158);
            this.cbVal18.Name = "cbVal18";
            this.cbVal18.Size = new System.Drawing.Size(15, 14);
            this.cbVal18.TabIndex = 27;
            this.cbVal18.UseVisualStyleBackColor = true;
            this.cbVal18.Visible = false;
            // 
            // cbVal17
            // 
            this.cbVal17.AutoSize = true;
            this.cbVal17.Location = new System.Drawing.Point(622, 158);
            this.cbVal17.Name = "cbVal17";
            this.cbVal17.Size = new System.Drawing.Size(15, 14);
            this.cbVal17.TabIndex = 26;
            this.cbVal17.UseVisualStyleBackColor = true;
            this.cbVal17.Visible = false;
            // 
            // cbVal16
            // 
            this.cbVal16.AutoSize = true;
            this.cbVal16.Location = new System.Drawing.Point(592, 158);
            this.cbVal16.Name = "cbVal16";
            this.cbVal16.Size = new System.Drawing.Size(15, 14);
            this.cbVal16.TabIndex = 25;
            this.cbVal16.UseVisualStyleBackColor = true;
            this.cbVal16.Visible = false;
            // 
            // cbVal15
            // 
            this.cbVal15.AutoSize = true;
            this.cbVal15.Location = new System.Drawing.Point(562, 158);
            this.cbVal15.Name = "cbVal15";
            this.cbVal15.Size = new System.Drawing.Size(15, 14);
            this.cbVal15.TabIndex = 24;
            this.cbVal15.UseVisualStyleBackColor = true;
            this.cbVal15.Visible = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(562, 158);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 115;
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.Visible = false;
            // 
            // cbVal14
            // 
            this.cbVal14.AutoSize = true;
            this.cbVal14.Location = new System.Drawing.Point(532, 158);
            this.cbVal14.Name = "cbVal14";
            this.cbVal14.Size = new System.Drawing.Size(15, 14);
            this.cbVal14.TabIndex = 23;
            this.cbVal14.UseVisualStyleBackColor = true;
            this.cbVal14.Visible = false;
            // 
            // cbVal13
            // 
            this.cbVal13.AutoSize = true;
            this.cbVal13.Location = new System.Drawing.Point(502, 158);
            this.cbVal13.Name = "cbVal13";
            this.cbVal13.Size = new System.Drawing.Size(15, 14);
            this.cbVal13.TabIndex = 22;
            this.cbVal13.UseVisualStyleBackColor = true;
            this.cbVal13.Visible = false;
            // 
            // cbVal12
            // 
            this.cbVal12.AutoSize = true;
            this.cbVal12.Location = new System.Drawing.Point(472, 158);
            this.cbVal12.Name = "cbVal12";
            this.cbVal12.Size = new System.Drawing.Size(15, 14);
            this.cbVal12.TabIndex = 21;
            this.cbVal12.UseVisualStyleBackColor = true;
            this.cbVal12.Visible = false;
            // 
            // cbVal11
            // 
            this.cbVal11.AutoSize = true;
            this.cbVal11.Location = new System.Drawing.Point(442, 158);
            this.cbVal11.Name = "cbVal11";
            this.cbVal11.Size = new System.Drawing.Size(15, 14);
            this.cbVal11.TabIndex = 20;
            this.cbVal11.UseVisualStyleBackColor = true;
            this.cbVal11.Visible = false;
            // 
            // cbVal10
            // 
            this.cbVal10.AutoSize = true;
            this.cbVal10.Location = new System.Drawing.Point(412, 158);
            this.cbVal10.Name = "cbVal10";
            this.cbVal10.Size = new System.Drawing.Size(15, 14);
            this.cbVal10.TabIndex = 19;
            this.cbVal10.UseVisualStyleBackColor = true;
            this.cbVal10.Visible = false;
            // 
            // cbVal9
            // 
            this.cbVal9.AutoSize = true;
            this.cbVal9.Location = new System.Drawing.Point(382, 158);
            this.cbVal9.Name = "cbVal9";
            this.cbVal9.Size = new System.Drawing.Size(15, 14);
            this.cbVal9.TabIndex = 18;
            this.cbVal9.UseVisualStyleBackColor = true;
            this.cbVal9.Visible = false;
            // 
            // cbVal8
            // 
            this.cbVal8.AutoSize = true;
            this.cbVal8.Location = new System.Drawing.Point(352, 158);
            this.cbVal8.Name = "cbVal8";
            this.cbVal8.Size = new System.Drawing.Size(15, 14);
            this.cbVal8.TabIndex = 17;
            this.cbVal8.UseVisualStyleBackColor = true;
            this.cbVal8.Visible = false;
            // 
            // cbVal7
            // 
            this.cbVal7.AutoSize = true;
            this.cbVal7.Location = new System.Drawing.Point(322, 158);
            this.cbVal7.Name = "cbVal7";
            this.cbVal7.Size = new System.Drawing.Size(15, 14);
            this.cbVal7.TabIndex = 16;
            this.cbVal7.UseVisualStyleBackColor = true;
            this.cbVal7.Visible = false;
            // 
            // cbVal6
            // 
            this.cbVal6.AutoSize = true;
            this.cbVal6.Location = new System.Drawing.Point(292, 158);
            this.cbVal6.Name = "cbVal6";
            this.cbVal6.Size = new System.Drawing.Size(15, 14);
            this.cbVal6.TabIndex = 15;
            this.cbVal6.UseVisualStyleBackColor = true;
            this.cbVal6.Visible = false;
            // 
            // cbVal5
            // 
            this.cbVal5.AutoSize = true;
            this.cbVal5.Location = new System.Drawing.Point(262, 158);
            this.cbVal5.Name = "cbVal5";
            this.cbVal5.Size = new System.Drawing.Size(15, 14);
            this.cbVal5.TabIndex = 14;
            this.cbVal5.UseVisualStyleBackColor = true;
            this.cbVal5.Visible = false;
            // 
            // cbVal4
            // 
            this.cbVal4.AutoSize = true;
            this.cbVal4.Location = new System.Drawing.Point(232, 158);
            this.cbVal4.Name = "cbVal4";
            this.cbVal4.Size = new System.Drawing.Size(15, 14);
            this.cbVal4.TabIndex = 13;
            this.cbVal4.UseVisualStyleBackColor = true;
            this.cbVal4.Visible = false;
            // 
            // cbVal3
            // 
            this.cbVal3.AutoSize = true;
            this.cbVal3.Location = new System.Drawing.Point(202, 158);
            this.cbVal3.Name = "cbVal3";
            this.cbVal3.Size = new System.Drawing.Size(15, 14);
            this.cbVal3.TabIndex = 12;
            this.cbVal3.UseVisualStyleBackColor = true;
            this.cbVal3.Visible = false;
            // 
            // cbVal2
            // 
            this.cbVal2.AutoSize = true;
            this.cbVal2.Location = new System.Drawing.Point(172, 158);
            this.cbVal2.Name = "cbVal2";
            this.cbVal2.Size = new System.Drawing.Size(15, 14);
            this.cbVal2.TabIndex = 11;
            this.cbVal2.UseVisualStyleBackColor = true;
            this.cbVal2.Visible = false;
            // 
            // cbVal1
            // 
            this.cbVal1.AutoSize = true;
            this.cbVal1.Location = new System.Drawing.Point(142, 158);
            this.cbVal1.Name = "cbVal1";
            this.cbVal1.Size = new System.Drawing.Size(15, 14);
            this.cbVal1.TabIndex = 10;
            this.cbVal1.UseVisualStyleBackColor = true;
            this.cbVal1.Visible = false;
            // 
            // cbRelatedOp
            // 
            this.cbRelatedOp.FormattingEnabled = true;
            this.cbRelatedOp.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "30",
            "35",
            "40",
            "50",
            "60",
            "70",
            "1000"});
            this.cbRelatedOp.Location = new System.Drawing.Point(822, 78);
            this.cbRelatedOp.Name = "cbRelatedOp";
            this.cbRelatedOp.Size = new System.Drawing.Size(103, 21);
            this.cbRelatedOp.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(673, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 20);
            this.label8.TabIndex = 99;
            this.label8.Text = "Related Operation:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPartDesc
            // 
            this.txtPartDesc.Enabled = false;
            this.txtPartDesc.Location = new System.Drawing.Point(445, 52);
            this.txtPartDesc.Name = "txtPartDesc";
            this.txtPartDesc.Size = new System.Drawing.Size(479, 20);
            this.txtPartDesc.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(319, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 20);
            this.label7.TabIndex = 97;
            this.label7.Text = "Part Description:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(29, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 20);
            this.label6.TabIndex = 96;
            this.label6.Text = "Value/s:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUnitType
            // 
            this.cbUnitType.FormattingEnabled = true;
            this.cbUnitType.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbUnitType.Location = new System.Drawing.Point(634, 26);
            this.cbUnitType.Name = "cbUnitType";
            this.cbUnitType.Size = new System.Drawing.Size(103, 21);
            this.cbUnitType.TabIndex = 3;
            // 
            // cbPartCategory
            // 
            this.cbPartCategory.FormattingEnabled = true;
            this.cbPartCategory.Location = new System.Drawing.Point(142, 78);
            this.cbPartCategory.Name = "cbPartCategory";
            this.cbPartCategory.Size = new System.Drawing.Size(243, 21);
            this.cbPartCategory.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(29, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 93;
            this.label4.Text = "Part Category:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(29, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 91;
            this.label3.Text = "Part Number:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(508, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 20);
            this.label2.TabIndex = 90;
            this.label2.Text = "Unit Type:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(29, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 89;
            this.label1.Text = "Digit:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPartNum
            // 
            this.cbPartNum.FormattingEnabled = true;
            this.cbPartNum.Location = new System.Drawing.Point(135, 52);
            this.cbPartNum.Name = "cbPartNum";
            this.cbPartNum.Size = new System.Drawing.Size(178, 21);
            this.cbPartNum.TabIndex = 4;
            this.cbPartNum.SelectedIndexChanged += new System.EventHandler(this.cbPartNum_SelectedIndexChanged);
            // 
            // gbHeatType
            // 
            this.gbHeatType.Controls.Add(this.rbDirectFired);
            this.gbHeatType.Controls.Add(this.rbElectric);
            this.gbHeatType.Controls.Add(this.rbIndirectFired);
            this.gbHeatType.Location = new System.Drawing.Point(262, 103);
            this.gbHeatType.Name = "gbHeatType";
            this.gbHeatType.Size = new System.Drawing.Size(300, 49);
            this.gbHeatType.TabIndex = 170;
            this.gbHeatType.TabStop = false;
            this.gbHeatType.Text = "Heat Type:";
            this.gbHeatType.Visible = false;
            // 
            // rbDirectFired
            // 
            this.rbDirectFired.AutoSize = true;
            this.rbDirectFired.Location = new System.Drawing.Point(211, 19);
            this.rbDirectFired.Name = "rbDirectFired";
            this.rbDirectFired.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbDirectFired.Size = new System.Drawing.Size(79, 17);
            this.rbDirectFired.TabIndex = 171;
            this.rbDirectFired.TabStop = true;
            this.rbDirectFired.Text = "Direct Fired";
            this.rbDirectFired.UseVisualStyleBackColor = true;
            // 
            // rbElectric
            // 
            this.rbElectric.AutoSize = true;
            this.rbElectric.Location = new System.Drawing.Point(122, 20);
            this.rbElectric.Name = "rbElectric";
            this.rbElectric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbElectric.Size = new System.Drawing.Size(60, 17);
            this.rbElectric.TabIndex = 1;
            this.rbElectric.TabStop = true;
            this.rbElectric.Text = "Electric";
            this.rbElectric.UseVisualStyleBackColor = true;
            // 
            // rbIndirectFired
            // 
            this.rbIndirectFired.AutoSize = true;
            this.rbIndirectFired.Location = new System.Drawing.Point(12, 21);
            this.rbIndirectFired.Name = "rbIndirectFired";
            this.rbIndirectFired.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbIndirectFired.Size = new System.Drawing.Size(86, 17);
            this.rbIndirectFired.TabIndex = 0;
            this.rbIndirectFired.TabStop = true;
            this.rbIndirectFired.Text = "Indirect Fired";
            this.rbIndirectFired.UseVisualStyleBackColor = true;
            this.rbIndirectFired.CheckedChanged += new System.EventHandler(this.rbIndirectFired_CheckedChanged);
            // 
            // gbUnitType
            // 
            this.gbUnitType.Controls.Add(this.rbOAU123DKN);
            this.gbUnitType.Controls.Add(this.rbOALBG);
            this.gbUnitType.Location = new System.Drawing.Point(592, 103);
            this.gbUnitType.Name = "gbUnitType";
            this.gbUnitType.Size = new System.Drawing.Size(225, 49);
            this.gbUnitType.TabIndex = 171;
            this.gbUnitType.TabStop = false;
            this.gbUnitType.Text = "Unit Type:";
            this.gbUnitType.Visible = false;
            // 
            // rbOAU123DKN
            // 
            this.rbOAU123DKN.AutoSize = true;
            this.rbOAU123DKN.Location = new System.Drawing.Point(121, 20);
            this.rbOAU123DKN.Name = "rbOAU123DKN";
            this.rbOAU123DKN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbOAU123DKN.Size = new System.Drawing.Size(89, 17);
            this.rbOAU123DKN.TabIndex = 1;
            this.rbOAU123DKN.TabStop = true;
            this.rbOAU123DKN.Text = "OAU123DKN";
            this.rbOAU123DKN.UseVisualStyleBackColor = true;
            // 
            // rbOALBG
            // 
            this.rbOALBG.AutoSize = true;
            this.rbOALBG.Location = new System.Drawing.Point(12, 21);
            this.rbOALBG.Name = "rbOALBG";
            this.rbOALBG.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbOALBG.Size = new System.Drawing.Size(61, 17);
            this.rbOALBG.TabIndex = 0;
            this.rbOALBG.TabStop = true;
            this.rbOALBG.Text = "OALBG";
            this.rbOALBG.UseVisualStyleBackColor = true;
            // 
            // lbAddMsg
            // 
            this.lbAddMsg.ForeColor = System.Drawing.Color.Red;
            this.lbAddMsg.Location = new System.Drawing.Point(833, 121);
            this.lbAddMsg.Name = "lbAddMsg";
            this.lbAddMsg.Size = new System.Drawing.Size(193, 23);
            this.lbAddMsg.TabIndex = 172;
            this.lbAddMsg.Text = "label11";
            this.lbAddMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbAddMsg.Visible = false;
            // 
            // frmAddRule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 360);
            this.Controls.Add(this.lbAddMsg);
            this.Controls.Add(this.gbUnitType);
            this.Controls.Add(this.gbHeatType);
            this.Controls.Add(this.cbPartNum);
            this.Controls.Add(this.txtRuleID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdCancel);
            this.Controls.Add(this.txtBatchID);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cbDigit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.nudReqQty);
            this.Controls.Add(this.lbVal30);
            this.Controls.Add(this.lbVal29);
            this.Controls.Add(this.lbVal28);
            this.Controls.Add(this.lbVal27);
            this.Controls.Add(this.lbVal26);
            this.Controls.Add(this.lbVal25);
            this.Controls.Add(this.lbVal24);
            this.Controls.Add(this.lbVal23);
            this.Controls.Add(this.lbVal22);
            this.Controls.Add(this.lbVal21);
            this.Controls.Add(this.lbVal20);
            this.Controls.Add(this.lbVal19);
            this.Controls.Add(this.lbVal18);
            this.Controls.Add(this.lbVal17);
            this.Controls.Add(this.lbVal16);
            this.Controls.Add(this.lbVal15);
            this.Controls.Add(this.lbVal14);
            this.Controls.Add(this.lbVal13);
            this.Controls.Add(this.lbVal12);
            this.Controls.Add(this.lbVal11);
            this.Controls.Add(this.lbVal10);
            this.Controls.Add(this.lbVal9);
            this.Controls.Add(this.lbVal8);
            this.Controls.Add(this.lbVal7);
            this.Controls.Add(this.lbVal6);
            this.Controls.Add(this.lbVal5);
            this.Controls.Add(this.lbVal4);
            this.Controls.Add(this.lbVal3);
            this.Controls.Add(this.lbVal2);
            this.Controls.Add(this.lbVal1);
            this.Controls.Add(this.cbVal30);
            this.Controls.Add(this.cbVal29);
            this.Controls.Add(this.cbVal28);
            this.Controls.Add(this.cbVal27);
            this.Controls.Add(this.cbVal26);
            this.Controls.Add(this.cbVal25);
            this.Controls.Add(this.cbVal24);
            this.Controls.Add(this.cbVal23);
            this.Controls.Add(this.cbVal22);
            this.Controls.Add(this.cbVal21);
            this.Controls.Add(this.cbVal20);
            this.Controls.Add(this.cbVal19);
            this.Controls.Add(this.cbVal18);
            this.Controls.Add(this.cbVal17);
            this.Controls.Add(this.cbVal16);
            this.Controls.Add(this.cbVal15);
            this.Controls.Add(this.checkBox9);
            this.Controls.Add(this.cbVal14);
            this.Controls.Add(this.cbVal13);
            this.Controls.Add(this.cbVal12);
            this.Controls.Add(this.cbVal11);
            this.Controls.Add(this.cbVal10);
            this.Controls.Add(this.cbVal9);
            this.Controls.Add(this.cbVal8);
            this.Controls.Add(this.cbVal7);
            this.Controls.Add(this.cbVal6);
            this.Controls.Add(this.cbVal5);
            this.Controls.Add(this.cbVal4);
            this.Controls.Add(this.cbVal3);
            this.Controls.Add(this.cbVal2);
            this.Controls.Add(this.cbVal1);
            this.Controls.Add(this.cbRelatedOp);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPartDesc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbUnitType);
            this.Controls.Add(this.cbPartCategory);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmAddRule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Rule";
            ((System.ComponentModel.ISupportInitialize)(this.nudReqQty)).EndInit();
            this.gbHeatType.ResumeLayout(false);
            this.gbHeatType.PerformLayout();
            this.gbUnitType.ResumeLayout(false);
            this.gbUnitType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtRuleID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnUpdCancel;
        public System.Windows.Forms.TextBox txtBatchID;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox cbDigit;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.NumericUpDown nudReqQty;
        public System.Windows.Forms.Label lbVal30;
        public System.Windows.Forms.Label lbVal29;
        public System.Windows.Forms.Label lbVal28;
        public System.Windows.Forms.Label lbVal27;
        public System.Windows.Forms.Label lbVal26;
        public System.Windows.Forms.Label lbVal25;
        public System.Windows.Forms.Label lbVal24;
        public System.Windows.Forms.Label lbVal23;
        public System.Windows.Forms.Label lbVal22;
        public System.Windows.Forms.Label lbVal21;
        public System.Windows.Forms.Label lbVal20;
        public System.Windows.Forms.Label lbVal19;
        public System.Windows.Forms.Label lbVal18;
        public System.Windows.Forms.Label lbVal17;
        public System.Windows.Forms.Label lbVal16;
        public System.Windows.Forms.Label lbVal15;
        public System.Windows.Forms.Label lbVal14;
        public System.Windows.Forms.Label lbVal13;
        public System.Windows.Forms.Label lbVal12;
        public System.Windows.Forms.Label lbVal11;
        public System.Windows.Forms.Label lbVal10;
        public System.Windows.Forms.Label lbVal9;
        public System.Windows.Forms.Label lbVal8;
        public System.Windows.Forms.Label lbVal7;
        public System.Windows.Forms.Label lbVal6;
        public System.Windows.Forms.Label lbVal5;
        public System.Windows.Forms.Label lbVal4;
        public System.Windows.Forms.Label lbVal3;
        public System.Windows.Forms.Label lbVal2;
        public System.Windows.Forms.Label lbVal1;
        public System.Windows.Forms.CheckBox cbVal30;
        public System.Windows.Forms.CheckBox cbVal29;
        public System.Windows.Forms.CheckBox cbVal28;
        public System.Windows.Forms.CheckBox cbVal27;
        public System.Windows.Forms.CheckBox cbVal26;
        public System.Windows.Forms.CheckBox cbVal25;
        public System.Windows.Forms.CheckBox cbVal24;
        public System.Windows.Forms.CheckBox cbVal23;
        public System.Windows.Forms.CheckBox cbVal22;
        public System.Windows.Forms.CheckBox cbVal21;
        public System.Windows.Forms.CheckBox cbVal20;
        public System.Windows.Forms.CheckBox cbVal19;
        public System.Windows.Forms.CheckBox cbVal18;
        public System.Windows.Forms.CheckBox cbVal17;
        public System.Windows.Forms.CheckBox cbVal16;
        public System.Windows.Forms.CheckBox cbVal15;
        public System.Windows.Forms.CheckBox checkBox9;
        public System.Windows.Forms.CheckBox cbVal14;
        public System.Windows.Forms.CheckBox cbVal13;
        public System.Windows.Forms.CheckBox cbVal12;
        public System.Windows.Forms.CheckBox cbVal11;
        public System.Windows.Forms.CheckBox cbVal10;
        public System.Windows.Forms.CheckBox cbVal9;
        public System.Windows.Forms.CheckBox cbVal8;
        public System.Windows.Forms.CheckBox cbVal7;
        public System.Windows.Forms.CheckBox cbVal6;
        public System.Windows.Forms.CheckBox cbVal5;
        public System.Windows.Forms.CheckBox cbVal4;
        public System.Windows.Forms.CheckBox cbVal3;
        public System.Windows.Forms.CheckBox cbVal2;
        public System.Windows.Forms.CheckBox cbVal1;
        public System.Windows.Forms.ComboBox cbRelatedOp;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtPartDesc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cbUnitType;
        public System.Windows.Forms.ComboBox cbPartCategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cbPartNum;
        private System.Windows.Forms.GroupBox gbHeatType;
        private System.Windows.Forms.RadioButton rbDirectFired;
        private System.Windows.Forms.RadioButton rbElectric;
        private System.Windows.Forms.RadioButton rbIndirectFired;
        private System.Windows.Forms.GroupBox gbUnitType;
        private System.Windows.Forms.RadioButton rbOAU123DKN;
        private System.Windows.Forms.RadioButton rbOALBG;
        private System.Windows.Forms.Label lbAddMsg;
    }
}