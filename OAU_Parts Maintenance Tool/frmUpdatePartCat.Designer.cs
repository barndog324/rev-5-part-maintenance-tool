﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdatePartCat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUpdPartCatPullOrder = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtUpdPartCatLastModDate = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtUpdPartCatModBy = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtUpdPartCat_CategoryDesc = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUpdPartCatSave = new System.Windows.Forms.Button();
            this.btnUpdPartCatCancel = new System.Windows.Forms.Button();
            this.txtUpdPartCat_CategoryName = new System.Windows.Forms.TextBox();
            this.lbUpdPartCatID = new System.Windows.Forms.Label();
            this.cbUpdPartCatCritComp = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtUpdPartCatPullOrder
            // 
            this.txtUpdPartCatPullOrder.Location = new System.Drawing.Point(152, 71);
            this.txtUpdPartCatPullOrder.Name = "txtUpdPartCatPullOrder";
            this.txtUpdPartCatPullOrder.Size = new System.Drawing.Size(90, 20);
            this.txtUpdPartCatPullOrder.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label18.Location = new System.Drawing.Point(11, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(135, 20);
            this.label18.TabIndex = 208;
            this.label18.Text = "Pull Order:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdPartCatLastModDate
            // 
            this.txtUpdPartCatLastModDate.Enabled = false;
            this.txtUpdPartCatLastModDate.Location = new System.Drawing.Point(152, 149);
            this.txtUpdPartCatLastModDate.Name = "txtUpdPartCatLastModDate";
            this.txtUpdPartCatLastModDate.Size = new System.Drawing.Size(122, 20);
            this.txtUpdPartCatLastModDate.TabIndex = 207;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label17.Location = new System.Drawing.Point(11, 149);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(135, 20);
            this.label17.TabIndex = 206;
            this.label17.Text = "Last Modified:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdPartCatModBy
            // 
            this.txtUpdPartCatModBy.Enabled = false;
            this.txtUpdPartCatModBy.Location = new System.Drawing.Point(152, 123);
            this.txtUpdPartCatModBy.Name = "txtUpdPartCatModBy";
            this.txtUpdPartCatModBy.Size = new System.Drawing.Size(90, 20);
            this.txtUpdPartCatModBy.TabIndex = 205;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label16.Location = new System.Drawing.Point(11, 123);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(135, 20);
            this.label16.TabIndex = 204;
            this.label16.Text = "Modified By:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdPartCat_CategoryDesc
            // 
            this.txtUpdPartCat_CategoryDesc.Location = new System.Drawing.Point(152, 45);
            this.txtUpdPartCat_CategoryDesc.Name = "txtUpdPartCat_CategoryDesc";
            this.txtUpdPartCat_CategoryDesc.Size = new System.Drawing.Size(246, 20);
            this.txtUpdPartCat_CategoryDesc.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label12.Location = new System.Drawing.Point(11, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 20);
            this.label12.TabIndex = 200;
            this.label12.Text = "Categoty Desc";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(11, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 20);
            this.label4.TabIndex = 199;
            this.label4.Text = "Category Name:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUpdPartCatSave
            // 
            this.btnUpdPartCatSave.Location = new System.Drawing.Point(323, 100);
            this.btnUpdPartCatSave.Name = "btnUpdPartCatSave";
            this.btnUpdPartCatSave.Size = new System.Drawing.Size(75, 30);
            this.btnUpdPartCatSave.TabIndex = 4;
            this.btnUpdPartCatSave.Text = "Update";
            this.btnUpdPartCatSave.UseVisualStyleBackColor = true;
            this.btnUpdPartCatSave.Click += new System.EventHandler(this.btnUpdPartCatSave_Click);
            // 
            // btnUpdPartCatCancel
            // 
            this.btnUpdPartCatCancel.Location = new System.Drawing.Point(323, 139);
            this.btnUpdPartCatCancel.Name = "btnUpdPartCatCancel";
            this.btnUpdPartCatCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdPartCatCancel.TabIndex = 5;
            this.btnUpdPartCatCancel.Text = "Cancel";
            this.btnUpdPartCatCancel.UseVisualStyleBackColor = true;
            this.btnUpdPartCatCancel.Click += new System.EventHandler(this.btnUpdPartCatCancel_Click);
            // 
            // txtUpdPartCat_CategoryName
            // 
            this.txtUpdPartCat_CategoryName.Location = new System.Drawing.Point(152, 19);
            this.txtUpdPartCat_CategoryName.Name = "txtUpdPartCat_CategoryName";
            this.txtUpdPartCat_CategoryName.Size = new System.Drawing.Size(184, 20);
            this.txtUpdPartCat_CategoryName.TabIndex = 1;
            // 
            // lbUpdPartCatID
            // 
            this.lbUpdPartCatID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpdPartCatID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbUpdPartCatID.Location = new System.Drawing.Point(340, 19);
            this.lbUpdPartCatID.Name = "lbUpdPartCatID";
            this.lbUpdPartCatID.Size = new System.Drawing.Size(37, 20);
            this.lbUpdPartCatID.TabIndex = 209;
            this.lbUpdPartCatID.Text = "ID Hidden:";
            this.lbUpdPartCatID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbUpdPartCatID.Visible = false;
            // 
            // cbUpdPartCatCritComp
            // 
            this.cbUpdPartCatCritComp.AutoSize = true;
            this.cbUpdPartCatCritComp.Location = new System.Drawing.Point(152, 100);
            this.cbUpdPartCatCritComp.Name = "cbUpdPartCatCritComp";
            this.cbUpdPartCatCritComp.Size = new System.Drawing.Size(15, 14);
            this.cbUpdPartCatCritComp.TabIndex = 214;
            this.cbUpdPartCatCritComp.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(11, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 20);
            this.label1.TabIndex = 213;
            this.label1.Text = "Critical Component:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmUpdatePartCat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 187);
            this.Controls.Add(this.cbUpdPartCatCritComp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbUpdPartCatID);
            this.Controls.Add(this.txtUpdPartCat_CategoryName);
            this.Controls.Add(this.txtUpdPartCatPullOrder);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtUpdPartCatLastModDate);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtUpdPartCatModBy);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtUpdPartCat_CategoryDesc);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnUpdPartCatSave);
            this.Controls.Add(this.btnUpdPartCatCancel);
            this.Name = "frmUpdatePartCat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Part Category Head";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtUpdPartCatPullOrder;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txtUpdPartCatLastModDate;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtUpdPartCatModBy;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtUpdPartCat_CategoryDesc;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button btnUpdPartCatSave;
        private System.Windows.Forms.Button btnUpdPartCatCancel;
        public System.Windows.Forms.TextBox txtUpdPartCat_CategoryName;
        public System.Windows.Forms.Label lbUpdPartCatID;
        public System.Windows.Forms.CheckBox cbUpdPartCatCritComp;
        private System.Windows.Forms.Label label1;
    }
}