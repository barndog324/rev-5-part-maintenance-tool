﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateFurnaceData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUpdFurnaceMBHkW = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbUpdFurnacePartNum = new System.Windows.Forms.ComboBox();
            this.txtUpdFurnaceAmps = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUpdFurnaceLastModDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUpdFurnaceModifedBy = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtUpdFurnacePhase = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUpdFurnaceBurnerRatio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbUpdFurnaceID = new System.Windows.Forms.Label();
            this.txtUpdFurnacePartDesc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUpdFurnaceRuleHeadID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnUpdFurnaceSave = new System.Windows.Forms.Button();
            this.btnUpdFurnaceCancel = new System.Windows.Forms.Button();
            this.gbFuelType = new System.Windows.Forms.GroupBox();
            this.rbUpdFurnaceNaturalGas = new System.Windows.Forms.RadioButton();
            this.rbUpdFurnacePropane = new System.Windows.Forms.RadioButton();
            this.rbUpdFurnaceElectric = new System.Windows.Forms.RadioButton();
            this.gbVolts = new System.Windows.Forms.GroupBox();
            this.rbUpdFurnaceVolts575 = new System.Windows.Forms.RadioButton();
            this.rbUpdFurnaceVolts460 = new System.Windows.Forms.RadioButton();
            this.rbUpdFurnaceVolts208 = new System.Windows.Forms.RadioButton();
            this.rbUpdFurnaceVolts120 = new System.Windows.Forms.RadioButton();
            this.gbFuelType.SuspendLayout();
            this.gbVolts.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtUpdFurnaceMBHkW
            // 
            this.txtUpdFurnaceMBHkW.Location = new System.Drawing.Point(136, 174);
            this.txtUpdFurnaceMBHkW.Name = "txtUpdFurnaceMBHkW";
            this.txtUpdFurnaceMBHkW.Size = new System.Drawing.Size(90, 20);
            this.txtUpdFurnaceMBHkW.TabIndex = 170;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(16, 174);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(114, 20);
            this.label11.TabIndex = 188;
            this.label11.Text = "MBH/KW:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbUpdFurnacePartNum
            // 
            this.cbUpdFurnacePartNum.DropDownWidth = 600;
            this.cbUpdFurnacePartNum.Enabled = false;
            this.cbUpdFurnacePartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUpdFurnacePartNum.FormattingEnabled = true;
            this.cbUpdFurnacePartNum.Location = new System.Drawing.Point(136, 41);
            this.cbUpdFurnacePartNum.Name = "cbUpdFurnacePartNum";
            this.cbUpdFurnacePartNum.Size = new System.Drawing.Size(164, 23);
            this.cbUpdFurnacePartNum.TabIndex = 163;
            this.cbUpdFurnacePartNum.SelectedIndexChanged += new System.EventHandler(this.cbUpdFurnacePartNum_SelectedIndexChanged);
            // 
            // txtUpdFurnaceAmps
            // 
            this.txtUpdFurnaceAmps.Location = new System.Drawing.Point(136, 95);
            this.txtUpdFurnaceAmps.Name = "txtUpdFurnaceAmps";
            this.txtUpdFurnaceAmps.Size = new System.Drawing.Size(90, 20);
            this.txtUpdFurnaceAmps.TabIndex = 165;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(16, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 20);
            this.label10.TabIndex = 186;
            this.label10.Text = "Amps:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdFurnaceLastModDate
            // 
            this.txtUpdFurnaceLastModDate.Enabled = false;
            this.txtUpdFurnaceLastModDate.Location = new System.Drawing.Point(136, 226);
            this.txtUpdFurnaceLastModDate.Name = "txtUpdFurnaceLastModDate";
            this.txtUpdFurnaceLastModDate.Size = new System.Drawing.Size(122, 20);
            this.txtUpdFurnaceLastModDate.TabIndex = 185;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(16, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(114, 20);
            this.label9.TabIndex = 184;
            this.label9.Text = "Last Modified:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdFurnaceModifedBy
            // 
            this.txtUpdFurnaceModifedBy.Enabled = false;
            this.txtUpdFurnaceModifedBy.Location = new System.Drawing.Point(136, 200);
            this.txtUpdFurnaceModifedBy.Name = "txtUpdFurnaceModifedBy";
            this.txtUpdFurnaceModifedBy.Size = new System.Drawing.Size(90, 20);
            this.txtUpdFurnaceModifedBy.TabIndex = 183;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(16, 200);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 20);
            this.label8.TabIndex = 182;
            this.label8.Text = "Modified By:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdFurnacePhase
            // 
            this.txtUpdFurnacePhase.Location = new System.Drawing.Point(136, 148);
            this.txtUpdFurnacePhase.Name = "txtUpdFurnacePhase";
            this.txtUpdFurnacePhase.Size = new System.Drawing.Size(90, 20);
            this.txtUpdFurnacePhase.TabIndex = 168;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(16, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 20);
            this.label7.TabIndex = 181;
            this.label7.Text = "Phase:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdFurnaceBurnerRatio
            // 
            this.txtUpdFurnaceBurnerRatio.Location = new System.Drawing.Point(136, 122);
            this.txtUpdFurnaceBurnerRatio.Name = "txtUpdFurnaceBurnerRatio";
            this.txtUpdFurnaceBurnerRatio.Size = new System.Drawing.Size(90, 20);
            this.txtUpdFurnaceBurnerRatio.TabIndex = 167;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(16, 122);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 20);
            this.label6.TabIndex = 180;
            this.label6.Text = "Burner Ratio:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbUpdFurnaceID
            // 
            this.lbUpdFurnaceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUpdFurnaceID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbUpdFurnaceID.Location = new System.Drawing.Point(290, 9);
            this.lbUpdFurnaceID.Name = "lbUpdFurnaceID";
            this.lbUpdFurnaceID.Size = new System.Drawing.Size(75, 20);
            this.lbUpdFurnaceID.TabIndex = 179;
            this.lbUpdFurnaceID.Text = "ID Hidden:";
            this.lbUpdFurnaceID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbUpdFurnaceID.Visible = false;
            // 
            // txtUpdFurnacePartDesc
            // 
            this.txtUpdFurnacePartDesc.Enabled = false;
            this.txtUpdFurnacePartDesc.Location = new System.Drawing.Point(136, 69);
            this.txtUpdFurnacePartDesc.Name = "txtUpdFurnacePartDesc";
            this.txtUpdFurnacePartDesc.Size = new System.Drawing.Size(246, 20);
            this.txtUpdFurnacePartDesc.TabIndex = 164;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(16, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 177;
            this.label2.Text = "Part Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(16, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 176;
            this.label1.Text = "Part Number:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtUpdFurnaceRuleHeadID
            // 
            this.txtUpdFurnaceRuleHeadID.Enabled = false;
            this.txtUpdFurnaceRuleHeadID.Location = new System.Drawing.Point(136, 17);
            this.txtUpdFurnaceRuleHeadID.Name = "txtUpdFurnaceRuleHeadID";
            this.txtUpdFurnaceRuleHeadID.Size = new System.Drawing.Size(59, 20);
            this.txtUpdFurnaceRuleHeadID.TabIndex = 162;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(16, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 20);
            this.label5.TabIndex = 175;
            this.label5.Text = "Rule Head ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnUpdFurnaceSave
            // 
            this.btnUpdFurnaceSave.Location = new System.Drawing.Point(290, 216);
            this.btnUpdFurnaceSave.Name = "btnUpdFurnaceSave";
            this.btnUpdFurnaceSave.Size = new System.Drawing.Size(75, 30);
            this.btnUpdFurnaceSave.TabIndex = 173;
            this.btnUpdFurnaceSave.Text = "Update";
            this.btnUpdFurnaceSave.UseVisualStyleBackColor = true;
            this.btnUpdFurnaceSave.Click += new System.EventHandler(this.btnUpdFurnaceSave_Click);
            // 
            // btnUpdFurnaceCancel
            // 
            this.btnUpdFurnaceCancel.Location = new System.Drawing.Point(390, 216);
            this.btnUpdFurnaceCancel.Name = "btnUpdFurnaceCancel";
            this.btnUpdFurnaceCancel.Size = new System.Drawing.Size(75, 30);
            this.btnUpdFurnaceCancel.TabIndex = 174;
            this.btnUpdFurnaceCancel.Text = "Cancel";
            this.btnUpdFurnaceCancel.UseVisualStyleBackColor = true;
            this.btnUpdFurnaceCancel.Click += new System.EventHandler(this.btnUpdFurnaceCancel_Click);
            // 
            // gbFuelType
            // 
            this.gbFuelType.Controls.Add(this.rbUpdFurnaceNaturalGas);
            this.gbFuelType.Controls.Add(this.rbUpdFurnacePropane);
            this.gbFuelType.Controls.Add(this.rbUpdFurnaceElectric);
            this.gbFuelType.ForeColor = System.Drawing.Color.Green;
            this.gbFuelType.Location = new System.Drawing.Point(367, 94);
            this.gbFuelType.Name = "gbFuelType";
            this.gbFuelType.Size = new System.Drawing.Size(98, 91);
            this.gbFuelType.TabIndex = 189;
            this.gbFuelType.TabStop = false;
            this.gbFuelType.Text = "Fuel Type";
            // 
            // rbUpdFurnaceNaturalGas
            // 
            this.rbUpdFurnaceNaturalGas.AutoSize = true;
            this.rbUpdFurnaceNaturalGas.Location = new System.Drawing.Point(10, 63);
            this.rbUpdFurnaceNaturalGas.Name = "rbUpdFurnaceNaturalGas";
            this.rbUpdFurnaceNaturalGas.Size = new System.Drawing.Size(81, 17);
            this.rbUpdFurnaceNaturalGas.TabIndex = 10;
            this.rbUpdFurnaceNaturalGas.Text = "Natural Gas";
            this.rbUpdFurnaceNaturalGas.UseVisualStyleBackColor = true;
            this.rbUpdFurnaceNaturalGas.CheckedChanged += new System.EventHandler(this.rbUpdFurnaceNaturalGas_CheckedChanged);
            // 
            // rbUpdFurnacePropane
            // 
            this.rbUpdFurnacePropane.AutoSize = true;
            this.rbUpdFurnacePropane.Location = new System.Drawing.Point(10, 41);
            this.rbUpdFurnacePropane.Name = "rbUpdFurnacePropane";
            this.rbUpdFurnacePropane.Size = new System.Drawing.Size(65, 17);
            this.rbUpdFurnacePropane.TabIndex = 9;
            this.rbUpdFurnacePropane.Text = "Propane";
            this.rbUpdFurnacePropane.UseVisualStyleBackColor = true;
            this.rbUpdFurnacePropane.CheckedChanged += new System.EventHandler(this.rbUpdFurnacePropane_CheckedChanged);
            // 
            // rbUpdFurnaceElectric
            // 
            this.rbUpdFurnaceElectric.AutoSize = true;
            this.rbUpdFurnaceElectric.Location = new System.Drawing.Point(10, 20);
            this.rbUpdFurnaceElectric.Name = "rbUpdFurnaceElectric";
            this.rbUpdFurnaceElectric.Size = new System.Drawing.Size(60, 17);
            this.rbUpdFurnaceElectric.TabIndex = 8;
            this.rbUpdFurnaceElectric.Text = "Electric";
            this.rbUpdFurnaceElectric.UseVisualStyleBackColor = true;
            this.rbUpdFurnaceElectric.CheckedChanged += new System.EventHandler(this.rbUpdFurnaceElectric_CheckedChanged);
            // 
            // gbVolts
            // 
            this.gbVolts.Controls.Add(this.rbUpdFurnaceVolts575);
            this.gbVolts.Controls.Add(this.rbUpdFurnaceVolts460);
            this.gbVolts.Controls.Add(this.rbUpdFurnaceVolts208);
            this.gbVolts.Controls.Add(this.rbUpdFurnaceVolts120);
            this.gbVolts.ForeColor = System.Drawing.Color.Red;
            this.gbVolts.Location = new System.Drawing.Point(267, 94);
            this.gbVolts.Name = "gbVolts";
            this.gbVolts.Size = new System.Drawing.Size(78, 117);
            this.gbVolts.TabIndex = 190;
            this.gbVolts.TabStop = false;
            this.gbVolts.Text = "Volts";
            // 
            // rbUpdFurnaceVolts575
            // 
            this.rbUpdFurnaceVolts575.AutoSize = true;
            this.rbUpdFurnaceVolts575.Location = new System.Drawing.Point(10, 86);
            this.rbUpdFurnaceVolts575.Name = "rbUpdFurnaceVolts575";
            this.rbUpdFurnaceVolts575.Size = new System.Drawing.Size(43, 17);
            this.rbUpdFurnaceVolts575.TabIndex = 11;
            this.rbUpdFurnaceVolts575.Text = "575";
            this.rbUpdFurnaceVolts575.UseVisualStyleBackColor = true;
            this.rbUpdFurnaceVolts575.CheckedChanged += new System.EventHandler(this.rbUpdFurnaceVolts575_CheckedChanged);
            // 
            // rbUpdFurnaceVolts460
            // 
            this.rbUpdFurnaceVolts460.AutoSize = true;
            this.rbUpdFurnaceVolts460.Location = new System.Drawing.Point(10, 63);
            this.rbUpdFurnaceVolts460.Name = "rbUpdFurnaceVolts460";
            this.rbUpdFurnaceVolts460.Size = new System.Drawing.Size(43, 17);
            this.rbUpdFurnaceVolts460.TabIndex = 10;
            this.rbUpdFurnaceVolts460.Text = "460";
            this.rbUpdFurnaceVolts460.UseVisualStyleBackColor = true;
            this.rbUpdFurnaceVolts460.CheckedChanged += new System.EventHandler(this.rbUpdFurnaceVolts460_CheckedChanged);
            // 
            // rbUpdFurnaceVolts208
            // 
            this.rbUpdFurnaceVolts208.AutoSize = true;
            this.rbUpdFurnaceVolts208.Location = new System.Drawing.Point(10, 41);
            this.rbUpdFurnaceVolts208.Name = "rbUpdFurnaceVolts208";
            this.rbUpdFurnaceVolts208.Size = new System.Drawing.Size(43, 17);
            this.rbUpdFurnaceVolts208.TabIndex = 9;
            this.rbUpdFurnaceVolts208.Text = "208";
            this.rbUpdFurnaceVolts208.UseVisualStyleBackColor = true;
            this.rbUpdFurnaceVolts208.CheckedChanged += new System.EventHandler(this.rbUpdFurnaceVolts208_CheckedChanged);
            // 
            // rbUpdFurnaceVolts120
            // 
            this.rbUpdFurnaceVolts120.AutoSize = true;
            this.rbUpdFurnaceVolts120.Location = new System.Drawing.Point(10, 20);
            this.rbUpdFurnaceVolts120.Name = "rbUpdFurnaceVolts120";
            this.rbUpdFurnaceVolts120.Size = new System.Drawing.Size(43, 17);
            this.rbUpdFurnaceVolts120.TabIndex = 8;
            this.rbUpdFurnaceVolts120.Text = "120";
            this.rbUpdFurnaceVolts120.UseVisualStyleBackColor = true;
            this.rbUpdFurnaceVolts120.CheckedChanged += new System.EventHandler(this.rbUpdFurnaceVolts120_CheckedChanged);
            // 
            // frmUpdateFurnaceData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 267);
            this.Controls.Add(this.gbVolts);
            this.Controls.Add(this.gbFuelType);
            this.Controls.Add(this.txtUpdFurnaceMBHkW);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbUpdFurnacePartNum);
            this.Controls.Add(this.txtUpdFurnaceAmps);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtUpdFurnaceLastModDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtUpdFurnaceModifedBy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtUpdFurnacePhase);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtUpdFurnaceBurnerRatio);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lbUpdFurnaceID);
            this.Controls.Add(this.txtUpdFurnacePartDesc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUpdFurnaceRuleHeadID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnUpdFurnaceSave);
            this.Controls.Add(this.btnUpdFurnaceCancel);
            this.Name = "frmUpdateFurnaceData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Furnace Data";
            this.gbFuelType.ResumeLayout(false);
            this.gbFuelType.PerformLayout();
            this.gbVolts.ResumeLayout(false);
            this.gbVolts.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtUpdFurnaceMBHkW;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.ComboBox cbUpdFurnacePartNum;
        public System.Windows.Forms.TextBox txtUpdFurnaceAmps;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtUpdFurnaceLastModDate;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtUpdFurnaceModifedBy;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtUpdFurnacePhase;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtUpdFurnaceBurnerRatio;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lbUpdFurnaceID;
        public System.Windows.Forms.TextBox txtUpdFurnacePartDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtUpdFurnaceRuleHeadID;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btnUpdFurnaceSave;
        private System.Windows.Forms.Button btnUpdFurnaceCancel;
        private System.Windows.Forms.GroupBox gbFuelType;
        public System.Windows.Forms.RadioButton rbUpdFurnaceNaturalGas;
        public System.Windows.Forms.RadioButton rbUpdFurnacePropane;
        public System.Windows.Forms.RadioButton rbUpdFurnaceElectric;
        private System.Windows.Forms.GroupBox gbVolts;
        public System.Windows.Forms.RadioButton rbUpdFurnaceVolts575;
        public System.Windows.Forms.RadioButton rbUpdFurnaceVolts460;
        public System.Windows.Forms.RadioButton rbUpdFurnaceVolts208;
        public System.Windows.Forms.RadioButton rbUpdFurnaceVolts120;
    }
}