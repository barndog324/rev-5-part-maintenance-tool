﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdatePartRuleHead : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        private frmMain m_parent;
               
        public frmUpdatePartRuleHead(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
        }

        private void btnUpdCancel_Click(object sender, EventArgs e)
        {
            m_parent.mPartCategory = "NoUpdate";
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string pickListValStr = "No";
            string configPart = "No";
            string modByStr = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

            int slashPos = modByStr.IndexOf("\\");

            objPart.RuleHeadID = txtPRH_RuleHeadID.Text;            
            objPart.PartNum = txtPRH_PartNum.Text;
            objPart.PartCategory = cbPRH_PartCategory.Text;
            objPart.RelatedOperation = cbPRH_RelatedOp.Text;
            objPart.AssemblySeq = txtPRH_AsmSeq.Text;
            objPart.LinesideBin = txtLinesideBin.Text;
            objPart.PartType = txtPRH_PartType.Text;
            objPart.StationLoc = cbStationLoc.Text;

            if (chkBoxPicklist.Checked == true)
            {
                pickListValStr = "Yes";
            }

            if (chkBoxConfigPart.Checked == true)
            {
                configPart = "Yes";
            }

            objPart.PickList = pickListValStr;
            objPart.ConfigurablePart = configPart;

            if (slashPos > 0)
            {
                objPart.UserName = modByStr.Substring((slashPos + 1), modByStr.Length - (slashPos + 1));
            }
            else
            {
                objPart.UserName = "";
            }

            objPart.DateMod = DateTime.Now.ToString("g");  // 2/27/2009 12:12 PM format
            
            if (validFurnaceDataUpdates() == true)
            {
                objPart.UpdatePartRulesHead();

                m_parent.mPartCategory = objPart.PartCategory;
                m_parent.mRelatedOp = objPart.RelatedOperation;
                m_parent.mModBy = objPart.UserName;
                m_parent.mLastModDate = objPart.DateMod;
                m_parent.mAssemblySeq = objPart.AssemblySeq;
                m_parent.mPartType = objPart.PartType;
                m_parent.mStationLoc = objPart.StationLoc;
                m_parent.mLinesideBin = objPart.LinesideBin;
                m_parent.mPicklist = objPart.PickList;
                m_parent.mConfigPart = objPart.ConfigurablePart;

                this.Close();
            }
        }

        private bool validFurnaceDataUpdates()
        {
            bool retVal = true;           

            string errors = String.Empty;

            if (cbPRH_PartCategory.SelectedIndex == 0)
            {
                errors += "Error - No Part Category has been selected.\n";
            }

            if (txtPRH_AsmSeq.Text.Length == 0)
            {
                errors += "Error - AssemblySeq is a required integer field.\n";
            }
            else
            {
                try
                {
                    int asmSeqInt = Int32.Parse(txtPRH_AsmSeq.Text);
                }
                catch
                {
                    errors += "Error - Invalid data in the AssemblySeq field. AssemblySeq is an integer field.\n";
                }
            }

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string partNum = txtPRH_PartNum.Text;

            DialogResult result1 = MessageBox.Show("Are you sure you want to do this Part Number: " + partNum + "? Press Yes to delete; No to cancel!",
                                                       "Deleting a PartRulesHead Row",
                                                       MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                objPart.RuleHeadID = txtPRH_RuleHeadID.Text;
                objPart.DeletePartRulesHead();
                m_parent.mPartCategory = "Delete";
                this.Close();
            }
        }
    }
}
