﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmCreateSS_Rules : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        public frmCreateSS_Rules()
        {
            InitializeComponent();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            int rowIdx = 0;
            string ruleBatchID = "0";
            string partNumStr = "";

            foreach (DataGridViewRow row in dgvSelectedParts.Rows)
            {
                partNumStr = row.Cells["PartNum"].Value.ToString();
                objPart.PartNum = partNumStr;

                try
                {
                    DataTable dtRules = objPart.GetPartRulesByPartNumber();
                    objPart.PartNum += "-SS";

                    try
                    {
                        DataTable dtPRH = objPart.GetPartRulesHead();
                        if (dtPRH.Rows.Count == 0)
                        {
                            objPart.PartNum = partNumStr;
                            dtPRH = objPart.GetPartRulesHead();
                            if (dtPRH.Rows.Count > 0)
                            {
                                DataRow dr2 = dtPRH.Rows[0];
                                objPart.PartNum += "-SS";
                                objPart.PartCategory = dr2["PartCategory"].ToString();
                                objPart.RelatedOperation = dr2["RelatedOperation"].ToString();
                                objPart.AssemblySeq = dr2["AssemblySeq"].ToString();
                                objPart.LinesideBin = dr2["LinesideBin"].ToString();
                                objPart.PartType = dr2["PartType"].ToString();
                                objPart.StationLoc = dr2["StationLocation"].ToString();
                                objPart.PickList = dr2["Picklist"].ToString();
                                objPart.ConfigurablePart = dr2["ConfigurablePart"].ToString();
                                objPart.UserName = "system";
                                try
                                {
                                    objPart.InsertPartRulesHead();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("ERROR - Inserting " + objPart.PartNum + " into PartRulesHead - " + ex);
                                }
                            }
                        }

                        foreach (DataRow drow in dtRules.Rows)
                        {
                            objPart.RuleHeadID = "0";
                            objPart.RuleBatchID = ruleBatchID;
                            objPart.Digit = drow["Digit"].ToString();
                            objPart.HeatType = drow["HeatType"].ToString();
                            objPart.UnitType = drow["Unit"].ToString();
                            if (objPart.Digit == "24")
                            {
                                objPart.Value = "1,2,3,5,9";
                            }
                            else
                            {
                                objPart.Value = drow["Value"].ToString();
                            }
                            objPart.Qty = drow["Qty"].ToString();
                            objPart.RelatedOperation = drow["RelatedOperation"].ToString();                          
                            objPart.RfgComponent = drow["RfgComponent"].ToString();
                            objPart.CircuitCharge = drow["CircuitCharge"].ToString();
                            objPart.Circuit1 = drow["Circuit1"].ToString();
                            objPart.Circuit2 = drow["Circuit2"].ToString();
                            objPart.SubAssemblyPart = bool.Parse(drow["SubAssemblyPart"].ToString());
                            objPart.SubAsmMtlPart = bool.Parse(drow["SubAsmMtlPart"].ToString());
                            objPart.ImmediateParent = drow["ImmediateParent"].ToString();
                            objPart.Comments = drow["Comments"].ToString();
                            objPart.UserName = "system";
                            try
                            {
                                ruleBatchID = objPart.Insert_ROA_PartConditionsDTL();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("ERROR - Inserting " + objPart.PartNum + " into PartConditionsDTL - " + ex);
                            }
                        }

                        try
                        {
                            DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();
                            if (dt.Rows.Count > 0)
                            {
                                try
                                {
                                    objPart.UpdatePartMaster(dt);                                    
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("ERROR - Inerting into ROA_PartMaster - " + ex);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ERROR - Reading PartConditionsDTL table. - " + ex);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR - Reading PartRulesHead table. - " + ex);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - Reading PartConditionsDTL table. - " + ex);
                }
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            while (dgvSelectedParts.Rows.Count > 0)
            {
                dgvSelectedParts.Rows.Remove(dgvSelectedParts.Rows[0]);
            }         
        }

        private void dgvVmeParts_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvVmeParts.CurrentRow.Index;

            objPart.PartNum = dgvVmeParts.Rows[rowIdx].Cells["PartNum"].Value.ToString();

            bool partFound = false;            

            foreach (DataGridViewRow row in dgvSelectedParts.Rows)
            {
                if (objPart.PartNum == row.Cells["Partnum"].Value.ToString())
                {
                    partFound = true;
                }
            }

            if (partFound == false)
            {
                objPart.PartNum += "-SS";
                DataTable dtPart = objPart.GetPartRulesByPartNumber();
                if (dtPart.Rows.Count > 0)
                {
                    MessageBox.Show("WARNING - Stainless Steel rule already exist for this Part number.");
                }
                else
                {
                    dtPart = objPart.GetPartDetail();
                    if (dtPart.Rows.Count == 0)
                    {
                        MessageBox.Show("ERROR - Part number - " + objPart.PartNum + " does Not exist in Epicor!");
                    }
                    else
                    {
                        var index = dgvSelectedParts.Rows.Add();
                        dgvSelectedParts.Rows[index].Cells["PartNum"].Value = dgvVmeParts.Rows[rowIdx].Cells["PartNum"].Value.ToString();
                    }
                }
            }
            else
            {
                MessageBox.Show("ERROR - Part Number -> " + objPart.PartNum + " already in list");
            }
                       
        }

        private void dgvSelectedParts_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvSelectedParts.CurrentRow.Index;

            dgvSelectedParts.Rows.Remove(dgvSelectedParts.Rows[rowIdx]);
        }

        private void txtSearchPartNum_KeyUp(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.                

                tmpStr = this.txtSearchPartNum.Text.ToUpper();
                if (tmpStr.Length > 3)
                {                   
                    foreach (DataGridViewRow dgr in dgvVmeParts.Rows)
                    {
                        if (dgr.Cells["PartNum"].Value != null)
                        {
                            if (dgr.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["PartNum"].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvVmeParts.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvVmeParts.Rows[rowIdx].Selected = true;
                                dgvVmeParts.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvVmeParts.Focus();
                                break;
                            }
                        }
                    }
                }
            }
            this.txtSearchPartNum.Focus();
        }
       
    }
}
