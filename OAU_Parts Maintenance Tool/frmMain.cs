﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmMain : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        ModelNumDescBO objModel = new ModelNumDescBO();
        CompressorBO objComp = new CompressorBO();
        MotorBO objMotor = new MotorBO();
        FurnaceBO objFurn = new FurnaceBO();
        PartCategoryBO objCat = new PartCategoryBO();

        int curSelRowIdx = 0;
        int curSearchDigitNo = 0;
        int curVoltage = -1;
        string curHeatType = "";
        string curOAUTypeCode = "";
        string curPartNum = "";

        bool initModelRender = true;
        
        // PartNum master fields
        public string mRuleHeadID { get; set; }
        public string mRuleBatchID { get; set; }
        public string mPartNumber { get; set; }
        public string mPartDesc { get; set; }
        public string mRelatedOp { get; set; }
        public string mPartCategory { get; set; }
        public string mUnitType { get; set; }
        public string mDigit { get; set; }
        public string mValueStr { get; set; }
        public string mQty { get; set; } 
        public string mUpdatedValueStr { get; set; }        
        public string mAssemblySeq { get; set; }
        public string mPartType { get; set; }
        public string mStationLoc { get; set; }
        public string mLinesideBin { get; set; }
        public string mPicklist { get; set; }
        public string mConfigPart { get; set; }

        // ModelNumDesc fields
        public string mDigitNo { get; set; }
        public string mDigitVal { get; set; }
        public string mHeatType { get; set; }
        public string mDigitDesc { get; set; }
        public string mDigitRep { get; set; }
        public string mOAUTypeCode { get; set; }
        public string mComboBoxItemPos { get; set; }

        // Compressor Data fields
        public string mVoltage { get; set; }
        public string mPhase { get; set; }
        public string mRLA { get; set; }
        public string mLRA { get; set; }
        public string mCircuit1 { get; set; }
        public string mCircuit1_Reheat { get; set; }
        public string mCircuit2 { get; set; }
        public string mCircuit2_Reheat { get; set; }
        public string mCoolingCap { get; set; }       
        public string mModBy { get; set; }
        public string mLastModDate { get; set; }

        // Motor Data fields
        public string mMotorType { get; set; }
        public string mFLA { get; set; }
        public string mHertz { get; set; }
        public string mMCC { get; set; }
        public string mHP { get; set; }

        // Furnace Data fields
        public string mAmps { get; set; }
        public string mVolts { get; set; }
        public string mBurnerRatio { get; set; }
        public string mFuelType { get; set; }
        public string mMBHkW { get; set; }

        // Part Category Head Fields
        public string mCategoryName { get; set; }       
        public string mCategoryDesc { get; set; }
        public string mPullOrder { get; set; }
        public string mCritComp { get; set; }

        public string EnvironmentStr { get; set; }
        public Color EnvColor { get; set; }

        public frmMain()
        {
            InitializeComponent();

#if DEBUG
            try           // If in the debug environment then set the Database connection string to the test database.
            {
                EnvColor = Color.LightCoral;
                EnvironmentStr = "Development Environment";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
            }
#if TEST2
            try           // If in the debug environment then set the Database connection string to the test database.
            {
                EnvColor = Color.SpringGreen;
                EnvironmentStr = "Test 2 Environment";
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.ConnectionStrings.ConnectionStrings["ProdDatabase.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=KCCWVTEPICSQL01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ".This is invalid connection", "Incorrect server/Database - " + ex);
            }
#endif
#endif

            //Load PartRulesHead DataGridView
            objPart.PartNum = "ALL";
            DataTable dt = objPart.GetPartRulesHead();
            populatePartRuleHeadList(dt, true);

            //Load PartConditionsDTL DataGridView
            dt = objPart.GetOAU_PartsAndRulesList();
            populateMainPartsList(dt);            

            //Load Model No Desc DataGridView
            curSearchDigitNo = -1;
            curHeatType = "ALL";
            curOAUTypeCode = "";

            dt = objModel.GetOAU_ModelNoValues(curSearchDigitNo, curHeatType, null, null);
            populateModelNoList(dt, true);

            ////Load Compressor Detail DataGridView
            //curVoltage = -1;
            //curPartNum = "";
            //dt = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
            //populateCompressorDetailList(dt, true);

            ////Load Compressor Charge DataGridView
            //curVoltage = -1;
            //curPartNum = "";
            //objComp.UnitModel = "ALL";
            //objComp.CoolingCap = "ALL";
            //objComp.HeatType = "";
            //dt = objComp.GetROA_CompressorChargeData();
            //populateCompressorChargeList(dt, true);

            ////Load Motor Data DataGridView

            //objMotor.RuleHeadID = "-1";
            //dt = objMotor.GetROA_AllMotorDataDTL();
            //populateMotorDataList(dt, true);

            //Load Furnace Data DataGridView

            //objFurn.RuleHeadID = "-1";
            //dt = objFurn.GetROA_FurnaceDataDTL();
            //populateFurnaceDataList(dt, true);

            ////Load Part Category Head DataGridView

            objCat.PullOrder = "-1";
            dt = objCat.GetPartCategoryHead();
            populatePartCategoryDataList(dt, true);

            this.txtPRH_SearchPartNum.Focus();
            lbMainTitle.Text = "REV 5 -- Part Rules Head";
            lbEnvironment.Text = EnvironmentStr;
            this.BackColor = EnvColor;
        }


        #region Main Form Events

        private void btnMainExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            int tabIdx = 0;
            tabIdx = tabMain.SelectedIndex;            

            switch (tabIdx)
            {
                case 0: lbMainTitle.Text = "REV 5 -- Part Rules Head";
                    break;
                case 1: lbMainTitle.Text = "REV 5 -- Part Rule Conditions";
                    break;
                case 2: lbMainTitle.Text = "REV 5 -- Model No Description";
                    break;
                case 3: lbMainTitle.Text = "REV 5 -- Compressor Data Detail";
                    break;
                case 4: lbMainTitle.Text = "REV 5 -- Compressor Charge Data";
                    break;
                case 5: lbMainTitle.Text = "REV 5 -- Motor Data Detail";
                    break;
                case 6: lbMainTitle.Text = "REV 5 -- Furnace Data Detail";
                    break;
                case 7: lbMainTitle.Text = "REV 5 -- Part Category";
                    break;
                default: break;
            }
        }
        #endregion

        #region OAU Part Rules/Conditions Tab Events
        private void dgvPartsMain_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string partNum;
            string partDesc;
            string partCategory;
            string unitType;
            string value;
            string heatType = "N/A";
            string modBy;
            string modDate;
            string id = "";            
            string immedParent;
            string comments;

            int ruleHeadID;
            int ruleBatchID;
            int relatedOp;
            int digit;

            decimal reqQty;

            bool rfgComp = false;
            bool circuit1 = false;
            bool circuit2 = false;
            bool subAssemblyPart = false;
            bool subAsmMtlPart = false;
           
            //var dataIndexNo = dgvPartsMain.Rows[e.RowIndex].Index.ToString();
            //MessageBox.Show("Row == " + dataIndexNo.ToString());
            ruleHeadID = (int)dgvPartsMain.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
            ruleBatchID = (int)dgvPartsMain.Rows[e.RowIndex].Cells["RuleBatchID"].Value;
            partNum = dgvPartsMain.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
            partDesc = dgvPartsMain.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
            relatedOp = (int)dgvPartsMain.Rows[e.RowIndex].Cells["RelatedOperation"].Value;
            partCategory = dgvPartsMain.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
            unitType = dgvPartsMain.Rows[e.RowIndex].Cells["Unit"].Value.ToString();
            digit = (int)dgvPartsMain.Rows[e.RowIndex].Cells["Digit"].Value;
            heatType = dgvPartsMain.Rows[e.RowIndex].Cells["HeatType"].Value.ToString();
            value = dgvPartsMain.Rows[e.RowIndex].Cells["Value"].Value.ToString();
            reqQty = (decimal)dgvPartsMain.Rows[e.RowIndex].Cells["Qty"].Value;
            rfgComp = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["RfgComponent"].Value;
            circuit1 = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["Circuit1"].Value;
            circuit2 = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["Circuit2"].Value;
            subAssemblyPart = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["SubAssemblyPart"].Value;  
            subAsmMtlPart = (bool)dgvPartsMain.Rows[e.RowIndex].Cells["SubAsmMtlPart"].Value;
            immedParent = dgvPartsMain.Rows[e.RowIndex].Cells["ImmediateParent"].Value.ToString();
            comments = dgvPartsMain.Rows[e.RowIndex].Cells["Comments"].Value.ToString(); 
            modDate = dgvPartsMain.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            modBy = dgvPartsMain.Rows[e.RowIndex].Cells["UserName"].Value.ToString();    
        
            if (heatType == "" || heatType == " ")
            {
                heatType = "NA";
            }

            frmUpdate frmUpd = new frmUpdate(this);
            frmUpd.cbPartNum.Text = partNum;
            frmUpd.txtPartDesc.Text = partDesc;
            frmUpd.txtRelatedOp.Text = relatedOp.ToString();
            frmUpd.lbModBy.Text = modBy;
            frmUpd.lbModDate.Text = modDate;           

            DataTable dt1 = objModel.GetModelNoDigitList();
            frmUpd.cbDigit.DisplayMember = "DigitHeat";
            frmUpd.cbDigit.ValueMember = "HeatType";

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "0/NA = CircuitBreaker";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);

            frmUpd.cbDigit.DataSource = dt1;
            //frmUpd.cbDigit.SelectedIndex = 0;

            frmUpd.cbDigit.SelectedIndex = frmUpd.cbDigit.FindString(digit.ToString() + "/" + heatType);
            //frmUpd.cbDigit.Text = digit.ToString();
          
            frmUpd.txtPartCategory.Text = partCategory;
            frmUpd.lbUnitType.Text = unitType;
            frmUpd.txtRuleID.Text = ruleHeadID.ToString();
            frmUpd.txtBatchID.Text = ruleBatchID.ToString();
            frmUpd.nudReqQty.Value = reqQty;

            setupValueCheckBoxes(frmUpd, digit, value, unitType);

            switch (unitType)
            {
                case "OAB":
                    frmUpd.rbOAB.Checked = true;
                    break;
                case "OAD":
                    frmUpd.rbOAD.Checked = true;
                    break;
                case "OAG":
                    frmUpd.rbOAG.Checked = true;
                    break;
                case "OAK":
                    frmUpd.rbOAK.Checked = true;
                    break;
                case "OAN":
                    frmUpd.rbOAN.Checked = true;
                    break;
                default:
                    break;
            }

            frmUpd.gbUnitType.Enabled = false;

            objPart.PartNum = partNum;
            objPart.RuleBatchID = ruleBatchID.ToString();
            objPart.RevType = "REV5";

            if (subAssemblyPart == true)    
            {
                frmUpd.chkBoxSubAsm.Checked = true;
                frmUpd.cbImmediateParentPartNum.SelectedValue = immedParent;

                //DataTable dtSub = objPart.GetSubAssemblyParentData();

                //if (dtSub.Rows.Count > 0)
                //{
                    //DataRow drSub = dtSub.Rows[0];

                    //frmUpd.nudBOM_Level.Value = (int)drSub["BOM_Level"];
                    //frmUpd.cbImmediateParentPartNum.SelectedValue = drSub["PrimaryAssemblyPartNum"].ToString();

                    //if (drSub["InnerAssemblyPartNum"] != null)
                    //{
                    //    frmUpd.cbInnerAsmPartNum.SelectedValue = drSub["InnerAssemblyPartNum"].ToString();
                    //}
                    //else
                    //{
                    //    frmUpd.cbInnerAsmPartNum.SelectedIndex = 0;
                    //}
                //}
            }

            if (subAsmMtlPart == true)   
            {
                frmUpd.chkBoxMtlPart.Checked = true;
            }

            frmUpd.rtbComments.Text = comments;

            //if (preConfigAsm == true)     
            //{
            //    frmUpd.chkBoxPreConfigAsm.Checked = true;
            //}

            DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

            foreach (DataRow dr in dt.Rows)
            {
                int index = frmUpd.dgvUpdateRules.Rows.Add();

                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleHeadID"].Value = dr["RuleHeadID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = dr["RuleBatchID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = dr["PartNum"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = dr["PartDescription"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = dr["RelatedOperation"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = dr["PartCategory"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = dr["Unit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = dr["Digit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = dr["HeatType"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColValue"].Value = dr["Value"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColQty"].Value = dr["Qty"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Update";
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColID"].Value = dr["ID"].ToString();

                if (value == dr["Value"].ToString())
                {
                    frmUpd.lbSelectedRow.Text = index.ToString();                   
                }
            }           

            if (rfgComp == true)
            {
                frmUpd.chkBoxRfgComp.Checked = true;
                frmUpd.gbCircuits.Visible = true;
                if (circuit1 == true)
                {
                    frmUpd.chkBoxCircuit1.Checked = true;
                }
                if (circuit2 == true)
                {
                    frmUpd.chkBoxCircuit2.Checked = true;
                }
            }



            frmUpd.lbScreenMode.Text = "UpdateMode";
            this.btnMainExit.Enabled = false;
            frmUpd.ShowDialog();

            if (mUpdatedValueStr != "NoUpdate")
            {               
                dt = objPart.GetOAU_PartsAndRulesList();
                populateMainPartsList(dt);

                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, -1); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }
            this.btnMainExit.Enabled = true;
        }
              
        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtSearchPartNum.Text = "";
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            this.txtSearchPartNum.Text = "";           
            dgvPartsMain.DataSource = null;
            DataTable dt = objPart.GetOAU_PartsAndRulesList();
            dgvPartsMain.DataSource = dt;            
            populateMainPartsList(dt);
        }

        private void dgvPartsMain_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string strValue = "";

            int iRuleId = 0;
            int iBatchId = 0;
            int iCurRuleId = -1;
            int iCurBatchId = -1;

            Color prevBackground = Color.White;
            Color foreGround = Color.Black;
            Color curBackGround = Color.LightGray;
            Color nextBackGround = Color.White;

            foreach (DataGridViewRow row in dgvPartsMain.Rows)
            {

                strValue = row.Cells["RuleHeadID"].Value.ToString();
                iRuleId = Int32.Parse(strValue);
                strValue = row.Cells["RuleBatchID"].Value.ToString();
                iBatchId = Int32.Parse(strValue);

                if ((iCurRuleId != iRuleId) || (iCurBatchId != iBatchId))
                {
                    iCurRuleId = iRuleId;
                    iCurBatchId = iBatchId;

                    prevBackground = curBackGround;
                    curBackGround = nextBackGround;
                    nextBackGround = prevBackground;
                }

                row.DefaultCellStyle.BackColor = curBackGround;
                row.DefaultCellStyle.ForeColor = foreGround;
            }
        }

        private void dgvPartsMain_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {          
            //int rowIdx = e.RowIndex;           
            //int ruleHeadID = Int32.Parse(dgvPartsMain.Rows[rowIdx].Cells["RuleHeadID"].Value.ToString());
            //int ruleBatchID = Int32.Parse(dgvPartsMain.Rows[rowIdx].Cells["RuleBatchID"].Value.ToString());

            //string partNumberStr = dgvPartsMain.Rows[rowIdx].Cells[].Value.ToString();

            //DataTable rulesDT = dgvPartsMain.DataSource as DataTable;

            //objPart.RuleHeadID = ruleHeadID.ToString();
            //objPart.RuleBatchID = ruleBatchID.ToString();

            //if (e.ColumnIndex == 0)  // Add button within datagridview.
            //{
            //    frmAddRule frmAdd = new frmAddRule(this);
            //    DataTable dt = objPart.GetPartCategoryList();

            //    mPartNumber = dgvPartsMain.Rows[rowIdx].Cells[4].Value.ToString();
            //    mPartDesc = dgvPartsMain.Rows[rowIdx].Cells[5].Value.ToString();
            //    mRelatedOp = dgvPartsMain.Rows[rowIdx].Cells[6].Value.ToString();
            //    mPartCategory = dgvPartsMain.Rows[rowIdx].Cells[7].Value.ToString();
            //    mUnitType = dgvPartsMain.Rows[rowIdx].Cells[8].Value.ToString();                
               
            //    frmAdd.cbPartCategory.DisplayMember = "CategoryName";
            //    frmAdd.cbPartCategory.ValueMember = "CategoryName";               
            //    frmAdd.cbPartCategory.DataSource = dt;

            //    objPart.PartCategory = null;
            //    dt = objPart.GetOAU_MainPartList();
            //    frmAdd.cbPartNum.DisplayMember = "PartNum";
            //    frmAdd.cbPartNum.ValueMember = "ID";

            //    DataRow dr = dt.NewRow();
            //    dr["ID"] = 0;
            //    dr["PartNum"] = "Select a Part Number.";
            //    dt.Rows.InsertAt(dr, 0);
            //    frmAdd.cbPartNum.DataSource = dt;                
            //    frmAdd.cbPartNum.SelectedIndex = 0;
            //    frmAdd.cbPartNum.Text = partNumberStr;
            //    frmAdd.cbPartNum.Enabled = false;
            //    frmAdd.txtPartDesc.Text = dgvPartsMain.Rows[rowIdx].Cells[5].Value.ToString(); 

            //    frmAdd.txtRuleID.Text = ruleHeadID.ToString();
            //    frmAdd.txtBatchID.Text =ruleBatchID.ToString();
            //    frmAdd.txtBatchID.Enabled = false;
            //    frmAdd.cbRelatedOp.Text = dgvPartsMain.Rows[rowIdx].Cells[6].Value.ToString();
            //    frmAdd.cbRelatedOp.Enabled = false;
            //    frmAdd.cbPartCategory.Text = dgvPartsMain.Rows[rowIdx].Cells[7].Value.ToString();
            //    frmAdd.cbPartCategory.Enabled = false;
            //    frmAdd.cbUnitType.Text = dgvPartsMain.Rows[rowIdx].Cells[8].Value.ToString();
            //    frmAdd.cbUnitType.Enabled = false;
            //    frmAdd.nudReqQty.Value = decimal.Parse(dgvPartsMain.Rows[rowIdx].Cells[11].Value.ToString());
            //    frmAdd.nudReqQty.Enabled = false;
            //    frmAdd.cbDigit.Focus();
                
            //    frmAdd.ShowDialog();

            //    if (mQty != "-1")
            //    {
            //        DataRow row = rulesDT.NewRow();                                            
                   
            //        row[0] = mRuleHeadID;
            //        row[1] = mRuleBatchID;
            //        row[2] = mPartNumber;
            //        row[3] = mPartDesc;
            //        row[4] = Int32.Parse(mRelatedOp);
            //        row[5] = mPartCategory;
            //        row[6] = mUnitType;
            //        row[7] = Int32.Parse(mDigit);
            //        row[8] = mValueStr;
            //        row[9] = decimal.Parse(mQty);

            //        rulesDT.Rows.InsertAt(row, (rowIdx + 1));

            //        dgvPartsMain.DataSource = rulesDT;
            //        dgvPartsMain.Refresh();
            //    }
            //}
            //else if (e.ColumnIndex == 1)  // Delete button internally is really in column #1 not 11 as it appears on the screen.
            //{
            //    objPart.Digit = dgvPartsMain.Rows[rowIdx].Cells[9].Value.ToString();                

            //    DialogResult result1 = MessageBox.Show("Attempting to Delete a rule (RuleHeadID/RuleBatchID/Digit): " + objPart.RuleHeadID + "/" + objPart.RuleBatchID + "/" + objPart.Digit +
            //                                           " for Part Number: " + partNumberStr + " from the ROA_PartConditionsDTL Table. Are you sure you want to do this?",
            //                                           "Deleting a Rule",
            //                                           MessageBoxButtons.YesNo);
            //    if (result1 == DialogResult.Yes)
            //    {
            //        objPart.DeleteFromROA_PartConditionsDTL();
            //        DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

            //        if (dt.Rows.Count > 0)
            //        {
            //            objPart.UpdateOAU_PartMaster(dt);
            //        }
            //        dgvPartsMain.DataSource = null;
            //        DataTable dtRules = objPart.GetOAU_PartsAndRulesList();
            //        dgvPartsMain.DataSource = dtRules;
            //        populateMainPartsList(dtRules);
            //        dgvPartsMain.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
            //        dgvPartsMain.Rows[--rowIdx].Selected = true;
            //        dgvPartsMain.FirstDisplayedScrollingRowIndex = rowIdx;
            //        dgvPartsMain.Focus();
            //    }               
            //}   
            //else
            //{
            //    curSelRowIdx = e.RowIndex;
            //}
        }

        private void btnAddPart_Click(object sender, EventArgs e)
        {
            //frmAddRule frmNew = new frmAddRule(this);           
            frmUpdate frmNew = new frmUpdate(this);

            DataTable dt = objPart.GetOAU_MainPartList();
            frmNew.cbPartNum.DisplayMember = "PartNum";
            frmNew.cbPartNum.ValueMember = "PartNum";

            DataRow dr = dt.NewRow();
            dr["ID"] = 0;
            dr["PartNum"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);

            DataTable dt1 = objModel.GetModelNoDigitList();
            frmNew.cbDigit.DisplayMember = "DigitHeat";
            frmNew.cbDigit.ValueMember = "HeatType";           

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "Select Digit Number Please.";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);

            frmNew.cbDigit.DataSource = dt1;
            frmNew.cbDigit.SelectedIndex = 0;
            frmNew.cbDigit.Enabled = false;

            frmNew.gbValues.Visible = false;
            //frmNew.lbAddMsg.Text = "";
            frmNew.lbScreenMode.Text = "NewPartMode";
            //frmNew.cbPartNum.Enabled = true;
            frmNew.cbPartNum.DataSource = dt;
            frmNew.cbPartNum.SelectedIndex = 0;
            frmNew.cbPartNum.Select();
            //frmNew.lbScreenMode.Text = "AddNewPart";
            frmNew.btnAddNewRule.Text = "Save New Rule";
            frmNew.btnUpdate.Enabled = false;
            frmNew.Text = "Rev 5 - Add New Part";
            frmNew.lbMsgText.Text = "Select Unit Type to enable Part Number field.";

            frmNew.ShowDialog();

            if (mQty != "-1")
            {
                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {

            int curSelRowIdx = 0;

            bool rfgComp = false;
            bool circuit1 = false;
            bool circuit2 = false;

            //foreach (DataGridViewRow dgr in dgvPartsMain.Rows)
            //{
            //    if (dgr.Selected == true)
            //    {
            //        break;
            //    }
            //    ++curSelRowIdx;
            //}

            int rowIdx = dgvPartsMain.CurrentRow.Index;
            int iDigit = 0;

            frmUpdate frmUpd = new frmUpdate(this);
            //DataTable dt = objPart.GetPartCategoryList();

            mRuleHeadID = dgvPartsMain.Rows[rowIdx].Cells["RuleHeadID"].Value.ToString();
            mRuleBatchID = dgvPartsMain.Rows[rowIdx].Cells["RuleBatchID"].Value.ToString();
            mPartNumber = dgvPartsMain.Rows[rowIdx].Cells["PartNum"].Value.ToString();
            mPartDesc = dgvPartsMain.Rows[rowIdx].Cells["PartDescription"].Value.ToString();
            mRelatedOp = dgvPartsMain.Rows[rowIdx].Cells["RelatedOperation"].Value.ToString();
            mPartCategory = dgvPartsMain.Rows[rowIdx].Cells["PartCategory"].Value.ToString();
            mUnitType = dgvPartsMain.Rows[rowIdx].Cells["Unit"].Value.ToString();
            mDigit = dgvPartsMain.Rows[rowIdx].Cells["Digit"].Value.ToString();
            mHeatType = dgvPartsMain.Rows[rowIdx].Cells["HeatType"].Value.ToString();
            mValueStr = dgvPartsMain.Rows[rowIdx].Cells["Value"].Value.ToString();
            mQty = dgvPartsMain.Rows[rowIdx].Cells["Qty"].Value.ToString();
            rfgComp = (bool)dgvPartsMain.Rows[rowIdx].Cells["RfgComponent"].Value;
            circuit1 = (bool)dgvPartsMain.Rows[rowIdx].Cells["Circuit1"].Value;
            circuit2 = (bool)dgvPartsMain.Rows[rowIdx].Cells["Circuit2"].Value;

            objPart.PartNum = mPartNumber;
            objPart.GetOAU_PartNumberInfo();

            objPart.PartCategory = null;
            DataTable dt = objPart.GetOAU_MainPartList();
            frmUpd.cbPartNum.DisplayMember = "PartNum";
            frmUpd.cbPartNum.ValueMember = "ID";

            DataRow dr = dt.NewRow();
            dr["ID"] = 0;
            dr["PartNum"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);
            frmUpd.cbPartNum.DataSource = dt;
            frmUpd.cbPartNum.SelectedIndex = 0;
            frmUpd.cbPartNum.Text = mPartNumber;
            frmUpd.cbPartNum.Enabled = true;

            if (mUnitType == "OAB")
            {
                frmUpd.rbOAB.Checked = true;
            }
            else if (mUnitType == "OAD")
            {
                frmUpd.rbOAD.Checked = true;
            }
            else if (mUnitType == "OAG")
            {
                frmUpd.rbOAG.Checked = true;
            }
            else if (mUnitType == "OAK")
            {
                frmUpd.rbOAK.Checked = true;
            }
            else if (mUnitType == "OAN")
            {
                frmUpd.rbOAN.Checked = true;
            }
            //frmUpd.cbUnitType.Text = mUnitType;
            frmUpd.txtRelatedOp.Text = mRelatedOp;
            frmUpd.txtPartDesc.Text = mPartDesc;

            frmUpd.txtRuleID.Text = mRuleHeadID;
            frmUpd.txtBatchID.Text = objPart.RuleBatchID.ToString();

            DataTable dt1 = objModel.GetModelNoDigitList();
            frmUpd.cbDigit.DisplayMember = "DigitHeat";
            frmUpd.cbDigit.ValueMember = "HeatType";

            DataRow dr1 = dt1.NewRow();
            dr1["DigitHeat"] = "Select Digit Number Please.";
            dr1["HeatType"] = "NA";
            dt1.Rows.InsertAt(dr1, 0);

            frmUpd.cbDigit.DataSource = dt1;
            frmUpd.cbDigit.SelectedIndex = 0;
            frmUpd.cbDigit.Enabled = false;

            int selIdx = objPart.getDigitSelectedIndex(mDigit, mHeatType, frmUpd.cbDigit);

            frmUpd.cbDigit.SelectedIndex = selIdx;
            iDigit = Int32.Parse(mDigit);
            frmUpd.gbValues.Enabled = true;
            frmUpd.gbValues.Visible = true;

            //objPart.GetOAU_PartNumberInfo();

            frmUpd.txtPartDesc.Text = mPartDesc;
            frmUpd.txtPartCategory.Text = mPartCategory;
            frmUpd.txtRuleID.Text = mRuleHeadID.ToString();
            frmUpd.txtBatchID.Text = "00";
            frmUpd.nudReqQty.Value = decimal.Parse(mQty);

            setupValueCheckBoxesCopyRule(frmUpd, iDigit, mValueStr, mHeatType, mUnitType);

            objPart.RuleHeadID = mRuleHeadID;
            objPart.PartNum = mPartNumber;
            objPart.RuleBatchID = mRuleBatchID;
            dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();
                        
            foreach (DataRow dr2 in dt.Rows)
            {
                int index = frmUpd.dgvUpdateRules.Rows.Add();

                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleHeadID"].Value = dr2["RuleHeadID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRuleBatchID"].Value = dr2["RuleBatchID"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartNumber"].Value = dr2["PartNum"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartDesc"].Value = dr2["PartDescription"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColRelatedOp"].Value = dr2["RelatedOperation"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColPartCat"].Value = dr2["PartCategory"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUnitType"].Value = dr2["Unit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColDigit"].Value = dr2["Digit"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColHeatType"].Value = dr2["HeatType"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColValue"].Value = dr2["Value"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColQty"].Value = dr2["Qty"].ToString();
                frmUpd.dgvUpdateRules.Rows[index].Cells["ColUpdateType"].Value = "Insert";

                if (mValueStr == dr2["Value"].ToString())
                {
                    frmUpd.lbSelectedRow.Text = index.ToString();
                }
            }

            if (rfgComp == true)
            {
                frmUpd.chkBoxRfgComp.Checked = true;
                frmUpd.gbCircuits.Visible = true;
                if (circuit1 == true)
                {
                    frmUpd.chkBoxCircuit1.Checked = true;
                }
                if (circuit2 == true)
                {
                    frmUpd.chkBoxCircuit2.Checked = true;
                }
            }

            //string unitType = frmUpd.dgvUpdateRules.Rows[0].Cells["ColUnitType"].Value.ToString();

            //if (unitType == "OAB")
            //{
            //    frmUpd.rbOAB.Checked = true;
            //}
            //else if (unitType == "OAD")
            //{
            //    frmUpd.rbOAD.Checked = true;
            //}
            //else if (unitType == "OAG")
            //{
            //    frmUpd.rbOAG.Checked = true;
            //}
            //else if (unitType == "OAK")
            //{
            //    frmUpd.rbOAK.Checked = true;
            //}
            //else if (unitType == "OAN")
            //{
            //    frmUpd.rbOAN.Checked = true;
            //}

            this.btnMainExit.Enabled = false;
            frmUpd.lbScreenMode.Text = "CopyMode";
            frmUpd.btnUpdate.Enabled = true;
            frmUpd.btnAllRuleSets.Enabled = true;
            frmUpd.btnSave.Enabled = false;

            //frmUpd.cbPartNum.Enabled = false;
            frmUpd.cbDigit.Enabled = false;
            frmUpd.nudReqQty.Enabled = false;

            frmUpd.Text = "Copy Rule Set";
            frmUpd.lbMsgText.Text = "Select UnitType to activate other fields.";
            frmUpd.ShowDialog();

            btnMainExit.Enabled = true;

            if (mQty != "-1")
            {
                if (mRuleBatchID != null)
                {
                    positionDataGrid(mPartNumber, Int32.Parse(mRuleBatchID)); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
                else
                {
                    positionDataGrid(mPartNumber, null); // mPartNumber & mRuleBatchID were set in the frmAddRule.cs SaveClick event.
                }
            }
        }

        private void btnCreateSSRules_Click(object sender, EventArgs e)
        {
            frmCreateSS_Rules frmCrt = new frmCreateSS_Rules();
            DataTable dtPart = objPart.GetVMEASM_PartsWithRules();
            frmCrt.dgvVmeParts.DataSource = dtPart;
            frmCrt.dgvVmeParts.Columns["PartNum"].Width = 135;           // Part Number
            frmCrt.dgvVmeParts.Columns["PartNum"].HeaderText = "Part Number";
            frmCrt.dgvVmeParts.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmCrt.dgvVmeParts.Columns["PartNum"].DisplayIndex = 0;
            frmCrt.dgvVmeParts.Columns["PartDescription"].Width = 400;           // Part Description
            frmCrt.dgvVmeParts.Columns["PartDescription"].HeaderText = "Part Description";
            frmCrt.dgvVmeParts.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            frmCrt.dgvVmeParts.Columns["PartDescription"].DisplayIndex = 1;
            frmCrt.ShowDialog();

            //Load PartConditionsDTL DataGridView
            DataTable dt = objPart.GetOAU_PartsAndRulesList();
            populateMainPartsList(dt);
        }

        #endregion

        #region Other Methods

        private void setupValueCheckBoxesCopyRule(frmUpdate frmAdd, int digit, string actValueStr, string heatType, string unitType)
        {
            int idx = 0;
            string valueStr = "";
            string oauTypeCode = "";

            if (unitType == "OAB" || unitType == "OAG" || unitType == "OAL")
            {
                oauTypeCode = "OALBG";
            }
            else if (unitType == "OAD" || unitType == "OAK" || unitType == "OAN")
            {
                oauTypeCode = "OAU123DKN";
            }

            DataTable dt = objModel.GetOAU_ModelNoValues(digit, heatType, null, oauTypeCode);

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmAdd.lbVal1);
            labelControls.Add(frmAdd.lbVal2);
            labelControls.Add(frmAdd.lbVal3);
            labelControls.Add(frmAdd.lbVal4);
            labelControls.Add(frmAdd.lbVal5);
            labelControls.Add(frmAdd.lbVal6);
            labelControls.Add(frmAdd.lbVal7);
            labelControls.Add(frmAdd.lbVal8);
            labelControls.Add(frmAdd.lbVal9);
            labelControls.Add(frmAdd.lbVal10);
            labelControls.Add(frmAdd.lbVal11);
            labelControls.Add(frmAdd.lbVal12);
            labelControls.Add(frmAdd.lbVal13);
            labelControls.Add(frmAdd.lbVal14);
            labelControls.Add(frmAdd.lbVal15);
            labelControls.Add(frmAdd.lbVal16);
            labelControls.Add(frmAdd.lbVal17);
            labelControls.Add(frmAdd.lbVal18);
            labelControls.Add(frmAdd.lbVal19);
            labelControls.Add(frmAdd.lbVal20);
            labelControls.Add(frmAdd.lbVal21);
            labelControls.Add(frmAdd.lbVal22);
            labelControls.Add(frmAdd.lbVal23);
            labelControls.Add(frmAdd.lbVal24);
            labelControls.Add(frmAdd.lbVal25);
            labelControls.Add(frmAdd.lbVal26);
            labelControls.Add(frmAdd.lbVal27);
            labelControls.Add(frmAdd.lbVal28);
            labelControls.Add(frmAdd.lbVal29);
            labelControls.Add(frmAdd.lbVal30);

            checkBoxControls.Add(frmAdd.cbVal1);
            checkBoxControls.Add(frmAdd.cbVal2);
            checkBoxControls.Add(frmAdd.cbVal3);
            checkBoxControls.Add(frmAdd.cbVal4);
            checkBoxControls.Add(frmAdd.cbVal5);
            checkBoxControls.Add(frmAdd.cbVal6);
            checkBoxControls.Add(frmAdd.cbVal7);
            checkBoxControls.Add(frmAdd.cbVal8);
            checkBoxControls.Add(frmAdd.cbVal9);
            checkBoxControls.Add(frmAdd.cbVal10);
            checkBoxControls.Add(frmAdd.cbVal11);
            checkBoxControls.Add(frmAdd.cbVal12);
            checkBoxControls.Add(frmAdd.cbVal13);
            checkBoxControls.Add(frmAdd.cbVal14);
            checkBoxControls.Add(frmAdd.cbVal15);
            checkBoxControls.Add(frmAdd.cbVal16);
            checkBoxControls.Add(frmAdd.cbVal17);
            checkBoxControls.Add(frmAdd.cbVal18);
            checkBoxControls.Add(frmAdd.cbVal19);
            checkBoxControls.Add(frmAdd.cbVal20);
            checkBoxControls.Add(frmAdd.cbVal21);
            checkBoxControls.Add(frmAdd.cbVal22);
            checkBoxControls.Add(frmAdd.cbVal23);
            checkBoxControls.Add(frmAdd.cbVal24);
            checkBoxControls.Add(frmAdd.cbVal25);
            checkBoxControls.Add(frmAdd.cbVal26);
            checkBoxControls.Add(frmAdd.cbVal27);
            checkBoxControls.Add(frmAdd.cbVal28);
            checkBoxControls.Add(frmAdd.cbVal29);
            checkBoxControls.Add(frmAdd.cbVal30);

            foreach (DataRow dr in dt.Rows)
            {
                valueStr = dr["DigitVal"].ToString();
                labelControls[idx].Text = valueStr;
                labelControls[idx].Visible = true;

                if (actValueStr.Contains(valueStr))
                {
                    checkBoxControls[idx].Checked = true;
                }
                checkBoxControls[idx].Enabled = true;
                checkBoxControls[idx++].Visible = true;
            }

        }       

        private void setupValueCheckBoxes(frmUpdate frmUpd, int digit, string actValueStr, string unitType)
        {
            int idx = 0;
            string valueStr = "";
            string oauTypeCode = "ALL";;

            if (unitType == "OAB" || unitType == "OAG" || unitType == "OAL")
            {
                oauTypeCode = "OALBG";
            }
            else if (unitType == "OAD" || unitType == "OAK" || unitType == "OAN")
            {
                oauTypeCode = "OAU123DKN";
            }

            DataTable dt = objModel.GetOAU_ModelNoValues(digit, "NA", null, oauTypeCode);

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmUpd.lbVal1);
            labelControls.Add(frmUpd.lbVal2);
            labelControls.Add(frmUpd.lbVal3);
            labelControls.Add(frmUpd.lbVal4);
            labelControls.Add(frmUpd.lbVal5);
            labelControls.Add(frmUpd.lbVal6);
            labelControls.Add(frmUpd.lbVal7);
            labelControls.Add(frmUpd.lbVal8);
            labelControls.Add(frmUpd.lbVal9);
            labelControls.Add(frmUpd.lbVal10);
            labelControls.Add(frmUpd.lbVal11);
            labelControls.Add(frmUpd.lbVal12);
            labelControls.Add(frmUpd.lbVal13);
            labelControls.Add(frmUpd.lbVal14);
            labelControls.Add(frmUpd.lbVal15);
            labelControls.Add(frmUpd.lbVal16);
            labelControls.Add(frmUpd.lbVal17);
            labelControls.Add(frmUpd.lbVal18);
            labelControls.Add(frmUpd.lbVal19);
            labelControls.Add(frmUpd.lbVal20);
            labelControls.Add(frmUpd.lbVal21);
            labelControls.Add(frmUpd.lbVal22);
            labelControls.Add(frmUpd.lbVal23);
            labelControls.Add(frmUpd.lbVal24);
            labelControls.Add(frmUpd.lbVal25);
            labelControls.Add(frmUpd.lbVal26);
            labelControls.Add(frmUpd.lbVal27);
            labelControls.Add(frmUpd.lbVal28);
            labelControls.Add(frmUpd.lbVal29);
            labelControls.Add(frmUpd.lbVal30);

            checkBoxControls.Add(frmUpd.cbVal1);
            checkBoxControls.Add(frmUpd.cbVal2);
            checkBoxControls.Add(frmUpd.cbVal3);
            checkBoxControls.Add(frmUpd.cbVal4);
            checkBoxControls.Add(frmUpd.cbVal5);
            checkBoxControls.Add(frmUpd.cbVal6);
            checkBoxControls.Add(frmUpd.cbVal7);
            checkBoxControls.Add(frmUpd.cbVal8);
            checkBoxControls.Add(frmUpd.cbVal9);
            checkBoxControls.Add(frmUpd.cbVal10);
            checkBoxControls.Add(frmUpd.cbVal11);
            checkBoxControls.Add(frmUpd.cbVal12);
            checkBoxControls.Add(frmUpd.cbVal13);
            checkBoxControls.Add(frmUpd.cbVal14);
            checkBoxControls.Add(frmUpd.cbVal15);
            checkBoxControls.Add(frmUpd.cbVal16);
            checkBoxControls.Add(frmUpd.cbVal17);
            checkBoxControls.Add(frmUpd.cbVal18);
            checkBoxControls.Add(frmUpd.cbVal19);
            checkBoxControls.Add(frmUpd.cbVal20);
            checkBoxControls.Add(frmUpd.cbVal21);
            checkBoxControls.Add(frmUpd.cbVal22);
            checkBoxControls.Add(frmUpd.cbVal23);
            checkBoxControls.Add(frmUpd.cbVal24);
            checkBoxControls.Add(frmUpd.cbVal25);
            checkBoxControls.Add(frmUpd.cbVal26);
            checkBoxControls.Add(frmUpd.cbVal27);
            checkBoxControls.Add(frmUpd.cbVal28);
            checkBoxControls.Add(frmUpd.cbVal29);
            checkBoxControls.Add(frmUpd.cbVal30);

            foreach (DataRow dr in dt.Rows)
            {
                valueStr = dr["DigitVal"].ToString();
                labelControls[idx].Text = valueStr;
                labelControls[idx].Visible = true;

                if (actValueStr.Contains(valueStr))
                {
                    checkBoxControls[idx].Checked = true;
                }
                checkBoxControls[idx].Visible = true;
                frmUpd.toolTip1.SetToolTip(labelControls[idx], dr["DigitDescription"].ToString());
                frmUpd.toolTip1.SetToolTip(checkBoxControls[idx++], dr["DigitDescription"].ToString());
                frmUpd.toolTip1.ShowAlways = true;
            }

        }       

        private void positionDataGrid(string partNum, int? ruleBatchID)
        {
            int rowIdx = 0;

            dgvPartsMain.DataSource = null;
            DataTable dtPRL = objPart.GetOAU_PartsAndRulesList();
            dgvPartsMain.DataSource = dtPRL;
            populateMainPartsList(dtPRL);

            if (ruleBatchID != null)
            {
                foreach (DataGridViewRow row in dgvPartsMain.Rows)
                {
                    if (row.Cells["PartNum"].Value.ToString() != "")
                    {
                        //if (row.Cells["PartNum"].Value.ToString() == partNum && Int32.Parse(row.Cells["RuleBatchID"].Value.ToString()) == ruleBatchID)
                        if (row.Cells["PartNum"].Value.ToString() == partNum)
                        {
                            rowIdx = row.Index;
                            dgvPartsMain.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                            dgvPartsMain.Rows[rowIdx].Selected = true;
                            dgvPartsMain.FirstDisplayedScrollingRowIndex = rowIdx;
                            dgvPartsMain.Focus();
                            break;
                        }
                    }
                }
            }
        }

        private void populatePartRuleHeadList(DataTable dt, bool firstPass)
        {

            dgvPartRulesHead.DataSource = dt;
                                
            dgvPartRulesHead.Columns["PartNum"].Width = 200;           // Part Number
            dgvPartRulesHead.Columns["PartNum"].HeaderText = "Part Number";
            dgvPartRulesHead.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartNum"].DisplayIndex = 0;
            dgvPartRulesHead.Columns["PartDescription"].Width = 400;           // Part Description
            dgvPartRulesHead.Columns["PartDescription"].HeaderText = "Part Description";
            dgvPartRulesHead.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartDescription"].DisplayIndex = 1;
            dgvPartRulesHead.Columns["RelatedOperation"].Width = 60;            // Related Operation
            dgvPartRulesHead.Columns["RelatedOperation"].HeaderText = "Related\nOperation";
            dgvPartRulesHead.Columns["RelatedOperation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartRulesHead.Columns["RelatedOperation"].DisplayIndex = 2;
            dgvPartRulesHead.Columns["PartCategory"].Width = 125;           // PartCategory   
            dgvPartRulesHead.Columns["PartCategory"].HeaderText = "Part Category";
            dgvPartRulesHead.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartCategory"].DisplayIndex = 3;
            dgvPartRulesHead.Columns["AssemblySeq"].Width = 50;            // AssemblySeq
            dgvPartRulesHead.Columns["AssemblySeq"].HeaderText = "Asm\nSeq";
            dgvPartRulesHead.Columns["AssemblySeq"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartRulesHead.Columns["AssemblySeq"].DisplayIndex = 4;
            dgvPartRulesHead.Columns["PartType"].Width = 100;            // PartType
            dgvPartRulesHead.Columns["PartType"].HeaderText = "Part Type";
            dgvPartRulesHead.Columns["PartType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["PartType"].DisplayIndex = 5;
            dgvPartRulesHead.Columns["StationLocation"].Width = 80;            // StationLocation
            dgvPartRulesHead.Columns["StationLocation"].HeaderText = "Station Loc";
            dgvPartRulesHead.Columns["StationLocation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["StationLocation"].DisplayIndex = 6;
            dgvPartRulesHead.Columns["LinesideBin"].Width = 60;            // LinesideBin
            dgvPartRulesHead.Columns["LinesideBin"].HeaderText = "Lineside\nBin";
            dgvPartRulesHead.Columns["LinesideBin"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["LinesideBin"].DisplayIndex = 7;
            dgvPartRulesHead.Columns["Picklist"].Width = 55;            // Picklist
            dgvPartRulesHead.Columns["Picklist"].HeaderText = "Picklist";
            dgvPartRulesHead.Columns["Picklist"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["Picklist"].DisplayIndex = 8;
            dgvPartRulesHead.Columns["ConfigurablePart"].Width = 60;            // ConfigurablePart
            dgvPartRulesHead.Columns["ConfigurablePart"].HeaderText = "Config\nPart";
            dgvPartRulesHead.Columns["ConfigurablePart"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["ConfigurablePart"].DisplayIndex = 9;
            dgvPartRulesHead.Columns["UserName"].Width = 125;            // UserName
            dgvPartRulesHead.Columns["UserName"].HeaderText = "UserName";
            dgvPartRulesHead.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartRulesHead.Columns["UserName"].DisplayIndex = 10;
            dgvPartRulesHead.Columns["DateMod"].Width = 90;            // DateMod
            dgvPartRulesHead.Columns["DateMod"].HeaderText = "DateMod";
            dgvPartRulesHead.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartRulesHead.Columns["DateMod"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvPartRulesHead.Columns["DateMod"].DisplayIndex = 11;
            dgvPartRulesHead.Columns["ID"].Visible = false;            // ID
           
        }
       
        private void populateMainPartsList(DataTable dt)
        {

            dgvPartsMain.DataSource = dt;
            
            dgvPartsMain.Columns["RuleBatchID"].Width = 60;            // RuleBatchID
            dgvPartsMain.Columns["RuleBatchID"].HeaderText = "Rule\nBatch ID";
            dgvPartsMain.Columns["RuleBatchID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["RuleBatchID"].DisplayIndex = 0;
            dgvPartsMain.Columns["PartNum"].Width = 125;           // Part Number
            dgvPartsMain.Columns["PartNum"].HeaderText = "Part Number";
            dgvPartsMain.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["PartNum"].DisplayIndex = 1;
            dgvPartsMain.Columns["PartDescription"].Width = 225;           // Part Description
            dgvPartsMain.Columns["PartDescription"].HeaderText = "Part Description";
            dgvPartsMain.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["PartDescription"].DisplayIndex = 2;
            dgvPartsMain.Columns["RelatedOperation"].Width = 60;            // Related Operation
            dgvPartsMain.Columns["RelatedOperation"].HeaderText = "Related\nOperation";
            dgvPartsMain.Columns["RelatedOperation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["RelatedOperation"].DisplayIndex = 3;
            dgvPartsMain.Columns["PartCategory"].Width = 130;           // PartCategory   
            dgvPartsMain.Columns["PartCategory"].HeaderText = "Part Category";
            dgvPartsMain.Columns["PartCategory"].DisplayIndex = 4;
            dgvPartsMain.Columns["Unit"].Width = 50;            // Unit Type          
            dgvPartsMain.Columns["Unit"].HeaderText = "Unit\nType";
            dgvPartsMain.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["Unit"].DisplayIndex = 5;            
            dgvPartsMain.Columns["Digit"].Width = 60;            // Digit
            dgvPartsMain.Columns["Digit"].HeaderText = "Digit";
            dgvPartsMain.Columns["Digit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["Digit"].DisplayIndex = 6;
            dgvPartsMain.Columns["HeatType"].Width = 65;            // Heat Type  
            dgvPartsMain.Columns["HeatType"].HeaderText = "HeatType";
            dgvPartsMain.Columns["HeatType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["HeatType"].DisplayIndex = 7;
            dgvPartsMain.Columns["Value"].Width = 175;            // Value  
            dgvPartsMain.Columns["Value"].HeaderText = "Value";
            dgvPartsMain.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartsMain.Columns["Value"].DisplayIndex = 8;
            dgvPartsMain.Columns["Qty"].Width = 75;            // Qty
            dgvPartsMain.Columns["Qty"].HeaderText = "Quantity";
            dgvPartsMain.Columns["Qty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartsMain.Columns["Qty"].DisplayIndex = 9;
            dgvPartsMain.Columns["RfgComponent"].HeaderText = "Rfg Comp";
            dgvPartsMain.Columns["RfgComponent"].Width = 65;            // RfgComponent
            dgvPartsMain.Columns["RfgComponent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["RfgComponent"].DisplayIndex = 10;
            dgvPartsMain.Columns["Circuit1"].HeaderText = "Circuit1";
            dgvPartsMain.Columns["Circuit1"].Width = 65;            // Circuit1
            dgvPartsMain.Columns["Circuit1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["Circuit1"].DisplayIndex = 11;
            dgvPartsMain.Columns["Circuit2"].HeaderText = "Circuit2";
            dgvPartsMain.Columns["Circuit2"].Width = 65;            // Circuit2
            dgvPartsMain.Columns["Circuit2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["Circuit2"].DisplayIndex = 12;
            dgvPartsMain.Columns["SubAssemblyPart"].HeaderText = "SubAssemblyPart";                          
            dgvPartsMain.Columns["SubAssemblyPart"].Width = 65;            // SubAssemblyPart
            dgvPartsMain.Columns["SubAssemblyPart"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartsMain.Columns["SubAssemblyPart"].DisplayIndex = 13;
            dgvPartsMain.Columns["Comments"].Visible = false;
            dgvPartsMain.Columns["SubAsmMtlPart"].Visible = false;
            dgvPartsMain.Columns["ImmediateParent"].Visible = false;
            dgvPartsMain.Columns["RuleHeadID"].Visible = false;
            dgvPartsMain.Columns["DateMod"].Visible = false;
            dgvPartsMain.Columns["UserName"].Visible = false;            
            
        }

        private void populateModelNoList(DataTable dt, bool firstPass)
        {
            dgvModelNo.DataSource = dt;           
           
            dgvModelNo.Columns["DigitNo"].Width = 50;            // DigitNo
            dgvModelNo.Columns["DigitNo"].HeaderText = "Digit No";
            dgvModelNo.Columns["DigitNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvModelNo.Columns["DigitNo"].DisplayIndex = 0;
            dgvModelNo.Columns["DigitVal"].Width = 50;            // Digit Value
            dgvModelNo.Columns["DigitVal"].HeaderText = "Digit\nValue";
            dgvModelNo.Columns["DigitVal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitVal"].DisplayIndex = 1;
            dgvModelNo.Columns["DigitDescription"].Width = 250;           // Digit Description
            dgvModelNo.Columns["DigitDescription"].HeaderText = "Digit Description";
            dgvModelNo.Columns["DigitDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitDescription"].DisplayIndex = 2;
            dgvModelNo.Columns["HeatType"].Width = 75;           //Heat Type
            dgvModelNo.Columns["HeatType"].HeaderText = "Heat Type";
            dgvModelNo.Columns["HeatType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["HeatType"].DisplayIndex = 3;
            dgvModelNo.Columns["OAUTypeCode"].Width = 100;           //OAUTypeCode
            dgvModelNo.Columns["OAUTypeCode"].HeaderText = "OAUTypeCode";
            dgvModelNo.Columns["OAUTypeCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["OAUTypeCode"].DisplayIndex = 4;
            dgvModelNo.Columns["DigitRepresentation"].Width = 200;            //DigitRepresentation
            dgvModelNo.Columns["DigitRepresentation"].HeaderText = "Digit Representation";
            dgvModelNo.Columns["DigitRepresentation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvModelNo.Columns["DigitRepresentation"].DisplayIndex = 5;           
            dgvModelNo.Columns["ID"].Visible = false;
            dgvModelNo.Columns["DigitValDesc"].Visible = false;
            dgvModelNo.Columns["RowIndex"].Visible = false;    

        }

        //private void populateCompressorDetailList(DataTable dt, bool firstPass)
        //{
        //    dgvCompDetail.DataSource = dt;
            
        //    dgvCompDetail.Columns["RuleHeadID"].Width = 60;            // RuleHeadID
        //    dgvCompDetail.Columns["RuleHeadID"].HeaderText = "Rule\nHead ID";
        //    dgvCompDetail.Columns["RuleHeadID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompDetail.Columns["RuleHeadID"].DisplayIndex = 0;
        //    dgvCompDetail.Columns["PartNum"].Width = 150;            // Part Number
        //    dgvCompDetail.Columns["PartNum"].HeaderText = "Part Number";
        //    dgvCompDetail.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompDetail.Columns["PartNum"].DisplayIndex = 1;
        //    dgvCompDetail.Columns["PartDescription"].Width = 300;            // Part Description
        //    dgvCompDetail.Columns["PartDescription"].HeaderText = "Part Description";
        //    dgvCompDetail.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;            
        //    dgvCompDetail.Columns["PartDescription"].DisplayIndex = 2;
        //    dgvCompDetail.Columns["PartCategory"].Width = 125;           // PartCategory
        //    dgvCompDetail.Columns["PartCategory"].HeaderText = "Part Category";
        //    dgvCompDetail.Columns["PartCategory"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompDetail.Columns["PartCategory"].DisplayIndex = 3;
        //    dgvCompDetail.Columns["Voltage"].Width = 60;           // Voltage
        //    dgvCompDetail.Columns["Voltage"].HeaderText = "Voltage";
        //    dgvCompDetail.Columns["Voltage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvCompDetail.Columns["Voltage"].DisplayIndex = 4;
        //    dgvCompDetail.Columns["Phase"].Width = 50;           //Phase
        //    dgvCompDetail.Columns["Phase"].HeaderText = "Phase";            
        //    dgvCompDetail.Columns["Phase"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvCompDetail.Columns["Phase"].DisplayIndex = 5;
        //    dgvCompDetail.Columns["RLA"].Width = 60;           // RLA
        //    dgvCompDetail.Columns["RLA"].HeaderText = "RLA";
        //    dgvCompDetail.Columns["RLA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompDetail.Columns["RLA"].DisplayIndex = 6;
        //    dgvCompDetail.Columns["LRA"].Width = 60;           // LRA
        //    dgvCompDetail.Columns["LRA"].HeaderText = "LRA";
        //    dgvCompDetail.Columns["LRA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompDetail.Columns["LRA"].DisplayIndex = 7;
        //    dgvCompDetail.Columns["UserName"].Width = 100;            //UserName
        //    dgvCompDetail.Columns["UserName"].HeaderText = "UserName";
        //    dgvCompDetail.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompDetail.Columns["UserName"].DisplayIndex = 8;
        //    dgvCompDetail.Columns["DateMod"].Width = 150;           // Date Modified  
        //    dgvCompDetail.Columns["DateMod"].HeaderText = "Date Modified";
        //    dgvCompDetail.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompDetail.Columns["DateMod"].DisplayIndex = 9;           
        //    dgvCompDetail.Columns["ID"].Visible = false;
                   
        //}

        //private void populateCompressorChargeList(DataTable dt, bool firstPass)
        //{

        //    dgvCompCircuitData.DataSource = dt;


        //    //if (firstPass)
        //    //{
        //    //    DataGridViewButtonColumn AddButtonCell = new DataGridViewButtonColumn();
        //    //    AddButtonCell.Name = "Add";
        //    //    AddButtonCell.HeaderText = "Add";
        //    //    AddButtonCell.Text = "Add";
        //    //    AddButtonCell.UseColumnTextForButtonValue = true;
        //    //    dgvCompChargeData.Columns.Insert(0, AddButtonCell);
        //    //}

        //    //dgvCompChargeData.Columns["Add"].Width = 40;            // Add Button
        //    //dgvCompChargeData.Columns["Add"].DisplayIndex = 0;
        //    dgvCompCircuitData.Columns["UnitModel"].Width = 60;            // Unit Model
        //    dgvCompCircuitData.Columns["UnitModel"].HeaderText = "Unit Model";
        //    dgvCompCircuitData.Columns["UnitModel"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompCircuitData.Columns["UnitModel"].DisplayIndex = 0;
        //    dgvCompCircuitData.Columns["CoolingCap"].Width = 75;           // Cooling Cap
        //    dgvCompCircuitData.Columns["CoolingCap"].HeaderText = "Cooling\nCapacity";
        //    dgvCompCircuitData.Columns["CoolingCap"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvCompCircuitData.Columns["CoolingCap"].DisplayIndex = 1;
        //    dgvCompCircuitData.Columns["HeatType"].Width = 75;           // heat Type
        //    dgvCompCircuitData.Columns["HeatType"].HeaderText = "Heat Type";
        //    dgvCompCircuitData.Columns["HeatType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompCircuitData.Columns["HeatType"].DisplayIndex = 2;
        //    dgvCompCircuitData.Columns["Circuit1"].Width = 75;            // Circuit 1 Charge
        //    dgvCompCircuitData.Columns["Circuit1"].HeaderText = "Circuit 1";
        //    dgvCompCircuitData.Columns["Circuit1"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompCircuitData.Columns["Circuit1"].DisplayIndex = 3;
        //    dgvCompCircuitData.Columns["Circuit1_Reheat"].Width = 75;           // Circuit 1 Charge Reheat 
        //    dgvCompCircuitData.Columns["Circuit1_Reheat"].HeaderText = "Circuit1\nReheat";
        //    dgvCompCircuitData.Columns["Circuit1_Reheat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompCircuitData.Columns["Circuit1_Reheat"].DisplayIndex = 4;
        //    dgvCompCircuitData.Columns["Circuit2"].Width = 75;            // Circuit 2          
        //    dgvCompCircuitData.Columns["Circuit2"].HeaderText = "Circuit 2";
        //    dgvCompCircuitData.Columns["Circuit2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompCircuitData.Columns["Circuit2"].DisplayIndex = 5;
        //    dgvCompCircuitData.Columns["Circuit2_Reheat"].Width = 75;           // Circuit 2 Charge Reheat 
        //    dgvCompCircuitData.Columns["Circuit2_Reheat"].HeaderText = "Circuit2\nReheat";
        //    dgvCompCircuitData.Columns["Circuit2_Reheat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompCircuitData.Columns["Circuit2_Reheat"].DisplayIndex = 6;
        //    dgvCompCircuitData.Columns["ModifiedBy"].Width = 150;            // Modified By
        //    dgvCompCircuitData.Columns["ModifiedBy"].HeaderText = "Modified By";
        //    dgvCompCircuitData.Columns["ModifiedBy"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompCircuitData.Columns["ModifiedBy"].DisplayIndex = 7;
        //    dgvCompCircuitData.Columns["DateMod"].Width = 150;            // Modified By
        //    dgvCompCircuitData.Columns["DateMod"].HeaderText = "Date Modified";
        //    dgvCompCircuitData.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvCompCircuitData.Columns["DateMod"].DisplayIndex = 8;
        //    dgvCompCircuitData.Columns["ID"].Width = 60;            // ID
        //    dgvCompCircuitData.Columns["ID"].HeaderText = "ID";
        //    dgvCompCircuitData.Columns["ID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvCompCircuitData.Columns["ID"].DisplayIndex = 9;
        //    dgvCompCircuitData.Columns["ID"].Visible = false;

        //    if (firstPass)
        //    {
        //        DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
        //        DelButtonCell.Name = "Del";
        //        DelButtonCell.HeaderText = "Del";
        //        DelButtonCell.Text = "Del";
        //        DelButtonCell.UseColumnTextForButtonValue = true;
        //        dgvCompCircuitData.Columns.Insert(10, DelButtonCell);
        //        dgvCompCircuitData.Columns["Del"].Width = 40;            // Delete Button
        //        dgvCompCircuitData.Columns["Del"].DisplayIndex = 10;
        //    }
        //}

        //private void populateMotorDataList(DataTable dt, bool firstPass)
        //{
        //    dgvMotorData.DataSource = dt;
           
        //    dgvMotorData.Columns["RuleHeadID"].Width = 60;            // RuleHeadID
        //    dgvMotorData.Columns["RuleHeadID"].HeaderText = "Rule\nHead ID";
        //    dgvMotorData.Columns["RuleHeadID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvMotorData.Columns["RuleHeadID"].DisplayIndex = 0;
        //    dgvMotorData.Columns["PartNum"].Width = 100;            // Part Number
        //    dgvMotorData.Columns["PartNum"].HeaderText = "Part Number";
        //    dgvMotorData.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvMotorData.Columns["PartNum"].DisplayIndex = 1;
        //    dgvMotorData.Columns["PartDescription"].Width = 250;            // Part Description
        //    dgvMotorData.Columns["PartDescription"].HeaderText = "Part Description";
        //    dgvMotorData.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvMotorData.Columns["PartDescription"].DisplayIndex = 2;
        //    dgvMotorData.Columns["MotorType"].Width = 80;           // MotorType
        //    dgvMotorData.Columns["MotorType"].HeaderText = "Motor Type";
        //    dgvMotorData.Columns["MotorType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvMotorData.Columns["MotorType"].DisplayIndex = 3;
        //    dgvMotorData.Columns["FLA"].Width = 60;           // FLA
        //    dgvMotorData.Columns["FLA"].HeaderText = "FLA";
        //    dgvMotorData.Columns["FLA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvMotorData.Columns["FLA"].DisplayIndex = 4;
        //    dgvMotorData.Columns["Hertz"].Width = 50;           // Hertz
        //    dgvMotorData.Columns["Hertz"].HeaderText = "Hertz";
        //    dgvMotorData.Columns["Hertz"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvMotorData.Columns["Hertz"].DisplayIndex = 5;
        //    dgvMotorData.Columns["Phase"].Width = 50;           //Phase
        //    dgvMotorData.Columns["Phase"].HeaderText = "Phase";
        //    dgvMotorData.Columns["Phase"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvMotorData.Columns["Phase"].DisplayIndex = 6;
        //    dgvMotorData.Columns["MCC"].Width = 40;           // MCC
        //    dgvMotorData.Columns["MCC"].HeaderText = "MCC";
        //    dgvMotorData.Columns["MCC"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvMotorData.Columns["MCC"].DisplayIndex = 7;
        //    dgvMotorData.Columns["RLA"].Width = 40;           // RLA
        //    dgvMotorData.Columns["RLA"].HeaderText = "RLA";
        //    dgvMotorData.Columns["RLA"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvMotorData.Columns["RLA"].DisplayIndex = 8;
        //    dgvMotorData.Columns["Voltage"].Width = 90;            //Voltage
        //    dgvMotorData.Columns["Voltage"].HeaderText = "Voltage";
        //    dgvMotorData.Columns["Voltage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvMotorData.Columns["Voltage"].DisplayIndex = 9;
        //    dgvMotorData.Columns["HP"].Width = 60;           // HP
        //    dgvMotorData.Columns["HP"].HeaderText = "RLA";
        //    dgvMotorData.Columns["HP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvMotorData.Columns["HP"].DisplayIndex = 10;
        //    dgvMotorData.Columns["UserName"].Width = 100;            //UserName
        //    dgvMotorData.Columns["UserName"].HeaderText = "UserName";
        //    dgvMotorData.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvMotorData.Columns["UserName"].DisplayIndex = 11;
        //    dgvMotorData.Columns["DateMod"].Width = 150;           // Date Modified  
        //    dgvMotorData.Columns["DateMod"].HeaderText = "Date Modified";
        //    dgvMotorData.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvMotorData.Columns["DateMod"].DisplayIndex = 12;
        //    dgvMotorData.Columns["ID"].Visible = false;

        //    //if (firstPass)
        //    //{
        //    //    DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
        //    //    DelButtonCell.Name = "Del";
        //    //    DelButtonCell.HeaderText = "Del";
        //    //    DelButtonCell.Text = "Del";
        //    //    DelButtonCell.UseColumnTextForButtonValue = true;
        //    //    dgvMotorData.Columns.Insert(14, DelButtonCell);
        //    //    dgvMotorData.Columns["Del"].Width = 50;            // Delete Button
        //    //    dgvMotorData.Columns["Del"].DisplayIndex = 14;
        //    //}
        //}

        //private void populateFurnaceDataList(DataTable dt, bool firstPass)
        //{
        //    dgvFurnaceData.DataSource = dt;

        //    dgvFurnaceData.Columns["RuleHeadID"].Width = 60;            // RuleHeadID
        //    dgvFurnaceData.Columns["RuleHeadID"].HeaderText = "Rule\nHead ID";
        //    dgvFurnaceData.Columns["RuleHeadID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvFurnaceData.Columns["RuleHeadID"].DisplayIndex = 0;
        //    dgvFurnaceData.Columns["PartNum"].Width = 100;            // Part Number
        //    dgvFurnaceData.Columns["PartNum"].HeaderText = "Part Number";
        //    dgvFurnaceData.Columns["PartNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvFurnaceData.Columns["PartNum"].DisplayIndex = 1;
        //    dgvFurnaceData.Columns["PartDescription"].Width = 250;            // Part Description
        //    dgvFurnaceData.Columns["PartDescription"].HeaderText = "Part Description";
        //    dgvFurnaceData.Columns["PartDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvFurnaceData.Columns["PartDescription"].DisplayIndex = 2;
        //    dgvFurnaceData.Columns["Amps"].Width = 60;           // Amps
        //    dgvFurnaceData.Columns["Amps"].HeaderText = "Amps";
        //    dgvFurnaceData.Columns["Amps"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvFurnaceData.Columns["Amps"].DisplayIndex = 3;
        //    dgvFurnaceData.Columns["Volts"].Width = 70;           // Volts
        //    dgvFurnaceData.Columns["Volts"].HeaderText = "Volts";
        //    dgvFurnaceData.Columns["Volts"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        //    dgvFurnaceData.Columns["Volts"].DisplayIndex = 4;
        //    dgvFurnaceData.Columns["BurnerRatio"].Width = 75;           // BurnerRation
        //    dgvFurnaceData.Columns["BurnerRatio"].HeaderText = "Burner Ratio";
        //    dgvFurnaceData.Columns["BurnerRatio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvFurnaceData.Columns["BurnerRatio"].DisplayIndex = 5;
        //    dgvFurnaceData.Columns["Phase"].Width = 50;           //Phase
        //    dgvFurnaceData.Columns["Phase"].HeaderText = "Phase";
        //    dgvFurnaceData.Columns["Phase"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
        //    dgvFurnaceData.Columns["Phase"].DisplayIndex = 6;
        //    dgvFurnaceData.Columns["FuelType"].Width = 80;           // FuelType
        //    dgvFurnaceData.Columns["FuelType"].HeaderText = "Fuel Type";
        //    dgvFurnaceData.Columns["FuelType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvFurnaceData.Columns["FuelType"].DisplayIndex = 7;
        //    dgvFurnaceData.Columns["MBHkW"].Width = 75;           // MBHkW
        //    dgvFurnaceData.Columns["MBHkW"].HeaderText = "MBH/KW";
        //    dgvFurnaceData.Columns["MBHkW"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvFurnaceData.Columns["MBHkW"].DisplayIndex = 8;
        //    dgvFurnaceData.Columns["UserName"].Width = 100;            //UserName
        //    dgvFurnaceData.Columns["UserName"].HeaderText = "UserName";
        //    dgvFurnaceData.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvFurnaceData.Columns["UserName"].DisplayIndex = 9;
        //    dgvFurnaceData.Columns["DateMod"].Width = 150;           // Date Modified  
        //    dgvFurnaceData.Columns["DateMod"].HeaderText = "Date Modified";
        //    dgvFurnaceData.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        //    dgvFurnaceData.Columns["DateMod"].DisplayIndex = 10;
        //    dgvFurnaceData.Columns["ID"].Visible = false;

        //    //if (firstPass)
        //    //{
        //    //    DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
        //    //    DelButtonCell.Name = "Del";
        //    //    DelButtonCell.HeaderText = "Del";
        //    //    DelButtonCell.Text = "Del";
        //    //    DelButtonCell.UseColumnTextForButtonValue = true;
        //    //    dgvFurnaceData.Columns.Insert(12, DelButtonCell);
        //    //    dgvFurnaceData.Columns["Del"].Width = 50;            // Delete Button
        //    //    dgvFurnaceData.Columns["Del"].DisplayIndex = 12;
        //    //}
        //}

        private void populatePartCategoryDataList(DataTable dt, bool firstPass)
        {
            dgvPartCat.DataSource = dt;

            dgvPartCat.Columns["CategoryName"].Width = 150;            // CategoryName
            dgvPartCat.Columns["CategoryName"].HeaderText = "Category Name";
            dgvPartCat.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["CategoryName"].DisplayIndex = 0;
            dgvPartCat.Columns["CategoryDesc"].Width = 200;            // CategoryDesc
            dgvPartCat.Columns["CategoryDesc"].HeaderText = "Category Desc";
            dgvPartCat.Columns["CategoryDesc"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["CategoryDesc"].DisplayIndex = 1;
            dgvPartCat.Columns["PullOrder"].Width = 45;            // Pull Order
            dgvPartCat.Columns["PullOrder"].HeaderText = "Pull\nOrder";
            dgvPartCat.Columns["PullOrder"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPartCat.Columns["PullOrder"].DisplayIndex = 2;
            dgvPartCat.Columns["CriticalComp"].Width = 70;            //CriticalComponent
            dgvPartCat.Columns["CriticalComp"].HeaderText = "Critical\nComponent";
            dgvPartCat.Columns["CriticalComp"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvPartCat.Columns["CriticalComp"].DisplayIndex = 3;
            dgvPartCat.Columns["UserName"].Width = 100;            //UserName
            dgvPartCat.Columns["UserName"].HeaderText = "UserName";
            dgvPartCat.Columns["UserName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["UserName"].DisplayIndex = 4;
            dgvPartCat.Columns["DateMod"].Width = 150;           // Date Modified  
            dgvPartCat.Columns["DateMod"].HeaderText = "Date Modified";
            dgvPartCat.Columns["DateMod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvPartCat.Columns["DateMod"].DisplayIndex = 5;                    
            dgvPartCat.Columns["ID"].Visible = false;

            if (firstPass)
            {
                DataGridViewButtonColumn DelButtonCell = new DataGridViewButtonColumn();
                DelButtonCell.Name = "Del";
                DelButtonCell.HeaderText = "Del";
                DelButtonCell.Text = "Del";
                DelButtonCell.UseColumnTextForButtonValue = true;
                dgvPartCat.Columns.Insert(6, DelButtonCell);
                dgvPartCat.Columns["Del"].Width = 50;            // Delete Button
                dgvPartCat.Columns["Del"].DisplayIndex = 6;
            }
        }

        private string convertKeyValue(int keyValue)
        {
            // This function converts the integer KeyValue passed into it to its string equivalent.

            if ((keyValue == 48) || (keyValue == 96))
            {
                return "0";
            }
            else if ((keyValue == 49) || (keyValue == 97))
            {
                return "1";
            }
            else if ((keyValue == 50) || (keyValue == 98))
            {
                return "2";
            }
            else if ((keyValue == 51) || (keyValue == 99))
            {
                return "3";
            }
            else if ((keyValue == 52) || (keyValue == 100))
            {
                return "4";
            }
            else if ((keyValue == 53) || (keyValue == 101))
            {
                return "5";
            }
            else if ((keyValue == 54) || (keyValue == 102))
            {
                return "6";
            }
            else if ((keyValue == 55) || (keyValue == 103))
            {
                return "7";
            }
            else if ((keyValue == 56) || (keyValue == 104))
            {
                return "8";
            }
            else if ((keyValue == 57) || (keyValue == 105))
            {
                return "9";
            }

            return "";
        }

        private void txtSearchPartNum_KeyUp(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtSearchPartNum.Text.ToUpper();
                if (tmpStr.Length > 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow row in dgvPartsMain.Rows)
                    {
                        if (row.Cells["PartNum"].Value != null)
                        {
                            if (row.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || row.Cells["PartNum"].Value.ToString() == tmpStr)
                            {
                                rowIdx = row.Index;
                                dgvPartsMain.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvPartsMain.Rows[rowIdx].Selected = true;
                                dgvPartsMain.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvPartsMain.Focus();
                                break;
                            }
                        }
                    }
                }
                this.txtSearchPartNum.Focus();
            }
        }

        private void modelNoHeatTypeChanged(string digitNoStr, string heatType)
        {
            string unitType = "ALL";
            int digitNo = -1;

            if (txtSearchModelNo.Text != "")
            {
                try
                {
                    digitNo = Int32.Parse(txtSearchModelNo.Text);
                    if (digitNo < 0)
                    {
                        MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                    }
                    else if (digitNo > 59)
                    {
                        if (digitNo != 567 || digitNo != 2324 || digitNo != 2728)
                        {
                            MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                }
            }
           
            dgvModelNo.DataSource = null;

            curSearchDigitNo = digitNo;
            curHeatType = heatType;
            curOAUTypeCode = unitType;

            DataTable dt = objModel.GetOAU_ModelNoValues(digitNo, heatType, null, mOAUTypeCode);
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);
        }

        private void modelNoUnitTypeChanged(string digitNoStr, string unitType)
        {
            string heatType = "ALL";
            int digitNo = -1;

            if (txtSearchModelNo.Text != "")
            {
                try
                {
                    digitNo = Int32.Parse(txtSearchModelNo.Text);
                    if (digitNo < 0)
                    {
                        MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                    }
                    else if (digitNo > 59)
                    {
                        if (digitNo != 567 || digitNo != 2324 || digitNo != 2728)
                        {
                            MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
                }
            }

            if (rbModelNoHeatTypeNA.Checked == true)
            {
                heatType = "NA";
            }
            else if (rbModelNoHeatTypeElec.Checked == true)
            {
                heatType = "ELEC";
            }
            else if (rbModelNoHeatTypeIF.Checked == true)
            {
                heatType = "IF";
            }
            else if (rbModelNoHeatTypeDF.Checked == true)
            {
                heatType = "DF";
            }

            dgvModelNo.DataSource = null;

            curSearchDigitNo = digitNo;
            curHeatType = heatType;
            curOAUTypeCode = unitType;

            DataTable dt = objModel.GetOAU_ModelNoValues(digitNo, heatType, null, mOAUTypeCode);
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);
        }

        #endregion              

        #region ModelNo Desc Events

        private void txtSearchModelNo_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {
                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.                

                tmpStr = this.txtSearchModelNo.Text + convertKeyValue(e.KeyValue);
                tmpStr = tmpStr.ToUpper();

                foreach (DataGridViewRow dgr in dgvModelNo.Rows)
                {
                    if (dgr.Cells["DigitNo"].Value != null)
                    {
                        if (dgr.Cells["DigitNo"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["DigitNo"].Value.ToString() == tmpStr)
                        {
                            dgr.Selected = true;
                            dgvModelNo.FirstDisplayedScrollingRowIndex = dgr.Index;
                            break;
                        }
                    }
                }

            }
        }

        private void dgvModelNo_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            string strDigitVal = "";

            int iPos;

            Color prevBackground = Color.White;
            Color foreGround = Color.Black;
            Color curBackGround = Color.LightGray;           
            Color nextBackGround = Color.White;            

            foreach (DataGridViewRow row in dgvModelNo.Rows)
            {
                if (initModelRender)
                {
                    iPos = 0;
                }
                else
                {
                    iPos = 2;
                }

                if (row.Cells["DigitNo"].Value.ToString() != strDigitVal)
                {
                    strDigitVal = row.Cells["DigitNo"].Value.ToString();

                    prevBackground = curBackGround;
                    curBackGround = nextBackGround;
                    nextBackGround = prevBackground;
                }

                row.DefaultCellStyle.BackColor = curBackGround;
                row.DefaultCellStyle.ForeColor = foreGround;
            }
        }
      
        private void btnModelNoCopy_Click(object sender, EventArgs e)
        {
            string digitValue;
            string heatType;
            string digitDesc;
            string digitRep;
            string oauTypeCode;
            string digitNoStr;
            string IDStr;

            int selRow = 0;

            foreach (DataGridViewRow dgvr in dgvModelNo.Rows)
            {
                if (dgvr.Selected == true)
                {
                    break;
                }
                ++selRow;
            }            

            digitNoStr = dgvModelNo.Rows[selRow].Cells["DigitNo"].Value.ToString();
            digitValue = dgvModelNo.Rows[selRow].Cells["DigitVal"].Value.ToString();
            digitDesc = dgvModelNo.Rows[selRow].Cells["DigitDescription"].Value.ToString();
            heatType = dgvModelNo.Rows[selRow].Cells["HeatType"].Value.ToString();
            oauTypeCode = dgvModelNo.Rows[selRow].Cells["OAUTypeCode"].Value.ToString();
            digitRep = dgvModelNo.Rows[selRow].Cells["DigitRepresentation"].Value.ToString();
            IDStr = "-1";

            frmUpdateModelNoDesc frmUpd = new frmUpdateModelNoDesc(this);
            frmUpd.txtModelNoUpdDigitNo.Text = digitNoStr;
            frmUpd.txtModelNoUpdDigitVal.Text = digitValue;
            frmUpd.txtModelNoUpdDigitDesc.Text = digitDesc;
            frmUpd.txtModelNoUpdDigitRep.Text = digitRep;
            frmUpd.lbModelNoID.Text = IDStr;
            frmUpd.btnSave.Text = "Add";

            if (heatType == "NA")
            {
                frmUpd.rbModelNoHeatTypeNA.Checked = true;
            }
            else if (heatType == "IF")
            {
                frmUpd.rbModelNoHeatTypeIF.Checked = true;
            }
            else if (heatType == "ELEC")
            {
                frmUpd.rbModelNoHeatTypeElec.Checked = true;
            }
            else if (heatType == "DF")
            {
                frmUpd.rbModelNoHeatTypeDF.Checked = true;
            }
            else if (heatType == "HOTWATER")
            {
                frmUpd.rbModelNoHeatTypeHotWater.Checked = true;
            }

            if (oauTypeCode == "OALBG")
            {
                frmUpd.rbOALBG.Checked = true;
            }
            else
            {
                frmUpd.rbOAU123DKN.Checked = true;
            }

            frmUpd.ShowDialog();

            if (mHeatType != "NoUpdate")
            {
                dgvModelNo.DataSource = null;

                curSearchDigitNo = -1;
                curHeatType = "ALL";
                curOAUTypeCode = "ALL";

                DataTable dt = objModel.GetOAU_ModelNoValues(-1, "ALL", null, null);
                dgvModelNo.DataSource = dt;
                populateModelNoList(dt, false);
            }            
        }

        //private void btnModelNoSearch_Click(object sender, EventArgs e)
        //{
        //    int digitNo = -1;
        //    string heatType = "ALL";           

        //    if (txtSearchModelNo.Text != "")
        //    {
        //        try
        //        {
        //            digitNo = Int32.Parse(txtSearchModelNo.Text);
        //            if (digitNo < 0)
        //            {
        //                MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
        //            }
        //            else if (digitNo > 59)
        //            {
        //                if (digitNo != 567 && digitNo != 2324 && digitNo != 2728)
        //                {
        //                    MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
        //                }
        //            }
        //        }
        //        catch
        //        {
        //            MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
        //        }
        //    }
        //    else
        //    {
        //        digitNo = -1;
        //        //MessageBox.Show("ERROR! - Invalid Search Digit No.  Value must be an integer value between 0 - 59");
        //    }

        //    if ( rbModelNoHeatTypeNA.Checked == true)
        //    {
        //        heatType = "NA";
        //    }
        //    else if (rbModelNoHeatTypeIF.Checked == true)
        //    {
        //        heatType = "IF";
        //    }
        //    else if (rbModelNoHeatTypeElec.Checked == true)
        //    {
        //        heatType = "ELEC";
        //    }
        //    else if (rbModelNoHeatTypeDF.Checked == true)
        //    {
        //        heatType = "DF";
        //    }
        //    else if (rbModelNoHeatTypeHotWater.Checked == true)
        //    {
        //        heatType = "HOTWATER";
        //    }         

        //    dgvModelNo.DataSource = null;

        //    curSearchDigitNo = digitNo;
        //    curHeatType = heatType;

        //    DataTable dt = objModel.GetOAU_ModelNoValues(digitNo, heatType, null, "");
        //    dgvModelNo.DataSource = dt;
        //    populateModelNoList(dt, false);
        //}          

        private void btnModelNoReset_Click(object sender, EventArgs e)
        {
            txtSearchModelNo.Text = "";
            rbModelNoHeatTypeAll.Checked = true;

            initModelRender = false;
            
            dgvModelNo.DataSource = null;

            curSearchDigitNo = -1;
            curHeatType = "ALL";
            curOAUTypeCode = "ALL";

            DataTable dt = objModel.GetOAU_ModelNoValues(-1, "ALL", null, null);
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);
        }        

        private void rbModelNoHeatTypeAll_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeAll.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "ALL");
            }
        }

        private void rbModelNoHeatTypeNA_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeNA.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "NA");
            }
        }

        private void rbModelNoHeatTypeElec_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeElec.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "ELEC");
            }
        }

        private void rbModelNoHeatTypeIF_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeIF.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "IF");
            }
        }

        private void rbModelNoHeatTypeDF_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeDF.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "DF");
            }
        }

        private void rbModelNoHeatTypeHotWater_CheckedChanged(object sender, EventArgs e)
        {
            if (rbModelNoHeatTypeHotWater.Checked == true)
            {
                modelNoHeatTypeChanged(txtSearchModelNo.Text, "HOTWATER");
            }
        }              

        private void dgvModelNo_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string digitValue;
            string heatType;
            string digitDesc;
            string digitRep;
            string oauTypeCode;
            string digitNoStr;                      
            string IDStr;

            digitNoStr = dgvModelNo.Rows[e.RowIndex].Cells["DigitNo"].Value.ToString();
            digitValue = dgvModelNo.Rows[e.RowIndex].Cells["DigitVal"].Value.ToString();
            digitDesc = dgvModelNo.Rows[e.RowIndex].Cells["DigitDescription"].Value.ToString();
            heatType = dgvModelNo.Rows[e.RowIndex].Cells["HeatType"].Value.ToString();
            oauTypeCode = dgvModelNo.Rows[e.RowIndex].Cells["OAUTypeCode"].Value.ToString();
            digitRep = dgvModelNo.Rows[e.RowIndex].Cells["DigitRepresentation"].Value.ToString();            
            IDStr = dgvModelNo.Rows[e.RowIndex].Cells["ID"].Value.ToString();
           
            frmUpdateModelNoDesc frmUpd = new frmUpdateModelNoDesc(this);
            frmUpd.txtModelNoUpdDigitNo.Text = digitNoStr;
            frmUpd.txtModelNoUpdDigitVal.Text = digitValue;
            frmUpd.txtModelNoUpdDigitDesc.Text = digitDesc;
            frmUpd.txtModelNoUpdDigitRep.Text = digitRep;            
            frmUpd.lbModelNoID.Text = IDStr;
            frmUpd.btnSave.Text = "Update";

            if (heatType == "NA")
            {
                frmUpd.rbModelNoHeatTypeNA.Checked = true;
            }
            else if (heatType == "IF")
            {
                frmUpd.rbModelNoHeatTypeIF.Checked = true;
            }
            else if (heatType == "ELEC")
            {
                frmUpd.rbModelNoHeatTypeElec.Checked = true;
            }
            else if (heatType == "DF")
            {
                frmUpd.rbModelNoHeatTypeDF.Checked = true;
            }
            else if (heatType == "HOTWATER")
            {
                frmUpd.rbModelNoHeatTypeHotWater.Checked = true;
            }          
            
            if (oauTypeCode == "OALBG")
            {
                frmUpd.rbOALBG.Checked = true;
            }
            else
            {
                frmUpd.rbOAU123DKN.Checked = true;
            }

            frmUpd.ShowDialog();

            if (mHeatType != "NoUpdate")
            {
                dgvModelNo.Rows[e.RowIndex].Cells["DigitVal"].Value = mDigitVal;
                dgvModelNo.Rows[e.RowIndex].Cells["DigitDescription"].Value = mDigitDesc;
                dgvModelNo.Rows[e.RowIndex].Cells["HeatType"].Value = mHeatType;
                dgvModelNo.Rows[e.RowIndex].Cells["OAUTypeCode"].Value = mOAUTypeCode;
                dgvModelNo.Rows[e.RowIndex].Cells["DigitRepresentation"].Value = mDigitRep;               
                dgvModelNo.Refresh();
            }
            else
            {
                
                dgvModelNo.DataSource = null;

                curSearchDigitNo = -1;
                curHeatType = "ALL";
                curOAUTypeCode = "ALL";

                DataTable dt = objModel.GetOAU_ModelNoValues(-1, "ALL", null, null);
                dgvModelNo.DataSource = dt;
                populateModelNoList(dt, false);
            }
        }       

        //private void dgvModelNo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    int rowIdx = e.RowIndex;

        //    string digitValue;
        //    string heatType;
        //    string digitDesc;
        //    string digitRep;
        //    string oauTypeCode;

        //    int digitNo;           
        //    int ID;

        //    DataTable dt = new DataTable();

        //    digitNo = (int)dgvModelNo.Rows[e.RowIndex].Cells["DigitNo"].Value;
        //    digitValue = dgvModelNo.Rows[e.RowIndex].Cells["Value"].Value.ToString();
        //    digitDesc = dgvModelNo.Rows[e.RowIndex].Cells["DigitDescription"].Value.ToString();
        //    heatType = dgvModelNo.Rows[e.RowIndex].Cells["HeatType"].Value.ToString();
        //    oauTypeCode = dgvModelNo.Rows[e.RowIndex].Cells["OAUTypeCode"].Value.ToString();
        //    digitRep = dgvModelNo.Rows[e.RowIndex].Cells["DigitRepresentation"].Value.ToString();           
        //    ID = (int)dgvModelNo.Rows[e.RowIndex].Cells["ID"].Value;

        //    frmUpdateModelNoDesc frmUpd = new frmUpdateModelNoDesc(this);
        //    frmUpd.txtModelNoUpdDigitNo.Text = digitNo.ToString();
        //    frmUpd.txtModelNoUpdDigitVal.Text = digitValue;
        //    frmUpd.txtModelNoUpdDigitDesc.Text = digitDesc;
        //    frmUpd.txtModelNoUpdDigitRep.Text = digitRep;            
        //    frmUpd.lbModelNoID.Text = ID.ToString();

        //    if (heatType == "NA")
        //    {
        //        frmUpd.rbModelNoHeatTypeNA.Checked = true;
        //    }
        //    else if (heatType == "IF")
        //    {
        //        frmUpd.rbModelNoHeatTypeIF.Checked = true;
        //    }
        //    else if (heatType == "ELEC")
        //    {
        //        frmUpd.rbModelNoHeatTypeElec.Checked = true;
        //    }
        //    else if (heatType == "DF")
        //    {
        //        frmUpd.rbModelNoHeatTypeDF.Checked = true;
        //    }
        //    else if (heatType == "HOTWATER")
        //    {
        //        frmUpd.rbModelNoHeatTypeHotWater.Checked = true;
        //    }         

        //    //if (e.ColumnIndex == 0)  // Add button within datagridview.
        //    //{                                 
        //    //    frmUpd.btnSave.Text = "Add";                               
        //    //    frmUpd.ShowDialog();
                                         
        //    //}
        //    //else if (e.ColumnIndex == 1)  // Delete button internally is really in column #1 not 11 as it appears on the screen.
        //    //{
        //    //    frmUpd.btnSave.Text = "Delete";
        //    //    frmUpd.ShowDialog();                
        //    //}
        //    //else
        //    //{
        //    //    //curSelRowIdx = e.RowIndex;
        //    //}
        //    dgvModelNo.DataSource = null;
        //    dt = objModel.GetOAU_ModelNoValues(curSearchDigitNo, curHeatType, null, null);
        //    dgvModelNo.DataSource = dt;
        //    populateModelNoList(dt, false);
        //}            

        private void btnModelNoAddRow_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();

            frmUpdateModelNoDesc frmUpd = new frmUpdateModelNoDesc(this);
            frmUpd.txtModelNoUpdDigitNo.Enabled = true;
            frmUpd.btnSave.Text = "Add";
            frmUpd.ShowDialog();
            
            dgvModelNo.DataSource = null;
            dt = objModel.GetOAU_ModelNoValues(curSearchDigitNo, curHeatType, null, "");
            dgvModelNo.DataSource = dt;
            populateModelNoList(dt, false);

        }

        #endregion      

        #region Compressor Detail Events
        //private void rbCompDtlVoltAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbCompDtlVoltAll.Checked == true)
        //    {
        //        if (this.txtCompDtlSearchPartNum.Text == "")
        //        {
        //            compressorDataSearch(txtCompDtlSearchPartNum.Text, -1);
        //        }
        //    }
        //}

        //private void rbCompDtlVolt208_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbCompDtlVolt208.Checked == true)
        //    {
        //        if (this.txtCompDtlSearchPartNum.Text == "")
        //        {
        //            compressorDataSearch(txtCompDtlSearchPartNum.Text, 208);
        //        }
        //    }
        //}

        //private void rbCompDtlVolt460_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbCompDtlVolt460.Checked == true)
        //    {
        //        if (this.txtCompDtlSearchPartNum.Text == "")
        //        {
        //            compressorDataSearch(txtCompDtlSearchPartNum.Text, 460);
        //        }
        //    }
        //}

        //private void rbCompDtlVolt575_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (rbCompDtlVolt575.Checked == true)
        //    {
        //        if (this.txtCompDtlSearchPartNum.Text == "")
        //        {
        //            compressorDataSearch(txtCompDtlSearchPartNum.Text, 575);
        //        }
        //    }
        //}

        //private void compressorDataSearch(string partNum, int voltage)
        //{                        

        //    dgvCompDetail.DataSource = null;

        //    curVoltage = voltage;
        //    curPartNum = partNum;

        //    DataTable dt = objComp.GetROA_CompressorDataDtl(curPartNum, voltage);
        //    dgvCompDetail.DataSource = dt;
        //    populateCompressorDetailList(dt, false);
        //}              

        //private void btnCompDtlReset_Click(object sender, EventArgs e)
        //{
        //    this.txtCompDtlSearchPartNum.Text = "";
        //    curPartNum = "";
        //    curVoltage = -1;

        //    dgvCompDetail.DataSource = null;
        //    DataTable dt = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
        //    dgvCompDetail.DataSource = dt;
        //    populateCompressorDetailList(dt, false);
            
        //}        

        //private void dgvCompDetail_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    string sPartNum;
        //    string sPartDesc;
        //    string sPartCategory;
        //    string sModBy;
        //    string sLastModDate;

        //    int iRuleHeadID;
        //    int iVoltage;
        //    int iPhase;
        //    int iID;

        //    decimal dRLA;
        //    decimal dLRA;

        //    iRuleHeadID = (int)dgvCompDetail.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
        //    sPartNum = dgvCompDetail.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
        //    sPartDesc = dgvCompDetail.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
        //    sPartCategory = dgvCompDetail.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
        //    iVoltage = (int)dgvCompDetail.Rows[e.RowIndex].Cells["Voltage"].Value;
        //    iPhase = (int)dgvCompDetail.Rows[e.RowIndex].Cells["Phase"].Value;
        //    dRLA = (decimal)dgvCompDetail.Rows[e.RowIndex].Cells["RLA"].Value;
        //    dLRA = (decimal)dgvCompDetail.Rows[e.RowIndex].Cells["LRA"].Value;
        //    sModBy = dgvCompDetail.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
        //    sLastModDate = dgvCompDetail.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
        //    iID = (int)dgvCompDetail.Rows[e.RowIndex].Cells["ID"].Value;

        //    frmUpdateCompDataDtl frmUpd = new frmUpdateCompDataDtl(this);
        //    frmUpd.txtCompDtlRuleHeadID.Text = iRuleHeadID.ToString();
        //    frmUpd.cbCompDtl_PartNum.Text = sPartNum;
        //    frmUpd.txtCompDtlPartDesc.Text = sPartDesc;
        //    frmUpd.txtCompDtlPartCategory.Text = sPartCategory;            
        //    frmUpd.txtCompDtlPhase.Text = iPhase.ToString();
        //    frmUpd.txtCompDtlRLA.Text = dRLA.ToString();
        //    frmUpd.txtCompDtlLRA.Text = dLRA.ToString();
        //    frmUpd.txtCompDtlModifedBy.Text = sModBy;
        //    frmUpd.txtCompDtlLastModDate.Text = sLastModDate;
        //    frmUpd.lbCompDtlID.Text = iID.ToString();
        //    frmUpd.btnCompDtlSave.Text = "Update";

        //    if (iVoltage == 208)
        //    {
        //        frmUpd.rbCompDtlVolt208.Checked = true;
        //    }
        //    else  if (iVoltage == 460)
        //    {
        //        frmUpd.rbCompDtlVolt460.Checked = true;
        //    }
        //    else  if (iVoltage == 575)
        //    {
        //        frmUpd.rbCompDtlVolt575.Checked = true;
        //    }           

        //    frmUpd.ShowDialog();

        //    if (mVoltage != "NoUpdate")
        //    {
        //        dgvCompDetail.Rows[e.RowIndex].Cells["Voltage"].Value = mVoltage;
        //        dgvCompDetail.Rows[e.RowIndex].Cells["Phase"].Value = mPhase;
        //        dgvCompDetail.Rows[e.RowIndex].Cells["RLA"].Value = mRLA;
        //        dgvCompDetail.Rows[e.RowIndex].Cells["LRA"].Value = mLRA;
        //        dgvCompDetail.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
        //        dgvCompDetail.Rows[e.RowIndex].Cells["ModDate"].Value = mLastModDate;
        //        dgvCompDetail.Refresh();
        //    }
        //    else
        //    {
        //        curVoltage = -1;
        //        curPartNum = "";
        //        DataTable dt = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
        //        populateCompressorDetailList(dt, false);
        //    }
        //}       

        //private void btnCompDtlAddRow_Click(object sender, EventArgs e)
        //{
        //    frmUpdateCompDataDtl frmAdd = new frmUpdateCompDataDtl(this);

        //    DataTable dt = objPart.GetOAU_PartNumbersByCategory("Compressor,DigitalScroll,TandemCompAssembly");
        //    frmAdd.cbCompDtl_PartNum.DataSource = dt;
        //    frmAdd.cbCompDtl_PartNum.ValueMember = "RuleHeadID";
        //    frmAdd.cbCompDtl_PartNum.DisplayMember = "PartNumDesc";

        //    //DataRow dr = dt.NewRow();
        //    //dr["RuleHeadID"] = 0;
        //    //dr["PartNumDesc"] = "Select a Part Number.";
        //    //dt.Rows.InsertAt(dr, 0);
        //    //frmAdd.cbCompDtl_PartNum.SelectedIndex = 0;

        //    frmAdd.cbCompDtl_PartNum.Enabled = true;
            
        //    frmAdd.btnCompDtlSave.Text = "Add";
        //    frmAdd.ShowDialog();

        //    if (mVoltage != "NoUpdate")
        //    {
        //        curVoltage = -1;
        //        curPartNum = "";
        //        DataTable dtComp = objComp.GetROA_CompressorDataDtl(curPartNum, curVoltage);
        //        populateCompressorDetailList(dtComp, false);
        //    }           
        //}

        //private void txtCompDtlSearchPartNum_KeyUp(object sender, KeyEventArgs e)
        //{
        //    string tmpStr = "";
        //    int rowIdx = 0;

        //    // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
        //    // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
        //    // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
        //    if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
        //    {
        //        return;
        //    }
        //    // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
        //    else
        //    {

        //        // Because the key value has not been added to the StartAt textbox
        //        // at this point it must be added in order to search properly.
        //        //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

        //        tmpStr = this.txtCompDtlSearchPartNum.Text.ToUpper();
        //        if (tmpStr.Length > 3)
        //        {
        //            //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
        //            //{
        //            //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
        //            //}
        //            foreach (DataGridViewRow dgr in dgvCompDetail.Rows)
        //            {
        //                if (dgr.Cells["PartNum"].Value != null)
        //                {
        //                    if (dgr.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["PartNum"].Value.ToString() == tmpStr)
        //                    {
        //                        rowIdx = dgr.Index;
        //                        dgvCompDetail.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
        //                        dgvCompDetail.Rows[rowIdx].Selected = true;
        //                        dgvCompDetail.FirstDisplayedScrollingRowIndex = rowIdx;
        //                        dgvCompDetail.Focus();
        //                        break;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    this.txtCompDtlSearchPartNum.Focus();
        //}

       

        #endregion            
        
        #region PartRuleHead Events

        private void btnPRH_AddPart_Click(object sender, EventArgs e)
        {
            string partNum = "";
            int rowIdx = 0;

            frmAddPartRuleHead frmAdd = new frmAddPartRuleHead(this);

            DataTable dt = objPart.GetOA_PartNumbersBasedOnClassID();

            frmAdd.cbAddPRH_PartNum.DataSource = dt;
            frmAdd.cbAddPRH_PartNum.DisplayMember = "PartNumDesc";
            frmAdd.cbAddPRH_PartNum.ValueMember = "PartNum";

            DataRow dr = dt.NewRow();
            dr["PartNum"] = 0;
            dr["PartNumDesc"] = "Select a Part Number.";
            dt.Rows.InsertAt(dr, 0);
            frmAdd.cbAddPRH_PartNum.SelectedIndex = 0;

            dt = objPart.GetPartCategoryList();
            frmAdd.cbAddPRH_PartCategory.DataSource = dt;
            frmAdd.cbAddPRH_PartCategory.DisplayMember = "CategoryName";
            frmAdd.cbAddPRH_PartCategory.ValueMember = "CategoryName";

            frmAdd.cbAddPRH_RelatedOp.SelectedIndex = 0;

            frmAdd.txtAddPRH_AsmSeq.Text = "0";

            frmAdd.ShowDialog();

            partNum = mPartNumber;
            objPart.PartNum = "ALL";
            dgvPartRulesHead.DataSource = null;
            dt = objPart.GetPartRulesHead();         
            populatePartRuleHeadList(dt, false);
           
            foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
            {
                if (dgr.Cells["PartNum"].Value != null)
                {
                    if (dgr.Cells["PartNum"].Value.ToString() == partNum)
                    {
                        rowIdx = dgr.Index;
                        dgvPartRulesHead.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                        dgvPartRulesHead.Rows[rowIdx].Selected = true;
                        dgvPartRulesHead.FirstDisplayedScrollingRowIndex = rowIdx;
                        dgvPartRulesHead.Focus();
                        break;
                    }
                }
            }            
        }
       
        private void dgvPartRulesHead_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string partNum;
            string partDesc;
            string partCategory;
            string modBy;
            string lastModDate;
            string assemblySeq;
            string partType;
            string stationLoc;
            string linesideBin;
            string pickList;
            string configPart;

            int ruleHeadID;           
            int relatedOp;

            ruleHeadID = (int)dgvPartRulesHead.Rows[e.RowIndex].Cells["ID"].Value;
            partNum = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
            partDesc = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
            relatedOp = (int)dgvPartRulesHead.Rows[e.RowIndex].Cells["RelatedOperation"].Value;
            partCategory = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartCategory"].Value.ToString();
            modBy = dgvPartRulesHead.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            lastModDate = dgvPartRulesHead.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            assemblySeq = dgvPartRulesHead.Rows[e.RowIndex].Cells["AssemblySeq"].Value.ToString();
            linesideBin = dgvPartRulesHead.Rows[e.RowIndex].Cells["LinesideBin"].Value.ToString();
            partType = dgvPartRulesHead.Rows[e.RowIndex].Cells["PartType"].Value.ToString();
            stationLoc = dgvPartRulesHead.Rows[e.RowIndex].Cells["StationLocation"].Value.ToString();
            pickList = dgvPartRulesHead.Rows[e.RowIndex].Cells["PickList"].Value.ToString();
            configPart = dgvPartRulesHead.Rows[e.RowIndex].Cells["ConfigurablePart"].Value.ToString();

            frmUpdatePartRuleHead frmUpd = new frmUpdatePartRuleHead(this);
            frmUpd.txtPRH_PartNum.Text = partNum;
            frmUpd.txtPRH_PartDesc.Text = partDesc;
            frmUpd.cbPRH_RelatedOp.Text = relatedOp.ToString();
            frmUpd.txtPRH_AsmSeq.Text = assemblySeq;
            frmUpd.txtPRH_PartType.Text = partType;
            frmUpd.cbStationLoc.Text = stationLoc;

            if (pickList == "Yes")
            {
                frmUpd.chkBoxPicklist.Checked = true;
            }

            if (configPart == "Yes")
            {
                frmUpd.chkBoxConfigPart.Checked = true;
            }

            DataTable dt = objPart.GetPartCategoryList();

            frmUpd.cbPRH_PartCategory.DisplayMember = "CategoryName";
            frmUpd.cbPRH_PartCategory.ValueMember = "CategoryName";
            frmUpd.cbPRH_PartCategory.DataSource = dt;    

            frmUpd.cbPRH_PartCategory.Text = partCategory;            
            frmUpd.txtPRH_RuleHeadID.Text = ruleHeadID.ToString();
            frmUpd.txtPRH_ModBy.Text = modBy;
            frmUpd.txtPRH_DateModified.Text = lastModDate;
            frmUpd.txtPRH_AsmSeq.Text = assemblySeq;
            frmUpd.txtPRH_PartType.Text = partType;
            frmUpd.cbStationLoc.Text = stationLoc;
            frmUpd.txtLinesideBin.Text = linesideBin;

            frmUpd.ShowDialog();

            if (mPartCategory == "Delete")
            {
                txtPRH_SearchPartNum.Text = "";
                dgvPartRulesHead.DataSource = null;
                objPart.PartCategory = null;
                objPart.PartNum = "ALL";
                dt = objPart.GetPartRulesHead();
                populatePartRuleHeadList(dt, false);
            }
            else if (mPartCategory != "NoUpdate")
            {
                dgvPartRulesHead.Rows[e.RowIndex].Cells["RelatedOperation"].Value = Int32.Parse(mRelatedOp);
                dgvPartRulesHead.Rows[e.RowIndex].Cells["PartCategory"].Value = mPartCategory;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["AssemblySeq"].Value = mAssemblySeq;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["PartType"].Value = mPartType;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["StationLocation"].Value = mStationLoc;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["LinesideBin"].Value = mLinesideBin;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["PickList"].Value = mPicklist;
                dgvPartRulesHead.Rows[e.RowIndex].Cells["ConfigurablePart"].Value = mConfigPart;
                dgvPartRulesHead.Refresh();
            }
        }         

        private void txtPRH_SearchPartNum_KeyUp(object sender, KeyEventArgs e)
        {
            string tmpStr = "";
            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else
            {

                // Because the key value has not been added to the StartAt textbox
                // at this point it must be added in order to search properly.
                //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

                tmpStr = this.txtPRH_SearchPartNum.Text.ToUpper();
                if (tmpStr.Length > 3)
                {
                    //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    //{
                    //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
                    //}
                    foreach (DataGridViewRow dgr in dgvPartRulesHead.Rows)
                    {
                        if (dgr.Cells["PartNum"].Value != null)
                        {
                            if (dgr.Cells["PartNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["PartNum"].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvPartRulesHead.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvPartRulesHead.Rows[rowIdx].Selected = true;
                                dgvPartRulesHead.FirstDisplayedScrollingRowIndex = rowIdx;
                                dgvPartRulesHead.Focus();
                                break;
                            }
                        }
                    }            
                }
            }
            this.txtPRH_SearchPartNum.Focus();
        }        

        private void btnPRH_Reset_Click(object sender, EventArgs e)
        {
            txtPRH_SearchPartNum.Text = "ALL";
            dgvPartRulesHead.DataSource = null;
            objPart.PartCategory = null;
            DataTable dt = objPart.GetPartRulesHead();
            populatePartRuleHeadList(dt, false);   
        }
        #endregion        

        #region CompCircuitChargeData Events
       
        //private void btnCompChrg_Reset_Click(object sender, EventArgs e)
        //{
        //    txtSearchCoolCap.Text = "";
        //    dgvCompCircuitData.DataSource = null;
        //    objComp.UnitModel = "ALL";
        //    objComp.CoolingCap = "ALL";
        //    objComp.HeatType = "";
        //    DataTable dt = objComp.GetROA_CompressorChargeData();
        //    populateCompressorChargeList(dt, false);
        //}

        //private void dgvCompChargeData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    int rowIdx = e.RowIndex;

        //    string unitModel;
        //    string coolingCap;
        //    string heatType;
        //    string modifiedBy;
        //    string lastModDate;

        //    int ID;

        //    decimal circuit1;
        //    decimal circuit1_Reheat;
        //    decimal circuit2;
        //    decimal circuit2_Reheat;

        //    //ID = (int)dgvCompChargeData.Rows[e.RowIndex].Cells[1].Value;
        //    //unitModel = dgvCompChargeData.Rows[e.RowIndex].Cells[2].Value.ToString();
        //    //coolingCap = dgvCompChargeData.Rows[e.RowIndex].Cells[3].Value.ToString();
        //    //heatType = dgvCompChargeData.Rows[e.RowIndex].Cells[4].Value.ToString();
        //    //circuit1 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[5].Value;
        //    //circuit1_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[6].Value;
        //    //circuit2 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[7].Value;
        //    //circuit2_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[8].Value;
        //    //modifiedBy = dgvCompChargeData.Rows[e.RowIndex].Cells[9].Value.ToString();
        //    //lastModDate = dgvCompChargeData.Rows[e.RowIndex].Cells[10].Value.ToString();

        //    frmUpdateCompCharge frmUpd = new frmUpdateCompCharge(this);
        //    //frmUpd.txtUpdCCD_ID.Text = ID.ToString();
        //    //frmUpd.cbUpdCCD_UnitModel.Text = unitModel;
        //    //frmUpd.cbUpdCCD_CoolingCapacity.Text = coolingCap;
        //    //frmUpd.txtUpdCCD_Circuit1.Text = circuit1.ToString();
        //    //frmUpd.txtUpdCCD_Circuit1Reheat.Text = circuit1_Reheat.ToString();
        //    //frmUpd.txtUpdCCD_Circuit2.Text = circuit2.ToString();
        //    //frmUpd.txtUpdCCD_Circuit2Reheat.Text = circuit2_Reheat.ToString();
        //    //frmUpd.txtUpdCCD_ModifedBy.Text = modifiedBy;
        //    //frmUpd.txtUpdCCD_LastModDate.Text = lastModDate;

        //    //if (heatType == "Air")
        //    //{
        //    //    frmUpd.rbUpdCCD_Air.Checked = true;
        //    //}
        //    //else if (heatType == "Normal")
        //    //{
        //    //    frmUpd.rbUpdCCD_Normal.Checked = true;
        //    //}
        //    //else
        //    //{
        //    //    frmUpd.rbUpdCCD_Water.Checked = true;
        //    //}
            

        //    if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
        //    {
        //        frmUpd.btnUpdCCD_Save.Text = "Delete";
        //        frmUpd.ShowDialog();

        //    }
        //    else
        //    {
        //        //curSelRowIdx = e.RowIndex;
        //    }

        //    if (mUnitType != "NoUpdate")
        //    {
        //        txtSearchCoolCap.Text = "";
        //        dgvCompCircuitData.DataSource = null;
        //        objComp.UnitModel = "ALL";
        //        objComp.CoolingCap = "ALL";
        //        objComp.HeatType = "";
        //        DataTable dt = objComp.GetROA_CompressorChargeData();
        //        populateCompressorChargeList(dt, false);
        //    }          
        //}

        //private void btnCCD_Search_Click(object sender, EventArgs e)
        //{
        //    //txtSearchCoolCap.Text = txtSearchCoolCap.Text.ToUpper();
        //    //objComp.UnitModel = txtSearchCoolCap.Text;
        //    //objComp.CoolingCap = "ALL";
        //    //objComp.HeatType = "";
        //    //DataTable dt = objComp.GetROA_CompressorChargeData();
        //    //if (dt.Rows.Count > 0)
        //    //{
        //    //    dgvCompChargeData.DataSource = null;
        //    //    populateCompressorChargeList(dt, false);
        //    //}

        //}                            

        //private void dgvCompChargeData_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    string unitModel;
        //    string coolingCap;
        //    string heatType;
        //    string modifiedBy;
        //    string lastModDate;

        //    int ID;

        //    decimal circuit1;
        //    decimal circuit1_Reheat;
        //    decimal circuit2;
        //    decimal circuit2_Reheat;

        //    //ID = (int)dgvCompChargeData.Rows[e.RowIndex].Cells[1].Value;
        //    //unitModel = dgvCompChargeData.Rows[e.RowIndex].Cells[2].Value.ToString();
        //    //coolingCap = dgvCompChargeData.Rows[e.RowIndex].Cells[3].Value.ToString();
        //    //heatType = dgvCompChargeData.Rows[e.RowIndex].Cells[4].Value.ToString();
        //    //circuit1 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[5].Value;
        //    //circuit1_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[6].Value;
        //    //circuit2 = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[7].Value;
        //    //circuit2_Reheat = (decimal)dgvCompChargeData.Rows[e.RowIndex].Cells[8].Value;
        //    //modifiedBy = dgvCompChargeData.Rows[e.RowIndex].Cells[9].Value.ToString();
        //    //lastModDate = dgvCompChargeData.Rows[e.RowIndex].Cells[10].Value.ToString();

        //    frmUpdateCompCharge frmUpd = new frmUpdateCompCharge(this);
        //    //frmUpd.txtUpdCCD_ID.Text = ID.ToString();
        //    //frmUpd.cbUpdCCD_UnitModel.Text = unitModel;
        //    //frmUpd.cbUpdCCD_CoolingCapacity.Text = coolingCap;
        //    //frmUpd.txtUpdCCD_Circuit1.Text = circuit1.ToString();
        //    //frmUpd.txtUpdCCD_Circuit1Reheat.Text = circuit1_Reheat.ToString();
        //    //frmUpd.txtUpdCCD_Circuit2.Text = circuit2.ToString();
        //    //frmUpd.txtUpdCCD_Circuit2Reheat.Text = circuit2_Reheat.ToString();
        //    //frmUpd.txtUpdCCD_ModifedBy.Text = modifiedBy;
        //    //frmUpd.txtUpdCCD_LastModDate.Text = lastModDate;
        //    //frmUpd.btnUpdCCD_Save.Text = "Update";

        //    //if (heatType == "Air")
        //    //{
        //    //    frmUpd.rbUpdCCD_Air.Checked = true;
        //    //}
        //    //else if (heatType == "Normal")
        //    //{
        //    //    frmUpd.rbUpdCCD_Normal.Checked = true;
        //    //}
        //    //else
        //    //{
        //    //    frmUpd.rbUpdCCD_Water.Checked = true;
        //    //}

        //    frmUpd.ShowDialog();

        //    //if (mUnitType != "NoUpdate")
        //    //{
        //    //    dgvCompChargeData.Rows[e.RowIndex].Cells[5].Value = Decimal.Parse(mCircuit1);
        //    //    dgvCompChargeData.Rows[e.RowIndex].Cells[6].Value = Decimal.Parse(mCircuit1_Reheat);
        //    //    dgvCompChargeData.Rows[e.RowIndex].Cells[7].Value = Decimal.Parse(mCircuit2);
        //    //    dgvCompChargeData.Rows[e.RowIndex].Cells[8].Value = Decimal.Parse(mCircuit2_Reheat);
        //    //    dgvCompChargeData.Rows[e.RowIndex].Cells[9].Value = mModBy;
        //    //    dgvCompChargeData.Rows[e.RowIndex].Cells[10].Value = mLastModDate;
        //    //    dgvCompChargeData.Refresh();
        //    //}
        //}

        //private void btnCompChargeAdd_Click(object sender, EventArgs e)
        //{
        //    frmUpdateCompCharge frmUpd = new frmUpdateCompCharge(this);

        //    frmUpd.cbUpdCCD_UnitModel.Enabled = true;
        //    frmUpd.cbUpdCCD_CoolingCapacity.Enabled = true;
        //    frmUpd.gbUpdCCD_HeatType.Enabled = true;
        //    frmUpd.btnUpdCCD_Save.Text = "Add";
        //    frmUpd.ShowDialog();

        //    if (mUnitType != "NoUpdate")
        //    {
        //        txtSearchCoolCap.Text = "";
        //        dgvCompCircuitData.DataSource = null;
        //        objComp.UnitModel = "ALL";
        //        objComp.CoolingCap = "ALL";
        //        objComp.HeatType = "";
        //        DataTable dt = objComp.GetROA_CompressorChargeData();
        //        populateCompressorChargeList(dt, false);
        //    }
        //}
        #endregion       
      
        #region ROA_MotorDataDTL Events
        //private void txtMotorDataSearchPartNum_KeyUp(object sender, KeyEventArgs e)
        //{
        //    string tmpStr = "";
        //    int rowIdx = 0;

        //    // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
        //    // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
        //    // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
        //    if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
        //    {
        //        return;
        //    }
        //    // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
        //    else
        //    {

        //        // Because the key value has not been added to the StartAt textbox
        //        // at this point it must be added in order to search properly.
        //        //tmpStr = (this.txtSearchPartNum.Text + convertKeyValue(e.KeyValue)).ToUpper();

        //        tmpStr = this.txtMotorDataSearchPartNum.Text.ToUpper();
        //        if (tmpStr.Length > 3)
        //        {
        //            //if ((e.KeyValue == 8) && (tmpStr.Length > 0))
        //            //{
        //            //    tmpStr = tmpStr.Substring(0, (tmpStr.Length - 1));
        //            //}
        //            foreach (DataGridViewRow dgr in dgvMotorData.Rows)
        //            {
        //                if (dgr.Cells[2].Value != null)
        //                {
        //                    if (dgr.Cells[2].Value.ToString().StartsWith(tmpStr) || dgr.Cells[2].Value.ToString() == tmpStr)
        //                    {
        //                        rowIdx = dgr.Index;
        //                        dgvMotorData.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
        //                        dgvMotorData.Rows[rowIdx].Selected = true;
        //                        dgvMotorData.FirstDisplayedScrollingRowIndex = rowIdx;
        //                        dgvMotorData.Focus();
        //                        break;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    this.txtMotorDataSearchPartNum.Focus();
        //}      

        //private void dgvMotorData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    //string sPartNum;
        //    //string sPartDesc;
        //    //string sMotorType;
        //    //string sModBy;
        //    //string sLastModDate;
        //    //string sVoltage;
        //    //string sFLA;

        //    //int iRuleHeadID;

        //    //int iPhase;
        //    //int iID;

        //    //decimal dMCC;
        //    //decimal dRLA;
        //    //decimal dHertz;
        //    //decimal dHP;

        //    //iRuleHeadID = (int)dgvMotorData.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
        //    //sPartNum = dgvMotorData.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
        //    //sPartDesc = dgvMotorData.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
        //    //sMotorType = dgvMotorData.Rows[e.RowIndex].Cells["MotorType"].Value.ToString();
        //    //sFLA = dgvMotorData.Rows[e.RowIndex].Cells["FLA"].Value.ToString();
        //    //dHertz = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["Hertz"].Value;
        //    //iPhase = (int)dgvMotorData.Rows[e.RowIndex].Cells["Phase"].Value;
        //    //dMCC = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["MCC"].Value;
        //    //dRLA = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["RLA"].Value;
        //    //sVoltage = dgvMotorData.Rows[e.RowIndex].Cells["Voltage"].Value.ToString();
        //    //dHP = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["HP"].Value;
        //    //sModBy = dgvMotorData.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
        //    //sLastModDate = dgvMotorData.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
        //    //iID = (int)dgvMotorData.Rows[e.RowIndex].Cells["ID"].Value;

        //    //frmUpdateMotorData frmUpd = new frmUpdateMotorData(this);
        //    //frmUpd.txtUpdMotorRuleHeadID.Text = iRuleHeadID.ToString();
        //    //frmUpd.cbUpdMotorPartNum.Text = sPartNum;
        //    //frmUpd.txtUpdMotorPartDesc.Text = sPartDesc;
        //    //frmUpd.cbUpdMotor_MotorType.Text = sMotorType;
        //    //frmUpd.txtUpdMotorFLA.Text = sFLA;
        //    //frmUpd.txtUpdMotorHertz.Text = dHertz.ToString();
        //    //frmUpd.txtUpdMotorPhase.Text = iPhase.ToString();
        //    //frmUpd.txtUpdMotorMCC.Text = dMCC.ToString();
        //    //frmUpd.txtUpdMotorRLA.Text = dRLA.ToString();

        //    //if (sVoltage == "208")
        //    //{
        //    //    frmUpd.rbUpdMotor208_60_3.Checked = true;
        //    //}
        //    //else if (sVoltage == "460")
        //    //{
        //    //    frmUpd.rbUpdMotor460_60_3.Checked = true;
        //    //}
        //    //else
        //    //{
        //    //    frmUpd.rbUpdMotor575_60_3.Checked = true;
        //    //}

        //    ////frmUpd.txtUpdMotorVoltage.Text = sVoltage;
        //    //frmUpd.txtUpdMotorHP.Text = dHP.ToString();
        //    //frmUpd.txtUpdMotorModifedBy.Text = sModBy;
        //    //frmUpd.txtUpdMotorLastModDate.Text = sLastModDate;
        //    //frmUpd.lbUpdMotorID.Text = iID.ToString();
            
        //    //if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
        //    //{
        //    //    frmUpd.btnUpdMotorSave.Text = "Delete";
        //    //    frmUpd.ShowDialog();

        //    //}          

        //    //if (mVoltage != "NoUpdate")
        //    //{
        //    //    txtMotorDataSearchPartNum.Text = "";
        //    //    objMotor.RuleHeadID = "-1";
        //    //    DataTable dtMtr = objMotor.GetROA_MotorDataDTL();
        //    //    populateMotorDataList(dtMtr, false);
        //    //}          
        //}

        //private void dgvMotorData_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    string sPartNum;
        //    string sPartDesc;
        //    string sMotorType;
        //    string sModBy;
        //    string sLastModDate;
        //    string sVoltage;
        //    string sFLA;

        //    int iRuleHeadID;

        //    int iPhase;
        //    int iID;

        //    decimal dMCC;
        //    decimal dRLA;
        //    decimal dHertz;
        //    decimal dHP;

        //    iRuleHeadID = (int)dgvMotorData.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
        //    sPartNum = dgvMotorData.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
        //    sPartDesc = dgvMotorData.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
        //    sMotorType = dgvMotorData.Rows[e.RowIndex].Cells["MotorType"].Value.ToString();
        //    sFLA = dgvMotorData.Rows[e.RowIndex].Cells["FLA"].Value.ToString();
        //    dHertz = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["Hertz"].Value;
        //    iPhase = (int)dgvMotorData.Rows[e.RowIndex].Cells["Phase"].Value;
        //    dMCC = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["MCC"].Value;
        //    dRLA = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["RLA"].Value;
        //    sVoltage = dgvMotorData.Rows[e.RowIndex].Cells["Voltage"].Value.ToString();
        //    dHP = (decimal)dgvMotorData.Rows[e.RowIndex].Cells["HP"].Value;
        //    sModBy = dgvMotorData.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
        //    sLastModDate = dgvMotorData.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
        //    iID = (int)dgvMotorData.Rows[e.RowIndex].Cells["ID"].Value;

        //    frmUpdateMotorData frmUpd = new frmUpdateMotorData(this);
        //    frmUpd.txtUpdMotorRuleHeadID.Text = iRuleHeadID.ToString();
        //    frmUpd.cbUpdMotorPartNum.Text = sPartNum;
        //    frmUpd.txtUpdMotorPartDesc.Text = sPartDesc;
        //    frmUpd.cbUpdMotor_MotorType.Text = sMotorType;
        //    frmUpd.txtUpdMotorFLA.Text = sFLA;
        //    frmUpd.txtUpdMotorHertz.Text = dHertz.ToString();
        //    frmUpd.txtUpdMotorPhase.Text = iPhase.ToString();
        //    frmUpd.txtUpdMotorMCC.Text = dMCC.ToString();
        //    frmUpd.txtUpdMotorRLA.Text = dRLA.ToString();
            
        //    if (sVoltage == "208")
        //    {
        //        frmUpd.rbUpdMotor208_60_3.Checked = true;
        //    }
        //    else if (sVoltage == "460")
        //    {
        //        frmUpd.rbUpdMotor460_60_3.Checked = true;
        //    }
        //    else
        //    {
        //        frmUpd.rbUpdMotor575_60_3.Checked = true;
        //    }

        //    //frmUpd.txtUpdMotorVoltage.Text = sVoltage;
        //    frmUpd.txtUpdMotorHP.Text = dHP.ToString();
        //    frmUpd.txtUpdMotorModifedBy.Text = sModBy;
        //    frmUpd.txtUpdMotorLastModDate.Text = sLastModDate;
        //    frmUpd.lbUpdMotorID.Text = iID.ToString();
        //    frmUpd.btnUpdMotorSave.Text = "Update";

        //    frmUpd.ShowDialog();

        //    if (mVoltage != "NoUpdate")
        //    {
        //        dgvMotorData.Rows[e.RowIndex].Cells["MotorType"].Value = mMotorType;
        //        dgvMotorData.Rows[e.RowIndex].Cells["FLA"].Value = mFLA;
        //        dgvMotorData.Rows[e.RowIndex].Cells["Hertz"].Value = Decimal.Parse(mHertz);
        //        dgvMotorData.Rows[e.RowIndex].Cells["Phase"].Value = Decimal.Parse(mPhase);
        //        dgvMotorData.Rows[e.RowIndex].Cells["MCC"].Value = Decimal.Parse(mMCC);
        //        dgvMotorData.Rows[e.RowIndex].Cells["RLA"].Value = Decimal.Parse(mRLA);
        //        dgvMotorData.Rows[e.RowIndex].Cells["Voltage"].Value = mVoltage;
        //        dgvMotorData.Rows[e.RowIndex].Cells["HP"].Value = Decimal.Parse(mHP);
        //        dgvMotorData.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
        //        dgvMotorData.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
        //        dgvMotorData.Refresh();
        //    }
        //    else
        //    {
        //        //Re-Load Motor Data DataGridView

        //        objMotor.RuleHeadID = "-1";
        //        DataTable dt = objMotor.GetROA_AllMotorDataDTL();
        //        populateMotorDataList(dt, true);
        //    }
        //}       

        //private void btnMotorDataAdd_Click(object sender, EventArgs e)
        //{
        //    frmUpdateMotorData frmAdd = new frmUpdateMotorData(this);
        //    DataTable dt = objPart.GetOAU_PartNumbersByCategory("Motor,Off-Site Mtr,Wheels");
        //    frmAdd.cbUpdMotorPartNum.DataSource = dt;
        //    frmAdd.cbUpdMotorPartNum.ValueMember = "PartNum";
        //    frmAdd.cbUpdMotorPartNum.DisplayMember = "PartNumDesc";

        //    frmAdd.cbUpdMotorPartNum.Enabled = true;
        //    frmAdd.btnUpdMotorSave.Text = "Add";
        //    frmAdd.ShowDialog();
        //    if (mVoltage != "NoUpdate")
        //    {
        //        txtMotorDataSearchPartNum.Text = "";
        //        DataTable dtMtr = objMotor.GetROA_AllMotorDataDTL();
        //        populateMotorDataList(dtMtr, false);
        //    }
        //}        

        //private void btnMotorDataReset_Click(object sender, EventArgs e)
        //{
        //    txtMotorDataSearchPartNum.Text = "";
        //    objMotor.RuleHeadID = "-1";
        //    DataTable dt = objMotor.GetROA_MotorDataDTL();
        //    populateMotorDataList(dt, false);
        //}

        #endregion       

        #region FurnaceDataDTL Events
        //private void btnFurnaceDataReset_Click(object sender, EventArgs e)
        //{
        //    txtFurnaceDataSearchPartNum.Text = "";
        //    objFurn.RuleHeadID = "-1";
        //    DataTable dt = objFurn.GetROA_FurnaceDataDTL();
        //    populateFurnaceDataList(dt, false);
        //}

        //private void btnFurnaceDataAdd_Click(object sender, EventArgs e)
        //{
        //    frmUpdateFurnaceData frmAdd = new frmUpdateFurnaceData(this);
        //    DataTable dt = objPart.GetOAU_PartNumbersByCategory("Furnace/Heater,Pre-Heat");
        //    frmAdd.cbUpdFurnacePartNum.DataSource = dt;
        //    frmAdd.cbUpdFurnacePartNum.ValueMember = "RuleHeadID";
        //    frmAdd.cbUpdFurnacePartNum.DisplayMember = "PartNumDesc";

        //    frmAdd.cbUpdFurnacePartNum.Enabled = true;
        //    frmAdd.btnUpdFurnaceSave.Text = "Add";
        //    frmAdd.ShowDialog();
        //    if (mVolts != "NoUpdate")
        //    {
        //        txtFurnaceDataSearchPartNum.Text = "";
        //        objFurn.RuleHeadID = "-1";
        //        DataTable dtFurn = objFurn.GetROA_FurnaceDataDTL();
        //        populateFurnaceDataList(dtFurn, false);
        //    }
        //}

        //private void dgvFurnaceData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    string sPartNum;
        //    string sPartDesc;
        //    string sBurnerRatio;
        //    string sModBy;
        //    string sLastModDate;
        //    string sFuelType;
        //    string sMBHkW;

        //    int iRuleHeadID;
        //    int iPhase;
        //    int iID;

        //    decimal dAmps;
        //    decimal dVolts;

        //    iRuleHeadID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells[1].Value;
        //    sPartNum = dgvFurnaceData.Rows[e.RowIndex].Cells[2].Value.ToString();
        //    sPartDesc = dgvFurnaceData.Rows[e.RowIndex].Cells[3].Value.ToString();
        //    dAmps = (decimal)dgvFurnaceData.Rows[e.RowIndex].Cells[4].Value;
        //    dVolts = (decimal)dgvFurnaceData.Rows[e.RowIndex].Cells[5].Value;
        //    sBurnerRatio = dgvFurnaceData.Rows[e.RowIndex].Cells[6].Value.ToString();
        //    iPhase = (int)dgvFurnaceData.Rows[e.RowIndex].Cells[7].Value;
        //    sFuelType = dgvFurnaceData.Rows[e.RowIndex].Cells[8].Value.ToString();
        //    sMBHkW = dgvFurnaceData.Rows[e.RowIndex].Cells[9].Value.ToString();
        //    sModBy = dgvFurnaceData.Rows[e.RowIndex].Cells[10].Value.ToString();
        //    sLastModDate = dgvFurnaceData.Rows[e.RowIndex].Cells[11].Value.ToString();
        //    iID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells[12].Value;

        //    frmUpdateFurnaceData frmUpd = new frmUpdateFurnaceData(this);
        //    frmUpd.txtUpdFurnaceRuleHeadID.Text = iRuleHeadID.ToString();
        //    frmUpd.cbUpdFurnacePartNum.Text = sPartNum;
        //    frmUpd.txtUpdFurnacePartDesc.Text = sPartDesc;
        //    frmUpd.txtUpdFurnaceAmps.Text = dAmps.ToString();
        //    frmUpd.txtUpdFurnaceBurnerRatio.Text = sBurnerRatio;
        //    frmUpd.txtUpdFurnacePhase.Text = iPhase.ToString();

        //    if (dVolts == 120.00M)
        //    {
        //        frmUpd.rbUpdFurnaceVolts120.Checked = true;
        //    }
        //    else if (dVolts == 208.00M)
        //    {
        //        frmUpd.rbUpdFurnaceVolts208.Checked = true;
        //    }
        //    else if (dVolts == 460.00M)
        //    {
        //        frmUpd.rbUpdFurnaceVolts460.Checked = true;
        //    }
        //    else
        //    {
        //        frmUpd.rbUpdFurnaceVolts575.Checked = true;
        //    }

        //    if (sFuelType == "ELEC")
        //    {
        //        frmUpd.rbUpdFurnaceElectric.Checked = true;
        //    }
        //    else if (sFuelType == "LP")
        //    {
        //        frmUpd.rbUpdFurnacePropane.Checked = true;
        //    }
        //    else
        //    {
        //        frmUpd.rbUpdFurnaceNaturalGas.Checked = true;
        //    }

        //    frmUpd.txtUpdFurnaceMBHkW.Text = sMBHkW;
        //    frmUpd.txtUpdFurnaceModifedBy.Text = sModBy;
        //    frmUpd.txtUpdFurnaceLastModDate.Text = sLastModDate;
        //    frmUpd.lbUpdFurnaceID.Text = iID.ToString();                    

        //    if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
        //    {
        //        frmUpd.btnUpdFurnaceSave.Text = "Delete";                 
        //        frmUpd.ShowDialog();
        //    }

        //    if (mVolts != "NoUpdate")
        //    {
        //        txtFurnaceDataSearchPartNum.Text = "";
        //        objFurn.RuleHeadID = "-1";
        //        DataTable dtFurn = objFurn.GetROA_FurnaceDataDTL();
        //        populateFurnaceDataList(dtFurn, false);
        //    }
        //}

        //private void dgvFurnaceData_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    string sPartNum;
        //    string sPartDesc;
        //    string sBurnerRatio;
        //    string sModBy;
        //    string sLastModDate;
        //    string sFuelType;
        //    string sMBHkW;

        //    int iRuleHeadID;
        //    int iPhase;
        //    int iID;

        //    decimal dAmps;
        //    decimal dVolts;                       

        //    iRuleHeadID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells["RuleHeadID"].Value;
        //    sPartNum = dgvFurnaceData.Rows[e.RowIndex].Cells["PartNum"].Value.ToString();
        //    sPartDesc = dgvFurnaceData.Rows[e.RowIndex].Cells["PartDescription"].Value.ToString();
        //    dAmps = (decimal)dgvFurnaceData.Rows[e.RowIndex].Cells["Amps"].Value;
        //    dVolts = (decimal)dgvFurnaceData.Rows[e.RowIndex].Cells["Volts"].Value;
        //    sBurnerRatio = dgvFurnaceData.Rows[e.RowIndex].Cells["BurnerRatio"].Value.ToString();
        //    iPhase = (int)dgvFurnaceData.Rows[e.RowIndex].Cells["Phase"].Value;
        //    sFuelType = dgvFurnaceData.Rows[e.RowIndex].Cells["FuelType"].Value.ToString();
        //    sMBHkW = dgvFurnaceData.Rows[e.RowIndex].Cells["MBHkW"].Value.ToString();           
        //    sModBy = dgvFurnaceData.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
        //    sLastModDate = dgvFurnaceData.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
        //    iID = (int)dgvFurnaceData.Rows[e.RowIndex].Cells["ID"].Value;

        //    frmUpdateFurnaceData frmUpd = new frmUpdateFurnaceData(this);
        //    frmUpd.txtUpdFurnaceRuleHeadID.Text = iRuleHeadID.ToString();
        //    frmUpd.cbUpdFurnacePartNum.Text = sPartNum;
        //    frmUpd.txtUpdFurnacePartDesc.Text = sPartDesc;
        //    frmUpd.txtUpdFurnaceAmps.Text = dAmps.ToString();           
        //    frmUpd.txtUpdFurnaceBurnerRatio.Text = sBurnerRatio;
        //    frmUpd.txtUpdFurnacePhase.Text = iPhase.ToString();

        //    if (dVolts == 120.00M)
        //    {
        //        frmUpd.rbUpdFurnaceVolts120.Checked = true;
        //    }
        //    else  if (dVolts == 208.00M)
        //    {
        //        frmUpd.rbUpdFurnaceVolts208.Checked = true;
        //    }
        //    else  if (dVolts == 460.00M)
        //    {
        //        frmUpd.rbUpdFurnaceVolts460.Checked = true;
        //    }
        //    else 
        //    {
        //        frmUpd.rbUpdFurnaceVolts575.Checked = true;
        //    }

        //    if (sFuelType == "ELEC")
        //    {
        //        frmUpd.rbUpdFurnaceElectric.Checked = true;
        //    }
        //    else if (sFuelType == "LP")
        //    {
        //        frmUpd.rbUpdFurnacePropane.Checked = true;
        //    }
        //    else
        //    {
        //        frmUpd.rbUpdFurnaceNaturalGas.Checked = true;
        //    }
           
        //    frmUpd.txtUpdFurnaceMBHkW.Text = sMBHkW;            
        //    frmUpd.txtUpdFurnaceModifedBy.Text = sModBy;
        //    frmUpd.txtUpdFurnaceLastModDate.Text = sLastModDate;
        //    frmUpd.lbUpdFurnaceID.Text = iID.ToString();
        //    frmUpd.btnUpdFurnaceSave.Text = "Update";

        //    frmUpd.ShowDialog();

        //    if (mVolts != "NoUpdate")
        //    {
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["Amps"].Value = Decimal.Parse(mAmps);
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["Volts"].Value = Decimal.Parse(mVolts);
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["BurnerRatio"].Value = mBurnerRatio;
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["Phase"].Value = Int32.Parse(mPhase);
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["FuelType"].Value = mFuelType;
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["MBHkW"].Value = mMBHkW;                
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
        //        dgvFurnaceData.Rows[e.RowIndex].Cells["ModDate"].Value = mLastModDate;
        //        dgvFurnaceData.Refresh();
        //    }
        //}

        #endregion  
      
        #region PartCategoryHead Events

        private void btnPartCatReset_Click(object sender, EventArgs e)
        {
            objCat.PullOrder = "-1";
            DataTable dt = objCat.GetPartCategoryHead();
            populatePartCategoryDataList(dt, false);
        }

        private void btnPartCatAdd_Click(object sender, EventArgs e)
        {
            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.btnUpdPartCatSave.Text = "Add";
            frmUpd.ShowDialog();
           

            if (mVolts != "NoUpdate")
            {
                objCat.PullOrder = "-1";
                DataTable dt = objCat.GetPartCategoryHead();
                populatePartCategoryDataList(dt, false);
            }
        }       

        private void dgvPartCat_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string sCategoryName;
            string sCategoryDesc;
            string sModBy;
            string sLastModDate;
            string sCritComp;

            int iPullOrder;
            int iID;

            sCategoryName = dgvPartCat.Rows[e.RowIndex].Cells["CategoryName"].Value.ToString();
            sCategoryDesc = dgvPartCat.Rows[e.RowIndex].Cells["CategoryDesc"].Value.ToString();
            iPullOrder = (int)dgvPartCat.Rows[e.RowIndex].Cells["PullOrder"].Value;
            sCritComp = dgvPartCat.Rows[e.RowIndex].Cells["CriticalComp"].Value.ToString();
            sModBy = dgvPartCat.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            sLastModDate = dgvPartCat.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            iID = (int)dgvPartCat.Rows[e.RowIndex].Cells["ID"].Value;

            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.lbUpdPartCatID.Text = iID.ToString();
            frmUpd.txtUpdPartCat_CategoryName.Text = sCategoryName;
            frmUpd.txtUpdPartCat_CategoryDesc.Text = sCategoryDesc;
            frmUpd.txtUpdPartCatPullOrder.Text = iPullOrder.ToString();
            frmUpd.txtUpdPartCatModBy.Text = sModBy;
            frmUpd.txtUpdPartCatLastModDate.Text = sLastModDate;

            if (sCritComp == "True")
            {
                frmUpd.cbUpdPartCatCritComp.Checked = true;
            }
            else
            {
                frmUpd.cbUpdPartCatCritComp.Checked = false;
            }


            if (e.ColumnIndex == 0)  // Delete button internally is really in column #0 not 10 as it appears on the screen.
            {
                frmUpd.btnUpdPartCatSave.Text = "Delete";
                frmUpd.ShowDialog();
            }

            if (mPullOrder != "NoUpdate")
            {
                objCat.PullOrder = "-1";
                DataTable dt = objCat.GetPartCategoryHead();
                populatePartCategoryDataList(dt, false);
            }
        }

        private void dgvPartCat_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string sCategoryName;
            string sCategoryDesc;            
            string sModBy;
            string sLastModDate;
            string sCritComp;
           
            int iPullOrder;
            int iID;
                     
            sCategoryName = dgvPartCat.Rows[e.RowIndex].Cells["CategoryName"].Value.ToString();
            sCategoryDesc = dgvPartCat.Rows[e.RowIndex].Cells["CategoryDesc"].Value.ToString();
            iPullOrder = (int)dgvPartCat.Rows[e.RowIndex].Cells["PullOrder"].Value;
            sCritComp = dgvPartCat.Rows[e.RowIndex].Cells["CriticalComp"].Value.ToString();
            sModBy = dgvPartCat.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
            sLastModDate = dgvPartCat.Rows[e.RowIndex].Cells["DateMod"].Value.ToString();
            iID = (int)dgvPartCat.Rows[e.RowIndex].Cells["ID"].Value;

            frmUpdatePartCat frmUpd = new frmUpdatePartCat(this);
            frmUpd.lbUpdPartCatID.Text = iID.ToString();
            frmUpd.txtUpdPartCat_CategoryName.Text = sCategoryName;
            frmUpd.txtUpdPartCat_CategoryDesc.Text = sCategoryDesc;
            frmUpd.txtUpdPartCatPullOrder.Text = iPullOrder.ToString();

            if (sCritComp == "True")
            {
                frmUpd.cbUpdPartCatCritComp.Checked = true;
            }
            else
            {
                frmUpd.cbUpdPartCatCritComp.Checked = false;
            }

            frmUpd.txtUpdPartCatModBy.Text = sModBy;
            frmUpd.txtUpdPartCatLastModDate.Text = sLastModDate;            
            frmUpd.btnUpdPartCatSave.Text = "Update";

            frmUpd.ShowDialog();

            if (mPullOrder != "NoUpdate")
            {
                dgvPartCat.Rows[e.RowIndex].Cells["CategoryName"].Value = mCategoryName;
                dgvPartCat.Rows[e.RowIndex].Cells["CategoryDesc"].Value = mCategoryDesc;
                dgvPartCat.Rows[e.RowIndex].Cells["PullOrder"].Value = iPullOrder;
                dgvPartCat.Rows[e.RowIndex].Cells["CriticalComp"].Value = mCritComp;
                dgvPartCat.Rows[e.RowIndex].Cells["UserName"].Value = mModBy;
                dgvPartCat.Rows[e.RowIndex].Cells["DateMod"].Value = mLastModDate;
                dgvPartCat.Refresh();
            }
        }
        #endregion              
               
       
    }
}
