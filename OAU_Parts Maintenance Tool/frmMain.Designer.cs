﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPagePartRuleHead = new System.Windows.Forms.TabPage();
            this.btnPRH_Reset = new System.Windows.Forms.Button();
            this.btnPRH_AddPart = new System.Windows.Forms.Button();
            this.txtPRH_SearchPartNum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvPartRulesHead = new System.Windows.Forms.DataGridView();
            this.tabPagePartConditions = new System.Windows.Forms.TabPage();
            this.btnCreateSSRules = new System.Windows.Forms.Button();
            this.dgvPartsMain = new System.Windows.Forms.DataGridView();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnAddPart = new System.Windows.Forms.Button();
            this.txtSearchPartNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPageModelNumDesc = new System.Windows.Forms.TabPage();
            this.btnModelNoCopy = new System.Windows.Forms.Button();
            this.btnModelNoAddRow = new System.Windows.Forms.Button();
            this.gbModelNoHeatType = new System.Windows.Forms.GroupBox();
            this.rbModelNoHeatTypeHotWater = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeNA = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeDF = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeElec = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeIF = new System.Windows.Forms.RadioButton();
            this.rbModelNoHeatTypeAll = new System.Windows.Forms.RadioButton();
            this.btnModelNoReset = new System.Windows.Forms.Button();
            this.txtSearchModelNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvModelNo = new System.Windows.Forms.DataGridView();
            this.tabPagePartCatHead = new System.Windows.Forms.TabPage();
            this.btnPartCatAdd = new System.Windows.Forms.Button();
            this.btnPartCatReset = new System.Windows.Forms.Button();
            this.dgvPartCat = new System.Windows.Forms.DataGridView();
            this.btnMainExit = new System.Windows.Forms.Button();
            this.lbMainTitle = new System.Windows.Forms.Label();
            this.lbEnvironment = new System.Windows.Forms.Label();
            this.tabMain.SuspendLayout();
            this.tabPagePartRuleHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartRulesHead)).BeginInit();
            this.tabPagePartConditions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartsMain)).BeginInit();
            this.tabPageModelNumDesc.SuspendLayout();
            this.gbModelNoHeatType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelNo)).BeginInit();
            this.tabPagePartCatHead.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartCat)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPagePartRuleHead);
            this.tabMain.Controls.Add(this.tabPagePartConditions);
            this.tabMain.Controls.Add(this.tabPageModelNumDesc);
            this.tabMain.Controls.Add(this.tabPagePartCatHead);
            this.tabMain.Location = new System.Drawing.Point(13, 68);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1356, 644);
            this.tabMain.TabIndex = 0;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabPagePartRuleHead
            // 
            this.tabPagePartRuleHead.Controls.Add(this.btnPRH_Reset);
            this.tabPagePartRuleHead.Controls.Add(this.btnPRH_AddPart);
            this.tabPagePartRuleHead.Controls.Add(this.txtPRH_SearchPartNum);
            this.tabPagePartRuleHead.Controls.Add(this.label4);
            this.tabPagePartRuleHead.Controls.Add(this.dgvPartRulesHead);
            this.tabPagePartRuleHead.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartRuleHead.Name = "tabPagePartRuleHead";
            this.tabPagePartRuleHead.Size = new System.Drawing.Size(1348, 618);
            this.tabPagePartRuleHead.TabIndex = 8;
            this.tabPagePartRuleHead.Text = "Part Rule Head";
            this.tabPagePartRuleHead.UseVisualStyleBackColor = true;
            // 
            // btnPRH_Reset
            // 
            this.btnPRH_Reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRH_Reset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPRH_Reset.Location = new System.Drawing.Point(1121, 10);
            this.btnPRH_Reset.Name = "btnPRH_Reset";
            this.btnPRH_Reset.Size = new System.Drawing.Size(88, 23);
            this.btnPRH_Reset.TabIndex = 10;
            this.btnPRH_Reset.Text = "Reset";
            this.btnPRH_Reset.UseVisualStyleBackColor = true;
            this.btnPRH_Reset.Click += new System.EventHandler(this.btnPRH_Reset_Click);
            // 
            // btnPRH_AddPart
            // 
            this.btnPRH_AddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPRH_AddPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPRH_AddPart.Location = new System.Drawing.Point(1242, 10);
            this.btnPRH_AddPart.Name = "btnPRH_AddPart";
            this.btnPRH_AddPart.Size = new System.Drawing.Size(88, 23);
            this.btnPRH_AddPart.TabIndex = 11;
            this.btnPRH_AddPart.Text = "Add Part";
            this.btnPRH_AddPart.UseVisualStyleBackColor = true;
            this.btnPRH_AddPart.Click += new System.EventHandler(this.btnPRH_AddPart_Click);
            // 
            // txtPRH_SearchPartNum
            // 
            this.txtPRH_SearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPRH_SearchPartNum.Location = new System.Drawing.Point(643, 10);
            this.txtPRH_SearchPartNum.Name = "txtPRH_SearchPartNum";
            this.txtPRH_SearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtPRH_SearchPartNum.TabIndex = 9;
            this.txtPRH_SearchPartNum.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPRH_SearchPartNum_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(475, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Search Part Number:";
            // 
            // dgvPartRulesHead
            // 
            this.dgvPartRulesHead.AllowUserToAddRows = false;
            this.dgvPartRulesHead.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            this.dgvPartRulesHead.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPartRulesHead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartRulesHead.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPartRulesHead.Location = new System.Drawing.Point(7, 40);
            this.dgvPartRulesHead.MultiSelect = false;
            this.dgvPartRulesHead.Name = "dgvPartRulesHead";
            this.dgvPartRulesHead.ReadOnly = true;
            this.dgvPartRulesHead.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartRulesHead.Size = new System.Drawing.Size(1327, 564);
            this.dgvPartRulesHead.TabIndex = 6;
            this.dgvPartRulesHead.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartRulesHead_CellMouseDoubleClick);
            // 
            // tabPagePartConditions
            // 
            this.tabPagePartConditions.Controls.Add(this.btnCreateSSRules);
            this.tabPagePartConditions.Controls.Add(this.dgvPartsMain);
            this.tabPagePartConditions.Controls.Add(this.btnClear);
            this.tabPagePartConditions.Controls.Add(this.btnReset);
            this.tabPagePartConditions.Controls.Add(this.btnCopy);
            this.tabPagePartConditions.Controls.Add(this.btnAddPart);
            this.tabPagePartConditions.Controls.Add(this.txtSearchPartNum);
            this.tabPagePartConditions.Controls.Add(this.label1);
            this.tabPagePartConditions.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartConditions.Name = "tabPagePartConditions";
            this.tabPagePartConditions.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePartConditions.Size = new System.Drawing.Size(1348, 618);
            this.tabPagePartConditions.TabIndex = 0;
            this.tabPagePartConditions.Text = "Part Conditions";
            this.tabPagePartConditions.UseVisualStyleBackColor = true;
            // 
            // btnCreateSSRules
            // 
            this.btnCreateSSRules.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateSSRules.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCreateSSRules.Location = new System.Drawing.Point(18, 9);
            this.btnCreateSSRules.Name = "btnCreateSSRules";
            this.btnCreateSSRules.Size = new System.Drawing.Size(125, 23);
            this.btnCreateSSRules.TabIndex = 7;
            this.btnCreateSSRules.Text = "Create SS Rules";
            this.btnCreateSSRules.UseVisualStyleBackColor = true;
            this.btnCreateSSRules.Click += new System.EventHandler(this.btnCreateSSRules_Click);
            // 
            // dgvPartsMain
            // 
            this.dgvPartsMain.AllowUserToAddRows = false;
            this.dgvPartsMain.AllowUserToDeleteRows = false;
            this.dgvPartsMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartsMain.Location = new System.Drawing.Point(7, 40);
            this.dgvPartsMain.Name = "dgvPartsMain";
            this.dgvPartsMain.ReadOnly = true;
            this.dgvPartsMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartsMain.Size = new System.Drawing.Size(1333, 572);
            this.dgvPartsMain.TabIndex = 6;
            this.dgvPartsMain.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPartsMain_CellContentClick);
            this.dgvPartsMain.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartsMain_CellMouseDoubleClick);
            this.dgvPartsMain.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvPartsMain_DataBindingComplete);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnClear.Location = new System.Drawing.Point(1027, 6);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 23);
            this.btnClear.TabIndex = 2;
            this.btnClear.Text = "Clear ";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnReset.Location = new System.Drawing.Point(908, 6);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(88, 23);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnCopy
            // 
            this.btnCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopy.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnCopy.Location = new System.Drawing.Point(1252, 6);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(88, 23);
            this.btnCopy.TabIndex = 5;
            this.btnCopy.Text = "Copy ";
            this.btnCopy.UseVisualStyleBackColor = true;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnAddPart
            // 
            this.btnAddPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnAddPart.Location = new System.Drawing.Point(1140, 6);
            this.btnAddPart.Name = "btnAddPart";
            this.btnAddPart.Size = new System.Drawing.Size(88, 23);
            this.btnAddPart.TabIndex = 4;
            this.btnAddPart.Text = "Add Part";
            this.btnAddPart.UseVisualStyleBackColor = true;
            this.btnAddPart.Click += new System.EventHandler(this.btnAddPart_Click);
            // 
            // txtSearchPartNum
            // 
            this.txtSearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchPartNum.Location = new System.Drawing.Point(560, 8);
            this.txtSearchPartNum.Name = "txtSearchPartNum";
            this.txtSearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtSearchPartNum.TabIndex = 2;
            this.txtSearchPartNum.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchPartNum_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(392, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Search Part Number:";
            // 
            // tabPageModelNumDesc
            // 
            this.tabPageModelNumDesc.Controls.Add(this.btnModelNoCopy);
            this.tabPageModelNumDesc.Controls.Add(this.btnModelNoAddRow);
            this.tabPageModelNumDesc.Controls.Add(this.gbModelNoHeatType);
            this.tabPageModelNumDesc.Controls.Add(this.btnModelNoReset);
            this.tabPageModelNumDesc.Controls.Add(this.txtSearchModelNo);
            this.tabPageModelNumDesc.Controls.Add(this.label2);
            this.tabPageModelNumDesc.Controls.Add(this.dgvModelNo);
            this.tabPageModelNumDesc.Location = new System.Drawing.Point(4, 22);
            this.tabPageModelNumDesc.Name = "tabPageModelNumDesc";
            this.tabPageModelNumDesc.Size = new System.Drawing.Size(1348, 618);
            this.tabPageModelNumDesc.TabIndex = 3;
            this.tabPageModelNumDesc.Text = "ModelNoDesc";
            this.tabPageModelNumDesc.UseVisualStyleBackColor = true;
            // 
            // btnModelNoCopy
            // 
            this.btnModelNoCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoCopy.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoCopy.Location = new System.Drawing.Point(734, 56);
            this.btnModelNoCopy.Name = "btnModelNoCopy";
            this.btnModelNoCopy.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoCopy.TabIndex = 10;
            this.btnModelNoCopy.Text = "Copy Row";
            this.btnModelNoCopy.UseVisualStyleBackColor = true;
            this.btnModelNoCopy.Click += new System.EventHandler(this.btnModelNoCopy_Click);
            // 
            // btnModelNoAddRow
            // 
            this.btnModelNoAddRow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoAddRow.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoAddRow.Location = new System.Drawing.Point(734, 29);
            this.btnModelNoAddRow.Name = "btnModelNoAddRow";
            this.btnModelNoAddRow.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoAddRow.TabIndex = 9;
            this.btnModelNoAddRow.Text = "Add Row";
            this.btnModelNoAddRow.UseVisualStyleBackColor = true;
            this.btnModelNoAddRow.Click += new System.EventHandler(this.btnModelNoAddRow_Click);
            // 
            // gbModelNoHeatType
            // 
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeHotWater);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeNA);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeDF);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeElec);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeIF);
            this.gbModelNoHeatType.Controls.Add(this.rbModelNoHeatTypeAll);
            this.gbModelNoHeatType.ForeColor = System.Drawing.Color.Red;
            this.gbModelNoHeatType.Location = new System.Drawing.Point(384, 6);
            this.gbModelNoHeatType.Name = "gbModelNoHeatType";
            this.gbModelNoHeatType.Size = new System.Drawing.Size(274, 70);
            this.gbModelNoHeatType.TabIndex = 7;
            this.gbModelNoHeatType.TabStop = false;
            this.gbModelNoHeatType.Text = "Heat Type";
            // 
            // rbModelNoHeatTypeHotWater
            // 
            this.rbModelNoHeatTypeHotWater.AutoSize = true;
            this.rbModelNoHeatTypeHotWater.Location = new System.Drawing.Point(188, 16);
            this.rbModelNoHeatTypeHotWater.Name = "rbModelNoHeatTypeHotWater";
            this.rbModelNoHeatTypeHotWater.Size = new System.Drawing.Size(74, 17);
            this.rbModelNoHeatTypeHotWater.TabIndex = 10;
            this.rbModelNoHeatTypeHotWater.Text = "Hot Water";
            this.rbModelNoHeatTypeHotWater.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeHotWater.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeHotWater_CheckedChanged);
            // 
            // rbModelNoHeatTypeNA
            // 
            this.rbModelNoHeatTypeNA.AutoSize = true;
            this.rbModelNoHeatTypeNA.Location = new System.Drawing.Point(7, 39);
            this.rbModelNoHeatTypeNA.Name = "rbModelNoHeatTypeNA";
            this.rbModelNoHeatTypeNA.Size = new System.Drawing.Size(40, 17);
            this.rbModelNoHeatTypeNA.TabIndex = 9;
            this.rbModelNoHeatTypeNA.Text = "NA";
            this.rbModelNoHeatTypeNA.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeNA.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeNA_CheckedChanged);
            // 
            // rbModelNoHeatTypeDF
            // 
            this.rbModelNoHeatTypeDF.AutoSize = true;
            this.rbModelNoHeatTypeDF.Location = new System.Drawing.Point(188, 39);
            this.rbModelNoHeatTypeDF.Name = "rbModelNoHeatTypeDF";
            this.rbModelNoHeatTypeDF.Size = new System.Drawing.Size(79, 17);
            this.rbModelNoHeatTypeDF.TabIndex = 8;
            this.rbModelNoHeatTypeDF.Text = "Direct Fired";
            this.rbModelNoHeatTypeDF.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeDF.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeDF_CheckedChanged);
            // 
            // rbModelNoHeatTypeElec
            // 
            this.rbModelNoHeatTypeElec.AutoSize = true;
            this.rbModelNoHeatTypeElec.Location = new System.Drawing.Point(80, 16);
            this.rbModelNoHeatTypeElec.Name = "rbModelNoHeatTypeElec";
            this.rbModelNoHeatTypeElec.Size = new System.Drawing.Size(60, 17);
            this.rbModelNoHeatTypeElec.TabIndex = 8;
            this.rbModelNoHeatTypeElec.Text = "Electric";
            this.rbModelNoHeatTypeElec.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeElec.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeElec_CheckedChanged);
            // 
            // rbModelNoHeatTypeIF
            // 
            this.rbModelNoHeatTypeIF.AutoSize = true;
            this.rbModelNoHeatTypeIF.Location = new System.Drawing.Point(80, 39);
            this.rbModelNoHeatTypeIF.Name = "rbModelNoHeatTypeIF";
            this.rbModelNoHeatTypeIF.Size = new System.Drawing.Size(86, 17);
            this.rbModelNoHeatTypeIF.TabIndex = 8;
            this.rbModelNoHeatTypeIF.Text = "Indirect Fired";
            this.rbModelNoHeatTypeIF.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeIF.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeIF_CheckedChanged);
            // 
            // rbModelNoHeatTypeAll
            // 
            this.rbModelNoHeatTypeAll.AutoSize = true;
            this.rbModelNoHeatTypeAll.Checked = true;
            this.rbModelNoHeatTypeAll.Location = new System.Drawing.Point(7, 16);
            this.rbModelNoHeatTypeAll.Name = "rbModelNoHeatTypeAll";
            this.rbModelNoHeatTypeAll.Size = new System.Drawing.Size(44, 17);
            this.rbModelNoHeatTypeAll.TabIndex = 0;
            this.rbModelNoHeatTypeAll.TabStop = true;
            this.rbModelNoHeatTypeAll.Text = "ALL";
            this.rbModelNoHeatTypeAll.UseVisualStyleBackColor = true;
            this.rbModelNoHeatTypeAll.CheckedChanged += new System.EventHandler(this.rbModelNoHeatTypeAll_CheckedChanged);
            // 
            // btnModelNoReset
            // 
            this.btnModelNoReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModelNoReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnModelNoReset.Location = new System.Drawing.Point(734, 3);
            this.btnModelNoReset.Name = "btnModelNoReset";
            this.btnModelNoReset.Size = new System.Drawing.Size(88, 23);
            this.btnModelNoReset.TabIndex = 6;
            this.btnModelNoReset.Text = "Reset";
            this.btnModelNoReset.UseVisualStyleBackColor = true;
            this.btnModelNoReset.Click += new System.EventHandler(this.btnModelNoReset_Click);
            // 
            // txtSearchModelNo
            // 
            this.txtSearchModelNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchModelNo.Location = new System.Drawing.Point(239, 36);
            this.txtSearchModelNo.Name = "txtSearchModelNo";
            this.txtSearchModelNo.Size = new System.Drawing.Size(61, 21);
            this.txtSearchModelNo.TabIndex = 4;
            this.txtSearchModelNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchModelNo_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(114, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Search Digit No:";
            // 
            // dgvModelNo
            // 
            this.dgvModelNo.AllowUserToAddRows = false;
            this.dgvModelNo.AllowUserToDeleteRows = false;
            this.dgvModelNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModelNo.Location = new System.Drawing.Point(7, 82);
            this.dgvModelNo.MultiSelect = false;
            this.dgvModelNo.Name = "dgvModelNo";
            this.dgvModelNo.ReadOnly = true;
            this.dgvModelNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvModelNo.Size = new System.Drawing.Size(817, 525);
            this.dgvModelNo.TabIndex = 0;
            this.dgvModelNo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvModelNo_CellMouseDoubleClick);
            this.dgvModelNo.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvModelNo_DataBindingComplete);
            // 
            // tabPagePartCatHead
            // 
            this.tabPagePartCatHead.Controls.Add(this.btnPartCatAdd);
            this.tabPagePartCatHead.Controls.Add(this.btnPartCatReset);
            this.tabPagePartCatHead.Controls.Add(this.dgvPartCat);
            this.tabPagePartCatHead.Location = new System.Drawing.Point(4, 22);
            this.tabPagePartCatHead.Name = "tabPagePartCatHead";
            this.tabPagePartCatHead.Size = new System.Drawing.Size(1348, 618);
            this.tabPagePartCatHead.TabIndex = 4;
            this.tabPagePartCatHead.Text = "PartCategory";
            this.tabPagePartCatHead.UseVisualStyleBackColor = true;
            // 
            // btnPartCatAdd
            // 
            this.btnPartCatAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartCatAdd.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPartCatAdd.Location = new System.Drawing.Point(746, 8);
            this.btnPartCatAdd.Name = "btnPartCatAdd";
            this.btnPartCatAdd.Size = new System.Drawing.Size(100, 23);
            this.btnPartCatAdd.TabIndex = 41;
            this.btnPartCatAdd.Text = "Add Category";
            this.btnPartCatAdd.UseVisualStyleBackColor = true;
            this.btnPartCatAdd.Click += new System.EventHandler(this.btnPartCatAdd_Click);
            // 
            // btnPartCatReset
            // 
            this.btnPartCatReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPartCatReset.ForeColor = System.Drawing.Color.RoyalBlue;
            this.btnPartCatReset.Location = new System.Drawing.Point(612, 8);
            this.btnPartCatReset.Name = "btnPartCatReset";
            this.btnPartCatReset.Size = new System.Drawing.Size(100, 23);
            this.btnPartCatReset.TabIndex = 38;
            this.btnPartCatReset.Text = "Reset";
            this.btnPartCatReset.UseVisualStyleBackColor = true;
            this.btnPartCatReset.Click += new System.EventHandler(this.btnPartCatReset_Click);
            // 
            // dgvPartCat
            // 
            this.dgvPartCat.AllowUserToAddRows = false;
            this.dgvPartCat.AllowUserToDeleteRows = false;
            this.dgvPartCat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartCat.Location = new System.Drawing.Point(8, 37);
            this.dgvPartCat.MultiSelect = false;
            this.dgvPartCat.Name = "dgvPartCat";
            this.dgvPartCat.ReadOnly = true;
            this.dgvPartCat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartCat.Size = new System.Drawing.Size(839, 575);
            this.dgvPartCat.TabIndex = 37;
            this.dgvPartCat.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPartCat_CellContentClick);
            this.dgvPartCat.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartCat_CellMouseDoubleClick);
            // 
            // btnMainExit
            // 
            this.btnMainExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMainExit.ForeColor = System.Drawing.Color.Black;
            this.btnMainExit.Location = new System.Drawing.Point(1277, 10);
            this.btnMainExit.Name = "btnMainExit";
            this.btnMainExit.Size = new System.Drawing.Size(88, 38);
            this.btnMainExit.TabIndex = 1;
            this.btnMainExit.Text = "Exit";
            this.btnMainExit.UseVisualStyleBackColor = true;
            this.btnMainExit.Click += new System.EventHandler(this.btnMainExit_Click);
            // 
            // lbMainTitle
            // 
            this.lbMainTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMainTitle.Location = new System.Drawing.Point(568, 38);
            this.lbMainTitle.Name = "lbMainTitle";
            this.lbMainTitle.Size = new System.Drawing.Size(297, 24);
            this.lbMainTitle.TabIndex = 23;
            this.lbMainTitle.Text = "Rev 5 Part Maintenance Tool";
            this.lbMainTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbEnvironment
            // 
            this.lbEnvironment.Font = new System.Drawing.Font("Engravers MT", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironment.ForeColor = System.Drawing.Color.Blue;
            this.lbEnvironment.Location = new System.Drawing.Point(360, 9);
            this.lbEnvironment.Name = "lbEnvironment";
            this.lbEnvironment.Size = new System.Drawing.Size(736, 24);
            this.lbEnvironment.TabIndex = 24;
            this.lbEnvironment.Text = "Rev 5 Part Maintenance Tool";
            this.lbEnvironment.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 724);
            this.Controls.Add(this.lbEnvironment);
            this.Controls.Add(this.lbMainTitle);
            this.Controls.Add(this.btnMainExit);
            this.Controls.Add(this.tabMain);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "REV 5 OAU Parts Maintenance Tool";
            this.tabMain.ResumeLayout(false);
            this.tabPagePartRuleHead.ResumeLayout(false);
            this.tabPagePartRuleHead.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartRulesHead)).EndInit();
            this.tabPagePartConditions.ResumeLayout(false);
            this.tabPagePartConditions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartsMain)).EndInit();
            this.tabPageModelNumDesc.ResumeLayout(false);
            this.tabPageModelNumDesc.PerformLayout();
            this.gbModelNoHeatType.ResumeLayout(false);
            this.gbModelNoHeatType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelNo)).EndInit();
            this.tabPagePartCatHead.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartCat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPagePartConditions;
        private System.Windows.Forms.Button btnMainExit;
        private System.Windows.Forms.TextBox txtSearchPartNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TabPage tabPageModelNumDesc;
        private System.Windows.Forms.TabPage tabPagePartCatHead;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnAddPart;
        private System.Windows.Forms.DataGridView dgvModelNo;
        private System.Windows.Forms.TextBox txtSearchModelNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnModelNoReset;
        private System.Windows.Forms.GroupBox gbModelNoHeatType;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeDF;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeElec;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeIF;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeAll;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeNA;
        private System.Windows.Forms.Button btnModelNoAddRow;
        private System.Windows.Forms.TabPage tabPagePartRuleHead;
        private System.Windows.Forms.Button btnPRH_Reset;
        private System.Windows.Forms.Button btnPRH_AddPart;
        private System.Windows.Forms.TextBox txtPRH_SearchPartNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvPartRulesHead;
        private System.Windows.Forms.Label lbMainTitle;
        private System.Windows.Forms.Button btnPartCatAdd;
        private System.Windows.Forms.Button btnPartCatReset;
        private System.Windows.Forms.DataGridView dgvPartCat;
        private System.Windows.Forms.RadioButton rbModelNoHeatTypeHotWater;
        private System.Windows.Forms.Button btnModelNoCopy;
        private System.Windows.Forms.DataGridView dgvPartsMain;
        private System.Windows.Forms.Button btnCreateSSRules;
        private System.Windows.Forms.Label lbEnvironment;
    }
}

