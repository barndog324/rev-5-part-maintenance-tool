﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lbModDate = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbModBy = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAllRuleSets = new System.Windows.Forms.Button();
            this.btnDeleteRuleSet = new System.Windows.Forms.Button();
            this.lbScreenMode = new System.Windows.Forms.Label();
            this.cbPartNum = new System.Windows.Forms.ComboBox();
            this.btnAddNewRule = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lbSelectedRow = new System.Windows.Forms.Label();
            this.gbValues = new System.Windows.Forms.GroupBox();
            this.cbVal17 = new System.Windows.Forms.CheckBox();
            this.cbVal1 = new System.Windows.Forms.CheckBox();
            this.cbVal2 = new System.Windows.Forms.CheckBox();
            this.cbVal3 = new System.Windows.Forms.CheckBox();
            this.cbVal4 = new System.Windows.Forms.CheckBox();
            this.cbVal5 = new System.Windows.Forms.CheckBox();
            this.cbVal6 = new System.Windows.Forms.CheckBox();
            this.cbVal7 = new System.Windows.Forms.CheckBox();
            this.cbVal8 = new System.Windows.Forms.CheckBox();
            this.cbVal9 = new System.Windows.Forms.CheckBox();
            this.cbVal10 = new System.Windows.Forms.CheckBox();
            this.cbVal11 = new System.Windows.Forms.CheckBox();
            this.cbVal12 = new System.Windows.Forms.CheckBox();
            this.cbVal13 = new System.Windows.Forms.CheckBox();
            this.cbVal14 = new System.Windows.Forms.CheckBox();
            this.lbVal30 = new System.Windows.Forms.Label();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.lbVal29 = new System.Windows.Forms.Label();
            this.cbVal15 = new System.Windows.Forms.CheckBox();
            this.lbVal28 = new System.Windows.Forms.Label();
            this.cbVal16 = new System.Windows.Forms.CheckBox();
            this.lbVal27 = new System.Windows.Forms.Label();
            this.cbVal18 = new System.Windows.Forms.CheckBox();
            this.lbVal26 = new System.Windows.Forms.Label();
            this.cbVal19 = new System.Windows.Forms.CheckBox();
            this.lbVal25 = new System.Windows.Forms.Label();
            this.cbVal20 = new System.Windows.Forms.CheckBox();
            this.lbVal24 = new System.Windows.Forms.Label();
            this.cbVal21 = new System.Windows.Forms.CheckBox();
            this.lbVal23 = new System.Windows.Forms.Label();
            this.cbVal22 = new System.Windows.Forms.CheckBox();
            this.lbVal22 = new System.Windows.Forms.Label();
            this.cbVal23 = new System.Windows.Forms.CheckBox();
            this.lbVal21 = new System.Windows.Forms.Label();
            this.cbVal24 = new System.Windows.Forms.CheckBox();
            this.lbVal20 = new System.Windows.Forms.Label();
            this.cbVal25 = new System.Windows.Forms.CheckBox();
            this.lbVal19 = new System.Windows.Forms.Label();
            this.cbVal26 = new System.Windows.Forms.CheckBox();
            this.lbVal18 = new System.Windows.Forms.Label();
            this.cbVal27 = new System.Windows.Forms.CheckBox();
            this.lbVal17 = new System.Windows.Forms.Label();
            this.cbVal28 = new System.Windows.Forms.CheckBox();
            this.lbVal16 = new System.Windows.Forms.Label();
            this.cbVal29 = new System.Windows.Forms.CheckBox();
            this.lbVal15 = new System.Windows.Forms.Label();
            this.cbVal30 = new System.Windows.Forms.CheckBox();
            this.lbVal14 = new System.Windows.Forms.Label();
            this.lbVal1 = new System.Windows.Forms.Label();
            this.lbVal13 = new System.Windows.Forms.Label();
            this.lbVal2 = new System.Windows.Forms.Label();
            this.lbVal12 = new System.Windows.Forms.Label();
            this.lbVal3 = new System.Windows.Forms.Label();
            this.lbVal11 = new System.Windows.Forms.Label();
            this.lbVal4 = new System.Windows.Forms.Label();
            this.lbVal10 = new System.Windows.Forms.Label();
            this.lbVal5 = new System.Windows.Forms.Label();
            this.lbVal9 = new System.Windows.Forms.Label();
            this.lbVal6 = new System.Windows.Forms.Label();
            this.lbVal8 = new System.Windows.Forms.Label();
            this.lbVal7 = new System.Windows.Forms.Label();
            this.txtRelatedOp = new System.Windows.Forms.TextBox();
            this.txtPartCategory = new System.Windows.Forms.TextBox();
            this.dgvUpdateRules = new System.Windows.Forms.DataGridView();
            this.ColRuleHeadID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRuleBatchID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPartNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPartDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColRelatedOp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPartCat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUnitType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDigit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColHeatType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUpdateType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColUpdateButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColDelButton = new System.Windows.Forms.DataGridViewButtonColumn();
            this.ColID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtRuleID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdCancel = new System.Windows.Forms.Button();
            this.txtBatchID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbDigit = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudReqQty = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPartDesc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lbUnitType = new System.Windows.Forms.Label();
            this.gbUnitType = new System.Windows.Forms.GroupBox();
            this.rbOAN = new System.Windows.Forms.RadioButton();
            this.rbOAK = new System.Windows.Forms.RadioButton();
            this.rbOAG = new System.Windows.Forms.RadioButton();
            this.rbOAD = new System.Windows.Forms.RadioButton();
            this.rbOAB = new System.Windows.Forms.RadioButton();
            this.lbMsgText = new System.Windows.Forms.Label();
            this.chkBoxRfgComp = new System.Windows.Forms.CheckBox();
            this.gbCircuits = new System.Windows.Forms.GroupBox();
            this.chkBoxCircuit2 = new System.Windows.Forms.CheckBox();
            this.chkBoxCircuit1 = new System.Windows.Forms.CheckBox();
            this.gbSubAsmData = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbImmediateParentPartNum = new System.Windows.Forms.ComboBox();
            this.chkBoxSubAsm = new System.Windows.Forms.CheckBox();
            this.chkBoxMtlPart = new System.Windows.Forms.CheckBox();
            this.rtbComments = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.gbValues.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdateRules)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqQty)).BeginInit();
            this.gbUnitType.SuspendLayout();
            this.gbCircuits.SuspendLayout();
            this.gbSubAsmData.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbModDate
            // 
            this.lbModDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModDate.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbModDate.Location = new System.Drawing.Point(819, 86);
            this.lbModDate.Name = "lbModDate";
            this.lbModDate.Size = new System.Drawing.Size(333, 20);
            this.lbModDate.TabIndex = 316;
            this.lbModDate.Text = "Modified Date:";
            this.lbModDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label12.Location = new System.Drawing.Point(737, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 20);
            this.label12.TabIndex = 315;
            this.label12.Text = "Mod Date:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbModBy
            // 
            this.lbModBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModBy.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbModBy.Location = new System.Drawing.Point(819, 66);
            this.lbModBy.Name = "lbModBy";
            this.lbModBy.Size = new System.Drawing.Size(333, 20);
            this.lbModBy.TabIndex = 314;
            this.lbModBy.Text = "Modified By:";
            this.lbModBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(737, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 313;
            this.label2.Text = "Mod By:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnAllRuleSets
            // 
            this.btnAllRuleSets.Enabled = false;
            this.btnAllRuleSets.Location = new System.Drawing.Point(1265, 118);
            this.btnAllRuleSets.Name = "btnAllRuleSets";
            this.btnAllRuleSets.Size = new System.Drawing.Size(75, 40);
            this.btnAllRuleSets.TabIndex = 312;
            this.btnAllRuleSets.Text = "All Rule Sets";
            this.btnAllRuleSets.UseVisualStyleBackColor = true;
            this.btnAllRuleSets.Click += new System.EventHandler(this.btnAllRuleSets_Click);
            // 
            // btnDeleteRuleSet
            // 
            this.btnDeleteRuleSet.Location = new System.Drawing.Point(1179, 118);
            this.btnDeleteRuleSet.Name = "btnDeleteRuleSet";
            this.btnDeleteRuleSet.Size = new System.Drawing.Size(75, 40);
            this.btnDeleteRuleSet.TabIndex = 311;
            this.btnDeleteRuleSet.Text = "Delete Rule Set";
            this.btnDeleteRuleSet.UseVisualStyleBackColor = true;
            this.btnDeleteRuleSet.Click += new System.EventHandler(this.btnDeleteRuleSet_Click);
            // 
            // lbScreenMode
            // 
            this.lbScreenMode.ForeColor = System.Drawing.Color.Red;
            this.lbScreenMode.Location = new System.Drawing.Point(1209, 298);
            this.lbScreenMode.Name = "lbScreenMode";
            this.lbScreenMode.Size = new System.Drawing.Size(75, 21);
            this.lbScreenMode.TabIndex = 310;
            this.lbScreenMode.Text = "ScreenMode";
            this.lbScreenMode.Visible = false;
            // 
            // cbPartNum
            // 
            this.cbPartNum.Enabled = false;
            this.cbPartNum.FormattingEnabled = true;
            this.cbPartNum.Location = new System.Drawing.Point(306, 15);
            this.cbPartNum.Name = "cbPartNum";
            this.cbPartNum.Size = new System.Drawing.Size(200, 21);
            this.cbPartNum.TabIndex = 309;
            this.cbPartNum.SelectedIndexChanged += new System.EventHandler(this.cbPartNum_SelectedIndexChanged);
            // 
            // btnAddNewRule
            // 
            this.btnAddNewRule.Location = new System.Drawing.Point(1179, 68);
            this.btnAddNewRule.Name = "btnAddNewRule";
            this.btnAddNewRule.Size = new System.Drawing.Size(75, 40);
            this.btnAddNewRule.TabIndex = 308;
            this.btnAddNewRule.Text = "Add New Rule";
            this.btnAddNewRule.UseVisualStyleBackColor = true;
            this.btnAddNewRule.Click += new System.EventHandler(this.btnAddNewRule_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(1179, 15);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 40);
            this.btnUpdate.TabIndex = 307;
            this.btnUpdate.Text = "Update Rule";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lbSelectedRow
            // 
            this.lbSelectedRow.AutoSize = true;
            this.lbSelectedRow.Enabled = false;
            this.lbSelectedRow.Location = new System.Drawing.Point(1283, 298);
            this.lbSelectedRow.Name = "lbSelectedRow";
            this.lbSelectedRow.Size = new System.Drawing.Size(71, 13);
            this.lbSelectedRow.TabIndex = 306;
            this.lbSelectedRow.Text = "SelectedRow";
            this.lbSelectedRow.Visible = false;
            // 
            // gbValues
            // 
            this.gbValues.Controls.Add(this.cbVal17);
            this.gbValues.Controls.Add(this.cbVal1);
            this.gbValues.Controls.Add(this.cbVal2);
            this.gbValues.Controls.Add(this.cbVal3);
            this.gbValues.Controls.Add(this.cbVal4);
            this.gbValues.Controls.Add(this.cbVal5);
            this.gbValues.Controls.Add(this.cbVal6);
            this.gbValues.Controls.Add(this.cbVal7);
            this.gbValues.Controls.Add(this.cbVal8);
            this.gbValues.Controls.Add(this.cbVal9);
            this.gbValues.Controls.Add(this.cbVal10);
            this.gbValues.Controls.Add(this.cbVal11);
            this.gbValues.Controls.Add(this.cbVal12);
            this.gbValues.Controls.Add(this.cbVal13);
            this.gbValues.Controls.Add(this.cbVal14);
            this.gbValues.Controls.Add(this.lbVal30);
            this.gbValues.Controls.Add(this.checkBox9);
            this.gbValues.Controls.Add(this.lbVal29);
            this.gbValues.Controls.Add(this.cbVal15);
            this.gbValues.Controls.Add(this.lbVal28);
            this.gbValues.Controls.Add(this.cbVal16);
            this.gbValues.Controls.Add(this.lbVal27);
            this.gbValues.Controls.Add(this.cbVal18);
            this.gbValues.Controls.Add(this.lbVal26);
            this.gbValues.Controls.Add(this.cbVal19);
            this.gbValues.Controls.Add(this.lbVal25);
            this.gbValues.Controls.Add(this.cbVal20);
            this.gbValues.Controls.Add(this.lbVal24);
            this.gbValues.Controls.Add(this.cbVal21);
            this.gbValues.Controls.Add(this.lbVal23);
            this.gbValues.Controls.Add(this.cbVal22);
            this.gbValues.Controls.Add(this.lbVal22);
            this.gbValues.Controls.Add(this.cbVal23);
            this.gbValues.Controls.Add(this.lbVal21);
            this.gbValues.Controls.Add(this.cbVal24);
            this.gbValues.Controls.Add(this.lbVal20);
            this.gbValues.Controls.Add(this.cbVal25);
            this.gbValues.Controls.Add(this.lbVal19);
            this.gbValues.Controls.Add(this.cbVal26);
            this.gbValues.Controls.Add(this.lbVal18);
            this.gbValues.Controls.Add(this.cbVal27);
            this.gbValues.Controls.Add(this.lbVal17);
            this.gbValues.Controls.Add(this.cbVal28);
            this.gbValues.Controls.Add(this.lbVal16);
            this.gbValues.Controls.Add(this.cbVal29);
            this.gbValues.Controls.Add(this.lbVal15);
            this.gbValues.Controls.Add(this.cbVal30);
            this.gbValues.Controls.Add(this.lbVal14);
            this.gbValues.Controls.Add(this.lbVal1);
            this.gbValues.Controls.Add(this.lbVal13);
            this.gbValues.Controls.Add(this.lbVal2);
            this.gbValues.Controls.Add(this.lbVal12);
            this.gbValues.Controls.Add(this.lbVal3);
            this.gbValues.Controls.Add(this.lbVal11);
            this.gbValues.Controls.Add(this.lbVal4);
            this.gbValues.Controls.Add(this.lbVal10);
            this.gbValues.Controls.Add(this.lbVal5);
            this.gbValues.Controls.Add(this.lbVal9);
            this.gbValues.Controls.Add(this.lbVal6);
            this.gbValues.Controls.Add(this.lbVal8);
            this.gbValues.Controls.Add(this.lbVal7);
            this.gbValues.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValues.ForeColor = System.Drawing.Color.RoyalBlue;
            this.gbValues.Location = new System.Drawing.Point(199, 104);
            this.gbValues.Name = "gbValues";
            this.gbValues.Size = new System.Drawing.Size(956, 152);
            this.gbValues.TabIndex = 305;
            this.gbValues.TabStop = false;
            this.gbValues.Text = "Values";
            // 
            // cbVal17
            // 
            this.cbVal17.AutoSize = true;
            this.cbVal17.Location = new System.Drawing.Point(493, 20);
            this.cbVal17.Name = "cbVal17";
            this.cbVal17.Size = new System.Drawing.Size(15, 14);
            this.cbVal17.TabIndex = 36;
            this.cbVal17.UseVisualStyleBackColor = true;
            this.cbVal17.Visible = false;
            // 
            // cbVal1
            // 
            this.cbVal1.AutoSize = true;
            this.cbVal1.Location = new System.Drawing.Point(13, 20);
            this.cbVal1.Name = "cbVal1";
            this.cbVal1.Size = new System.Drawing.Size(15, 14);
            this.cbVal1.TabIndex = 19;
            this.cbVal1.UseVisualStyleBackColor = true;
            this.cbVal1.Visible = false;
            // 
            // cbVal2
            // 
            this.cbVal2.AutoSize = true;
            this.cbVal2.Location = new System.Drawing.Point(43, 20);
            this.cbVal2.Name = "cbVal2";
            this.cbVal2.Size = new System.Drawing.Size(15, 14);
            this.cbVal2.TabIndex = 20;
            this.cbVal2.UseVisualStyleBackColor = true;
            this.cbVal2.Visible = false;
            // 
            // cbVal3
            // 
            this.cbVal3.AutoSize = true;
            this.cbVal3.Location = new System.Drawing.Point(73, 20);
            this.cbVal3.Name = "cbVal3";
            this.cbVal3.Size = new System.Drawing.Size(15, 14);
            this.cbVal3.TabIndex = 21;
            this.cbVal3.UseVisualStyleBackColor = true;
            this.cbVal3.Visible = false;
            // 
            // cbVal4
            // 
            this.cbVal4.AutoSize = true;
            this.cbVal4.Location = new System.Drawing.Point(103, 20);
            this.cbVal4.Name = "cbVal4";
            this.cbVal4.Size = new System.Drawing.Size(15, 14);
            this.cbVal4.TabIndex = 22;
            this.cbVal4.UseVisualStyleBackColor = true;
            this.cbVal4.Visible = false;
            // 
            // cbVal5
            // 
            this.cbVal5.AutoSize = true;
            this.cbVal5.Location = new System.Drawing.Point(133, 20);
            this.cbVal5.Name = "cbVal5";
            this.cbVal5.Size = new System.Drawing.Size(15, 14);
            this.cbVal5.TabIndex = 23;
            this.cbVal5.UseVisualStyleBackColor = true;
            this.cbVal5.Visible = false;
            // 
            // cbVal6
            // 
            this.cbVal6.AutoSize = true;
            this.cbVal6.Location = new System.Drawing.Point(163, 20);
            this.cbVal6.Name = "cbVal6";
            this.cbVal6.Size = new System.Drawing.Size(15, 14);
            this.cbVal6.TabIndex = 24;
            this.cbVal6.UseVisualStyleBackColor = true;
            this.cbVal6.Visible = false;
            // 
            // cbVal7
            // 
            this.cbVal7.AutoSize = true;
            this.cbVal7.Location = new System.Drawing.Point(193, 20);
            this.cbVal7.Name = "cbVal7";
            this.cbVal7.Size = new System.Drawing.Size(15, 14);
            this.cbVal7.TabIndex = 25;
            this.cbVal7.UseVisualStyleBackColor = true;
            this.cbVal7.Visible = false;
            // 
            // cbVal8
            // 
            this.cbVal8.AutoSize = true;
            this.cbVal8.Location = new System.Drawing.Point(223, 20);
            this.cbVal8.Name = "cbVal8";
            this.cbVal8.Size = new System.Drawing.Size(15, 14);
            this.cbVal8.TabIndex = 26;
            this.cbVal8.UseVisualStyleBackColor = true;
            this.cbVal8.Visible = false;
            // 
            // cbVal9
            // 
            this.cbVal9.AutoSize = true;
            this.cbVal9.Location = new System.Drawing.Point(253, 20);
            this.cbVal9.Name = "cbVal9";
            this.cbVal9.Size = new System.Drawing.Size(15, 14);
            this.cbVal9.TabIndex = 27;
            this.cbVal9.UseVisualStyleBackColor = true;
            this.cbVal9.Visible = false;
            // 
            // cbVal10
            // 
            this.cbVal10.AutoSize = true;
            this.cbVal10.Location = new System.Drawing.Point(283, 20);
            this.cbVal10.Name = "cbVal10";
            this.cbVal10.Size = new System.Drawing.Size(15, 14);
            this.cbVal10.TabIndex = 28;
            this.cbVal10.UseVisualStyleBackColor = true;
            this.cbVal10.Visible = false;
            // 
            // cbVal11
            // 
            this.cbVal11.AutoSize = true;
            this.cbVal11.Location = new System.Drawing.Point(313, 20);
            this.cbVal11.Name = "cbVal11";
            this.cbVal11.Size = new System.Drawing.Size(15, 14);
            this.cbVal11.TabIndex = 29;
            this.cbVal11.UseVisualStyleBackColor = true;
            this.cbVal11.Visible = false;
            // 
            // cbVal12
            // 
            this.cbVal12.AutoSize = true;
            this.cbVal12.Location = new System.Drawing.Point(343, 20);
            this.cbVal12.Name = "cbVal12";
            this.cbVal12.Size = new System.Drawing.Size(15, 14);
            this.cbVal12.TabIndex = 30;
            this.cbVal12.UseVisualStyleBackColor = true;
            this.cbVal12.Visible = false;
            // 
            // cbVal13
            // 
            this.cbVal13.AutoSize = true;
            this.cbVal13.Location = new System.Drawing.Point(373, 20);
            this.cbVal13.Name = "cbVal13";
            this.cbVal13.Size = new System.Drawing.Size(15, 14);
            this.cbVal13.TabIndex = 31;
            this.cbVal13.UseVisualStyleBackColor = true;
            this.cbVal13.Visible = false;
            // 
            // cbVal14
            // 
            this.cbVal14.AutoSize = true;
            this.cbVal14.Location = new System.Drawing.Point(403, 20);
            this.cbVal14.Name = "cbVal14";
            this.cbVal14.Size = new System.Drawing.Size(15, 14);
            this.cbVal14.TabIndex = 32;
            this.cbVal14.UseVisualStyleBackColor = true;
            this.cbVal14.Visible = false;
            // 
            // lbVal30
            // 
            this.lbVal30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal30.ForeColor = System.Drawing.Color.Black;
            this.lbVal30.Location = new System.Drawing.Point(881, 37);
            this.lbVal30.Name = "lbVal30";
            this.lbVal30.Size = new System.Drawing.Size(16, 66);
            this.lbVal30.TabIndex = 79;
            this.lbVal30.Text = "Heat Type:";
            this.lbVal30.Visible = false;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(433, 20);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 33;
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.Visible = false;
            // 
            // lbVal29
            // 
            this.lbVal29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal29.ForeColor = System.Drawing.Color.Black;
            this.lbVal29.Location = new System.Drawing.Point(851, 37);
            this.lbVal29.Name = "lbVal29";
            this.lbVal29.Size = new System.Drawing.Size(16, 66);
            this.lbVal29.TabIndex = 78;
            this.lbVal29.Text = "Heat Type:";
            this.lbVal29.Visible = false;
            // 
            // cbVal15
            // 
            this.cbVal15.AutoSize = true;
            this.cbVal15.Location = new System.Drawing.Point(433, 20);
            this.cbVal15.Name = "cbVal15";
            this.cbVal15.Size = new System.Drawing.Size(15, 14);
            this.cbVal15.TabIndex = 34;
            this.cbVal15.UseVisualStyleBackColor = true;
            this.cbVal15.Visible = false;
            // 
            // lbVal28
            // 
            this.lbVal28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal28.ForeColor = System.Drawing.Color.Black;
            this.lbVal28.Location = new System.Drawing.Point(821, 37);
            this.lbVal28.Name = "lbVal28";
            this.lbVal28.Size = new System.Drawing.Size(16, 66);
            this.lbVal28.TabIndex = 77;
            this.lbVal28.Text = "Heat Type:";
            this.lbVal28.Visible = false;
            // 
            // cbVal16
            // 
            this.cbVal16.AutoSize = true;
            this.cbVal16.Location = new System.Drawing.Point(463, 20);
            this.cbVal16.Name = "cbVal16";
            this.cbVal16.Size = new System.Drawing.Size(15, 14);
            this.cbVal16.TabIndex = 35;
            this.cbVal16.UseVisualStyleBackColor = true;
            this.cbVal16.Visible = false;
            // 
            // lbVal27
            // 
            this.lbVal27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal27.ForeColor = System.Drawing.Color.Black;
            this.lbVal27.Location = new System.Drawing.Point(791, 37);
            this.lbVal27.Name = "lbVal27";
            this.lbVal27.Size = new System.Drawing.Size(16, 66);
            this.lbVal27.TabIndex = 76;
            this.lbVal27.Text = "Heat Type:";
            this.lbVal27.Visible = false;
            // 
            // cbVal18
            // 
            this.cbVal18.AutoSize = true;
            this.cbVal18.Location = new System.Drawing.Point(523, 20);
            this.cbVal18.Name = "cbVal18";
            this.cbVal18.Size = new System.Drawing.Size(15, 14);
            this.cbVal18.TabIndex = 37;
            this.cbVal18.UseVisualStyleBackColor = true;
            this.cbVal18.Visible = false;
            // 
            // lbVal26
            // 
            this.lbVal26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal26.ForeColor = System.Drawing.Color.Black;
            this.lbVal26.Location = new System.Drawing.Point(761, 37);
            this.lbVal26.Name = "lbVal26";
            this.lbVal26.Size = new System.Drawing.Size(16, 66);
            this.lbVal26.TabIndex = 75;
            this.lbVal26.Text = "Heat Type:";
            this.lbVal26.Visible = false;
            // 
            // cbVal19
            // 
            this.cbVal19.AutoSize = true;
            this.cbVal19.Location = new System.Drawing.Point(553, 20);
            this.cbVal19.Name = "cbVal19";
            this.cbVal19.Size = new System.Drawing.Size(15, 14);
            this.cbVal19.TabIndex = 38;
            this.cbVal19.UseVisualStyleBackColor = true;
            this.cbVal19.Visible = false;
            // 
            // lbVal25
            // 
            this.lbVal25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal25.ForeColor = System.Drawing.Color.Black;
            this.lbVal25.Location = new System.Drawing.Point(731, 37);
            this.lbVal25.Name = "lbVal25";
            this.lbVal25.Size = new System.Drawing.Size(16, 66);
            this.lbVal25.TabIndex = 74;
            this.lbVal25.Text = "Heat Type:";
            this.lbVal25.Visible = false;
            // 
            // cbVal20
            // 
            this.cbVal20.AutoSize = true;
            this.cbVal20.Location = new System.Drawing.Point(583, 20);
            this.cbVal20.Name = "cbVal20";
            this.cbVal20.Size = new System.Drawing.Size(15, 14);
            this.cbVal20.TabIndex = 39;
            this.cbVal20.UseVisualStyleBackColor = true;
            this.cbVal20.Visible = false;
            // 
            // lbVal24
            // 
            this.lbVal24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal24.ForeColor = System.Drawing.Color.Black;
            this.lbVal24.Location = new System.Drawing.Point(701, 37);
            this.lbVal24.Name = "lbVal24";
            this.lbVal24.Size = new System.Drawing.Size(16, 66);
            this.lbVal24.TabIndex = 73;
            this.lbVal24.Text = "Heat Type:";
            this.lbVal24.Visible = false;
            // 
            // cbVal21
            // 
            this.cbVal21.AutoSize = true;
            this.cbVal21.Location = new System.Drawing.Point(613, 20);
            this.cbVal21.Name = "cbVal21";
            this.cbVal21.Size = new System.Drawing.Size(15, 14);
            this.cbVal21.TabIndex = 40;
            this.cbVal21.UseVisualStyleBackColor = true;
            this.cbVal21.Visible = false;
            // 
            // lbVal23
            // 
            this.lbVal23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal23.ForeColor = System.Drawing.Color.Black;
            this.lbVal23.Location = new System.Drawing.Point(671, 37);
            this.lbVal23.Name = "lbVal23";
            this.lbVal23.Size = new System.Drawing.Size(16, 66);
            this.lbVal23.TabIndex = 72;
            this.lbVal23.Text = "Heat Type:";
            this.lbVal23.Visible = false;
            // 
            // cbVal22
            // 
            this.cbVal22.AutoSize = true;
            this.cbVal22.Location = new System.Drawing.Point(643, 20);
            this.cbVal22.Name = "cbVal22";
            this.cbVal22.Size = new System.Drawing.Size(15, 14);
            this.cbVal22.TabIndex = 41;
            this.cbVal22.UseVisualStyleBackColor = true;
            this.cbVal22.Visible = false;
            // 
            // lbVal22
            // 
            this.lbVal22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal22.ForeColor = System.Drawing.Color.Black;
            this.lbVal22.Location = new System.Drawing.Point(641, 37);
            this.lbVal22.Name = "lbVal22";
            this.lbVal22.Size = new System.Drawing.Size(16, 66);
            this.lbVal22.TabIndex = 71;
            this.lbVal22.Text = "Heat Type:";
            this.lbVal22.Visible = false;
            // 
            // cbVal23
            // 
            this.cbVal23.AutoSize = true;
            this.cbVal23.Location = new System.Drawing.Point(673, 20);
            this.cbVal23.Name = "cbVal23";
            this.cbVal23.Size = new System.Drawing.Size(15, 14);
            this.cbVal23.TabIndex = 42;
            this.cbVal23.UseVisualStyleBackColor = true;
            this.cbVal23.Visible = false;
            // 
            // lbVal21
            // 
            this.lbVal21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal21.ForeColor = System.Drawing.Color.Black;
            this.lbVal21.Location = new System.Drawing.Point(611, 37);
            this.lbVal21.Name = "lbVal21";
            this.lbVal21.Size = new System.Drawing.Size(16, 66);
            this.lbVal21.TabIndex = 70;
            this.lbVal21.Text = "Heat Type:";
            this.lbVal21.Visible = false;
            // 
            // cbVal24
            // 
            this.cbVal24.AutoSize = true;
            this.cbVal24.Location = new System.Drawing.Point(703, 20);
            this.cbVal24.Name = "cbVal24";
            this.cbVal24.Size = new System.Drawing.Size(15, 14);
            this.cbVal24.TabIndex = 43;
            this.cbVal24.UseVisualStyleBackColor = true;
            this.cbVal24.Visible = false;
            // 
            // lbVal20
            // 
            this.lbVal20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal20.ForeColor = System.Drawing.Color.Black;
            this.lbVal20.Location = new System.Drawing.Point(581, 37);
            this.lbVal20.Name = "lbVal20";
            this.lbVal20.Size = new System.Drawing.Size(16, 66);
            this.lbVal20.TabIndex = 69;
            this.lbVal20.Text = "Heat Type:";
            this.lbVal20.Visible = false;
            // 
            // cbVal25
            // 
            this.cbVal25.AutoSize = true;
            this.cbVal25.Location = new System.Drawing.Point(733, 20);
            this.cbVal25.Name = "cbVal25";
            this.cbVal25.Size = new System.Drawing.Size(15, 14);
            this.cbVal25.TabIndex = 44;
            this.cbVal25.UseVisualStyleBackColor = true;
            this.cbVal25.Visible = false;
            // 
            // lbVal19
            // 
            this.lbVal19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal19.ForeColor = System.Drawing.Color.Black;
            this.lbVal19.Location = new System.Drawing.Point(551, 37);
            this.lbVal19.Name = "lbVal19";
            this.lbVal19.Size = new System.Drawing.Size(16, 66);
            this.lbVal19.TabIndex = 68;
            this.lbVal19.Text = "Heat Type:";
            this.lbVal19.Visible = false;
            // 
            // cbVal26
            // 
            this.cbVal26.AutoSize = true;
            this.cbVal26.Location = new System.Drawing.Point(763, 20);
            this.cbVal26.Name = "cbVal26";
            this.cbVal26.Size = new System.Drawing.Size(15, 14);
            this.cbVal26.TabIndex = 45;
            this.cbVal26.UseVisualStyleBackColor = true;
            this.cbVal26.Visible = false;
            // 
            // lbVal18
            // 
            this.lbVal18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal18.ForeColor = System.Drawing.Color.Black;
            this.lbVal18.Location = new System.Drawing.Point(521, 37);
            this.lbVal18.Name = "lbVal18";
            this.lbVal18.Size = new System.Drawing.Size(16, 66);
            this.lbVal18.TabIndex = 67;
            this.lbVal18.Text = "Heat Type:";
            this.lbVal18.Visible = false;
            // 
            // cbVal27
            // 
            this.cbVal27.AutoSize = true;
            this.cbVal27.Location = new System.Drawing.Point(793, 20);
            this.cbVal27.Name = "cbVal27";
            this.cbVal27.Size = new System.Drawing.Size(15, 14);
            this.cbVal27.TabIndex = 46;
            this.cbVal27.UseVisualStyleBackColor = true;
            this.cbVal27.Visible = false;
            // 
            // lbVal17
            // 
            this.lbVal17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal17.ForeColor = System.Drawing.Color.Black;
            this.lbVal17.Location = new System.Drawing.Point(491, 37);
            this.lbVal17.Name = "lbVal17";
            this.lbVal17.Size = new System.Drawing.Size(16, 66);
            this.lbVal17.TabIndex = 66;
            this.lbVal17.Text = "Heat Type:";
            this.lbVal17.Visible = false;
            // 
            // cbVal28
            // 
            this.cbVal28.AutoSize = true;
            this.cbVal28.Location = new System.Drawing.Point(823, 20);
            this.cbVal28.Name = "cbVal28";
            this.cbVal28.Size = new System.Drawing.Size(15, 14);
            this.cbVal28.TabIndex = 47;
            this.cbVal28.UseVisualStyleBackColor = true;
            this.cbVal28.Visible = false;
            // 
            // lbVal16
            // 
            this.lbVal16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal16.ForeColor = System.Drawing.Color.Black;
            this.lbVal16.Location = new System.Drawing.Point(461, 37);
            this.lbVal16.Name = "lbVal16";
            this.lbVal16.Size = new System.Drawing.Size(16, 66);
            this.lbVal16.TabIndex = 65;
            this.lbVal16.Text = "Heat Type:";
            this.lbVal16.Visible = false;
            // 
            // cbVal29
            // 
            this.cbVal29.AutoSize = true;
            this.cbVal29.Location = new System.Drawing.Point(853, 20);
            this.cbVal29.Name = "cbVal29";
            this.cbVal29.Size = new System.Drawing.Size(15, 14);
            this.cbVal29.TabIndex = 48;
            this.cbVal29.UseVisualStyleBackColor = true;
            this.cbVal29.Visible = false;
            // 
            // lbVal15
            // 
            this.lbVal15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal15.ForeColor = System.Drawing.Color.Black;
            this.lbVal15.Location = new System.Drawing.Point(431, 37);
            this.lbVal15.Name = "lbVal15";
            this.lbVal15.Size = new System.Drawing.Size(16, 66);
            this.lbVal15.TabIndex = 64;
            this.lbVal15.Text = "Heat Type:";
            this.lbVal15.Visible = false;
            // 
            // cbVal30
            // 
            this.cbVal30.AutoSize = true;
            this.cbVal30.Location = new System.Drawing.Point(883, 20);
            this.cbVal30.Name = "cbVal30";
            this.cbVal30.Size = new System.Drawing.Size(15, 14);
            this.cbVal30.TabIndex = 49;
            this.cbVal30.UseVisualStyleBackColor = true;
            this.cbVal30.Visible = false;
            // 
            // lbVal14
            // 
            this.lbVal14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal14.ForeColor = System.Drawing.Color.Black;
            this.lbVal14.Location = new System.Drawing.Point(401, 37);
            this.lbVal14.Name = "lbVal14";
            this.lbVal14.Size = new System.Drawing.Size(16, 66);
            this.lbVal14.TabIndex = 63;
            this.lbVal14.Text = "Heat Type:";
            this.lbVal14.Visible = false;
            // 
            // lbVal1
            // 
            this.lbVal1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal1.ForeColor = System.Drawing.Color.Black;
            this.lbVal1.Location = new System.Drawing.Point(11, 37);
            this.lbVal1.Name = "lbVal1";
            this.lbVal1.Size = new System.Drawing.Size(16, 66);
            this.lbVal1.TabIndex = 50;
            this.lbVal1.Tag = "1";
            this.lbVal1.Text = "Heat Type:";
            this.lbVal1.Visible = false;
            // 
            // lbVal13
            // 
            this.lbVal13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal13.ForeColor = System.Drawing.Color.Black;
            this.lbVal13.Location = new System.Drawing.Point(371, 37);
            this.lbVal13.Name = "lbVal13";
            this.lbVal13.Size = new System.Drawing.Size(16, 66);
            this.lbVal13.TabIndex = 62;
            this.lbVal13.Text = "Heat Type:";
            this.lbVal13.Visible = false;
            // 
            // lbVal2
            // 
            this.lbVal2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal2.ForeColor = System.Drawing.Color.Black;
            this.lbVal2.Location = new System.Drawing.Point(41, 37);
            this.lbVal2.Name = "lbVal2";
            this.lbVal2.Size = new System.Drawing.Size(16, 66);
            this.lbVal2.TabIndex = 51;
            this.lbVal2.Text = "Heat Type:";
            this.lbVal2.Visible = false;
            // 
            // lbVal12
            // 
            this.lbVal12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal12.ForeColor = System.Drawing.Color.Black;
            this.lbVal12.Location = new System.Drawing.Point(341, 37);
            this.lbVal12.Name = "lbVal12";
            this.lbVal12.Size = new System.Drawing.Size(16, 66);
            this.lbVal12.TabIndex = 61;
            this.lbVal12.Text = "Heat Type:";
            this.lbVal12.Visible = false;
            // 
            // lbVal3
            // 
            this.lbVal3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal3.ForeColor = System.Drawing.Color.Black;
            this.lbVal3.Location = new System.Drawing.Point(71, 37);
            this.lbVal3.Name = "lbVal3";
            this.lbVal3.Size = new System.Drawing.Size(16, 66);
            this.lbVal3.TabIndex = 52;
            this.lbVal3.Text = "Heat Type:";
            this.lbVal3.Visible = false;
            // 
            // lbVal11
            // 
            this.lbVal11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal11.ForeColor = System.Drawing.Color.Black;
            this.lbVal11.Location = new System.Drawing.Point(311, 37);
            this.lbVal11.Name = "lbVal11";
            this.lbVal11.Size = new System.Drawing.Size(16, 66);
            this.lbVal11.TabIndex = 60;
            this.lbVal11.Text = "Heat Type:";
            this.lbVal11.Visible = false;
            // 
            // lbVal4
            // 
            this.lbVal4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal4.ForeColor = System.Drawing.Color.Black;
            this.lbVal4.Location = new System.Drawing.Point(101, 37);
            this.lbVal4.Name = "lbVal4";
            this.lbVal4.Size = new System.Drawing.Size(16, 66);
            this.lbVal4.TabIndex = 53;
            this.lbVal4.Text = "Heat Type:";
            this.lbVal4.Visible = false;
            // 
            // lbVal10
            // 
            this.lbVal10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal10.ForeColor = System.Drawing.Color.Black;
            this.lbVal10.Location = new System.Drawing.Point(281, 37);
            this.lbVal10.Name = "lbVal10";
            this.lbVal10.Size = new System.Drawing.Size(16, 66);
            this.lbVal10.TabIndex = 59;
            this.lbVal10.Text = "Heat Type:";
            this.lbVal10.Visible = false;
            // 
            // lbVal5
            // 
            this.lbVal5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal5.ForeColor = System.Drawing.Color.Black;
            this.lbVal5.Location = new System.Drawing.Point(131, 37);
            this.lbVal5.Name = "lbVal5";
            this.lbVal5.Size = new System.Drawing.Size(16, 66);
            this.lbVal5.TabIndex = 54;
            this.lbVal5.Text = "Heat Type:";
            this.lbVal5.Visible = false;
            // 
            // lbVal9
            // 
            this.lbVal9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal9.ForeColor = System.Drawing.Color.Black;
            this.lbVal9.Location = new System.Drawing.Point(251, 37);
            this.lbVal9.Name = "lbVal9";
            this.lbVal9.Size = new System.Drawing.Size(16, 66);
            this.lbVal9.TabIndex = 58;
            this.lbVal9.Text = "Heat Type:";
            this.lbVal9.Visible = false;
            // 
            // lbVal6
            // 
            this.lbVal6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal6.ForeColor = System.Drawing.Color.Black;
            this.lbVal6.Location = new System.Drawing.Point(161, 37);
            this.lbVal6.Name = "lbVal6";
            this.lbVal6.Size = new System.Drawing.Size(16, 66);
            this.lbVal6.TabIndex = 55;
            this.lbVal6.Text = "Heat Type:";
            this.lbVal6.Visible = false;
            // 
            // lbVal8
            // 
            this.lbVal8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal8.ForeColor = System.Drawing.Color.Black;
            this.lbVal8.Location = new System.Drawing.Point(221, 37);
            this.lbVal8.Name = "lbVal8";
            this.lbVal8.Size = new System.Drawing.Size(16, 66);
            this.lbVal8.TabIndex = 57;
            this.lbVal8.Text = "Heat Type:";
            this.lbVal8.Visible = false;
            // 
            // lbVal7
            // 
            this.lbVal7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVal7.ForeColor = System.Drawing.Color.Black;
            this.lbVal7.Location = new System.Drawing.Point(191, 37);
            this.lbVal7.Name = "lbVal7";
            this.lbVal7.Size = new System.Drawing.Size(16, 66);
            this.lbVal7.TabIndex = 56;
            this.lbVal7.Text = "Heat Type:";
            this.lbVal7.Visible = false;
            // 
            // txtRelatedOp
            // 
            this.txtRelatedOp.Location = new System.Drawing.Point(1037, 45);
            this.txtRelatedOp.Name = "txtRelatedOp";
            this.txtRelatedOp.Size = new System.Drawing.Size(115, 20);
            this.txtRelatedOp.TabIndex = 304;
            // 
            // txtPartCategory
            // 
            this.txtPartCategory.Enabled = false;
            this.txtPartCategory.Location = new System.Drawing.Point(306, 45);
            this.txtPartCategory.Name = "txtPartCategory";
            this.txtPartCategory.Size = new System.Drawing.Size(200, 20);
            this.txtPartCategory.TabIndex = 301;
            // 
            // dgvUpdateRules
            // 
            this.dgvUpdateRules.AllowUserToAddRows = false;
            this.dgvUpdateRules.AllowUserToDeleteRows = false;
            this.dgvUpdateRules.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUpdateRules.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColRuleHeadID,
            this.ColRuleBatchID,
            this.ColPartNumber,
            this.ColPartDesc,
            this.ColRelatedOp,
            this.ColPartCat,
            this.ColUnitType,
            this.ColDigit,
            this.ColHeatType,
            this.ColValue,
            this.ColQty,
            this.ColUpdateType,
            this.ColUpdateButton,
            this.ColDelButton,
            this.ColID});
            this.dgvUpdateRules.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvUpdateRules.Location = new System.Drawing.Point(13, 319);
            this.dgvUpdateRules.MultiSelect = false;
            this.dgvUpdateRules.Name = "dgvUpdateRules";
            this.dgvUpdateRules.ReadOnly = true;
            this.dgvUpdateRules.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUpdateRules.Size = new System.Drawing.Size(1328, 290);
            this.dgvUpdateRules.TabIndex = 300;
            this.dgvUpdateRules.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUpdateRules_CellContentClick);
            this.dgvUpdateRules.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvUpdateRules_CellMouseDoubleClick);
            // 
            // ColRuleHeadID
            // 
            this.ColRuleHeadID.HeaderText = "Rule Head ID";
            this.ColRuleHeadID.Name = "ColRuleHeadID";
            this.ColRuleHeadID.ReadOnly = true;
            this.ColRuleHeadID.Width = 60;
            // 
            // ColRuleBatchID
            // 
            this.ColRuleBatchID.HeaderText = "Rule Batch ID";
            this.ColRuleBatchID.Name = "ColRuleBatchID";
            this.ColRuleBatchID.ReadOnly = true;
            this.ColRuleBatchID.Width = 60;
            // 
            // ColPartNumber
            // 
            this.ColPartNumber.HeaderText = "Part Number";
            this.ColPartNumber.Name = "ColPartNumber";
            this.ColPartNumber.ReadOnly = true;
            this.ColPartNumber.Width = 175;
            // 
            // ColPartDesc
            // 
            this.ColPartDesc.HeaderText = "Part Description";
            this.ColPartDesc.Name = "ColPartDesc";
            this.ColPartDesc.ReadOnly = true;
            this.ColPartDesc.Width = 275;
            // 
            // ColRelatedOp
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColRelatedOp.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColRelatedOp.HeaderText = "Related Op";
            this.ColRelatedOp.Name = "ColRelatedOp";
            this.ColRelatedOp.ReadOnly = true;
            this.ColRelatedOp.Width = 60;
            // 
            // ColPartCat
            // 
            this.ColPartCat.HeaderText = "Part Category";
            this.ColPartCat.Name = "ColPartCat";
            this.ColPartCat.ReadOnly = true;
            this.ColPartCat.Width = 150;
            // 
            // ColUnitType
            // 
            this.ColUnitType.HeaderText = "Unit Type";
            this.ColUnitType.Name = "ColUnitType";
            this.ColUnitType.ReadOnly = true;
            this.ColUnitType.Visible = false;
            this.ColUnitType.Width = 50;
            // 
            // ColDigit
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColDigit.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColDigit.HeaderText = "Digit";
            this.ColDigit.Name = "ColDigit";
            this.ColDigit.ReadOnly = true;
            this.ColDigit.Width = 60;
            // 
            // ColHeatType
            // 
            this.ColHeatType.HeaderText = "HeatType";
            this.ColHeatType.Name = "ColHeatType";
            this.ColHeatType.ReadOnly = true;
            this.ColHeatType.Width = 75;
            // 
            // ColValue
            // 
            this.ColValue.HeaderText = "Value";
            this.ColValue.Name = "ColValue";
            this.ColValue.ReadOnly = true;
            this.ColValue.Width = 175;
            // 
            // ColQty
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColQty.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColQty.HeaderText = "Quantity";
            this.ColQty.Name = "ColQty";
            this.ColQty.ReadOnly = true;
            // 
            // ColUpdateType
            // 
            this.ColUpdateType.HeaderText = "UpdateType";
            this.ColUpdateType.Name = "ColUpdateType";
            this.ColUpdateType.ReadOnly = true;
            this.ColUpdateType.Visible = false;
            // 
            // ColUpdateButton
            // 
            this.ColUpdateButton.HeaderText = "Upd";
            this.ColUpdateButton.Name = "ColUpdateButton";
            this.ColUpdateButton.ReadOnly = true;
            this.ColUpdateButton.Text = "Upd";
            this.ColUpdateButton.UseColumnTextForButtonValue = true;
            this.ColUpdateButton.Width = 40;
            // 
            // ColDelButton
            // 
            this.ColDelButton.HeaderText = "Del";
            this.ColDelButton.Name = "ColDelButton";
            this.ColDelButton.ReadOnly = true;
            this.ColDelButton.Text = "Del";
            this.ColDelButton.UseColumnTextForButtonValue = true;
            this.ColDelButton.Width = 40;
            // 
            // ColID
            // 
            this.ColID.HeaderText = "ID";
            this.ColID.Name = "ColID";
            this.ColID.ReadOnly = true;
            this.ColID.Visible = false;
            // 
            // txtRuleID
            // 
            this.txtRuleID.Enabled = false;
            this.txtRuleID.Location = new System.Drawing.Point(614, 45);
            this.txtRuleID.Name = "txtRuleID";
            this.txtRuleID.Size = new System.Drawing.Size(115, 20);
            this.txtRuleID.TabIndex = 299;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(510, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 298;
            this.label5.Text = "Rule ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(1265, 68);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 40);
            this.btnSave.TabIndex = 297;
            this.btnSave.Text = "Save All Changes";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdCancel
            // 
            this.btnUpdCancel.Location = new System.Drawing.Point(1265, 15);
            this.btnUpdCancel.Name = "btnUpdCancel";
            this.btnUpdCancel.Size = new System.Drawing.Size(75, 40);
            this.btnUpdCancel.TabIndex = 296;
            this.btnUpdCancel.Text = "Exit";
            this.btnUpdCancel.UseVisualStyleBackColor = true;
            this.btnUpdCancel.Click += new System.EventHandler(this.btnUpdCancel_Click);
            // 
            // txtBatchID
            // 
            this.txtBatchID.Enabled = false;
            this.txtBatchID.Location = new System.Drawing.Point(822, 45);
            this.txtBatchID.Name = "txtBatchID";
            this.txtBatchID.Size = new System.Drawing.Size(100, 20);
            this.txtBatchID.TabIndex = 295;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(737, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 20);
            this.label10.TabIndex = 294;
            this.label10.Text = "Batch ID:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbDigit
            // 
            this.cbDigit.Enabled = false;
            this.cbDigit.FormattingEnabled = true;
            this.cbDigit.Items.AddRange(new object[] {
            "OAB",
            "OAD",
            "OAG",
            "OAK",
            "OAN"});
            this.cbDigit.Location = new System.Drawing.Point(306, 76);
            this.cbDigit.MaxDropDownItems = 50;
            this.cbDigit.Name = "cbDigit";
            this.cbDigit.Size = new System.Drawing.Size(200, 21);
            this.cbDigit.TabIndex = 293;
            this.cbDigit.SelectedIndexChanged += new System.EventHandler(this.cbDigit_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(510, 76);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 20);
            this.label9.TabIndex = 292;
            this.label9.Text = "Required Qty:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nudReqQty
            // 
            this.nudReqQty.DecimalPlaces = 2;
            this.nudReqQty.Enabled = false;
            this.nudReqQty.Location = new System.Drawing.Point(614, 76);
            this.nudReqQty.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudReqQty.Name = "nudReqQty";
            this.nudReqQty.Size = new System.Drawing.Size(86, 20);
            this.nudReqQty.TabIndex = 291;
            this.nudReqQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudReqQty.ValueChanged += new System.EventHandler(this.nudReqQty_ValueChanged);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(946, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 20);
            this.label8.TabIndex = 290;
            this.label8.Text = "Related Op:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPartDesc
            // 
            this.txtPartDesc.Enabled = false;
            this.txtPartDesc.Location = new System.Drawing.Point(614, 15);
            this.txtPartDesc.Name = "txtPartDesc";
            this.txtPartDesc.Size = new System.Drawing.Size(538, 20);
            this.txtPartDesc.TabIndex = 289;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(510, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 20);
            this.label7.TabIndex = 288;
            this.label7.Text = "Description:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(196, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 20);
            this.label4.TabIndex = 287;
            this.label4.Text = "Part Category:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(196, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 20);
            this.label3.TabIndex = 286;
            this.label3.Text = "Part Number:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(196, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 20);
            this.label1.TabIndex = 285;
            this.label1.Text = "ModelNo Digit:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbUnitType
            // 
            this.lbUnitType.AutoSize = true;
            this.lbUnitType.Enabled = false;
            this.lbUnitType.Location = new System.Drawing.Point(1159, 298);
            this.lbUnitType.Name = "lbUnitType";
            this.lbUnitType.Size = new System.Drawing.Size(50, 13);
            this.lbUnitType.TabIndex = 317;
            this.lbUnitType.Text = "UnitType";
            this.lbUnitType.Visible = false;
            // 
            // gbUnitType
            // 
            this.gbUnitType.Controls.Add(this.rbOAN);
            this.gbUnitType.Controls.Add(this.rbOAK);
            this.gbUnitType.Controls.Add(this.rbOAG);
            this.gbUnitType.Controls.Add(this.rbOAD);
            this.gbUnitType.Controls.Add(this.rbOAB);
            this.gbUnitType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbUnitType.ForeColor = System.Drawing.Color.RoyalBlue;
            this.gbUnitType.Location = new System.Drawing.Point(15, 16);
            this.gbUnitType.Name = "gbUnitType";
            this.gbUnitType.Size = new System.Drawing.Size(178, 92);
            this.gbUnitType.TabIndex = 318;
            this.gbUnitType.TabStop = false;
            this.gbUnitType.Text = "UnitType";
            // 
            // rbOAN
            // 
            this.rbOAN.AutoSize = true;
            this.rbOAN.Location = new System.Drawing.Point(105, 46);
            this.rbOAN.Name = "rbOAN";
            this.rbOAN.Size = new System.Drawing.Size(51, 17);
            this.rbOAN.TabIndex = 4;
            this.rbOAN.Text = "OAN";
            this.rbOAN.UseVisualStyleBackColor = true;
            this.rbOAN.CheckedChanged += new System.EventHandler(this.rbOAN_CheckedChanged);
            // 
            // rbOAK
            // 
            this.rbOAK.AutoSize = true;
            this.rbOAK.Location = new System.Drawing.Point(106, 23);
            this.rbOAK.Name = "rbOAK";
            this.rbOAK.Size = new System.Drawing.Size(50, 17);
            this.rbOAK.TabIndex = 3;
            this.rbOAK.Text = "OAK";
            this.rbOAK.UseVisualStyleBackColor = true;
            this.rbOAK.CheckedChanged += new System.EventHandler(this.rbOAK_CheckedChanged);
            // 
            // rbOAG
            // 
            this.rbOAG.AutoSize = true;
            this.rbOAG.Location = new System.Drawing.Point(41, 66);
            this.rbOAG.Name = "rbOAG";
            this.rbOAG.Size = new System.Drawing.Size(51, 17);
            this.rbOAG.TabIndex = 2;
            this.rbOAG.Text = "OAG";
            this.rbOAG.UseVisualStyleBackColor = true;
            this.rbOAG.CheckedChanged += new System.EventHandler(this.rbOAG_CheckedChanged);
            // 
            // rbOAD
            // 
            this.rbOAD.AutoSize = true;
            this.rbOAD.Location = new System.Drawing.Point(41, 44);
            this.rbOAD.Name = "rbOAD";
            this.rbOAD.Size = new System.Drawing.Size(51, 17);
            this.rbOAD.TabIndex = 1;
            this.rbOAD.Text = "OAD";
            this.rbOAD.UseVisualStyleBackColor = true;
            this.rbOAD.CheckedChanged += new System.EventHandler(this.rbOAD_CheckedChanged);
            // 
            // rbOAB
            // 
            this.rbOAB.AutoSize = true;
            this.rbOAB.Location = new System.Drawing.Point(41, 23);
            this.rbOAB.Name = "rbOAB";
            this.rbOAB.Size = new System.Drawing.Size(50, 17);
            this.rbOAB.TabIndex = 0;
            this.rbOAB.Text = "OAB";
            this.rbOAB.UseVisualStyleBackColor = true;
            this.rbOAB.CheckedChanged += new System.EventHandler(this.rbOAB_CheckedChanged);
            // 
            // lbMsgText
            // 
            this.lbMsgText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lbMsgText.ForeColor = System.Drawing.Color.Black;
            this.lbMsgText.Location = new System.Drawing.Point(12, 636);
            this.lbMsgText.Name = "lbMsgText";
            this.lbMsgText.Size = new System.Drawing.Size(1331, 21);
            this.lbMsgText.TabIndex = 319;
            this.lbMsgText.Text = "MessageText";
            this.lbMsgText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkBoxRfgComp
            // 
            this.chkBoxRfgComp.AutoSize = true;
            this.chkBoxRfgComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxRfgComp.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxRfgComp.Location = new System.Drawing.Point(15, 114);
            this.chkBoxRfgComp.Name = "chkBoxRfgComp";
            this.chkBoxRfgComp.Size = new System.Drawing.Size(113, 17);
            this.chkBoxRfgComp.TabIndex = 320;
            this.chkBoxRfgComp.Text = "Rfg Component";
            this.chkBoxRfgComp.UseVisualStyleBackColor = true;
            this.chkBoxRfgComp.CheckedChanged += new System.EventHandler(this.chkBoxRfgComp_CheckedChanged);
            // 
            // gbCircuits
            // 
            this.gbCircuits.Controls.Add(this.chkBoxCircuit2);
            this.gbCircuits.Controls.Add(this.chkBoxCircuit1);
            this.gbCircuits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCircuits.ForeColor = System.Drawing.Color.RoyalBlue;
            this.gbCircuits.Location = new System.Drawing.Point(12, 137);
            this.gbCircuits.Name = "gbCircuits";
            this.gbCircuits.Size = new System.Drawing.Size(164, 85);
            this.gbCircuits.TabIndex = 321;
            this.gbCircuits.TabStop = false;
            this.gbCircuits.Text = "Circuits";
            this.gbCircuits.Visible = false;
            // 
            // chkBoxCircuit2
            // 
            this.chkBoxCircuit2.AutoSize = true;
            this.chkBoxCircuit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxCircuit2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxCircuit2.Location = new System.Drawing.Point(33, 47);
            this.chkBoxCircuit2.Name = "chkBoxCircuit2";
            this.chkBoxCircuit2.Size = new System.Drawing.Size(73, 17);
            this.chkBoxCircuit2.TabIndex = 322;
            this.chkBoxCircuit2.Text = "Circuit 2";
            this.chkBoxCircuit2.UseVisualStyleBackColor = true;
            // 
            // chkBoxCircuit1
            // 
            this.chkBoxCircuit1.AutoSize = true;
            this.chkBoxCircuit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxCircuit1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxCircuit1.Location = new System.Drawing.Point(33, 23);
            this.chkBoxCircuit1.Name = "chkBoxCircuit1";
            this.chkBoxCircuit1.Size = new System.Drawing.Size(73, 17);
            this.chkBoxCircuit1.TabIndex = 321;
            this.chkBoxCircuit1.Text = "Circuit 1";
            this.chkBoxCircuit1.UseVisualStyleBackColor = true;
            // 
            // gbSubAsmData
            // 
            this.gbSubAsmData.BackColor = System.Drawing.Color.Gainsboro;
            this.gbSubAsmData.Controls.Add(this.label6);
            this.gbSubAsmData.Controls.Add(this.cbImmediateParentPartNum);
            this.gbSubAsmData.Enabled = false;
            this.gbSubAsmData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbSubAsmData.ForeColor = System.Drawing.Color.RoyalBlue;
            this.gbSubAsmData.Location = new System.Drawing.Point(121, 262);
            this.gbSubAsmData.Name = "gbSubAsmData";
            this.gbSubAsmData.Size = new System.Drawing.Size(1032, 49);
            this.gbSubAsmData.TabIndex = 322;
            this.gbSubAsmData.TabStop = false;
            this.gbSubAsmData.Text = "Sub Assembly Parent Data";
            this.gbSubAsmData.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(11, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(190, 20);
            this.label6.TabIndex = 279;
            this.label6.Text = "Primary Assembly Part Num:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbImmediateParentPartNum
            // 
            this.cbImmediateParentPartNum.DropDownWidth = 500;
            this.cbImmediateParentPartNum.Enabled = false;
            this.cbImmediateParentPartNum.FormattingEnabled = true;
            this.cbImmediateParentPartNum.Location = new System.Drawing.Point(206, 17);
            this.cbImmediateParentPartNum.Name = "cbImmediateParentPartNum";
            this.cbImmediateParentPartNum.Size = new System.Drawing.Size(235, 21);
            this.cbImmediateParentPartNum.TabIndex = 278;
            // 
            // chkBoxSubAsm
            // 
            this.chkBoxSubAsm.AutoSize = true;
            this.chkBoxSubAsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxSubAsm.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxSubAsm.Location = new System.Drawing.Point(13, 263);
            this.chkBoxSubAsm.Name = "chkBoxSubAsm";
            this.chkBoxSubAsm.Size = new System.Drawing.Size(100, 17);
            this.chkBoxSubAsm.TabIndex = 323;
            this.chkBoxSubAsm.Text = "SubAssembly";
            this.chkBoxSubAsm.UseVisualStyleBackColor = true;
            this.chkBoxSubAsm.CheckedChanged += new System.EventHandler(this.chkBoxSubAsm_CheckedChanged);
            // 
            // chkBoxMtlPart
            // 
            this.chkBoxMtlPart.AutoSize = true;
            this.chkBoxMtlPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxMtlPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxMtlPart.Location = new System.Drawing.Point(13, 285);
            this.chkBoxMtlPart.Name = "chkBoxMtlPart";
            this.chkBoxMtlPart.Size = new System.Drawing.Size(98, 17);
            this.chkBoxMtlPart.TabIndex = 325;
            this.chkBoxMtlPart.Text = "Material Part";
            this.chkBoxMtlPart.UseVisualStyleBackColor = true;
            this.chkBoxMtlPart.CheckedChanged += new System.EventHandler(this.chkBoxMtlPart_CheckedChanged);
            // 
            // rtbComments
            // 
            this.rtbComments.Location = new System.Drawing.Point(1179, 184);
            this.rtbComments.Name = "rtbComments";
            this.rtbComments.Size = new System.Drawing.Size(161, 96);
            this.rtbComments.TabIndex = 326;
            this.rtbComments.Text = "";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label11.Location = new System.Drawing.Point(1176, 161);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(87, 20);
            this.label11.TabIndex = 327;
            this.label11.Text = "Comments";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1355, 662);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.rtbComments);
            this.Controls.Add(this.chkBoxMtlPart);
            this.Controls.Add(this.chkBoxSubAsm);
            this.Controls.Add(this.gbSubAsmData);
            this.Controls.Add(this.gbCircuits);
            this.Controls.Add(this.chkBoxRfgComp);
            this.Controls.Add(this.lbMsgText);
            this.Controls.Add(this.gbUnitType);
            this.Controls.Add(this.lbUnitType);
            this.Controls.Add(this.lbModDate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lbModBy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnAllRuleSets);
            this.Controls.Add(this.btnDeleteRuleSet);
            this.Controls.Add(this.lbScreenMode);
            this.Controls.Add(this.cbPartNum);
            this.Controls.Add(this.btnAddNewRule);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lbSelectedRow);
            this.Controls.Add(this.gbValues);
            this.Controls.Add(this.txtRelatedOp);
            this.Controls.Add(this.txtPartCategory);
            this.Controls.Add(this.dgvUpdateRules);
            this.Controls.Add(this.txtRuleID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnUpdCancel);
            this.Controls.Add(this.txtBatchID);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cbDigit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.nudReqQty);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPartDesc);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "frmUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Update Rules";
            this.Load += new System.EventHandler(this.frmUpdate_Load);
            this.gbValues.ResumeLayout(false);
            this.gbValues.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUpdateRules)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudReqQty)).EndInit();
            this.gbUnitType.ResumeLayout(false);
            this.gbUnitType.PerformLayout();
            this.gbCircuits.ResumeLayout(false);
            this.gbCircuits.PerformLayout();
            this.gbSubAsmData.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbModDate;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label lbModBy;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button btnAllRuleSets;
        public System.Windows.Forms.Button btnDeleteRuleSet;
        public System.Windows.Forms.Label lbScreenMode;
        public System.Windows.Forms.ComboBox cbPartNum;
        public System.Windows.Forms.Button btnAddNewRule;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.Label lbSelectedRow;
        public System.Windows.Forms.GroupBox gbValues;
        public System.Windows.Forms.CheckBox cbVal17;
        public System.Windows.Forms.CheckBox cbVal1;
        public System.Windows.Forms.CheckBox cbVal2;
        public System.Windows.Forms.CheckBox cbVal3;
        public System.Windows.Forms.CheckBox cbVal4;
        public System.Windows.Forms.CheckBox cbVal5;
        public System.Windows.Forms.CheckBox cbVal6;
        public System.Windows.Forms.CheckBox cbVal7;
        public System.Windows.Forms.CheckBox cbVal8;
        public System.Windows.Forms.CheckBox cbVal9;
        public System.Windows.Forms.CheckBox cbVal10;
        public System.Windows.Forms.CheckBox cbVal11;
        public System.Windows.Forms.CheckBox cbVal12;
        public System.Windows.Forms.CheckBox cbVal13;
        public System.Windows.Forms.CheckBox cbVal14;
        public System.Windows.Forms.Label lbVal30;
        public System.Windows.Forms.CheckBox checkBox9;
        public System.Windows.Forms.Label lbVal29;
        public System.Windows.Forms.CheckBox cbVal15;
        public System.Windows.Forms.Label lbVal28;
        public System.Windows.Forms.CheckBox cbVal16;
        public System.Windows.Forms.Label lbVal27;
        public System.Windows.Forms.CheckBox cbVal18;
        public System.Windows.Forms.Label lbVal26;
        public System.Windows.Forms.CheckBox cbVal19;
        public System.Windows.Forms.Label lbVal25;
        public System.Windows.Forms.CheckBox cbVal20;
        public System.Windows.Forms.Label lbVal24;
        public System.Windows.Forms.CheckBox cbVal21;
        public System.Windows.Forms.Label lbVal23;
        public System.Windows.Forms.CheckBox cbVal22;
        public System.Windows.Forms.Label lbVal22;
        public System.Windows.Forms.CheckBox cbVal23;
        public System.Windows.Forms.Label lbVal21;
        public System.Windows.Forms.CheckBox cbVal24;
        public System.Windows.Forms.Label lbVal20;
        public System.Windows.Forms.CheckBox cbVal25;
        public System.Windows.Forms.Label lbVal19;
        public System.Windows.Forms.CheckBox cbVal26;
        public System.Windows.Forms.Label lbVal18;
        public System.Windows.Forms.CheckBox cbVal27;
        public System.Windows.Forms.Label lbVal17;
        public System.Windows.Forms.CheckBox cbVal28;
        public System.Windows.Forms.Label lbVal16;
        public System.Windows.Forms.CheckBox cbVal29;
        public System.Windows.Forms.Label lbVal15;
        public System.Windows.Forms.CheckBox cbVal30;
        public System.Windows.Forms.Label lbVal14;
        public System.Windows.Forms.Label lbVal1;
        public System.Windows.Forms.Label lbVal13;
        public System.Windows.Forms.Label lbVal2;
        public System.Windows.Forms.Label lbVal12;
        public System.Windows.Forms.Label lbVal3;
        public System.Windows.Forms.Label lbVal11;
        public System.Windows.Forms.Label lbVal4;
        public System.Windows.Forms.Label lbVal10;
        public System.Windows.Forms.Label lbVal5;
        public System.Windows.Forms.Label lbVal9;
        public System.Windows.Forms.Label lbVal6;
        public System.Windows.Forms.Label lbVal8;
        public System.Windows.Forms.Label lbVal7;
        public System.Windows.Forms.TextBox txtRelatedOp;
        public System.Windows.Forms.TextBox txtPartCategory;
        public System.Windows.Forms.DataGridView dgvUpdateRules;
        public System.Windows.Forms.TextBox txtRuleID;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnUpdCancel;
        public System.Windows.Forms.TextBox txtBatchID;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.ComboBox cbDigit;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.NumericUpDown nudReqQty;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtPartDesc;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lbUnitType;
        public System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.GroupBox gbUnitType;
        public System.Windows.Forms.RadioButton rbOAN;
        public System.Windows.Forms.RadioButton rbOAK;
        public System.Windows.Forms.RadioButton rbOAG;
        public System.Windows.Forms.RadioButton rbOAD;
        public System.Windows.Forms.RadioButton rbOAB;
        public System.Windows.Forms.Label lbMsgText;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRuleHeadID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRuleBatchID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPartNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPartDesc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColRelatedOp;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPartCat;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUnitType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDigit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColHeatType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColUpdateType;
        private System.Windows.Forms.DataGridViewButtonColumn ColUpdateButton;
        private System.Windows.Forms.DataGridViewButtonColumn ColDelButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColID;
        public System.Windows.Forms.CheckBox chkBoxRfgComp;
        public System.Windows.Forms.GroupBox gbCircuits;
        public System.Windows.Forms.CheckBox chkBoxCircuit2;
        public System.Windows.Forms.CheckBox chkBoxCircuit1;
        public System.Windows.Forms.GroupBox gbSubAsmData;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cbImmediateParentPartNum;
        public System.Windows.Forms.CheckBox chkBoxSubAsm;
        public System.Windows.Forms.CheckBox chkBoxMtlPart;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.RichTextBox rtbComments;

    }
}