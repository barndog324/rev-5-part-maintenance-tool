﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateFurnaceData : Form
    {
        
        private frmMain m_parent;
        FurnaceBO objFurn = new FurnaceBO();
        PartsMainBO objPart = new PartsMainBO();
        string sFuelType = "";
        string sVolts = "";
       
        public frmUpdateFurnaceData(frmMain frmMn)
        {
            m_parent = frmMn;             
            InitializeComponent();
        }

        private void btnUpdFurnaceSave_Click(object sender, EventArgs e)
        {
            if (validFurnaceDataUpdates() == true)
            {
                objFurn.ID = lbUpdFurnaceID.Text;
                objFurn.RuleHeadID = txtUpdFurnaceRuleHeadID.Text;
                objFurn.PartNum = cbUpdFurnacePartNum.Text;
                objFurn.Amps = txtUpdFurnaceAmps.Text;
                objFurn.Volts = sVolts;
                objFurn.BurnerRatio = txtUpdFurnaceBurnerRatio.Text;
                objFurn.Phase = txtUpdFurnacePhase.Text;
                objFurn.FuelType = sFuelType;
                objFurn.MBHkW = txtUpdFurnaceMBHkW.Text;
                objFurn.Username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                objFurn.DateMod = DateTime.Now.ToString("g");  // 2/27/2009 12:12 PM format     

                m_parent.mAmps = objFurn.Amps;
                m_parent.mVolts = objFurn.Volts;
                m_parent.mBurnerRatio = objFurn.BurnerRatio;
                m_parent.mPhase = objFurn.Phase;
                m_parent.mFuelType = objFurn.FuelType;
                m_parent.mMBHkW = objFurn.MBHkW;
                m_parent.mModBy = objFurn.Username;
                m_parent.mLastModDate = objFurn.DateMod;

                if (this.btnUpdFurnaceSave.Text == "Update")
                {
                    objFurn.Update_ROA_FurnaceDataDTL();
                }
                else if (this.btnUpdFurnaceSave.Text == "Add")
                {
                    objFurn.InsertROA_FurnaceDataDTL();                    
                }
                else if (this.btnUpdFurnaceSave.Text == "Delete")
                {
                    DialogResult result1 = MessageBox.Show("Are you sure you want to Delete Furnace: " + objFurn.PartNum + "? Press Yes to delete; No to cancel!",
                                                      "Deleting a ROA_FurnaceDataDTL Row",
                                                      MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes)
                    {
                        objFurn.DeleteROA_FurnaceDataDTL();
                    }
                }
                this.Close();
            }
        }

        private void btnUpdFurnaceCancel_Click(object sender, EventArgs e)
        {
            m_parent.mVolts = "NoUpdate";
            this.Close();
        }       

        private bool validFurnaceDataUpdates()
        {
            bool retVal = true;
           
            decimal dAmps;            
            int iPhase;
            int iMBHkW;
           
            string errors = String.Empty;

            if (cbUpdFurnacePartNum.SelectedIndex == 0)
            {
                errors += "Error - No Part Number has been selected.\n";
            }           

            try
            {
                dAmps = Decimal.Parse(txtUpdFurnaceAmps.Text);

                if (dAmps <= 0)
                {
                    errors += "Error - Invalid Amps value, field must be a positive decimal value greater than zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Amps value, field must be a positive decimal value greater than zero.\n";
            }
           
            if (txtUpdFurnaceBurnerRatio.Text.Length == 0)
            {
                errors += "Error - Burner Ratio is a required field.\n";
            }

            try
            {
                iPhase = Int32.Parse(txtUpdFurnacePhase.Text);

                if (iPhase < 0)
                {
                    errors += "Error - Invalid Phase value, field must be a positive integer value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Phase value, field must be a positive integer value or zero.\n";
            }

            try
            {
                iMBHkW = Int32.Parse(txtUpdFurnaceMBHkW.Text);

                if (iMBHkW < 0)
                {
                    errors += "Error - Invalid MBHkW value, field must be a positive integer value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid MBHkW value, field must be a positive integer value or zero.\n";
            }            

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }

        private void rbUpdFurnaceElectric_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdFurnaceElectric.Checked == true)
            {
                sFuelType = "ELEC";
            }
        }

        private void rbUpdFurnacePropane_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdFurnacePropane.Checked == true)
            {
                sFuelType = "LP";
            }
        }

        private void rbUpdFurnaceNaturalGas_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdFurnaceNaturalGas.Checked == true)
            {
                sFuelType = "NG";
            }
        }

        private void rbUpdFurnaceVolts120_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdFurnaceVolts120.Checked == true)
            {
                sVolts = "120";
            }
        }

        private void rbUpdFurnaceVolts208_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdFurnaceVolts208.Checked == true)
            {
                sVolts = "208";
            }
        }

        private void rbUpdFurnaceVolts460_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdFurnaceVolts460.Checked == true)
            {
                sVolts = "460";
            }
        }

        private void rbUpdFurnaceVolts575_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdFurnaceVolts575.Checked == true)
            {
                sVolts = "575";
            }
        }

        private void cbUpdFurnacePartNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            string partDesc = "";
            string partNumDesc = "";
            string sMBHkW = "";
            int startLoc = 0;

            if (cbUpdFurnacePartNum.SelectedIndex != 0)
            {
                objFurn.RuleHeadID = cbUpdFurnacePartNum.SelectedValue.ToString();
                DataTable dt = objFurn.GetROA_FurnaceDataDTL();
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("ERROR - Furnace already exist in ROA_FurnaceDataDTL table!");
                }
                else
                {
                    txtUpdFurnaceRuleHeadID.Text = objFurn.RuleHeadID;
                    partNumDesc = cbUpdFurnacePartNum.Text;
                    startLoc = partNumDesc.IndexOf(" - ");
                    if (startLoc > -1)
                    {
                        partDesc = partNumDesc.Substring((startLoc + 3), (partNumDesc.Length - (startLoc + 3)));

                        if (partDesc.ToUpper().Contains("KW") == true)
                        {
                            startLoc = partDesc.ToUpper().IndexOf("KW");
                        }                       

                        if (startLoc > -1)
                        {
                            int x = 0;
                            if (partDesc[startLoc-1] == ' ')
                            {
                                x = startLoc - 2;
                            }
                            else
                            {
                                x = startLoc - 1;
                            }
                             
                            while (partDesc[x] != ' ' && x != 0)
                            {
                                --x;
                            }
                            sMBHkW = partDesc.Substring(x, (startLoc - x));
                            txtUpdFurnaceMBHkW.Text = sMBHkW.Trim();
                        }
                        else
                        {
                            if (partDesc.ToUpper().Contains("MBH") == true)
                            {
                                startLoc = partDesc.IndexOf("MBH");
                            }
                            int x = startLoc - 1;
                            while (partDesc[x] != ' ' && x != 0)
                            {
                                --x;
                            }
                            sMBHkW = partDesc.Substring(x, (startLoc - x));
                            txtUpdFurnaceMBHkW.Text = sMBHkW;
                        }

                        if (partDesc.Contains("208V") == true)
                        {
                            rbUpdFurnaceVolts208.Checked = true;
                        }
                        else if (partDesc.Contains("460V") == true)
                        {
                            rbUpdFurnaceVolts460.Checked = true;
                        }
                        else if (partDesc.Contains("575V") == true)
                        {
                            rbUpdFurnaceVolts575.Checked = true;
                        }

                        if (partDesc.ToUpper().Contains("ELEC") == true)
                        {
                            rbUpdFurnaceElectric.Checked = true;
                        }

                    }
                    txtUpdFurnacePartDesc.Text = partDesc;
                    txtUpdFurnacePhase.Text = "3";
                    txtUpdFurnaceAmps.Focus();
                }
            }
        }
    }
}
