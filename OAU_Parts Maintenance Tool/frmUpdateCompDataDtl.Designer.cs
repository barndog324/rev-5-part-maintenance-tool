﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmUpdateCompDataDtl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCompDtlID = new System.Windows.Forms.Label();
            this.txtCompDtlPhase = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCompDtlPartDesc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCompDtlRuleHeadID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCompDtlSave = new System.Windows.Forms.Button();
            this.btnCompDtlCancel = new System.Windows.Forms.Button();
            this.gbVoltage = new System.Windows.Forms.GroupBox();
            this.rbCompDtlVolt575 = new System.Windows.Forms.RadioButton();
            this.rbCompDtlVolt460 = new System.Windows.Forms.RadioButton();
            this.rbCompDtlVolt208 = new System.Windows.Forms.RadioButton();
            this.txtCompDtlRLA = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCompDtlLRA = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCompDtlModifedBy = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCompDtlLastModDate = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCompDtlPartCategory = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbCompDtl_PartNum = new System.Windows.Forms.ComboBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.gbVoltage.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbCompDtlID
            // 
            this.lbCompDtlID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCompDtlID.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lbCompDtlID.Location = new System.Drawing.Point(421, 126);
            this.lbCompDtlID.Name = "lbCompDtlID";
            this.lbCompDtlID.Size = new System.Drawing.Size(75, 20);
            this.lbCompDtlID.TabIndex = 121;
            this.lbCompDtlID.Text = "DigitNo:";
            this.lbCompDtlID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbCompDtlID.Visible = false;
            // 
            // txtCompDtlPhase
            // 
            this.txtCompDtlPhase.Location = new System.Drawing.Point(189, 130);
            this.txtCompDtlPhase.Name = "txtCompDtlPhase";
            this.txtCompDtlPhase.Size = new System.Drawing.Size(45, 20);
            this.txtCompDtlPhase.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(27, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 20);
            this.label4.TabIndex = 117;
            this.label4.Text = "Phase:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCompDtlPartDesc
            // 
            this.txtCompDtlPartDesc.Enabled = false;
            this.txtCompDtlPartDesc.Location = new System.Drawing.Point(189, 77);
            this.txtCompDtlPartDesc.Name = "txtCompDtlPartDesc";
            this.txtCompDtlPartDesc.Size = new System.Drawing.Size(214, 20);
            this.txtCompDtlPartDesc.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(27, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 20);
            this.label2.TabIndex = 113;
            this.label2.Text = "Part Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(27, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 20);
            this.label1.TabIndex = 111;
            this.label1.Text = "Part Number:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCompDtlRuleHeadID
            // 
            this.txtCompDtlRuleHeadID.Enabled = false;
            this.txtCompDtlRuleHeadID.Location = new System.Drawing.Point(189, 25);
            this.txtCompDtlRuleHeadID.Name = "txtCompDtlRuleHeadID";
            this.txtCompDtlRuleHeadID.Size = new System.Drawing.Size(59, 20);
            this.txtCompDtlRuleHeadID.TabIndex = 110;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(27, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 20);
            this.label5.TabIndex = 109;
            this.label5.Text = "Rule Head ID:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCompDtlSave
            // 
            this.btnCompDtlSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompDtlSave.ForeColor = System.Drawing.Color.Green;
            this.btnCompDtlSave.Location = new System.Drawing.Point(424, 151);
            this.btnCompDtlSave.Name = "btnCompDtlSave";
            this.btnCompDtlSave.Size = new System.Drawing.Size(75, 30);
            this.btnCompDtlSave.TabIndex = 11;
            this.btnCompDtlSave.Text = "Update";
            this.btnCompDtlSave.UseVisualStyleBackColor = true;
            this.btnCompDtlSave.Click += new System.EventHandler(this.btnCompDtlSave_Click);
            // 
            // btnCompDtlCancel
            // 
            this.btnCompDtlCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompDtlCancel.ForeColor = System.Drawing.Color.Red;
            this.btnCompDtlCancel.Location = new System.Drawing.Point(424, 188);
            this.btnCompDtlCancel.Name = "btnCompDtlCancel";
            this.btnCompDtlCancel.Size = new System.Drawing.Size(75, 30);
            this.btnCompDtlCancel.TabIndex = 12;
            this.btnCompDtlCancel.Text = "Cancel";
            this.btnCompDtlCancel.UseVisualStyleBackColor = true;
            this.btnCompDtlCancel.Click += new System.EventHandler(this.btnCompDtlCancel_Click);
            // 
            // gbVoltage
            // 
            this.gbVoltage.Controls.Add(this.rbCompDtlVolt575);
            this.gbVoltage.Controls.Add(this.rbCompDtlVolt460);
            this.gbVoltage.Controls.Add(this.rbCompDtlVolt208);
            this.gbVoltage.ForeColor = System.Drawing.Color.Green;
            this.gbVoltage.Location = new System.Drawing.Point(424, 29);
            this.gbVoltage.Name = "gbVoltage";
            this.gbVoltage.Size = new System.Drawing.Size(74, 94);
            this.gbVoltage.TabIndex = 7;
            this.gbVoltage.TabStop = false;
            this.gbVoltage.Text = "Voltage";
            // 
            // rbCompDtlVolt575
            // 
            this.rbCompDtlVolt575.AutoSize = true;
            this.rbCompDtlVolt575.Location = new System.Drawing.Point(16, 63);
            this.rbCompDtlVolt575.Name = "rbCompDtlVolt575";
            this.rbCompDtlVolt575.Size = new System.Drawing.Size(43, 17);
            this.rbCompDtlVolt575.TabIndex = 10;
            this.rbCompDtlVolt575.Text = "575";
            this.rbCompDtlVolt575.UseVisualStyleBackColor = true;
            // 
            // rbCompDtlVolt460
            // 
            this.rbCompDtlVolt460.AutoSize = true;
            this.rbCompDtlVolt460.Location = new System.Drawing.Point(16, 41);
            this.rbCompDtlVolt460.Name = "rbCompDtlVolt460";
            this.rbCompDtlVolt460.Size = new System.Drawing.Size(43, 17);
            this.rbCompDtlVolt460.TabIndex = 9;
            this.rbCompDtlVolt460.Text = "460";
            this.rbCompDtlVolt460.UseVisualStyleBackColor = true;
            // 
            // rbCompDtlVolt208
            // 
            this.rbCompDtlVolt208.AutoSize = true;
            this.rbCompDtlVolt208.Location = new System.Drawing.Point(16, 20);
            this.rbCompDtlVolt208.Name = "rbCompDtlVolt208";
            this.rbCompDtlVolt208.Size = new System.Drawing.Size(43, 17);
            this.rbCompDtlVolt208.TabIndex = 8;
            this.rbCompDtlVolt208.Text = "208";
            this.rbCompDtlVolt208.UseVisualStyleBackColor = true;
            // 
            // txtCompDtlRLA
            // 
            this.txtCompDtlRLA.Location = new System.Drawing.Point(189, 156);
            this.txtCompDtlRLA.Name = "txtCompDtlRLA";
            this.txtCompDtlRLA.Size = new System.Drawing.Size(45, 20);
            this.txtCompDtlRLA.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(27, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 20);
            this.label6.TabIndex = 123;
            this.label6.Text = "RLA:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCompDtlLRA
            // 
            this.txtCompDtlLRA.Location = new System.Drawing.Point(189, 182);
            this.txtCompDtlLRA.Name = "txtCompDtlLRA";
            this.txtCompDtlLRA.Size = new System.Drawing.Size(45, 20);
            this.txtCompDtlLRA.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(27, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(156, 20);
            this.label7.TabIndex = 125;
            this.label7.Text = "LRA:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCompDtlModifedBy
            // 
            this.txtCompDtlModifedBy.Enabled = false;
            this.txtCompDtlModifedBy.Location = new System.Drawing.Point(189, 208);
            this.txtCompDtlModifedBy.Name = "txtCompDtlModifedBy";
            this.txtCompDtlModifedBy.Size = new System.Drawing.Size(90, 20);
            this.txtCompDtlModifedBy.TabIndex = 128;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label8.Location = new System.Drawing.Point(27, 208);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 20);
            this.label8.TabIndex = 127;
            this.label8.Text = "Modified By:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCompDtlLastModDate
            // 
            this.txtCompDtlLastModDate.Enabled = false;
            this.txtCompDtlLastModDate.Location = new System.Drawing.Point(189, 234);
            this.txtCompDtlLastModDate.Name = "txtCompDtlLastModDate";
            this.txtCompDtlLastModDate.Size = new System.Drawing.Size(90, 20);
            this.txtCompDtlLastModDate.TabIndex = 130;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(27, 234);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 20);
            this.label9.TabIndex = 129;
            this.label9.Text = "Last Modified:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCompDtlPartCategory
            // 
            this.txtCompDtlPartCategory.Enabled = false;
            this.txtCompDtlPartCategory.Location = new System.Drawing.Point(189, 103);
            this.txtCompDtlPartCategory.Name = "txtCompDtlPartCategory";
            this.txtCompDtlPartCategory.Size = new System.Drawing.Size(214, 20);
            this.txtCompDtlPartCategory.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label10.Location = new System.Drawing.Point(27, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 20);
            this.label10.TabIndex = 131;
            this.label10.Text = "Part Category:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCompDtl_PartNum
            // 
            this.cbCompDtl_PartNum.DropDownWidth = 500;
            this.cbCompDtl_PartNum.Enabled = false;
            this.cbCompDtl_PartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCompDtl_PartNum.FormattingEnabled = true;
            this.cbCompDtl_PartNum.Location = new System.Drawing.Point(189, 49);
            this.cbCompDtl_PartNum.Name = "cbCompDtl_PartNum";
            this.cbCompDtl_PartNum.Size = new System.Drawing.Size(214, 23);
            this.cbCompDtl_PartNum.TabIndex = 1;
            this.cbCompDtl_PartNum.SelectedIndexChanged += new System.EventHandler(this.cbCompDtl_PartNum_SelectedIndexChanged);
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(424, 224);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 30);
            this.btnDelete.TabIndex = 132;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmUpdateCompDataDtl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 301);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.cbCompDtl_PartNum);
            this.Controls.Add(this.txtCompDtlPartCategory);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtCompDtlLastModDate);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCompDtlModifedBy);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtCompDtlLRA);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCompDtlRLA);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.gbVoltage);
            this.Controls.Add(this.lbCompDtlID);
            this.Controls.Add(this.txtCompDtlPhase);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCompDtlPartDesc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCompDtlRuleHeadID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnCompDtlSave);
            this.Controls.Add(this.btnCompDtlCancel);
            this.Name = "frmUpdateCompDataDtl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Compressor Detail Update";
            this.gbVoltage.ResumeLayout(false);
            this.gbVoltage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbCompDtlID;
        public System.Windows.Forms.TextBox txtCompDtlPhase;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtCompDtlPartDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtCompDtlRuleHeadID;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btnCompDtlSave;
        private System.Windows.Forms.Button btnCompDtlCancel;
        private System.Windows.Forms.GroupBox gbVoltage;
        public System.Windows.Forms.TextBox txtCompDtlRLA;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtCompDtlLRA;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtCompDtlModifedBy;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtCompDtlLastModDate;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtCompDtlPartCategory;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.RadioButton rbCompDtlVolt575;
        public System.Windows.Forms.RadioButton rbCompDtlVolt460;
        public System.Windows.Forms.RadioButton rbCompDtlVolt208;
        public System.Windows.Forms.ComboBox cbCompDtl_PartNum;
        public System.Windows.Forms.Button btnDelete;
    }
}