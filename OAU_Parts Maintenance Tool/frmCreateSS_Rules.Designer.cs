﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmCreateSS_Rules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.dgvSelectedParts = new System.Windows.Forms.DataGridView();
            this.PartNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvVmeParts = new System.Windows.Forms.DataGridView();
            this.txtSearchPartNum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSelectedParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVmeParts)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate.ForeColor = System.Drawing.Color.Green;
            this.btnCreate.Location = new System.Drawing.Point(523, 480);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(88, 23);
            this.btnCreate.TabIndex = 11;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Red;
            this.btnCancel.Location = new System.Drawing.Point(748, 480);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 23);
            this.btnCancel.TabIndex = 9;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.Blue;
            this.btnClear.Location = new System.Drawing.Point(636, 480);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 23);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Reset";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // dgvSelectedParts
            // 
            this.dgvSelectedParts.AllowUserToAddRows = false;
            this.dgvSelectedParts.AllowUserToDeleteRows = false;
            this.dgvSelectedParts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSelectedParts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PartNum});
            this.dgvSelectedParts.Location = new System.Drawing.Point(636, 48);
            this.dgvSelectedParts.Name = "dgvSelectedParts";
            this.dgvSelectedParts.Size = new System.Drawing.Size(200, 423);
            this.dgvSelectedParts.TabIndex = 8;
            this.dgvSelectedParts.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvSelectedParts_CellMouseDoubleClick);
            // 
            // PartNum
            // 
            this.PartNum.HeaderText = "PartNum";
            this.PartNum.Name = "PartNum";
            this.PartNum.Width = 150;
            // 
            // dgvVmeParts
            // 
            this.dgvVmeParts.AllowUserToAddRows = false;
            this.dgvVmeParts.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvVmeParts.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVmeParts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVmeParts.Location = new System.Drawing.Point(13, 48);
            this.dgvVmeParts.MultiSelect = false;
            this.dgvVmeParts.Name = "dgvVmeParts";
            this.dgvVmeParts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVmeParts.Size = new System.Drawing.Size(598, 423);
            this.dgvVmeParts.TabIndex = 7;
            this.dgvVmeParts.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvVmeParts_CellMouseDoubleClick);
            // 
            // txtSearchPartNum
            // 
            this.txtSearchPartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchPartNum.Location = new System.Drawing.Point(394, 12);
            this.txtSearchPartNum.Name = "txtSearchPartNum";
            this.txtSearchPartNum.Size = new System.Drawing.Size(235, 21);
            this.txtSearchPartNum.TabIndex = 13;
            this.txtSearchPartNum.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearchPartNum_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(226, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Search Part Number:";
            // 
            // frmCreateSS_Rules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 516);
            this.Controls.Add(this.txtSearchPartNum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.dgvSelectedParts);
            this.Controls.Add(this.dgvVmeParts);
            this.Name = "frmCreateSS_Rules";
            this.Text = "frmCreateSS_Rules";
            ((System.ComponentModel.ISupportInitialize)(this.dgvSelectedParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVmeParts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.DataGridView dgvSelectedParts;
        private System.Windows.Forms.DataGridViewTextBoxColumn PartNum;
        public System.Windows.Forms.DataGridView dgvVmeParts;
        private System.Windows.Forms.TextBox txtSearchPartNum;
        private System.Windows.Forms.Label label1;
    }
}