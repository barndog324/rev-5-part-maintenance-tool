﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmUpdateMotorData : Form
    {
        private frmMain m_parent;
        MotorBO objMotor = new MotorBO();
        PartsMainBO objPart = new PartsMainBO();

        string gVoltage = "";
       
        public frmUpdateMotorData(frmMain frmMn)
        {
            m_parent = frmMn;      
            InitializeComponent();
        }

        private void btnUpdMotorCancel_Click(object sender, EventArgs e)
        {
            m_parent.mVoltage = "NoUpdate";
            this.Close();
        }

        private void btnUpdMotorSave_Click(object sender, EventArgs e)
        {
            if (validMotorDataUpdates() == true)
            {
                objMotor.ID = lbUpdMotorID.Text;
                objMotor.RuleHeadID = txtUpdMotorRuleHeadID.Text;
                objMotor.PartNum = cbUpdMotorPartNum.Text;
                objMotor.MotorType = cbUpdMotor_MotorType.Text;
                objMotor.FLA = txtUpdMotorFLA.Text;
                objMotor.Hertz = txtUpdMotorHertz.Text;
                objMotor.Phase = txtUpdMotorPhase.Text;
                objMotor.MCC = txtUpdMotorMCC.Text;
                objMotor.RLA = txtUpdMotorRLA.Text;
                objMotor.Voltage = gVoltage;            
                objMotor.HP = txtUpdMotorHP.Text;
                objMotor.Username = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                objMotor.DateMod = DateTime.Now.ToString("g");  // 2/27/2009 12:12 PM format     

                m_parent.mMotorType = objMotor.MotorType;
                m_parent.mFLA = objMotor.FLA;
                m_parent.mHertz = objMotor.Hertz;
                m_parent.mPhase = objMotor.Phase;
                m_parent.mMCC = objMotor.MCC;
                m_parent.mRLA = objMotor.RLA;
                m_parent.mVoltage = objMotor.Voltage;
                m_parent.mHP = objMotor.HP;
                m_parent.mModBy = objMotor.Username;
                m_parent.mLastModDate = objMotor.DateMod;

                if (this.btnUpdMotorSave.Text == "Update")
                {
                    objMotor.Update_ROA_MotorDataDTL();                                       
                }
                else if (this.btnUpdMotorSave.Text == "Add")
                {
                    objMotor.InsertROA_MotorDataDTL();                    
                }
               
                this.Close();
            }
        }

        private bool validMotorDataUpdates()
        {
            bool retVal = true;
            decimal dPhase;
            decimal dHertz;
            decimal dMCC;
            decimal dRLA;
            decimal dHP;
            string errors = String.Empty;

            if (cbUpdMotor_MotorType.Text.Length == 0)
            {
                errors += "Error - Motor Type is a required field.\n";
            }

            if (txtUpdMotorFLA.Text.Length == 0)
            {
                errors += "Error - FLA is a required field and if more than one FLA is listed needs to appear as follows 2.1-4.2\n";
            }

            try
            {
                dHertz = Decimal.Parse(txtUpdMotorHertz.Text);

                if (dHertz < 0)
                {
                    errors += "Error - Invalid Hertz value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Hertz value, field must be a positive decimal value or zero.\n";
            }
           
            try
            {
                dPhase = Decimal.Parse(txtUpdMotorPhase.Text);

                if (dPhase < 0)
                {
                    errors += "Error - Invalid Phase value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid Phase value, field must be a positive decimal value or zero.\n";
            }

            try
            {
                dMCC = Decimal.Parse(txtUpdMotorMCC.Text);

                if (dMCC < 0)
                {
                    errors += "Error - Invalid MCC value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid MCC value, field must be a positive decimal value or zero.\n";
            }


            try
            {
                dRLA = Decimal.Parse(txtUpdMotorRLA.Text);

                if (dRLA < 0)
                {
                    errors += "Error - Invalid RLA value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid RLA value, field must be a positive decimal value or zero.\n";
            }

            try
            {
                dHP = Decimal.Parse(txtUpdMotorHP.Text);

                if (dHP < 0)
                {
                    errors += "Error - Invalid HP value, field must be a positive decimal value or zero.\n";
                }
            }
            catch
            {
                errors += "Error - Invalid HP value, field must be a positive decimal value or zero.\n";
            }

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
                retVal = false;
            }

            return retVal;
        }

        private void cbUpdMotorPartNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            int pos = 0;
            if (cbUpdMotorPartNum.SelectedIndex != 0)
            {
                objPart.PartNum = cbUpdMotorPartNum.SelectedValue.ToString();                                                           

                objPart.GetOAU_PartNumberInfo();
                if (objPart.PartDescription != "")
                {
                    lbUpdMotorID.Text = objPart.RuleHeadID;
                    txtUpdMotorPartDesc.Text = objPart.PartDescription;
                    txtUpdMotorHertz.Text = "0.00";
                    txtUpdMotorMCC.Text = "0.00";
                    txtUpdMotorRLA.Text = "0.00";
                    txtUpdMotorPhase.Text = "";                    
                    rbUpdMotor208_60_3.Checked = false;                    
                    rbUpdMotor460_60_3.Checked = false;
                    rbUpdMotor575_60_3.Checked = false;
                    //gFireEvent = true;
                    cbUpdMotor_MotorType.Enabled = true;
                    cbUpdMotor_MotorType.Select();
                }
            }
        }

        private void rbUpdMotor208_60_3_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdMotor208_60_3.Checked == true)
            {
                gVoltage = "208";
            }
        }

        private void rbUpdMotor460_60_3_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdMotor460_60_3.Checked == true)
            {
                gVoltage = "460";
            }
        }

        private void rbUpdMotor575_60_3_CheckedChanged(object sender, EventArgs e)
        {
            if (rbUpdMotor575_60_3.Checked == true)
            {
                gVoltage = "575";
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            objMotor.PartNum = cbUpdMotorPartNum.Text;
            objMotor.Voltage = gVoltage;
            objMotor.ID = lbUpdMotorID.Text;

            DialogResult result1 = MessageBox.Show("Are you sure you want to Delete Motor: " + objMotor.PartNum + "? Press Yes to delete; No to cancel!",
                                              "Deleting a ROA_MotorDataDTL Row",
                                              MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
            {
                m_parent.mVoltage = "NoUpdate";
                objMotor.DeleteROA_MotorDataDTL();
                this.Close();     
            }                    
        }
    }
}
