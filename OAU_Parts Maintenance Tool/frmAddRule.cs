﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OAU_Parts_Maintenance_Tool
{
    public partial class frmAddRule : Form
    {
        PartsMainBO objPart = new PartsMainBO();
        ModelNumDescBO objModel = new ModelNumDescBO();

        private frmMain m_parent;
        public string mHeatType { get; set; }
        public string mOAUTypeCode { get; set; }

        public frmAddRule(frmMain frmMn)
        {
            m_parent = frmMn;
            InitializeComponent();
            mHeatType = "";
            mOAUTypeCode = "";
        }

        private void btnUpdCancel_Click(object sender, EventArgs e)
        {
            m_parent.mQty = "-1";
            this.Close();
        }

        private void cbDigit_SelectedIndexChanged(object sender, EventArgs e)
        {
            string digitStr = cbDigit.Text;
            int digitInt = Int32.Parse(digitStr);
            setupValueCheckBoxes(this, digitInt);
            //if (digitInt == 22 || digitInt == 23)
            //{
            //    gbHeatType.Visible = true;
            //    gbUnitType.Visible = true;
            //}
            //else
            //{
            //    gbHeatType.Visible = false;
            //    gbUnitType.Visible = false;
            //}
        }        

        private void btnSave_Click(object sender, EventArgs e)
        {
            int batchID;
            string valueStr = "";
            string errorStr = "";

            bool firstValue = true;
            bool valueSelected = false;

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(this.lbVal1);
            labelControls.Add(this.lbVal2);
            labelControls.Add(this.lbVal3);
            labelControls.Add(this.lbVal4);
            labelControls.Add(this.lbVal5);
            labelControls.Add(this.lbVal6);
            labelControls.Add(this.lbVal7);
            labelControls.Add(this.lbVal8);
            labelControls.Add(this.lbVal9);
            labelControls.Add(this.lbVal10);
            labelControls.Add(this.lbVal11);
            labelControls.Add(this.lbVal12);
            labelControls.Add(this.lbVal13);
            labelControls.Add(this.lbVal14);
            labelControls.Add(this.lbVal15);
            labelControls.Add(this.lbVal16);
            labelControls.Add(this.lbVal17);
            labelControls.Add(this.lbVal18);
            labelControls.Add(this.lbVal19);
            labelControls.Add(this.lbVal20);
            labelControls.Add(this.lbVal21);
            labelControls.Add(this.lbVal22);
            labelControls.Add(this.lbVal23);
            labelControls.Add(this.lbVal24);
            labelControls.Add(this.lbVal25);
            labelControls.Add(this.lbVal26);
            labelControls.Add(this.lbVal27);
            labelControls.Add(this.lbVal28);
            labelControls.Add(this.lbVal29);
            labelControls.Add(this.lbVal30);

            checkBoxControls.Add(this.cbVal1);
            checkBoxControls.Add(this.cbVal2);
            checkBoxControls.Add(this.cbVal3);
            checkBoxControls.Add(this.cbVal4);
            checkBoxControls.Add(this.cbVal5);
            checkBoxControls.Add(this.cbVal6);
            checkBoxControls.Add(this.cbVal7);
            checkBoxControls.Add(this.cbVal8);
            checkBoxControls.Add(this.cbVal9);
            checkBoxControls.Add(this.cbVal10);
            checkBoxControls.Add(this.cbVal11);
            checkBoxControls.Add(this.cbVal12);
            checkBoxControls.Add(this.cbVal13);
            checkBoxControls.Add(this.cbVal14);
            checkBoxControls.Add(this.cbVal15);
            checkBoxControls.Add(this.cbVal16);
            checkBoxControls.Add(this.cbVal17);
            checkBoxControls.Add(this.cbVal18);
            checkBoxControls.Add(this.cbVal19);
            checkBoxControls.Add(this.cbVal20);
            checkBoxControls.Add(this.cbVal21);
            checkBoxControls.Add(this.cbVal22);
            checkBoxControls.Add(this.cbVal23);
            checkBoxControls.Add(this.cbVal24);
            checkBoxControls.Add(this.cbVal25);
            checkBoxControls.Add(this.cbVal26);
            checkBoxControls.Add(this.cbVal27);
            checkBoxControls.Add(this.cbVal28);
            checkBoxControls.Add(this.cbVal29);
            checkBoxControls.Add(this.cbVal30);

            for (int i = 0; i < 30; ++i)
            {
                if (checkBoxControls[i].Enabled == true)
                {
                    if (checkBoxControls[i].Checked == true)
                    {
                        valueSelected = true;
                        if (firstValue)
                        {
                            valueStr = labelControls[i].Text;
                            firstValue = false;
                        }
                        else
                        {
                            valueStr += "," + labelControls[i].Text;
                        }
                    }
                }
            }

            if (txtBatchID.Text.Length == 0)
            {
                errorStr += "ERROR -- Batch ID is a required field!\n";
            }
            else
            {
                try
                {
                    batchID = Int32.Parse(txtBatchID.Text);
                }
                catch
                {
                    errorStr += "ERROR -- Batch ID must be a valid integer value!\n";
                }
            }

            if (cbUnitType.Text.Length == 0)
            {
                errorStr += "ERROR -- Unit Type is a required field!\n";
            }
            
            if (cbPartNum.SelectedIndex == 0)
            {
                errorStr += "ERROR -- Part Number is a required field!\n";
            }

            if (cbPartCategory.SelectedIndex == 0)
            {
                errorStr += "ERROR -- Part Category is a required field!\n";
            }

            if (cbRelatedOp.Text.Length == 0)
            {
                errorStr += "ERROR -- Related Operation is a required field!\n";
            }

            if (cbDigit.SelectedIndex == 0)
            {
                errorStr += "ERROR -- Digit is a required field!\n";
            }
            else if (cbDigit.Text == "10" || cbDigit.Text == "19" || cbDigit.Text == "30" )
            {
                errorStr += "ERROR -- Digits 10, 19, & 30 are reserved for future use.\n";
            }

            if (valueSelected == false)
            {
                errorStr += "ERROR -- No values have been selected!\n";
            }

            if (nudReqQty.Value <= 0)
            {
                errorStr += "ERROR -- Required Qty must be Greater Than 0!";
            }

            if (errorStr.Length == 0)
            {
                objPart.RuleHeadID = this.txtRuleID.Text;
                objPart.RuleBatchID = this.txtBatchID.Text;
                objPart.Digit = this.cbDigit.Text;
                objPart.UnitType = this.cbUnitType.Text;
                objPart.Value = valueStr;
                objPart.Qty = this.nudReqQty.Value.ToString();
                objPart.UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();

                objPart.Insert_ROA_PartConditionsDTL();

                DataTable dt = objPart.GetOAU_PartRulesByRuleHeadID_AndRuleBatchID();

                if (dt.Rows.Count > 0)
                {
                    objPart.UpdatePartMaster(dt);
                    m_parent.mRuleHeadID = this.txtRuleID.Text;
                    m_parent.mRuleBatchID = this.txtBatchID.Text;
                    m_parent.mPartNumber = cbPartNum.Text;
                    m_parent.mDigit = this.cbDigit.Text;
                    m_parent.mUnitType = this.cbUnitType.Text;
                    m_parent.mQty = this.nudReqQty.Value.ToString();
                    m_parent.mValueStr = valueStr;
                }

                this.Close();
            }
            else
            {
               
                MessageBox.Show(errorStr);
            }
        }

        private void cbPartNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            objPart.PartNum = cbPartNum.Text;
            if (cbPartNum.SelectedIndex != 0)
            {
                objPart.GetOAU_PartNumberInfo();
                txtRuleID.Text = objPart.RuleHeadID.ToString();
                txtBatchID.Text = objPart.RuleBatchID.ToString();
                cbPartCategory.Text = objPart.PartCategory;
                txtPartDesc.Text = objPart.PartDescription;
                cbRelatedOp.Text = objPart.RelatedOperation.ToString();
                cbUnitType.Focus();
            }
        }

        private void rbIndirectFired_CheckedChanged(object sender, EventArgs e)
        {
            mHeatType = "IF";
            if (rbOALBG.Checked == true)
            {
                changeValueLabels("OALBG");
            }
            else if (rbOAU123DKN.Checked == true)
            {
                changeValueLabels("OAU123DKN");
            } 
        }


        #region Other Methods
        private void setupValueCheckBoxes(frmAddRule frmAdd, int digit)
        {
            int idx = 0;
            string valueStr = "";
            string unitType = cbUnitType.Text;            

            
            DataTable dt = objModel.GetOAU_ModelNoValues(digit, "NA", "", "");

            var labelControls = new List<Control>();
            var checkBoxControls = new List<CheckBox>();

            labelControls.Add(frmAdd.lbVal1);
            labelControls.Add(frmAdd.lbVal2);
            labelControls.Add(frmAdd.lbVal3);
            labelControls.Add(frmAdd.lbVal4);
            labelControls.Add(frmAdd.lbVal5);
            labelControls.Add(frmAdd.lbVal6);
            labelControls.Add(frmAdd.lbVal7);
            labelControls.Add(frmAdd.lbVal8);
            labelControls.Add(frmAdd.lbVal9);
            labelControls.Add(frmAdd.lbVal10);
            labelControls.Add(frmAdd.lbVal11);
            labelControls.Add(frmAdd.lbVal12);
            labelControls.Add(frmAdd.lbVal13);
            labelControls.Add(frmAdd.lbVal14);
            labelControls.Add(frmAdd.lbVal15);
            labelControls.Add(frmAdd.lbVal16);
            labelControls.Add(frmAdd.lbVal17);
            labelControls.Add(frmAdd.lbVal18);
            labelControls.Add(frmAdd.lbVal19);
            labelControls.Add(frmAdd.lbVal20);
            labelControls.Add(frmAdd.lbVal21);
            labelControls.Add(frmAdd.lbVal22);
            labelControls.Add(frmAdd.lbVal23);
            labelControls.Add(frmAdd.lbVal24);
            labelControls.Add(frmAdd.lbVal25);
            labelControls.Add(frmAdd.lbVal26);
            labelControls.Add(frmAdd.lbVal27);
            labelControls.Add(frmAdd.lbVal28);
            labelControls.Add(frmAdd.lbVal29);
            labelControls.Add(frmAdd.lbVal30);

            checkBoxControls.Add(frmAdd.cbVal1);
            checkBoxControls.Add(frmAdd.cbVal2);
            checkBoxControls.Add(frmAdd.cbVal3);
            checkBoxControls.Add(frmAdd.cbVal4);
            checkBoxControls.Add(frmAdd.cbVal5);
            checkBoxControls.Add(frmAdd.cbVal6);
            checkBoxControls.Add(frmAdd.cbVal7);
            checkBoxControls.Add(frmAdd.cbVal8);
            checkBoxControls.Add(frmAdd.cbVal9);
            checkBoxControls.Add(frmAdd.cbVal10);
            checkBoxControls.Add(frmAdd.cbVal11);
            checkBoxControls.Add(frmAdd.cbVal12);
            checkBoxControls.Add(frmAdd.cbVal13);
            checkBoxControls.Add(frmAdd.cbVal14);
            checkBoxControls.Add(frmAdd.cbVal15);
            checkBoxControls.Add(frmAdd.cbVal16);
            checkBoxControls.Add(frmAdd.cbVal17);
            checkBoxControls.Add(frmAdd.cbVal18);
            checkBoxControls.Add(frmAdd.cbVal19);
            checkBoxControls.Add(frmAdd.cbVal20);
            checkBoxControls.Add(frmAdd.cbVal21);
            checkBoxControls.Add(frmAdd.cbVal22);
            checkBoxControls.Add(frmAdd.cbVal23);
            checkBoxControls.Add(frmAdd.cbVal24);
            checkBoxControls.Add(frmAdd.cbVal25);
            checkBoxControls.Add(frmAdd.cbVal26);
            checkBoxControls.Add(frmAdd.cbVal27);
            checkBoxControls.Add(frmAdd.cbVal28);
            checkBoxControls.Add(frmAdd.cbVal29);
            checkBoxControls.Add(frmAdd.cbVal30);

            for (int x = 0; x < 30; ++x)
            {
                checkBoxControls[x].Visible = false;
                labelControls[x].Visible = false;
                labelControls[x].Text = "";
            }

            if (cbDigit.Text == "0")
            {
                ArrayList dtAry = new ArrayList();
                int aryIdx = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    dtAry.Add(Int32.Parse(dr["DigitVal"].ToString()));
                    ++aryIdx;
                }
                dtAry.Sort();

                for (int y = 0; y < aryIdx; ++y)
                {
                    valueStr = dtAry[y].ToString();
                    labelControls[y].Text = valueStr;
                    labelControls[y].Visible = true;
                    checkBoxControls[y].Visible = true;
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    valueStr = dr["DigitVal"].ToString();
                    labelControls[idx].Text = valueStr;
                    labelControls[idx].Visible = true;
                    checkBoxControls[idx++].Visible = true;
                }
            }            
        }

        private void changeValueLabels(string oauTypeCode)
        {

        }

        #endregion
    }
}
