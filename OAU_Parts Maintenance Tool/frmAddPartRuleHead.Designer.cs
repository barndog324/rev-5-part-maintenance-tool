﻿namespace OAU_Parts_Maintenance_Tool
{
    partial class frmAddPartRuleHead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbAddPRH_PartNum = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbAddPRH_RelatedOp = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbAddPRH_PartCategory = new System.Windows.Forms.ComboBox();
            this.btnAddPRH_Cancel = new System.Windows.Forms.Button();
            this.btnAddPRH_Add = new System.Windows.Forms.Button();
            this.txtAddPRH_AsmSeq = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPartType = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbStationLoc = new System.Windows.Forms.ComboBox();
            this.chkBoxConfigPart = new System.Windows.Forms.CheckBox();
            this.chkBoxPicklist = new System.Windows.Forms.CheckBox();
            this.txtLinesideBin = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbAddPRH_PartNum
            // 
            this.cbAddPRH_PartNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAddPRH_PartNum.FormattingEnabled = true;
            this.cbAddPRH_PartNum.Location = new System.Drawing.Point(167, 16);
            this.cbAddPRH_PartNum.Name = "cbAddPRH_PartNum";
            this.cbAddPRH_PartNum.Size = new System.Drawing.Size(481, 23);
            this.cbAddPRH_PartNum.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Part Number:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(17, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Related Operation:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAddPRH_RelatedOp
            // 
            this.cbAddPRH_RelatedOp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAddPRH_RelatedOp.FormattingEnabled = true;
            this.cbAddPRH_RelatedOp.Items.AddRange(new object[] {
            "10",
            "20",
            "30",
            "35",
            "36",
            "40",
            "41",
            "45",
            "50",
            "51",
            "55",
            "60",
            "65",
            "70",
            "71",
            "72",
            "80",
            "90"});
            this.cbAddPRH_RelatedOp.Location = new System.Drawing.Point(168, 74);
            this.cbAddPRH_RelatedOp.Name = "cbAddPRH_RelatedOp";
            this.cbAddPRH_RelatedOp.Size = new System.Drawing.Size(89, 23);
            this.cbAddPRH_RelatedOp.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label3.Location = new System.Drawing.Point(16, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Part Category:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbAddPRH_PartCategory
            // 
            this.cbAddPRH_PartCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbAddPRH_PartCategory.FormattingEnabled = true;
            this.cbAddPRH_PartCategory.Location = new System.Drawing.Point(167, 45);
            this.cbAddPRH_PartCategory.Name = "cbAddPRH_PartCategory";
            this.cbAddPRH_PartCategory.Size = new System.Drawing.Size(202, 23);
            this.cbAddPRH_PartCategory.TabIndex = 2;
            // 
            // btnAddPRH_Cancel
            // 
            this.btnAddPRH_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPRH_Cancel.ForeColor = System.Drawing.Color.Red;
            this.btnAddPRH_Cancel.Location = new System.Drawing.Point(483, 170);
            this.btnAddPRH_Cancel.Name = "btnAddPRH_Cancel";
            this.btnAddPRH_Cancel.Size = new System.Drawing.Size(75, 32);
            this.btnAddPRH_Cancel.TabIndex = 7;
            this.btnAddPRH_Cancel.Text = "Cancel";
            this.btnAddPRH_Cancel.UseVisualStyleBackColor = true;
            this.btnAddPRH_Cancel.Click += new System.EventHandler(this.btnAddPRH_Cancel_Click);
            // 
            // btnAddPRH_Add
            // 
            this.btnAddPRH_Add.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddPRH_Add.ForeColor = System.Drawing.Color.Green;
            this.btnAddPRH_Add.Location = new System.Drawing.Point(573, 170);
            this.btnAddPRH_Add.Name = "btnAddPRH_Add";
            this.btnAddPRH_Add.Size = new System.Drawing.Size(75, 32);
            this.btnAddPRH_Add.TabIndex = 8;
            this.btnAddPRH_Add.Text = "Add Part";
            this.btnAddPRH_Add.UseVisualStyleBackColor = true;
            this.btnAddPRH_Add.Click += new System.EventHandler(this.btnAddPRH_Add_Click);
            // 
            // txtAddPRH_AsmSeq
            // 
            this.txtAddPRH_AsmSeq.Location = new System.Drawing.Point(168, 103);
            this.txtAddPRH_AsmSeq.Name = "txtAddPRH_AsmSeq";
            this.txtAddPRH_AsmSeq.Size = new System.Drawing.Size(40, 20);
            this.txtAddPRH_AsmSeq.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label4.Location = new System.Drawing.Point(16, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Assembly Seq:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPartType
            // 
            this.txtPartType.Location = new System.Drawing.Point(168, 129);
            this.txtPartType.Name = "txtPartType";
            this.txtPartType.Size = new System.Drawing.Size(201, 20);
            this.txtPartType.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label5.Location = new System.Drawing.Point(15, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Part Type:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label6.Location = new System.Drawing.Point(16, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 20);
            this.label6.TabIndex = 13;
            this.label6.Text = "Station Location:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbStationLoc
            // 
            this.cbStationLoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbStationLoc.FormattingEnabled = true;
            this.cbStationLoc.ItemHeight = 15;
            this.cbStationLoc.Items.AddRange(new object[] {
            "",
            "Station1",
            "Station2",
            "Station3",
            "Station4",
            "Station5",
            "Station6",
            "Station7",
            "Station8",
            "SubStat1",
            "SubStat2",
            "SubStat3",
            "SubStat4",
            "SubStat5",
            "SubStat6",
            "SubStat7",
            "SubStat8",
            "SubStat9",
            "SubStat10",
            "SubStat11",
            "SubStat12"});
            this.cbStationLoc.Location = new System.Drawing.Point(167, 155);
            this.cbStationLoc.Name = "cbStationLoc";
            this.cbStationLoc.Size = new System.Drawing.Size(202, 23);
            this.cbStationLoc.TabIndex = 12;
            // 
            // chkBoxConfigPart
            // 
            this.chkBoxConfigPart.AutoSize = true;
            this.chkBoxConfigPart.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxConfigPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxConfigPart.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxConfigPart.Location = new System.Drawing.Point(502, 104);
            this.chkBoxConfigPart.Name = "chkBoxConfigPart";
            this.chkBoxConfigPart.Size = new System.Drawing.Size(146, 19);
            this.chkBoxConfigPart.TabIndex = 121;
            this.chkBoxConfigPart.Text = "Configurable Part: ";
            this.chkBoxConfigPart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxConfigPart.UseVisualStyleBackColor = true;
            // 
            // chkBoxPicklist
            // 
            this.chkBoxPicklist.AutoSize = true;
            this.chkBoxPicklist.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxPicklist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxPicklist.ForeColor = System.Drawing.Color.RoyalBlue;
            this.chkBoxPicklist.Location = new System.Drawing.Point(320, 104);
            this.chkBoxPicklist.Name = "chkBoxPicklist";
            this.chkBoxPicklist.Size = new System.Drawing.Size(80, 19);
            this.chkBoxPicklist.TabIndex = 120;
            this.chkBoxPicklist.Text = "Picklist: ";
            this.chkBoxPicklist.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkBoxPicklist.UseVisualStyleBackColor = true;
            // 
            // txtLinesideBin
            // 
            this.txtLinesideBin.Location = new System.Drawing.Point(167, 184);
            this.txtLinesideBin.Name = "txtLinesideBin";
            this.txtLinesideBin.Size = new System.Drawing.Size(80, 20);
            this.txtLinesideBin.TabIndex = 126;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label7.Location = new System.Drawing.Point(16, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 20);
            this.label7.TabIndex = 125;
            this.label7.Text = "Lineside Bin:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmAddPartRuleHead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 226);
            this.Controls.Add(this.txtLinesideBin);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.chkBoxConfigPart);
            this.Controls.Add(this.chkBoxPicklist);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbStationLoc);
            this.Controls.Add(this.txtPartType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAddPRH_AsmSeq);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnAddPRH_Add);
            this.Controls.Add(this.btnAddPRH_Cancel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbAddPRH_PartCategory);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbAddPRH_RelatedOp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbAddPRH_PartNum);
            this.Name = "frmAddPartRuleHead";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Part";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddPRH_Cancel;
        private System.Windows.Forms.Button btnAddPRH_Add;
        public System.Windows.Forms.ComboBox cbAddPRH_PartNum;
        public System.Windows.Forms.ComboBox cbAddPRH_RelatedOp;
        public System.Windows.Forms.ComboBox cbAddPRH_PartCategory;
        public System.Windows.Forms.TextBox txtAddPRH_AsmSeq;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtPartType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cbStationLoc;
        private System.Windows.Forms.CheckBox chkBoxConfigPart;
        private System.Windows.Forms.CheckBox chkBoxPicklist;
        public System.Windows.Forms.TextBox txtLinesideBin;
        private System.Windows.Forms.Label label7;
    }
}