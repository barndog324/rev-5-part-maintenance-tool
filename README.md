# OA Rev5 Part Maintenance Tool

This windows forms application lists all parts available and allows the user to create/update rules for each part.

## Project Structure

### Rev5_OAU_Parts Maintenance Tool
This project is a .NET Framework 4.5 windows application project. It inserts/updates the KCC database via ADO.NET.

## Data Access
All data access to the KCC database is done via ADO.NET and stored procedures. The following stored procedures are used:

* R5_ROA_GetCompressorDataDtl
* R5_ROA_GetCompressorDetailData
* R5_ROA_UpdateCompressorDataDTL
* R5_ROA_InsertCompressorDataDtl
* R5_ROA_DeleteCompressorDataDtl
* GetROA_CompressorChargeData
* InsertROA_CompressorChargeDataHead
* UpdateROA_CompressorChargeDataHead
* DeleteROA_CompressorChargeDataHead
* R5_ROA_GetFurnaceDataDTL
* Update_ROA_FurnaceDataDTL
* InsertROA_FurnaceDataDTL
* DeleteROA_FurnaceDataDTL
* R5_ROA_GetModelNumDesc
* R5_ROA_GetModelNoDigitList
* R5_ROA_UpdateModelNumDesc
* R5_ROA_InsertModelNumDesc
* R5_ROA_DeleteModelNumDesc
* R5_ROA_GetAllMotorDataDTL
* R5_ROA_GetMotorDataDTL
* Update_ROA_MotorDataDTL
* InsertROA_MotorDataDTL
* R5_ROA_DeleteMotorDataDTL
* R6_OA_GetPartCategoryHead
* R6_OA_UpdatePartCategoryHead
* R6_OA_InsertPartCategoryHead
* R6_OA_DeletePartCategoryHead
* R5_ROA_GetPartsAndRulesList
* R5_ROA_GetPartRulesByRuleHeadID_AndRuleBatchID
* R5_ROA_GetVMEASM_PartsWithRules
* R5_ROA_GetPartRulesDataByPartNum
* R6_OA_GetPartDetail
* R6_OA_GetPartCategoryList
* R5_ROA_GetMainPartList
* R5_ROA_GetPartRulesByRuleHeadID
* R5_ROA_GetPartNumberInfo
* R5_ROA_GetPartNumbersByCategory
* R6_OA_VKG_GetSubAsmPartNums
* R6_OA_VKG_GetInnerAssemblyPartNums
* R6_VKG_GetAssemblyPartNumDetail
* R6_OA_VKG_GetSubAsmParentData
* R6_OA_VKG_InsertSubAsmParentData
* R6_OA_VKG_UpdateSubAsmParentData
* R6_OA_VKG_DeleteSubAsmParentData
* R5_ROA_UpdatePartConditionsDTL
* R5_ROA_InsertPartConditionsDTL
* R5_ROA_InsertPartMaster
* R5_ROA_DeleteFromPartMaster
* R6_OA_InsertPartRulesHead
* R6_OA_GetPartRulesHead
* R6_OA_DeletePartRulesHead
* R6_OA_UpdatePartRulesHead
* R5_ROA_DeleteFromPartConditionsDTL
* R6_OA_GetPartNumbersBasedOnClassID

## How to run locally
Assuming a VPN connection, make sure solution is ready to run in Debug configuration. Change the connection string in frmMain.cs #if Debug section to point to the appropriate local or dev KCC database.
Build the project and run it. 

## How to deploy
One-click publish: [https://docs.microsoft.com/en-us/visualstudio/deployment/how-to-publish-a-clickonce-application-using-the-publish-wizard?view=vs-2019]

## Author/Devs
Tony Thoman